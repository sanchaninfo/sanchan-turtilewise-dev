//
//  StatsService.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface StatsService : BaseService

- (void)getStatsWithSuccess:(successCallback)success andfailure:(failureCallback)failure;

@end
