//
//  SettingsService.m
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "SettingsService.h"
#import "StringResponse.h"
#import "Settings.h"

@implementation SettingsService

#define SERVICE_PUT_RESET_PASSWORD @"user/settings/resetPassword"
#define SERVICE_PUT_FORGOT_RESET_PASSWORD @"resetPassword"
#define SERVICE_UNSUBSCRIBE @"user/settings/notification/unsubscribe"
#define SERVICE_SETTINGS @"user/settings/notification"
#define SERVICE_RESEND_EMAIL @"email/verify/resend"

#pragma mark - Service Calls

- (void)resetPassword:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_PUT_RESET_PASSWORD parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

- (void)forgotResetPassword:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_PUT_FORGOT_RESET_PASSWORD parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

- (void)getSettingsWthSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http get:SERVICE_SETTINGS success:success failure:failure entity:[Settings new]];
}

- (void)updateSettings:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_SETTINGS parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

- (void)unsubscribeEmailSettings:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_UNSUBSCRIBE parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

- (void)resendVerificationEmail:(successCallback)success andfailure:(failureCallback)failure {
    [http post:SERVICE_RESEND_EMAIL parameters:nil success:success failure:failure response:nil];
}

@end