//
//  TrackEventService.m
//  TurtleWise
//
//  Created by Usman Asif on 10/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TrackEventService.h"

@implementation TrackEventService

#define SERVICE_TRACK_EVENT @"analytics/trackEvent?%@"

#pragma mark - Service Calls

- (void)trackEvent:(NSString *)paramString
{
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_TRACK_EVENT, paramString];
    [http get:serviceCall success:^(id response) {
        
    } failure:^(NSError *error) {
        
    } response:nil];
}

@end
