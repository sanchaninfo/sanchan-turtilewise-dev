//
//  Service.h
//  10Pearls
//
//  Created by mohsin on 3/11/14.
//  Copyright (c) 2014 SocialRadar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TurtleSwagService.h"
#import "TrackEventService.h"
#import "QuestionService.h"
#import "SettingsService.h"
#import "PurchaseService.h"
#import "CatalogService.h"
#import "AnswerService.h"
#import "StatsService.h"
#import "ChatService.h"
#import "AuthService.h"
#import "UserService.h"
#import "TagsService.h"
#import "BrandedPageService.h"

typedef enum {
    ServiceTypeAuth            = 0,
    ServiceTypeUser            = 1,
    CheckInTypeService         = 2,
    ServiceTypeCatalog         = 3,
    ServiceTypeAnswer          = 4,
    ServiceTypeQuestion        = 5,
    ServiceTypeChat            = 6,
    ServiceTypeSettings        = 7,
    ServiceTypeStats           = 8,
    ServiceTypeTags            = 9,
    ServiceTypePurchase        = 10,
    ServiceTypeTurtleSwag      = 11,
    ServiceTypeTrackEvent      = 12,
    ServiceTypeBrandedPage     = 13
}ServiceType;


@interface Service : NSObject{
    NSDictionary *_servicesMap;
    id _delegate;
}

@property(nonatomic, retain) id delegate;
@property(nonatomic, strong) AuthService *auth;
@property(nonatomic, strong) UserService *user;
@property(nonatomic, strong) CatalogService *catalog;
@property(nonatomic, strong) AnswerService *answer;
@property(nonatomic, strong) QuestionService *question;
@property(nonatomic, strong) ChatService *chat;
@property(nonatomic, strong) SettingsService *settings;
@property(nonatomic, strong) StatsService *stats;
@property(nonatomic, strong) TagsService *tags;
@property(nonatomic, strong) PurchaseService *purchase;
@property(nonatomic, strong) TurtleSwagService *turtleSwag;
@property(nonatomic, strong) TrackEventService *trackEvent;
@property(nonatomic, strong) BrandedPageService *brandedPageService;

- (void)load:(NSArray*)services;

+ (Service*)get:(id)_delegate;

@end
