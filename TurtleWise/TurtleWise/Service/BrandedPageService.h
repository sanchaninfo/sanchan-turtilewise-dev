//
//  BrandedPageService.h
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface BrandedPageService : BaseService

- (void)getPageWithPageId:(NSString *)pageId withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)updatePage:(NSDictionary *)pageDetail withPageId:(NSString *)pageId withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)getPageswithSuccess:(successCallback)success;
- (void)deletePage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)followPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)unfollowPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)requestToFollowPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)findPendingId:(NSArray *)array withSuccess:(successCallback)success;
- (void)getApprovedList:(NSMutableArray*)array withSuccess:(successCallback)success;
@end
