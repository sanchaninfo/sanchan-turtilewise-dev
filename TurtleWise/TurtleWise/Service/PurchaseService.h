//
//  PurchaseService.h
//  TurtleWise
//
//  Created by Waleed Khan on 6/7/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface PurchaseService : BaseService

- (void)verifyReceipt:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;

@end
