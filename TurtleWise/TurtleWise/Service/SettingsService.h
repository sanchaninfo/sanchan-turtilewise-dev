//
//  SettingsService.h
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface SettingsService : BaseService

- (void)resetPassword:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)forgotResetPassword:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)getSettingsWthSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)updateSettings:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)unsubscribeEmailSettings:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)resendVerificationEmail:(successCallback)success andfailure:(failureCallback)failure;
@end
