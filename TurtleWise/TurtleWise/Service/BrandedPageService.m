//
//  BrandedPageService.m
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BrandedPageService.h"
#import "BrandedPage.h"
#import "StringResponse.h"

@implementation BrandedPageService

#define SERVICE_GET_PAGE @"pages/%@"
#define SERVICE_PUT_PAGE @"pages/%@"
#define SERVICE_PUT_AVATAR @"pages/avatar/%@"
#define SERVICE_PUT_COVER @"pages/cover/%@"
#define SERVICE_GET_PAGES @"pages"
#define SERVICE_DELETE_PAGE @"pages/delete"
#define SERVICE_FOLLOW @"users/follow"
#define SERVICE_UNFOLLOW @"users/unfollow"
#define SERVICE_REQUEST @"users/follow/request"
#define SERVICE_FIND_PENDINGIDS @"users/find"
#define SERVICE_FIND_APPROVEDPAGES @"pages/find"


- (void)getPageWithPageId:(NSString *)pageId withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_GET_PAGE, pageId];
    
    [http get:serviceCall success:success failure:failure entity:[BrandedPage new]];
}


- (void)updatePage:(NSDictionary *)pageDetail withPageId:(NSString *)pageId withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_PUT_PAGE, pageId];
    
    [http put:serviceCall parameters:pageDetail success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void)getPageswithSuccess:(successCallback)success {
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_GET_PAGES];
    
    [http get:serviceCall success:^(id response)
     {
         NSArray *data = [ParserUtils array:response key:KEY_API_DATA];
         
         if (success)
             success(data);
     }
      failure:^(NSError *error)
     {
         
     }
     response:nil];
}

- (void)deletePage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    
    [http put:SERVICE_DELETE_PAGE parameters:pageDetail success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void)followPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_FOLLOW parameters:pageDetail success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void)unfollowPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_UNFOLLOW parameters:pageDetail success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void) requestToFollowPage:(NSDictionary *)pageDetail withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    [http put:SERVICE_REQUEST parameters:pageDetail success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void) findPendingId:(NSArray *)array withSuccess:(successCallback)success {
    
    NSString *serviceCall = SERVICE_FIND_PENDINGIDS;
    if (array.count >0) {
        for (int i = 0; i < array.count; i++) {
            if (i == 0) {
                serviceCall = [NSString stringWithFormat:@"%@?pendingIds=%@",serviceCall, [array objectAtIndex:i]];
            } else {
                serviceCall = [NSString stringWithFormat:@"%@&pendingIds=%@",serviceCall, [array objectAtIndex:i]];
            }
        }
    }
    
    [http get:serviceCall success:^(id response)
     {
         NSArray *data = [ParserUtils array:response key:KEY_API_DATA];
         
         if (success)
             success(data);
     }
      failure:^(NSError *error)
     {
         
     }
     response:nil];
}

- (void) getApprovedList:(NSMutableArray*)array withSuccess:(successCallback)success {
    NSString *serviceCall = SERVICE_FIND_APPROVEDPAGES;
    if (array.count >0) {
        for (int i = 0; i < array.count; i++) {
            if (i == 0) {
                serviceCall = [NSString stringWithFormat:@"%@?list=%@",serviceCall, [array objectAtIndex:i]];
            } else {
                serviceCall = [NSString stringWithFormat:@"%@&list=%@",serviceCall, [array objectAtIndex:i]];
            }
        }
    }
    
    [http get:serviceCall success:^(id response)
     {
         NSArray *data = [ParserUtils array:response key:KEY_API_DATA];
         
         if (success)
             success(data);
     }
      failure:^(NSError *error)
     {
         
     }
     response:nil];
}

@end
