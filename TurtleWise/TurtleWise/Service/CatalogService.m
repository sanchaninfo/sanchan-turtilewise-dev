//
//  CatalogService.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/24/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "CatalogService.h"
#import "CatalogItem.h"
#import "Constant.h"

#define CATALOG_ITEM_LIST @"CatalogItems"
#define BENEFICIARY_LIST @"BeneficiaryList"

@implementation CatalogService

- (NSArray *)readFromFile:(NSString *)fileName
{    
    NSArray *arrRawData = [self loadObjectsPlist:fileName];
    
    NSMutableArray *arrOfEntities = [NSMutableArray new];
    
    [arrRawData enumerateObjectsUsingBlock:^(NSDictionary *catalogItem, NSUInteger idx, BOOL *stop)
     {
         CatalogItem *item = [CatalogItem new];
         
         [item set:catalogItem];
         [arrOfEntities addObject:item];
     }];
    
    return arrOfEntities;
}

- (NSArray *)getCatalogItems
{
    return [self readFromFile:CATALOG_ITEM_LIST];
}

- (NSArray *)getBeneficiaryList
{
    return [self readFromFile:BENEFICIARY_LIST];
}

@end
