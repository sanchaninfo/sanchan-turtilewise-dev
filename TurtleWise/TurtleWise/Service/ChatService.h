//
//  ChatService.h
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseService.h"
@class Question;

@class MessagesPagedResponse;

@interface ChatService : BaseService

- (void)initiateChatWithAdvisors:(NSArray *)advisors forQuestion:(NSString *)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)endChatWithId:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)rateAdvisors:(NSArray *)advisorsRating onChat:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)getConversationsForUserType:(NSString*)userType andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;
- (void)getConversationsForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;
- (void)getChatMessagesByChatId:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure response:(MessagesPagedResponse *)response;
- (void)getConversationDetails:(NSString*)conversationId onSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)sendMessage:(NSDictionary *)params toChat:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)rateAllChatsWithSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)checkAvailablityForAdvisor:(NSString *)advisorId forQuestion:(NSString *)questionId withSuccess:(successCallback)success andFailure:(failureCallback)failure;
- (void)addAdvisor:(NSDictionary *)params withId:(NSString *)advisorId withSuccess:(successCallback)success andFailure:(failureCallback)failure;

@end
