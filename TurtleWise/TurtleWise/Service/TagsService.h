//
//  TagsService.h
//  TurtleWise
//
//  Created by Usman Asif on 2/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface TagsService : BaseService

- (void)getSuggestedTagsFor:(NSString *)tag withSuccess:(successCallback)success;
- (void)saveTag:(NSString *)tag;

@end
