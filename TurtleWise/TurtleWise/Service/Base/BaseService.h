//
// Created by mohsin on 8/14/13.
//


#import <Foundation/Foundation.h>
#import "HttpRequestManager.h"
#import "Constant.h"
#import "PagedResponse.h"

@interface BaseService : NSObject{
    HttpRequestManager *http;

}

-(NSArray *)loadObjectsPlist:(NSString *)fileName;
@end
