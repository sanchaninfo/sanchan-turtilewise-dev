//
//  TagsService.m
//  TurtleWise
//
//  Created by Usman Asif on 2/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TagsService.h"
#import "StringResponse.h"
#import "Keys.h"

#define SERVICE_TAGS @"tags/%@"

@implementation TagsService

#pragma mark - Service Calls

- (void)getSuggestedTagsFor:(NSString *)tag withSuccess:(successCallback)success
{
    NSString * method = [[NSString stringWithFormat:SERVICE_TAGS, tag] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [http get:method success:^(id response)
     {
         NSDictionary *data = [ParserUtils object:response key:KEY_API_DATA];
         NSArray *tags = [ParserUtils object:data key:KEY_TAGS];
         
         if (success)
             success(tags);
     }
      failure:^(NSError *error)
     {
         
     }
       response:nil];
}

- (void)saveTag:(NSString *)tag
{
    NSString * method = [[NSString stringWithFormat:SERVICE_TAGS, tag] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [http post:method parameters:nil
       success:^(id response)
     {
         
     }
       failure:^(NSError *error)
     {
         
     }
      response:nil];
}

@end
