//
//  StatsService.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "StatsService.h"
#import "UserStats.h"

#define SERVICE_GET_STATS @"users/stats"

@implementation StatsService

- (void)getStatsWithSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http get:SERVICE_GET_STATS success:success failure:failure entity:[UserStats new]];
}

@end
