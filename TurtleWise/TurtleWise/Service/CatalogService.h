//
//  CatalogService.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/24/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseService.h"

@interface CatalogService : BaseService

- (NSArray *)getCatalogItems;
- (NSArray *)getBeneficiaryList;

@end
