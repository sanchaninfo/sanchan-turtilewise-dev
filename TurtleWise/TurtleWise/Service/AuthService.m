//
//  AuthService.m
//  TurtleWise
//
//  Created by Waaleed Khan on 11/30/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "StringResponse.h"
#import "UserDefaults.h"
#import "AuthResponse.h"
#import "AuthService.h"
#import "Keys.h"

//Keys
#define KEY_EMAIL @"email"
#define KEY_PASSWORD @"password"
#define KEY_ACCESS_TOCKEN @"accessToken"
#define KEY_CONFIRM_PASSWORD @"confirmPassword"
#define KEY_PUSH_TOKEN @"pushToken"

//Services
#define SERVICE_POST_SIGN_IN @"signin"
#define SERVICE_POST_SIGN_UP @"signup"
#define SERVICE_GET_LOG_OUT @"logout"

#define SERVICE_TYPE_EMAIL_LOGIN @"turtlewise"

@implementation AuthService

- (void)loginUserViaEmail:(NSString *)email andPassword:(NSString *)password withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    NSString *serviceCall = [NSString stringWithFormat:@"%@/%@", SERVICE_POST_SIGN_IN, SERVICE_TYPE_EMAIL_LOGIN];
    NSDictionary *json = @{
                            KEY_EMAIL    : email,
                            KEY_PASSWORD : password,
                            KEY_PUSH_TOKEN : [UserDefaults getPushToken],
                            KEY_DEVICE_TYPE : VALUE_DEVICE_TYPE
                           };
    
    [http post:serviceCall parameters:json success:success failure:failure response:[AuthResponse new]];
}

- (void)loginWithSocial:(NSString *)appName andAccessToken:(NSString *)accessToken withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    NSString *serviceCall = [NSString stringWithFormat:@"%@/%@", SERVICE_POST_SIGN_IN, appName];

    NSDictionary *params = @{
                             KEY_ACCESS_TOCKEN  :   accessToken,
                             KEY_PUSH_TOKEN : [UserDefaults getPushToken],
                             KEY_DEVICE_TYPE : VALUE_DEVICE_TYPE
                             };
    [http post:serviceCall parameters:params success:success failure:failure response:[AuthResponse new]];
}

- (void)signUpWithEmail:(NSString *)email andPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    NSDictionary *json = @{
                            KEY_EMAIL           : email,
                            KEY_PASSWORD        : password,
                            KEY_CONFIRM_PASSWORD : confirmPassword,
                            KEY_PUSH_TOKEN : [UserDefaults getPushToken],
                            KEY_DEVICE_TYPE     : VALUE_DEVICE_TYPE
                           };
    
    [http post:SERVICE_POST_SIGN_UP parameters:json success:success failure:failure response:[AuthResponse new]];
}


- (void)logoutUserWithSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    [http get:SERVICE_GET_LOG_OUT success:success failure:failure response:[StringResponse new]];
}

@end
