//
//  QuestionService.h
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseService.h"

@interface QuestionService : BaseService

- (void)getQuestionForId:(NSString *)questionId withSuccess:(successCallback)success andFailure:(failureCallback)failure;

- (void)askQuestionWithParams:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)getQuestions:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

- (void)closeQuestion:(NSDictionary *)params onSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)deleteQuestions:(NSArray *)questions onSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)getQuestionDetails:(NSString*)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure;

- (void)getAdvisorCountForAskQuestion:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure;

- (void)getAnswerWithQuestionId:(NSString *)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure;

- (void)archiveQuestion:(NSString*)qId WithParams:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;

-(void)getArchiveQuestions:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

-(void)getboardOfAdvisors:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

@end
