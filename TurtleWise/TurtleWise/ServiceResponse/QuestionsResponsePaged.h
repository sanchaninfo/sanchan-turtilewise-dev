//
//  PromotionResponsePaged.h
//  GuestLynx
//
//  Created by Ajdal on 10/5/15.
//  Copyright (c) 2015 10Pearls. All rights reserved.
//

#import "PagedResponse.h"

@interface QuestionsResponsePaged : PagedResponse
@end
