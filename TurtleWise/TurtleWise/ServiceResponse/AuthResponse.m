//
//  AuthResponse.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserDefaults.h"
#import "AuthResponse.h"
#import "ParserUtils.h"
#import "DateUtils.h"
#import "Keys.h"

#define KEY_USER_CREATED @"created"
#define KEY_LAST_LOGIN @"lastLogin"

@implementation AuthResponse

#pragma mark - Response Setter

- (void)set:(NSDictionary*)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    [UserDefaults setAccessToken:[ParserUtils stringValue:data key:KEY_TOKEN]];
    [UserDefaults setUserID:[ParserUtils stringValue:data key:KEY_USER_ID]];
    [UserDefaults setLastLogin:[DateUtils getDateFromUnixTimeStamp:[ParserUtils numberValue:data key:KEY_LAST_LOGIN]]];
    
    _isNewUser = [ParserUtils boolValue:data key:KEY_USER_CREATED];
}

@end
