//
//  MessagesPagedResponse.h
//  TurtleWise
//
//  Created by Waleed Khan on 4/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "PagedResponse.h"

@class Conversation;
@interface MessagesPagedResponse : PagedResponse

@property(nonatomic, strong) NSNumber *messageOffset;

- (void)setConversation:(Conversation *)conversation;

@end
