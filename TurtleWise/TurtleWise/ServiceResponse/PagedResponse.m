//
//  PagedResponse.m
//  GuestLynx
//
//  Created by Ajdal on 10/5/15.
//  Copyright (c) 2015 10Pearls. All rights reserved.
//

#import "PagedResponse.h"
#import "ParserUtils.h"
#import "Constant.h"

@implementation PagedResponse
-(void)set:(NSDictionary*)input{
    @throw [NSString stringWithFormat:@"Set method should be implemented in child class (%@)",self.description];
}

-(id) init {
    self = [super init];
    if(self) {
        self.pageSize = QUESTIONS_PAGE_SIZE;
        self.hasMoreData = YES;
    }
    return self;
}

-(void)reset{
    self.skip = 0;
    self.hasMoreData = YES;
    self.pageSize = QUESTIONS_PAGE_SIZE;
    self.refreshDataset = YES;
    self.isLoadingData = NO;
}

-(void)removeAllObjectsFromArray{
    self.refreshDataset = NO;
    self.hasMoreData = YES;
    [self.dataArray removeAllObjects];
}

-(void) setTotalCount:(int)totalCount{
    
    _totalCount = totalCount;
    if(self.dataArray.count >= totalCount)
        self.hasMoreData = NO;
}

@end
