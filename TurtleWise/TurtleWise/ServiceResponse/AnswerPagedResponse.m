//
//  AnswerPagedResponse.m
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "AnswerPagedResponse.h"
#import "Answer.h"
#import "AnswerOption.h"

#define KEY_ANSWERS @"answers"
#define KEY_ANSWER_COUNT @"answerCount"
#define KEY_TOTAL_RESPONSES @"totalResponse"
#define KEY_ACTIVE_CHAT_ID  @"activeChat"

@implementation AnswerPagedResponse
-(void)set:(NSDictionary*)input{
    if(!self.dataArray)
        self.dataArray = [[NSMutableArray alloc] init];
    
    if(self.refreshDataset){
        [super removeAllObjectsFromArray];
    }
    
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    NSArray *answers = [ParserUtils array:data key:KEY_ANSWERS];
    
    for(NSDictionary *dict in answers){
        Answer *ans = [Answer new];
        [ans set:dict];
        [self.dataArray addObject:ans];
    }
    
    
    
    NSDictionary *meta = [ParserUtils object:input key:KEY_META];
    
    self.activeChatId = [ParserUtils object:meta key:KEY_ACTIVE_CHAT_ID];
    
    self.answerOptions = [[NSMutableArray alloc] init];
    

    NSDictionary *question = [ParserUtils object:meta key:KEY_QUESTION];
    
    self.isUserSubscribed = [ParserUtils boolValue:question key:KEY_IS_SUBSCRIBED];
    NSArray *answerOptions = [ParserUtils array:question key:KEY_ANSWER_COUNT];
    
    for(NSDictionary *answerOption in answerOptions){
        AnswerOption *option = [AnswerOption new];
        [option set:answerOption];
        [self.answerOptions addObject:option];
    }
    
    self.skip = self.skip + (int)answers.count;
    
    
    self.totalCount = [[ParserUtils numberValue:question key:KEY_TOTAL_RESPONSES] intValue];
    
    
    if(self.dataArray.count >= self.totalCount)
        self.hasMoreData = NO;
}
@end
