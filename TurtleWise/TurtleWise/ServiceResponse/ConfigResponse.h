//
//  ConfigResponse.h
//  TurtleWise
//
//  Created by Waleed Khan on 7/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseResponse.h"

@interface ConfigResponse : BaseResponse

- (NSArray *)levelsTable;
- (NSArray *)charities;
- (NSArray *)levelBucks;
- (NSArray *)levelAmount;

@end
