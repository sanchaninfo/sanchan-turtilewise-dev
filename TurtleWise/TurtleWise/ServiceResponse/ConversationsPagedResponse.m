//
//  ConversationPagedResponse.m
//  TurtleWise
//
//  Created by Ajdal on 4/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ConversationsPagedResponse.h"
#import "Conversation.h"
#import "UserProfile.h"
#import "Question.h"

@implementation ConversationsPagedResponse
-(void)set:(NSDictionary*)input{
    if(!self.dataArray)
        self.dataArray = [[NSMutableArray alloc] init];
    
    
    NSArray *conversations = [ParserUtils array:input key:KEY_API_DATA];
    
    
    if(self.refreshDataset){
        [super removeAllObjectsFromArray];
    }

    
    self.skip = self.skip + (int)conversations.count;
    
    NSDictionary *meta = [ParserUtils object:input key:KEY_META];
    
    
    for(NSDictionary *dict in conversations){
        Conversation *conv = [Conversation new];
        [conv set:dict];
        if(!conv.timeDifferenceSystemAndServer)
            conv.timeDifferenceSystemAndServer = [ParserUtils numberValue:meta key:KEY_SERVER_TIME];
        
        if(!conv.question){//In case of getting chats for a particular question, the question object will be returned in meta.
            conv.question = [Question new];
            [conv.question set:[ParserUtils object:meta key:KEY_QUESTION]];
        }
        
        [self.dataArray addObject:conv];
    }
    
    self.totalCount = [[ParserUtils numberValue:meta key:KEY_TOTAL] intValue];
    
    if(self.dataArray.count >= self.totalCount)
        self.hasMoreData = NO;
}

@end
