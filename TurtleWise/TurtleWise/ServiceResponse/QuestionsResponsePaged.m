//
//  PromotionResponsePaged.m
//  GuestLynx
//
//  Created by Ajdal on 10/5/15.
//  Copyright (c) 2015 10Pearls. All rights reserved.
//

#import "QuestionsResponsePaged.h"
#import "Question.h"


@implementation QuestionsResponsePaged
-(void)set:(NSDictionary*)input{
    if(!self.dataArray)
        self.dataArray = [[NSMutableArray alloc] init];
    
    
    NSArray *questions = [ParserUtils array:input key:KEY_API_DATA];
    
    
    if(self.refreshDataset){
        [super removeAllObjectsFromArray];
    }
    
    self.skip = self.skip + (int)questions.count;
    
    NSDictionary *meta = [ParserUtils object:input key:KEY_META];
    
    
    for(NSDictionary *dict in questions){
        Question *ques = [Question new];
        [ques set:dict];
        if(!ques.timeDifferenceSystemAndServer)
            ques.timeDifferenceSystemAndServer = [ParserUtils numberValue:meta key:KEY_SERVER_TIME];
        [self.dataArray addObject:ques];
    }
    
    self.totalCount = [[ParserUtils numberValue:meta key:KEY_TOTAL] intValue];
    
    if(self.dataArray.count >= self.totalCount)
        self.hasMoreData = NO;
    
    
}
@end
