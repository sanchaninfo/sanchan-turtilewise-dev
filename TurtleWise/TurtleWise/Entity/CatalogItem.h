//
//  CatalogItem.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/24/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseEntity.h"

@interface CatalogItem : BaseEntity

//Properties
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *price;
@property(nonatomic, strong) NSString *imageUrl;

- (void)set:(NSDictionary *)data;

@end
