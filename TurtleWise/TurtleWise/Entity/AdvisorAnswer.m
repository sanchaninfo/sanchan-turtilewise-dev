//
//  AdvisorAnswer.m
//  TurtleWise
//
//  Created by Usman Asif on 2/16/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "AdvisorAnswer.h"

@implementation AdvisorAnswer

-(void)set:(NSDictionary*)input{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    NSDictionary *answer = [ParserUtils object:data key:KEY_ANSWER];
    
    self.answerText = [ParserUtils stringValue:answer key:KEY_ANSWER];
    self.reason = [ParserUtils stringValue:answer key:KEY_REASON];
    self.whyChooseMe = [ParserUtils stringValue:answer key:KEY_WHY_CHOOSE_ME];
    self.seekersFeedback = [ParserUtils stringValue:answer key:@"feedback"];
}
@end
