//
//  Advisors.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseEntity.h"

@interface Advisor : BaseEntity

@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSNumber *level;
@property(nonatomic, strong) NSString *advisorID;
@property(nonatomic, strong) NSString *profileImageUrl;
@property(nonatomic, strong) NSString *turtleIdentifier;
@property(nonatomic, strong) NSString *avatar;
@property(nonatomic) BOOL chatAvailability;

- (void)set:(NSDictionary *)data;
-(NSString*)getDisplayName;
@end
