//
//  UserStats.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UserDefaults.h"
#import "UserStats.h"
#import "Keys.h"

#define KEY_ACTIVE_QUESTIONS @"activeQuestions"
#define KEY_UNREAD_QUESTIONS @"unreadQuestions"
#define KEY_UN_ANSWERED_QUESTONS @"unansweredQuestions"
#define KEY_ADVISOR_CHATS @"advisorChats"
#define KEY_SEEKER_CHATS @"seekerChats"

#define KEY_QUESTIONS @"questions"
#define KEY_ANSWERS @"answers"
#define KEY_CHATS @"chats"
#define KEY_UNRATED_CHATS @"unrated"

@interface UserStats ()

@property(nonatomic, assign) long seekerUnreadChats;
@property(nonatomic, assign) long advisorUnReadChats;

- (long)decreaseCounterFor:(long)targetValue;

@end

@implementation UserStats

#pragma mark - Over Ride Methods

-(void)set:(NSDictionary*)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    NSDictionary *advisorStats = [ParserUtils object:data key:KEY_ADVISOR];
    NSDictionary *seekerStats = [ParserUtils object:data key:KEY_SEEKER];
    
    _seekerQuestions = [[ParserUtils numberValue:seekerStats key:KEY_QUESTIONS] longValue];
    _seekerAnswers = [[ParserUtils numberValue:seekerStats key:KEY_ANSWERS] longValue];
    _seekerUnreadChats = [[ParserUtils array:seekerStats key:KEY_CHATS] count];
    
    _advisorQuestions = [[ParserUtils numberValue:advisorStats key:KEY_QUESTIONS]longValue];
    _advisorUnReadChats = [[ParserUtils array:advisorStats key:KEY_CHATS] count];

    _unratedChats = [[ParserUtils numberValue:seekerStats key:KEY_UNRATED_CHATS] longValue];
}

- (long)unreadChats
{
   return (([UserDefaults getUserRole] == UserRoleAdvisor) ? _advisorUnReadChats : _seekerUnreadChats);
}

#pragma mark - Counter Decrement Methods

- (void)markActiveQuestionInActive
{
    _seekerQuestions = [self decreaseCounterFor:_seekerQuestions];
    
    [UserDefaults setUserStats:self];
}

- (void)markUnReadQuestionRead
{
    _seekerAnswers = [self decreaseCounterFor:_seekerAnswers];

    [UserDefaults setUserStats:self];
}

- (void)decreaseUnAnsweredQuestionCount
{
    _advisorQuestions = [self decreaseCounterFor:_advisorQuestions];
    
    [UserDefaults setUserStats:self];
}

- (long)decreaseCounterFor:(long)targetValue
{
    if (targetValue == 0)
    {
        return 0;
    }
        
    return targetValue -= 1;
}

- (void)markChatReadForUserRole
{
    ([UserDefaults getUserRole] == UserRoleAdvisor) ? [self decreaseCounterFor:_advisorUnReadChats] : [self decreaseCounterFor:_seekerUnreadChats];
}

#pragma mark - NSCoder Methods

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if (self != nil)
    {
        _seekerQuestions = [[coder decodeObjectForKey:KEY_ACTIVE_QUESTIONS] longValue];
        _seekerAnswers = [[coder decodeObjectForKey:KEY_UNREAD_QUESTIONS] longValue];
        _advisorQuestions = [[coder decodeObjectForKey:KEY_UN_ANSWERED_QUESTONS] longValue];
        _advisorUnReadChats = [[coder decodeObjectForKey:KEY_ADVISOR_CHATS] longValue];
        _seekerUnreadChats = [[coder decodeObjectForKey:KEY_SEEKER_CHATS] longValue];
        _unratedChats = [[coder decodeObjectForKey:KEY_UNRATED_CHATS] longValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:[NSNumber numberWithLong:_seekerQuestions] forKey:KEY_ACTIVE_QUESTIONS];
    [coder encodeObject:[NSNumber numberWithLong:_seekerAnswers] forKey:KEY_UNREAD_QUESTIONS];
    [coder encodeObject:[NSNumber numberWithLong:_advisorQuestions] forKey:KEY_UN_ANSWERED_QUESTONS];
    [coder encodeObject:[NSNumber numberWithLong:_advisorUnReadChats] forKey:KEY_ADVISOR_CHATS];
    [coder encodeObject:[NSNumber numberWithLong:_seekerUnreadChats] forKey:KEY_SEEKER_CHATS];
    [coder encodeObject:[NSNumber numberWithLong:_unratedChats] forKey:KEY_UNRATED_CHATS];
}

@end
