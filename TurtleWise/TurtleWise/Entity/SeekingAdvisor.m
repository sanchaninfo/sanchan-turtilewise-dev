//
//  SeekingAdvisor.m
//  TurtleWise
//
//  Created by Waleed Khan on 12/29/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "SeekingAdvisor.h"
#import "NSArray+Utils.h"
#import "StringUtils.h"
#import "UserProfile.h"
#import "DateUtils.h"
#import "Constant.h"
#import "Utility.h"
#import "Keys.h"

#define HEADER_AGE @"Age:"
#define HEADER_COUNTRY @"Country:"
#define HEADER_STATE @"State:"
#define HEADER_CITY @"City:"
#define HEADER_POLITICAL_AFFILIATION @"PoliticalAffiliation:"
#define HEADER_MARITAL_STATUS @"MaritalStatus:"
#define HEADER_OTHER_DETAILS @"OtherDetails:"
#define HEADER_SIBLINGS @"Siblings:"
#define HEADER_SCHOOL @"Schools:"
#define HEADER_EMPLOYER @"Employers:"
#define HEADER_HOBBIES @"Hobby:"
#define HEADER_KIDS @"Kids:"
#define HEADER_PROFESSION @"Professions:"
#define HEADER_EDUCATION @"EducationLevel:"

@interface SeekingAdvisor ()

@property(nonatomic, strong) NSMutableArray *attributeTags;

- (void)addHashTagForKey:(NSString *)key withValue:(NSString *)value;
- (void)addHashTagForKey:(NSString *)key withArrayValue:(NSArray *)arrayValue;

@end

@implementation SeekingAdvisor

#pragma mark - Life Cycle Methods

- (id)initWithUserProfile:(UserProfile *)userProfile
{
    if (self == [super init])
    {
        _minAge = _maxAge = [DateUtils ageFromDate:[userProfile dateOfBirth]];
        
        [self setGender:[userProfile gender]];
        [self setCountry:[userProfile country]];
        [self setState:[userProfile state]];
        [self setCity:[userProfile city]];
        [self setOrientation:[userProfile orientation]];
        [self setReligion:[userProfile religion]];
        [self setEthnicity:[userProfile ethnicity]];
        [self setPoliticalAffiliation:[userProfile politicalAffiliation]];
        [self setMaritalStatus:[userProfile maritalStatus]];
        [self setEducationLevel:[userProfile educationLevel]];
        
        [self setSchools:[userProfile schools]];
        [self setProfessions:[userProfile professions]];
        [self setEmployers:[userProfile employers]];
        [self setGuruTags:[userProfile guruTags]];
        
        [self setKids:[userProfile kids]];
        [self setSiblings:[userProfile siblings]];
    }
    
    return self;
}


#pragma mark - Override 

- (NSArray *)guruTags
{
    if ([super guruTags] == NULL)
    {
        [self setGuruTags:[NSArray new]];
    }
    
    return [super guruTags];
}

- (NSArray *)questionOptions
{
    if (!_questionOptions)
    {
        [self setQuestionOptions:[NSArray new]];
    }
    
    return _questionOptions;
}

- (NSDictionary *)getAsDictionary
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:[super getAsDictionary]];
    
    if ([_minAge integerValue] > 0)
    {
        [self setNumber:[Utility getNumberFromString:_minAge] inDictionary:params forKey:KEY_MIN_AGE];
    }
    
    if ([_maxAge integerValue] > 0)
    {
        [self setNumber:[Utility getNumberFromString:_maxAge] inDictionary:params forKey:KEY_MAX_AGE];
    }

    return params;
}


#pragma mark - Hash Tag Management

- (NSMutableArray *)attributeHashTags
{
    _attributeTags = [NSMutableArray new];
    
    [self addHashTagForKey:HEADER_GENDER withValue:[self gender]];
    [self addHashTagForKey:HEADER_COUNTRY withValue:[self country]];
    [self addHashTagForKey:HEADER_STATE withValue:[self state]];
    [self addHashTagForKey:HEADER_CITY withValue:[self city]];
    [self addHashTagForKey:HEADER_ORIENTATION withValue:[self orientation]];
    [self addHashTagForKey:HEADER_RELIGION withValue:[self religion]];
    [self addHashTagForKey:HEADER_ETHNICITY withValue:[self ethnicity]];
    [self addHashTagForKey:HEADER_POLITICAL_AFFILIATION withValue:[self politicalAffiliation]];
    [self addHashTagForKey:HEADER_MARITAL_STATUS withValue:[self maritalStatus]];
    [self addHashTagForKey:HEADER_EDUCATION withValue:[self educationLevel]];
    
    [self addHashTagForKey:HEADER_KIDS withValue:[[self kids] stringValue]];
    [self addHashTagForKey:HEADER_SIBLINGS withValue:[[self siblings] stringValue]];
    
    [self addHashTagForKey:HEADER_PROFESSION withArrayValue:[self professions]];
    [self addHashTagForKey:HEADER_SCHOOL withArrayValue:[self schools]];
    [self addHashTagForKey:HEADER_EMPLOYER withArrayValue:[self employers]];
    
    NSString *age = _minAge;
    
    if (![StringUtils compareString:_minAge withString:_maxAge])
    {
        age = [_minAge stringByAppendingString:[NSString stringWithFormat:@"-%@", _maxAge]];
    }
    
    if ([age integerValue] > 0)
    {
        [self addHashTagForKey:HEADER_AGE withValue:age];
    }

    return _attributeTags;
}

- (void)addHashTagForKey:(NSString *)key withValue:(NSString *)value
{
    if ([StringUtils isEmptyOrNull:value])
    {
        return;
    }
    
    [_attributeTags addObject:[NSString stringWithFormat:@"%@%@", key, value]];
}

- (void)addHashTagForKey:(NSString *)key withArrayValue:(NSArray *)arrayValue
{
    if (!arrayValue && [arrayValue isEmpty])
    {
        return;
    }
    
    [arrayValue enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        [self addHashTagForKey:key withValue:obj];
    }];
}

- (void)removeTagFromArray:(NSString *)tag
{
    NSArray *components = [tag componentsSeparatedByString:@":"];
    NSString *getterName = [[components objectAtIndex:0] lowercaseString];

    SEL getterSelector = NSSelectorFromString(getterName);
    
    if ([self respondsToSelector:getterSelector])
    {
        NSMutableArray *targetArray;
        SuppressPerformSelectorLeakWarning(
                                           targetArray = [NSMutableArray arrayWithArray:[self performSelector:getterSelector withObject:NULL]];
                                           );
        
        if (targetArray)
        {
            [targetArray removeObject:[components objectAtIndex:1]];
            
            [self updateValueFor:[components objectAtIndex:0] withValue:targetArray];
        }
    }
}

- (void)resetValueForTag:(NSString *)tag
{
    if ([StringUtils isEmptyOrNull:tag])
    {
        return;
    }
    
    if ([tag hasPrefix:HEADER_SCHOOL] || [tag hasPrefix:HEADER_PROFESSION] || [tag hasPrefix:HEADER_EMPLOYER])
    {
        [self removeTagFromArray:tag];
        
        return;
    }
    
    if ([tag hasPrefix:HEADER_AGE])
    {
        _minAge = _maxAge = @"";
        
        return;
    }
    
    NSArray *components = [tag componentsSeparatedByString:@":"];
    [self updateValueFor:[components objectAtIndex:0] withValue:NULL];
}

- (void)updateValueFor:(NSString *)setterMethod withValue:(id)object
{
    SEL setterSelector = NSSelectorFromString([NSString stringWithFormat:@"set%@:", setterMethod]);
    
    if ([self respondsToSelector:setterSelector])
    {
        SuppressPerformSelectorLeakWarning(
                                           [self performSelector:setterSelector withObject:object];
                                           );
    }
}

- (void)resetAllTags
{
    [_attributeTags enumerateObjectsUsingBlock:^(NSString *tag, NSUInteger idx, BOOL * _Nonnull stop)
    {
        [self resetValueForTag:tag];
    }];
    
    [_attributeTags removeAllObjects];
}

@end
