//
//  Article.h
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface Article : BaseEntity {
    NSString * image;
    NSString * link;
    NSString * title;
    NSString * articleId;
}

@property(nonatomic, retain) NSString* image;
@property(nonatomic, retain) NSString* link;
@property(nonatomic, retain) NSString* title;
@property(nonatomic, retain) NSString* articleId;
- (Article *) initWithDictionary:(NSDictionary*) dictionary;
@end
