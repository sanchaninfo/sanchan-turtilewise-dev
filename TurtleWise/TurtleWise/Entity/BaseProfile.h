//
//  BaseProfile.h
//  TurtleWise
//
//  Created by Waleed Khan on 12/29/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface BaseProfile : BaseEntity

@property(nonatomic, strong) NSString *gender;
@property(nonatomic, strong) NSString *country;
@property(nonatomic, strong) NSString *state;
@property(nonatomic, strong) NSString *city;
@property(nonatomic, strong) NSString *orientation;
@property(nonatomic, strong) NSString *religion;
@property(nonatomic, strong) NSString *ethnicity;
@property(nonatomic, strong) NSString *politicalAffiliation;
@property(nonatomic, strong) NSString *maritalStatus;
@property(nonatomic, strong) NSString *educationLevel;
@property(nonatomic, strong) NSString *signUpAs;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *summary;
@property(nonatomic, strong) NSString *articleOneURL;
@property(nonatomic, strong) NSString *articleTwoURL;
@property(nonatomic, strong) NSString *articleThreeURL;
@property(nonatomic, strong) NSString *eventOneURL;
@property(nonatomic, strong) NSString *eventTwoURL;
@property(nonatomic, strong) NSString *eventThreeURL;
@property(nonatomic, strong) NSString *publicOrPrivate;

@property(nonatomic, strong) NSArray *schools;
@property(nonatomic, strong) NSArray *professions;
@property(nonatomic, strong) NSArray *employers;
@property(nonatomic, strong) NSArray *guruTags;



@property(nonatomic, strong) NSNumber *kids;
@property(nonatomic, strong) NSNumber *siblings;

- (NSDictionary *)getAsDictionary;

- (void)setString:(NSString *)stringValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key;
- (void)setNumber:(NSNumber *)numberValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key;
- (void)setArray:(NSArray *)arrayValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key;

@end
