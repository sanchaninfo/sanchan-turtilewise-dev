//
//  Profile.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfile.h"

@interface UserProfile : BaseProfile

@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSString *about;
@property(nonatomic, strong) NSString *profilePercentage;
@property(nonatomic, strong) NSString *profilePrivacy;
@property(nonatomic, strong) NSString *userId;
@property(nonatomic, strong) UIImage *avatar;
@property(nonatomic, strong) UIImage *profileImage;
@property(nonatomic, strong) UIImage *coverImage;
@property(nonatomic, strong) NSString *imageURL;

@property(nonatomic, strong) NSDate *dateOfBirth;
@property(nonatomic, strong) NSDate *subscriptionExpiray;

@property(nonatomic, strong) NSArray *hobbies;
@property(nonatomic, strong) NSArray *explorerTags;

@property(nonatomic, assign) NSInteger thumbsUps;
@property(nonatomic, assign) NSInteger thumbsDown;
@property(nonatomic, assign) NSInteger thanks;

@property(nonatomic, assign) NSInteger level;

@property(nonatomic, strong) NSString *stage;
@property(nonatomic, assign) NSInteger points;
@property(nonatomic, assign) NSInteger bucks;

@property(nonatomic, assign) NSInteger chats;
@property(nonatomic, strong) NSNumber *memberSince;
@property(nonatomic, assign) NSInteger questionsAnswered;

@property(nonatomic, strong) NSMutableSet *privateAttributes;
@property(nonatomic, strong) NSString *turtleIdentifier;

@property(nonatomic, strong) NSArray *following;
@property(nonatomic, strong) NSArray *pending;

@property(nonatomic, strong) NSArray *pages;

@property(nonatomic, assign) BOOL isUserPremium;

@property(nonatomic) BOOL emailVerified;

- (NSString *)fullName;
- (BOOL)isProfilePublic;
- (NSArray *)getAsArrayWithEmptyValues:(BOOL)includeEmptyValues;
- (void)updateProfilePrivacy:(BOOL)isPublic;
- (NSString *)getMemberSince;
- (NSString *)getDisplayNameOrTurtleIdentifier;
//- (void)setFirstLastNameWithName:(NSString *)name;
-(void)setSeekerProfile:(NSDictionary*)seeker;
-(void)setSeekerProfile2:(NSDictionary *)seeker;
@end
