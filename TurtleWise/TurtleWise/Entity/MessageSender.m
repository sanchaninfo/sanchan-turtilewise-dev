//
//  messageSender.m
//  TurtleWise
//
//  Created by Waleed Khan on 4/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "Conversation.h"
#import "UserProfile.h"
#import "MessageSender.h"
#import "Question.h"
#import "Advisor.h"

@interface MessageSender ()

- (void)prepareDisplayName:(NSDictionary *)input;

@end

@implementation MessageSender

#pragma mark - Setter

- (void)set:(NSDictionary *)input
{
    _senderId = [ParserUtils stringValue:input key:KEY_CONTENT_ID];
    [self prepareDisplayName:input];
}

#pragma Helper Methods

- (void)prepareDisplayName:(NSDictionary *)input
{
    NSString *firstName = [ParserUtils stringValue:input key:KEY_FIRST_NAME];
    NSString *lastName = [ParserUtils stringValue:input key:KEY_LAST_NAME];
    NSString *turtleId = [ParserUtils stringValue:input key:KEY_TURTLE_IDENTIFIER];
    
    if(!firstName || !lastName || ([firstName length] == 0 && [lastName length] == 0))
    {
        _displayName = turtleId;
        
        return;
    }

    if ([lastName length] == 0)
    {
        _displayName = firstName;
        
        return;
    }
    
    if ([firstName length] == 0)
    {
        _displayName = lastName;
        
        return;
    }
    
    _displayName = [NSString stringWithFormat:@"%@ %@.", firstName ? firstName : @"", [[lastName ? lastName : @"" substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
}

- (NSString *)displayName
{
    return _displayName;
}


- (void)determineSenderRoleForConversation:(Conversation *)conversation
{
    UserProfile *seeker = [[conversation question] seeker];
    
    if ([[seeker userId] isEqualToString:_senderId])
    {
        _userRole = UserRoleSeeker;
        
        return;
    }
    
    _userRole = UserRoleAdvisor;
}

- (void)determineSenderNameForConversation:(Conversation *)conversation
{
    UserProfile *seeker = [[conversation question] seeker];
    
    if ([[seeker userId] isEqualToString:_senderId])
    {
        [self setDisplayName:[seeker getDisplayNameOrTurtleIdentifier]];
        
        return;
    }
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.advisorID == %@", [self senderId]];
    
    Advisor *chatSenderAdvisor =  [[[conversation advisors] filteredArrayUsingPredicate:filterPredicate] firstObject];
    
    [self setDisplayName:[chatSenderAdvisor getDisplayName]];
    
    [self determineSenderRoleForConversation:conversation];
}

@end
