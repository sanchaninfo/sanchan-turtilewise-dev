//
//  Article.m
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "Article.h"

@implementation Article
@synthesize image;
@synthesize link;
@synthesize title;
@synthesize articleId;

- (Article *) initWithDictionary:(NSDictionary*) dictionary {
    if(self == [super init]) {
        self.image = dictionary[KEY_IMAGE];
        self.title = dictionary[KEY_TITLE];
        self.link = dictionary[KEY_LINK];
    }
    return self;
}

@end
