//
//  Settings.m
//  TurtleWise
//
//  Created by Usman Asif on 2/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "Settings.h"
#import "Keys.h"

#define KEY_NOTIFICATION @"notification"

@implementation Settings

#pragma mark - Setter

- (void)set:(NSDictionary *)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    NSDictionary *notification = [ParserUtils object:data key:KEY_NOTIFICATION];
    NSDictionary *email = [ParserUtils object:notification key:KEY_EMAIL];
    
    _emailVerified = [ParserUtils boolValue:data key:KEY_EMAIL_VERIFIED];
    _shouldNotifyOnQuestion = [ParserUtils boolValue:email key:KEY_QUESTION_RECEIVED];
    _shouldNotifyOnAnswer = [ParserUtils boolValue:email key:KEY_ANSWER_RECEIVED];
    _shouldSendEmailDigest = [ParserUtils boolValue:email key:KEY_DAILY_DIGEST];
}

@end
