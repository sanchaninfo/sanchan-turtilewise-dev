//
//  Question.m
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "Question.h"
#import "Answer.h"
#import "AppDelegate.h"
#import "Font.h"
#import "Keys.h"
#import "UserProfile.h"
#import "StringUtils.h"
//Keys
#define KEY_EXPIRED_AT @"expiredAt"
#define KEY_CHAT_PERIOD_EXPIRY @"nextExpiry"
#define KEY_CURRENT @"current"
#define KEY_ANSWER_OPTIONS @"options"
#define KEY_IS_EXPIRED @"isExpired"
#define KEY_CAN_ANSWER @"canAnswer"
#define KEY_TOTAL_ADVISORS @"totalAdvisors"
#define KEY_TOTAL_ANSWERS @"totalAnswers"

//Constants
#define QUESTION_STATUS_CLOSED @"closed"
#define QUESTION_STATUS_ADVICE @"advice"

@implementation Question

-(void)set:(NSDictionary *)input{
    
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    if (data)
        input = data;

    _questionId = [ParserUtils stringValue:input key:KEY_CONTENT_ID];
    _questionText = [ParserUtils stringValue:input key:KEY_CONTENT];
    _questionType = [self questionTypeFromString:[ParserUtils stringValue:input key:KEY_TYPE]];
    _createdAt = [ParserUtils numberValue:input key:KEY_CREATED_AT];
    _expiry = [ParserUtils numberValue:input key:KEY_EXPIRED_AT];
    _nextExpiry = [ParserUtils numberValue:input key:KEY_CHAT_PERIOD_EXPIRY];
    self.timeDifferenceSystemAndServer = [ParserUtils numberValue:input key:KEY_SERVER_TIME];
    _status = [self questionStatusFromString:[ParserUtils stringValue:[ParserUtils object:input key:KEY_STATUS] key:KEY_CURRENT]];
    _answerOptions = [ParserUtils array:input key:KEY_ANSWER_OPTIONS];
    _isRead = [ParserUtils boolValue:input key:KEY_IS_READ];
    _isExpired = [ParserUtils boolValue:input key:KEY_IS_EXPIRED];
    _canAnswer = [ParserUtils boolValue:input key:KEY_CAN_ANSWER];
    _isSubscribed = [ParserUtils boolValue:input key:KEY_IS_SUBSCRIBED];

    _totalAdvisors = [ParserUtils intValue:input key:KEY_TOTAL_ADVISORS];
    _totalAnswers = [ParserUtils intValue:input key:KEY_TOTAL_ANSWERS];
    
    [self setAttributes:[ParserUtils object:input key:KEY_SEARCH_ATTRIBUTES]];
    [self setSearchTags:[ParserUtils array:input key:KEY_SEARCH_TAGS]];
    
    
    self.seeker = [UserProfile new];
//    [self.seeker  setSeekerProfile:[ParserUtils object:input key:KEY_SEEKER]];
    
//    if([StringUtils isEmptyOrNull:self.seeker.userId]){//Temporary code
        [self.seeker setSeekerProfile2:[ParserUtils object:input key:KEY_SEEKER]];
//    }

}


-(void)setAttributes:(NSDictionary *)advisorAttributes{
    
    NSMutableArray *attributes = [[NSMutableArray alloc] init];
    
    for(NSString *key in advisorAttributes){
        id value = [advisorAttributes objectForKey:key];
        if([value isKindOfClass:[NSString class]]){
            NSString *attribute = [NSString stringWithFormat:@"%@:%@",[key capitalizedString],[value capitalizedString]];
            [attributes addObject:attribute];
        }
        else if([value isKindOfClass:[NSArray class]]){
            for(NSString *val in value){
                NSString *attribute = [NSString stringWithFormat:@"%@:%@",[key capitalizedString],[val capitalizedString]];
                [attributes addObject:attribute];
            }
        }
    }
    
    self.advisorAttributes = [NSArray arrayWithArray:attributes];
    
    if ([[advisorAttributes allKeys] containsObject:KEY_MIN_AGE] && [[advisorAttributes allKeys] containsObject:KEY_MAX_AGE]){
        int minAge = [[advisorAttributes valueForKey:KEY_MIN_AGE] intValue];
        int maxAge = [[advisorAttributes valueForKey:KEY_MAX_AGE] intValue];
        
        if (minAge == 0 && maxAge == 0)
            return;
        
        if (minAge == maxAge)
            [attributes addObject:[NSString stringWithFormat:@"Age:%i",minAge]];
        else
            [attributes addObject:[NSString stringWithFormat:@"Age:%i-%i",minAge,maxAge]];
    }
    
    self.advisorAttributes = [NSArray arrayWithArray:attributes]; //added some filthy stuff :(
}

-(QuestionType)questionTypeFromString:(NSString *)type{
    if([type isEqualToString:QUESTION_TPYE_YES_NO])
        return QuestionTypeYN;
    else if([type isEqualToString:QUESTION_TYPE_MCQ])
        return QuestionTypeMultipleChoice;
    else
        return QuestionTypeComplex;
}

-(QuestionStatus)questionStatusFromString:(NSString *)status{
    if ([status isEqualToString:QUESTION_STATUS_CLOSED])
        return QuestionStatusClosed;
    
    if ([status isEqualToString:QUESTION_STATUS_ADVICE])
        return QuestionStatusAdvice;
    
    return QuestionStatusChat;
}

-(void)setTimeDifferenceSystemAndServer:(NSNumber *)severTime{
    if(!severTime)
        return;
    NSDate *serTime = [NSDate dateWithTimeIntervalSince1970:[severTime doubleValue]/1000];
    NSTimeInterval timeDifference = [serTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    
    _timeDifferenceSystemAndServer = [NSNumber numberWithDouble:timeDifference];
}

-(NSTimeInterval)getRemainingTime{
    

    NSNumber *expiry = (self.status == QuestionStatusAdvice) ? self.expiry : self.nextExpiry;
    
    if(!self.nextExpiry)
        expiry = self.expiry;
    
    NSDate *expiryTime = [NSDate dateWithTimeIntervalSince1970:[expiry doubleValue]/1000];
    NSTimeInterval remainingTime = (int)[expiryTime timeIntervalSinceNow] - [self.timeDifferenceSystemAndServer intValue];
    
    if(remainingTime < 0)
        return 0;
    return remainingTime;
}


-(CGFloat)getTextHeight{
    CGFloat maxWidth = [(AppDelegate*)[[UIApplication sharedApplication] delegate] window].frame.size.width-70;
    CGFloat height =  [Font getHeight:self.questionText andFont:[Font lightFontWithSize:17.0] andWidth:maxWidth];
    return height;
}

#pragma mark - ANSWERS
-(void)setAnswers:(NSArray *)answers{
    
    self.answersArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary *ans in answers){
        Answer *answer = [Answer new];
        [answer set:ans];
        [self.answersArray addObject:ans];
    }
}

-(BOOL)isAnswered{
    return self.answersArray.count;
}
@end
