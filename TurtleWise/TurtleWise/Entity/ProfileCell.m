//
//  ProfileCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

#pragma mark - Life Cycle Methods

- (id)initWithHeader:(NSString *)cellHeader content:(NSString *)cellContent stepInWizard:(NSInteger)stepInWizard responseKey:(NSString *)responseKey andPrivacy:(ProfileAttributeAccess)privacy
{
    if (self = [super init])
    {
        _cellContent = cellContent;
        _cellHeading = cellHeader;
        _stepInWizard = stepInWizard;
        _responseKey = responseKey;
        _profileAttributeAccess = privacy;
    }
    
    return self;
}

@end
