//
//  Events.h
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface Events : BaseEntity {
    NSString * image;
    NSString * link;
    NSString * title;
    NSString * date;
}
@property(nonatomic, retain) NSString* image;
@property(nonatomic, retain) NSString* link;
@property(nonatomic, retain) NSString* title;
@property(nonatomic, retain) NSString* date;
@end
