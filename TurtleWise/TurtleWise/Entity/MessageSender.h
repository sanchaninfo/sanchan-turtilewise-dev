//
//  messageSender.h
//  TurtleWise
//
//  Created by Waleed Khan on 4/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"
#import "Enum.h"

@class Conversation;

@interface MessageSender : BaseEntity

@property(nonatomic, strong) NSString *senderId;
@property(nonatomic, strong) NSString *displayName;
@property(nonatomic) UserRole userRole;

- (void)determineSenderRoleForConversation:(Conversation *)conversation;
- (void)determineSenderNameForConversation:(Conversation *)conversation;

@end
