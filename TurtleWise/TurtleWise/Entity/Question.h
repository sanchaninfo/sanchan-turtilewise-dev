//
//  Question.h
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseEntity.h"
#import "Enum.h"
@class UserProfile;
@interface Question : BaseEntity

@property (nonatomic, strong) NSString *questionId;
@property (nonatomic, strong) NSString *questionText;
@property (nonatomic, strong) NSNumber *createdAt;
@property (nonatomic, strong) NSNumber *expiry;
@property (nonatomic, strong) NSNumber *nextExpiry;
@property (nonatomic, strong) NSMutableArray *answersArray;
@property (nonatomic, strong) NSArray *answerOptions;
@property (nonatomic, assign) QuestionType questionType;
@property (nonatomic, strong) NSArray *advisorAttributes;
@property (nonatomic, strong) NSArray *searchTags;
@property (nonatomic, assign) QuestionStatus status;
@property (nonatomic, assign) BOOL isRead;
@property (nonatomic, assign) BOOL isExpired;
@property (nonatomic, assign) BOOL canAnswer;
@property (nonatomic, assign) BOOL isSubscribed;
@property (nonatomic, assign) NSInteger totalAdvisors;
@property (nonatomic, assign) NSInteger totalAnswers;
@property (nonatomic, strong) NSNumber *timeDifferenceSystemAndServer;


@property (nonatomic, strong) UserProfile *seeker;

-(NSTimeInterval)getRemainingTime;
-(BOOL)isAnswered;
-(CGFloat)getTextHeight;
@end
