//
//  BrandedPage.m
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BrandedPage.h"
#import "Keys.h"
#import "Constant.h"
#import "StringUtils.h"
#import "UserDefaults.h"
//#import "Article.h"
@implementation BrandedPage

@synthesize ownerId;
@synthesize type;
@synthesize privacy;
@synthesize summary;
@synthesize name;
@synthesize pageId;
@synthesize facebookLink;
@synthesize twitterLink;
@synthesize linkedInLink;
@synthesize cover;
@synthesize avatar;
@synthesize articles;
@synthesize events;
@synthesize approved;
@synthesize pending;

- (void)set:(NSDictionary *)input
{
    
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    NSDictionary *social = [ParserUtils object:data key:KEY_SOCIAL];
    
    NSDictionary *members = [ParserUtils object:data key:KEY_MEMBERS];
    
    ownerId = [ParserUtils stringValue:data key:KEY_OWNER];
    type = [ParserUtils stringValue:data key:KEY_TYPE];
    privacy = [ParserUtils stringValue:data key:KEY_PRIVACY];
    summary = [ParserUtils stringValue:data key:KEY_BRANDED_SUMMARY];
    name = [ParserUtils stringValue:data key:KEY_NAME];
    pageId = [ParserUtils stringValue:data key:KEY_PAGE];
    
    avatar = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:data key:KEY_AVATAR] withDefault:PHOTO_PLACEHOLDER_IMAGE];
    cover = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:data key:KEY_COVER_PIC] withDefault:PHOTO_PLACEHOLDER_COVER];
    
    facebookLink = [ParserUtils stringValue:social key:KEY_FACEBOOK];
    twitterLink = [ParserUtils stringValue:social key:KEY_TWITTER];
    linkedInLink = [ParserUtils stringValue:social key:KEY_LINKEDIN];
    
    articles = [[ParserUtils array:data key:KEY_ARTICLES] mutableCopy];
    events = [[ParserUtils array:data key:KEY_EVENTS] mutableCopy];
    
    approved = [[ParserUtils array:members key:KEY_APPROVED] mutableCopy];
    pending = [[ParserUtils array:members key:KEY_PENDING] mutableCopy];
}

- (BrandedPage *) initWithDictionary:(NSDictionary*) dictionary {
    if(self == [super init]) {
        NSDictionary *social = [ParserUtils object:dictionary key:KEY_SOCIAL];
        
        NSDictionary *members = [ParserUtils object:dictionary key:KEY_MEMBERS];
        
        self.ownerId = [ParserUtils stringValue:dictionary key:KEY_OWNER];
        self.type = [ParserUtils stringValue:dictionary key:KEY_TYPE];
        self.privacy = [ParserUtils stringValue:dictionary key:KEY_PRIVACY];
        self.summary = [ParserUtils stringValue:dictionary key:KEY_BRANDED_SUMMARY];
        self.name = [ParserUtils stringValue:dictionary key:KEY_NAME];
        self.pageId = [ParserUtils stringValue:dictionary key:KEY_ID];
        
        self.avatar = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:dictionary key:KEY_AVATAR] withDefault:PHOTO_PLACEHOLDER_IMAGE];
        self.cover = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:dictionary key:KEY_COVER_PIC] withDefault:PHOTO_PLACEHOLDER_IMAGE];
        
        self.facebookLink = [ParserUtils stringValue:social key:KEY_FACEBOOK];
        self.twitterLink = [ParserUtils stringValue:social key:KEY_TWITTER];
        self.linkedInLink = [ParserUtils stringValue:social key:KEY_LINKEDIN];
        
        self.articles = [[ParserUtils array:dictionary key:KEY_ARTICLES] mutableCopy];
        self.events = [[ParserUtils array:dictionary key:KEY_EVENTS] mutableCopy];
        
        self.approved = [[ParserUtils array:members key:KEY_APPROVED] mutableCopy];
        self.pending = [[ParserUtils array:members key:KEY_PENDING] mutableCopy];
    }
    return self;
}

@end
