//
//  Advisors.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "EnvironmentConstants.h"
#import "UserDefaults.h"
#import "ParserUtils.h"
#import "StringUtils.h"
#import "Advisor.h"

#define PHOTO_URL_STRING @"%@users/%@/avatarStream/%@"

@implementation Advisor

#pragma mark - Setter

- (void)set:(NSDictionary *)data{
    self.firstName = [ParserUtils stringValue:data key:KEY_FIRST_NAME];
    self.lastName = [ParserUtils stringValue:data key:KEY_LAST_NAME];
    self.turtleIdentifier = [ParserUtils stringValue:data key:KEY_TURTLE_IDENTIFIER];
    self.level = [ParserUtils numberValue:data key:KEY_LEVEL];
    self.avatar = [ParserUtils stringValue:data key:KEY_AVATAR];
    
    self.advisorID = [ParserUtils stringValue:data key:KEY_USER_ID];
    if (!self.advisorID)
        self.advisorID = [ParserUtils stringValue:data key:KEY_CONTENT_ID];
    
    self.profileImageUrl = (_avatar)?[NSString stringWithFormat:PHOTO_URL_STRING, SERVICE_URL, _avatar, [UserDefaults getAccessToken]]:nil;
}


-(NSString*)getDisplayName{
    
    if([StringUtils isEmptyOrNull:self.firstName] && [StringUtils isEmptyOrNull:self.lastName])//Both firstName and lastName unavailable
        return self.turtleIdentifier;
    
    
    if(![StringUtils isEmptyOrNull:self.firstName] && ![StringUtils isEmptyOrNull:self.lastName])//Both firstName and lastName available
        return [NSString stringWithFormat:@"%@ %@.",self.firstName ? self.firstName : @"",[[self.lastName ? self.lastName : @"" substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
    
    if (![StringUtils isEmptyOrNull:self.firstName])
        return self.firstName;

    else
        return self.lastName;

    
}
@end
