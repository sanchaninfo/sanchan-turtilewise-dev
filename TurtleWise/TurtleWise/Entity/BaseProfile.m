//
//  BaseProfile.m
//  TurtleWise
//
//  Created by Waleed Khan on 12/29/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "NSArray+Utils.h"
#import "StringUtils.h"
#import "BaseProfile.h"
#import "Constant.h"
#import "Keys.h"

@interface BaseProfile ()

@end

@implementation BaseProfile

#pragma mark Helper Methods

- (NSDictionary *)getAsDictionary
{
    NSMutableDictionary *profileAttributes = [NSMutableDictionary new];
    
    [self setString:[self city] inDictionary:profileAttributes forKey:KEY_CITY];
    [self setString:[self state] inDictionary:profileAttributes forKey:KEY_STATE];
    [self setString:[self country]  inDictionary:profileAttributes forKey:KEY_COUNTRY];
    
    [self setString:[self gender] inDictionary:profileAttributes forKey:KEY_GENDER];
    
    [self setString:[self religion] inDictionary:profileAttributes forKey:KEY_RELIGION];
    [self setString:[self ethnicity] inDictionary:profileAttributes forKey:KEY_ETHNICITY];
    [self setString:[self politicalAffiliation] inDictionary:profileAttributes forKey:KEY_POLITICAL_AFFILIATION];
    
    [self setString:[self orientation] inDictionary:profileAttributes forKey:KEY_ORIENTATION];
    [self setString:[self maritalStatus] inDictionary:profileAttributes forKey:KEY_MARITIAL_STATUS];
    [self setString:[self educationLevel] inDictionary:profileAttributes forKey:KEY_EDUCATION];
    
    [self setNumber:[self kids] inDictionary:profileAttributes forKey:KEY_KIDS];
    [self setNumber:[self siblings] inDictionary:profileAttributes forKey:KEY_SIBLINGS];
    
    [self setArray:[self schools] inDictionary:profileAttributes forKey:KEY_SCHOOL];
    [self setArray:[self employers] inDictionary:profileAttributes forKey:KEY_EMPLOYER];
    [self setArray:[self professions] inDictionary:profileAttributes forKey:KEY_PROFESSION];

    [self setString:[self articleOneURL] inDictionary:profileAttributes forKey:KEY_ARTICLE_ONE_URL];
    [self setString:[self articleTwoURL] inDictionary:profileAttributes forKey:KEY_ARTICLE_TWO_URL];
    [self setString:[self articleThreeURL] inDictionary:profileAttributes forKey:KEY_ARTICLE_THREE_URL];
    [self setString:[self eventOneURL] inDictionary:profileAttributes forKey:KEY_EVENT_ONE_URL];
    [self setString:[self eventTwoURL] inDictionary:profileAttributes forKey:KEY_EVENT_TWO_URL];
    [self setString:[self eventThreeURL] inDictionary:profileAttributes forKey:KEY_EVENT_THREE_URL];
    [self setString:[self publicOrPrivate] inDictionary:profileAttributes forKey:KEY_PUBLIC_OR_PRIVATE];
    

    return profileAttributes;
}

#pragma mark - Helper Methods

- (void)setString:(NSString *)stringValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key
{
    if (![StringUtils isEmptyOrNull:stringValue])
    {
        [dictionary setValue:stringValue forKey:key];
    }
}

- (void)setNumber:(NSNumber *)numberValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key
{
    if (numberValue)
    {
        [dictionary setValue:numberValue forKey:key];
    }
}

- (void)setArray:(NSArray *)arrayValue inDictionary:(NSDictionary *)dictionary forKey:(NSString *)key
{
    if (arrayValue && [arrayValue count] > 0)
    {
        [dictionary setValue:arrayValue forKey:key];
    }
}

@end
