//
//  Conversation.m
//  TurtleWise
//
//  Created by Waleed Khan on 4/21/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "Conversation.h"
#import "Keys.h"
#import "Question.h"
#import "UserProfile.h"
#import "Advisor.h"
#import "UserDefaults.h"

#define KEY_ADVISORS @"advisors"
#define KEY_LAST_MESSAGE @"lastMessage"
#define KEY_STATUS_CLOSED @"ended"
#define KEY_IS_RATED @"hasRated"

@implementation Conversation

#pragma mark - Overide Setter Method

- (void)set:(NSDictionary *)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    if (!data)
        data = input;
    
    
    self.conversationId = [ParserUtils stringValue:data key:KEY_CONTENT_ID];
    self.createdAt = [ParserUtils numberValue:data key:KEY_CREATED_AT];
    self.expiry = [ParserUtils numberValue:data key:KEY_EXPIRED_AT];
    self.timeDifferenceSystemAndServer = [ParserUtils numberValue:data key:KEY_SERVER_TIME];
    self.status =  [self chatStatusFromString:[ParserUtils stringValue:[ParserUtils object:data key:KEY_STATUS] key:KEY_CURRENT]];
    self.lastMessage = [ParserUtils stringValue:[ParserUtils object:data key:KEY_LAST_MESSAGE] key:KEY_API_MESSAGE];
    self.isRead = [ParserUtils boolValue:data key:KEY_IS_READ];
    self.isRated = [ParserUtils boolValue:data key:KEY_IS_RATED];
    self.message = [ParserUtils stringValue:data key:KEY_ERROR_MESSAGE];
    
    if([ParserUtils object:data key:KEY_QUESTION]){//In case of getting chats for a particular question, the question object will be returned in meta.
        self.question = [Question new];
        [self.question set:[ParserUtils object:data key:KEY_QUESTION]];
    }
    
    
    self.advisors = [[NSMutableArray alloc] init];
    for(NSDictionary *advisor in [ParserUtils object:data key:KEY_ADVISORS]){
        Advisor *adv = [Advisor new];
        [adv set:advisor];
        [self.advisors addObject:adv];
    }
    
}

-(void)setTimeDifferenceSystemAndServer:(NSNumber *)severTime{
    if(!severTime)
        return;
    NSDate *serTime = [NSDate dateWithTimeIntervalSince1970:[severTime doubleValue]/1000];
    NSTimeInterval timeDifference = [serTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    
    _timeDifferenceSystemAndServer = [NSNumber numberWithDouble:timeDifference];
}

-(NSTimeInterval)getRemainingTime{

    NSDate *expiryTime = [NSDate dateWithTimeIntervalSince1970:[self.expiry doubleValue]/1000];
    NSTimeInterval remainingTime = (int)[expiryTime timeIntervalSinceNow] - [self.timeDifferenceSystemAndServer intValue];
    
    if(remainingTime < 0)
        return 0;
    return remainingTime;
}

-(ConversationStatus)chatStatusFromString:(NSString *)type{
    if([type isEqualToString:KEY_STATUS_CLOSED])
        return ConversationStatusClosed;
    return ConversationStatusActive;
}

-(NSArray*)getParticipantsForUserRole:(UserRole)userRole andUserId:(NSString*)userId{
    NSMutableArray *participants = [[NSMutableArray alloc] init];
    
    for(Advisor *adv in self.advisors){
        if(userRole == UserRoleAdvisor && [adv.advisorID isEqualToString:userId])//I'm the advisor.
            continue;
        [participants addObject:[adv getDisplayName]];
    }
    
    
    if(userRole == UserRoleSeeker && ![self.question.seeker.userId isEqualToString:userId])//Don't add my own name to the participant list
        [participants addObject:[self.question.seeker getDisplayNameOrTurtleIdentifier]];
    
    if(userRole == UserRoleAdvisor)
        [participants addObject:[self.question.seeker getDisplayNameOrTurtleIdentifier]];
    
    return participants;
}


@end
