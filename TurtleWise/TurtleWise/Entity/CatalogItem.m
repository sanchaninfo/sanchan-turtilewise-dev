//
//  CatalogItem.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/24/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "ParserUtils.h"
#import "CatalogItem.h"

#define KEY_ITEM_NAME @"name"
#define KEY_ITEM_COST @"price"
#define KEY_ITEM_IMAGE @"image"

@implementation CatalogItem

#pragma mark - Setter

- (void)set:(NSDictionary *)data
{
    _name = [ParserUtils stringValue:data key:KEY_ITEM_NAME];
    _price = [ParserUtils stringValue:data key:KEY_ITEM_COST];
    _imageUrl = [ParserUtils stringValue:data key:KEY_ITEM_IMAGE];
}

@end
