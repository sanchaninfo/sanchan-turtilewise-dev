//
//  ProfileCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"
#import "Enum.h"

@interface ProfileCell : BaseEntity

@property(nonatomic, strong) NSString *cellHeading;
@property(nonatomic, strong) NSString *cellContent;
@property(nonatomic, strong) NSString *responseKey;

@property(nonatomic, assign) NSInteger stepInWizard;
@property(nonatomic, assign) ProfileAttributeAccess profileAttributeAccess;


- (id)initWithHeader:(NSString *)cellHeader content:(NSString *)cellContent stepInWizard:(NSInteger)stepInWizard responseKey:(NSString *)responseKey andPrivacy:(ProfileAttributeAccess)privacy;

@end
