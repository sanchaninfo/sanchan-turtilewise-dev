//
//  TurtleBuck.h
//  TurtleWise
//
//  Created by Ajdal on 6/7/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface UserLevel : BaseEntity
@property(nonatomic,strong) NSString *title;
@property(nonatomic,assign) NSInteger maxBucks;
@property(nonatomic,assign) NSInteger minBucks;
@property(nonatomic,assign) NSInteger level;
@property(nonatomic,assign) NSInteger chatReward;

@end
