//
//  Events.m
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "Events.h"

@implementation Events
@synthesize image;
@synthesize link;
@synthesize title;
@synthesize date;
@end
