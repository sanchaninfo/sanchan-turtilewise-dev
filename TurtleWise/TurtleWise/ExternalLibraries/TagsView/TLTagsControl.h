//
//  TLTagsControl.h
//  TagsInputSample
//
//  Created by Антон Кузнецов on 11.02.15.
//  Copyright (c) 2015 TheLightPrjg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLTagsControl;

@protocol TLTagsControlDelegate <NSObject>

@optional

//- (void)tagsControl:(TLTagsControl *)tagsControl tappedAtIndex:(NSInteger)index;
//- (void)tagAdded:(TLTagsControl *)tagsControl;
- (void)tagRemoved:(NSString *)removedTag with:(TLTagsControl *)tagsControl;

//- (void)tagsControlDidLayoutSubviews:(TLTagsControl*)tagControl;

- (void)getSuggestedTagsFor:(NSString *)tag success:(void(^)(NSArray * tags))successBlock;
- (void)tagsControlRequestToSave:(NSString *)tag;

@end

typedef NS_ENUM(NSUInteger, TLTagsControlMode) {
    TLTagsControlModeEdit,
    TLTagsControlModeReadOnly,
    TLTagsControlModeList,
};

IB_DESIGNABLE
@interface TLTagsControl : UIScrollView

@property (nonatomic, strong) NSMutableArray    *tags;
@property (nonatomic, strong) UIFont            *tagsFont;

@property (nonatomic) TLTagsControlMode mode;

@property (nonatomic, strong) IBInspectable UIColor *tagsBackgroundColor;
@property (nonatomic, strong) IBInspectable UIColor *tagsTextColor;
@property (nonatomic, strong) IBInspectable NSString *tagPlaceholder;

@property (assign, nonatomic) id<TLTagsControlDelegate> controlDelegate;

- (void)addTag:(NSString *)tag;
- (void)reload;
- (void)addTag;
@end