//
//  TLTagsControl.m
//  TagsInputSample
//
//  Created by Антон Кузнецов on 11.02.15.
//  Copyright (c) 2015 TheLightPrjg. All rights reserved.
//

#define DEFAULT_PADDING 5
#define DEFAULT_TAGS_BACKGROUND_COLOR [Color blueTagColor]
#define DEFAULT_TAGS_TEXT_COLOR [UIColor whiteColor]
#define DEFAULT_TAGS_FONT [Font lightFontWithSize:17]
#define TEXTFIELD_TOP_PADDING 5
#define TEXTFIELD_INIT_WIDTH 110
#define TEXTFIELD_CORNER_RADIUS 5
#define CHIP_HEIGHT ([@"Sample" sizeWithAttributes:@{NSFontAttributeName: tagInputField_.font}].height + TEXTFIELD_TOP_PADDING*2)

#define MAXLENGTH 30

#import "ACEAutocompleteBar.h"
#import "TLTagsControl.h"
#import "Color.h"
#import "Font.h"

#define DEBOUNCE_TIME 0.7

@interface TLTagsControl () < UIGestureRecognizerDelegate, ACEAutocompleteDataSource, ACEAutocompleteDelegate >
{
    UITextField * tagInputField_;
    NSTimer     * debounceTimer;
    NSString    * queryForSuggestedTags;
}
@end

@implementation TLTagsControl

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self != nil) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    _tags = [NSMutableArray array];
    
    if(tagInputField_ == nil){
        tagInputField_ = [[UITextField alloc] init];
        
        [[tagInputField_ layer] setCornerRadius:TEXTFIELD_CORNER_RADIUS];
        [tagInputField_ setAutocorrectionType:UITextAutocorrectionTypeNo];
        [tagInputField_ setTextColor:[UIColor blackColor]];
        [tagInputField_ setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEFAULT_PADDING, 1)];
        [tagInputField_ setLeftView:paddingView];
        [tagInputField_ setLeftViewMode:UITextFieldViewModeAlways];
        
        if (!_tagsBackgroundColor)
            _tagsBackgroundColor = DEFAULT_TAGS_BACKGROUND_COLOR;

        if (!_tagsTextColor)
            _tagsTextColor = DEFAULT_TAGS_TEXT_COLOR;
        
        if (!_tagsFont)
            _tagsFont = DEFAULT_TAGS_FONT;
        
        if (!_tagPlaceholder)
            [tagInputField_ setPlaceholder:@"tag"];
        
        [tagInputField_ setFrame:CGRectMake(DEFAULT_PADDING, 0, TEXTFIELD_INIT_WIDTH, CHIP_HEIGHT)];
        [self addSubview:tagInputField_];
        
        [tagInputField_ setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView)
         {
             inputView.font = [Font lightFontWithSize:20];
             inputView.textColor = [UIColor whiteColor];
         }];
    }
}

#pragma mark

- (void)setTags:(NSMutableArray *)tags {
    if (!tags)
    {
        tags = [NSMutableArray new];
        return;
    }
    _tags = tags;
}

- (void)setTagsBackgroundColor:(UIColor *)tagsBackgroundColor
{
    _tagsBackgroundColor = tagsBackgroundColor;
}
- (void)setTagPlaceholder:(NSString *)tagPlaceholder
{
    [tagInputField_ setPlaceholder:tagPlaceholder];
}
- (void)setTagsFont:(UIFont *)tagsFont
{
    _tagsFont = tagsFont;
    [tagInputField_ setFont:tagsFont];
}
- (void)setTagsTextColor:(UIColor *)tagsTextColor
{
    _tagsTextColor = tagsTextColor;
}

#pragma mark - layout

- (CGSize)intrinsicContentSize
{
    CGFloat defaultTagHeight = tagInputField_.frame.size.height;
    return CGSizeMake(UIViewNoIntrinsicMetric, defaultTagHeight);
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self reload];
}

#pragma mark - TagsView helper

- (CGPoint)updateOriginWith:(CGPoint)origin forSize:(CGSize)size forwardOrigin:(BOOL)forward
{
    if (forward)
        return CGPointMake(origin.x+size.width+DEFAULT_PADDING, origin.y);
    
    if (origin.x+size.width > self.bounds.size.width)
        return CGPointMake(0, origin.y+size.height+DEFAULT_PADDING);
    
    return origin;
}
- (UILabel *)customChipLabel:(NSString *)text tagNumber:(NSInteger)tagNumber height:(CGFloat)height
{
    UILabel * label = [[UILabel alloc] init];
    [label setFont:_tagsFont];
    [label setTextColor:_tagsTextColor];
    [label setText:text];
    CGFloat titleWidth = [label.text sizeWithAttributes:@{NSFontAttributeName: _tagsFont}].width;
    CGFloat maxTitleWidth = self.frame.size.width - DEFAULT_PADDING*2 - ((_mode==TLTagsControlModeReadOnly)?0:height);
    if (titleWidth > maxTitleWidth)
    {
        titleWidth = maxTitleWidth;
        UITapGestureRecognizer * tapTitleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
        [label addGestureRecognizer:tapTitleGesture];
        [label setUserInteractionEnabled:YES];
        [label setTag:tagNumber];
    }
    [label setFrame:CGRectMake(DEFAULT_PADDING, 0, titleWidth, height)];
    
    return label;
}
- (UIButton *)customChipButton:(CGRect)frame tagNumber:(NSInteger)tagNumber
{
    UIButton * button = [[UIButton alloc] init];
    [button setFrame:frame];
    [button setImage:[UIImage imageNamed:@"tag-delete-button"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(deleteTagButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setContentMode:UIViewContentModeScaleAspectFit];
    [button setTag:tagNumber];
    
    return button;
}
- (void)updateTagsViewLayout:(CGRect)frame
{
    [self setContentSize:frame.size];
    [self setFrame:frame];
    
    for (NSLayoutConstraint *constraint in self.constraints)
    {
        if (constraint.firstAttribute == NSLayoutAttributeHeight)
        {
            constraint.constant = frame.size.height;
            break;
        }
    }
    //
}

- (void)reload
{
    for (UIView * oldView in self.subviews) {
        if (oldView == tagInputField_)
            continue;
        
        [oldView removeFromSuperview];
    }
    
    [tagInputField_ setHidden:YES];
    CGPoint contentOrigin = CGPointMake(0, 0);
    
    for (int index=0; index<_tags.count; index++)
    {
        NSString * tag = [_tags objectAtIndex:index];
        
        UIView * tagView = [[UIView alloc] init];
        [[tagView layer] setCornerRadius:TEXTFIELD_CORNER_RADIUS];
        [tagView setClipsToBounds:YES];
        [tagView setBackgroundColor:_tagsBackgroundColor];
        [self addSubview:tagView];

        UILabel * title = [self customChipLabel:tag tagNumber:889000+index height:CHIP_HEIGHT];
        [tagView addSubview:title];
        
        CGSize chipSize = CGSizeMake(title.frame.origin.x+title.frame.size.width+DEFAULT_PADDING, CHIP_HEIGHT);
        
        if (_mode== TLTagsControlModeEdit || _mode== TLTagsControlModeList)
        {
            UIButton * btnDelete =[self customChipButton:CGRectMake((title.frame.origin.x+title.frame.size.width), 0, CHIP_HEIGHT, CHIP_HEIGHT) tagNumber:888000+index];
            [tagView addSubview:btnDelete];
            
            chipSize = CGSizeMake(btnDelete.frame.origin.x+btnDelete.frame.size.width, CHIP_HEIGHT);
        }
        
        contentOrigin = [self updateOriginWith:contentOrigin forSize:chipSize forwardOrigin:NO];
        [tagView setFrame:CGRectMake(contentOrigin.x, contentOrigin.y, chipSize.width, CHIP_HEIGHT)];
        
        contentOrigin = [self updateOriginWith:contentOrigin forSize:tagView.frame.size forwardOrigin:YES];
    }
    
    if (_mode == TLTagsControlModeEdit) {
        contentOrigin = [self updateOriginWith:contentOrigin forSize:tagInputField_.frame.size forwardOrigin:NO];
        [tagInputField_ setFrame:CGRectMake(contentOrigin.x, contentOrigin.y, (tagInputField_.text.length>0)? tagInputField_.frame.size.width:TEXTFIELD_INIT_WIDTH, CHIP_HEIGHT)];
        [tagInputField_ setHidden:NO];
        
        contentOrigin = [self updateOriginWith:contentOrigin forSize:tagInputField_.frame.size forwardOrigin:YES];
    }
    
    [self updateTagsViewLayout:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, contentOrigin.y + CHIP_HEIGHT)];
}

#pragma mark - action handlers

- (void)deleteTagButton:(UIButton *)sender {
    int index = (int)sender.tag - 888000;
    
    NSString *tagToRemove = [_tags objectAtIndex:index];
    
    [_tags removeObjectAtIndex:index];
    [self reload];
    
    if (_controlDelegate && [_controlDelegate respondsToSelector:@selector(tagRemoved:with:)])
    {
        [_controlDelegate tagRemoved:tagToRemove with:self];
    }
}

-(void)tapHandler:(UITapGestureRecognizer *)gestureRecognizer {
    int index = (int)gestureRecognizer.view.tag - 889000;
    
    if (_tags.count <= index)
        return;

    NSString * title = [_tags objectAtIndex:index];
    [Alert show:@"" andMessage:title];
}

#pragma mark -

- (BOOL)isDuplicatingTag:(NSString *)tag
{
    NSArray *oldTags = [_tags filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF MATCHES %@", tag]];
    if (oldTags.count > 0) {
        return YES;
    }
    return NO;
}

- (void)addTag:(NSString *)tag {
    if ([self isDuplicatingTag:tag])
        return;
    
    [_tags addObject:tag];
    [self reload];
    
    if (_controlDelegate && [_controlDelegate respondsToSelector:@selector(tagsControlRequestToSave:)]) {
        [_controlDelegate tagsControlRequestToSave:tag];
    }
}

- (void)updateFrameForTextField:(UITextField *)textField text:(NSString *)string
{
    CGFloat titleWidth = [string sizeWithAttributes:@{NSFontAttributeName: _tagsFont}].width;
    CGFloat maxAvailableWidth = CGRectGetMaxX(self.frame) - titleWidth;
    if (titleWidth < TEXTFIELD_INIT_WIDTH || titleWidth > maxAvailableWidth)
        return;
    
    CGRect frame = textField.frame;
    frame.size.width = titleWidth + DEFAULT_PADDING;
    [textField setFrame:frame];
}

#pragma mark - textfield stuff

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * lString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (![string isEqualToString:@""] && lString.length > MAXLENGTH) {
        return NO;
    }
    
    [self updateFrameForTextField:textField text:lString];
    
    queryForSuggestedTags = lString;
    [self restartDebounceTimer];
    
    return YES;
}

#pragma mark - ACEAutoCompleteBar dataSource/delegate

- (void)textField:(UITextField *)textField didSelectObject:(id)object inInputView:(ACEAutocompleteInputView *)inputView
{
    textField.text = object; // NSString
}

- (NSUInteger)minimumCharactersToTrigger:(ACEAutocompleteInputView *)inputView
{
    return 1;
}

- (void)inputView:(ACEAutocompleteInputView *)inputView itemsFor:(NSString *)query result:(void (^)(NSArray *))resultBlock
{
    resultBlock = nil;
}

#pragma mark -

- (void)restartDebounceTimer
{
    [debounceTimer invalidate];
    debounceTimer = nil;
    debounceTimer = [NSTimer scheduledTimerWithTimeInterval:DEBOUNCE_TIME target:self selector:@selector(getSuggestedTags) userInfo:nil repeats:NO];
}

- (void)getSuggestedTags
{
    if (_controlDelegate && [_controlDelegate respondsToSelector:@selector(getSuggestedTagsFor:success:)])
    {
        [_controlDelegate getSuggestedTagsFor:queryForSuggestedTags success:^(NSArray *tags)
         {
             [(ACEAutocompleteInputView *)[tagInputField_ inputAccessoryView] loadSuggestedWords:tags];
         }];
    }
}

#pragma mark - CallFromSuperToAddTag

- (void)addTag {
    if (tagInputField_.text.length > 0)
    {
        [self addTag:tagInputField_.text];
        [tagInputField_ setText:@""];
        [tagInputField_.inputAccessoryView setHidden:YES];
    }
}
@end
