//
//  Charity.m
//  TurtleWise
//
//  Created by Waleed Khan on 7/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "Charity.h"

//Keys
#define KEY_TITLE @"title"
#define KEY_DETAILS @"description"
#define KEY_IMAGE_URL @"image"

@interface Charity ()

@end

@implementation Charity

#pragma mark - Life Cycle Methods

- (id)initWithCoder:(NSCoder *)coder
{
    if ([super init] != nil)
    {
        _title = [coder decodeObjectForKey:KEY_TITLE];
        _details = [coder decodeObjectForKey:KEY_DETAILS];
        _imageUrl = [coder decodeObjectForKey:KEY_IMAGE_URL];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_title forKey:KEY_TITLE];
    [coder encodeObject:_details forKey:KEY_DETAILS];
    [coder encodeObject:_imageUrl forKey:KEY_IMAGE_URL];
}

#pragma mark - Setter

- (void)set:(NSDictionary *)input
{
    _title = [ParserUtils stringValue:input key:KEY_TITLE];
    _details = [ParserUtils stringValue:input key:KEY_DETAILS];
    _imageUrl = [ParserUtils stringValue:input key:KEY_IMAGE_URL];
}

@end
