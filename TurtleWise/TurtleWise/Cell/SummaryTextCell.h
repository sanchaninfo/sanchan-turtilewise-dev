//
//  SummaryTextCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BrandedPage;

@interface SummaryTextCell : UITableViewCell {
    NSString * fbLink;
    NSString * twitterLink;
    NSString * linkedInLink;
}
@property (weak, nonatomic) IBOutlet UILabel *lblSummaryText;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnLinkedin;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linkedinLeading;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linkedinHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linkedinWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTop;
-(void) setData:(BrandedPage*)page;
@end
