//
//  ChatListTableCell.h
//  TurtleWise
//
//  Created by Ajdal on 4/21/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TableViewCell.h"
#import "Label.h"
#import "MZTimerLabel.h"
#import "Conversation.h"

@interface ChatListTableCell : TableViewCell
@property(nonatomic,weak) IBOutlet Label *questionDescriptionLabel;
@property(nonatomic,weak) IBOutlet Label *participantNamesLabel;
@property(nonatomic,weak) IBOutlet Label *lastMessageLabel;
@property(nonatomic,weak) IBOutlet Label *lblRatingStatus;
@property(nonatomic,weak) IBOutlet MZTimerLabel *timerLbl;
@property(nonatomic,weak) IBOutlet UIView *unreadMessagesIndicator;

-(void)set:(Conversation *)conversation forRole:(UserRole)userRole;
-(void)setParticipantList:(NSString*)participants;
+ (NSString *)cellName;
@end
