//
//  AnswerOptionDetailCell.m
//  TurtleWise
//
//  Created by Ajdal on 1/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "AnswerOptionDetailCell.h"

@implementation AnswerOptionDetailCell

- (void)awakeFromNib {
    // Initialization code
}

+(NSString *)cellName
{
    return NSStringFromClass([AnswerOptionDetailCell class]);
}

-(void)setTitle:(NSString *)title andDetail:(NSString *)detail{
    [self.titleLabel setText:title];
    [self.detailLabel setText:detail];
}
@end
