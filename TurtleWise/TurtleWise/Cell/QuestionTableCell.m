//
//  QuestionTableCell.m
//  TurtleWise
//
//  Created by Irfan Gul on 08/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "QuestionTableCell.h"
#import "Constant.h"
#import "Question.h"
#import "Answer.h"
#import "Label.h"
#import "Color.h"
#import "Keys.h"
#import "UserDefaults.h"

@interface QuestionTableCell() < MZTimerLabelDelegate >{
    Question *_question;
}

- (void)setResponseCountersForQuestion:(Question *)question;

@end

@implementation QuestionTableCell

+ (NSString *)cellName
{
    return NSStringFromClass([QuestionTableCell class]);
}

+ (CGFloat)cellHeight
{
    return 138.0f;
}

- (void)set:(BaseEntity *)obj
{
    [self resetTimerLabel];
    
    if([obj isKindOfClass:[Question class]])
        [self setWithQuestion:(Question *)obj];
    
    [self showQuestionTimer];
}
-(void)resetTimerLabel{
    [self.questionTime setHidden:YES];
    [self.questionTime setTextColor:[Color timerNormalColorForCell]];
    [self.questionTime setDelegate:nil];
    [self.questionTime reset];
}

-(void)hideResponseCounter
{
    [self.lblResponseCounter setHidden:YES];
    [self.lblStaticText setHidden:YES];
}

-(void)hideQuestionTimer
{
    self.timerLabelHeightConstraint.constant = 0.0;
    [self layoutIfNeeded];
    [self.questionTime setHidden:YES];
    [self.lblCurrentPeriod setHidden:YES];
}

-(void)showQuestionTimer{
    self.timerLabelHeightConstraint.constant = 22.0;
    [self layoutIfNeeded];
    [self.questionTime setHidden:NO];
}

- (void)setWithAnswer:(Answer *)answer
{
//    [self.questionTime reset];
//    self.questionTitle.text = answer.question.questionText;
//    [self.questionImage setImage:[self getImageWithQuestionType:answer.question.questionType]];
//    [self.questionTime setTimerType:MZTimerLabelTypeTimer];
//    [self.questionTime setShouldCountBeyondHHLimit:YES];
//    [self.questionTime setCountDownTime:[answer.question getRemainingTime]];
//    [self.questionTime start];
}

- (void)setWithQuestion:(Question *)question
{
    //[self.questionTime pause];
    
    _question = question;
    [self configureTimerLabel];
    self.questionTitle.text = question.questionText;
    [self.questionImage setImage:[self getImageWithQuestionType:question.questionType]];
    [self setResponseCountersForQuestion:question];
}
#pragma mark -- TimerLabel
-(void)configureTimerLabel{
    [self.questionTime setHidden:NO];
    [self.questionTime setTextColor:[Color timerNormalColorForCell]];
    
    if (_question.status == QuestionStatusClosed || [_question isExpired]){
        [self.lblCurrentPeriod setHidden:YES];
        [self.questionTime setText:TITLE_TIMER_CLOSED];
        [self.questionTime setTextColor:[Color timerNormalColorForCell]];
        return;
    }
    [self.lblCurrentPeriod setHidden:NO];
    [self.lblCurrentPeriod setText:(_question.status == QuestionStatusAdvice) ? ADVICE_PERIOD : CHAT_PERIOD];
    
    [self.questionTime setTimerType:MZTimerLabelTypeTimer];
    [self.questionTime setShouldCountBeyondHHLimit:YES];
    [self.questionTime setCountDownTime:[_question getRemainingTime]];
    [self.questionTime startWithEndingBlock:^(NSTimeInterval countTime){
        [self onTimerStop];
    }];
    
    [self.questionTime setDelegate:self];
}

-(void)onTimerStop{
    
    switch (_question.status) {
        case QuestionStatusAdvice:
            [self onTimerStopForQuestionStatusAdvice];
            break;
        
        case QuestionStatusChat:
            [self onTimerStopForQuestionStatusChat];
            break;
            
        default:
            break;
    }
    
}

-(void)onTimerStopForQuestionStatusAdvice{
    
    
    if([UserDefaults getUserRole] == UserRoleSeeker){
        _question.status = QuestionStatusChat;
        [self configureTimerLabel];
    }
    else{
        [self onTimerStopForQuestionStatusChat];
    }
}

-(void)onTimerStopForQuestionStatusChat{
    if([UserDefaults getUserRole] == UserRoleAdvisor){
        if(self.delegate && [self.delegate respondsToSelector:@selector(removeClosedQuestionFromList:)])
            [self.delegate removeClosedQuestionFromList:_question];
        return;
    }
    _question.status = QuestionStatusClosed;
    [self.questionTime setTextColor:[Color timerNormalColorForCell]];
    [self.questionTime setText:TITLE_TIMER_CLOSED];
    [self.lblCurrentPeriod setHidden:YES];
    
}

#pragma mark --
- (void)setResponseCountersForQuestion:(Question*)question{
    NSString *responseCounterTxt = [NSString stringWithFormat:@"%ld/%ld", (long)[question totalAdvisors],  (long)[question totalAnswers]];
    
    if ([question isRead]){
        [self.lblResponseCounter setText:responseCounterTxt];
        return;
    }
    
    NSRange range = [responseCounterTxt rangeOfString:@"/"];
    range.length = [responseCounterTxt length] - range.location - 1;
    range.location += 1;
    
    NSMutableAttributedString *attributtedString = [[NSMutableAttributedString alloc] initWithString:responseCounterTxt];
    [attributtedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    
    [self.lblResponseCounter setAttributedText:attributtedString];
}

- (UIImage *)getImageWithQuestionType:(QuestionType )questionType
{
    switch (questionType)
    {
        case QuestionTypeYN:
             return [UIImage imageNamed:@"question-type-icon-yes-no"];
    
            break;
            
        case QuestionTypeComplex:
            return [UIImage imageNamed:@"question-type-icon-complex"];
            
            break;
            
        default:
            return [UIImage imageNamed:@"question-type-icon-multiple"];
            
            break;
    }
}

#pragma mark - MZTimer delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType
{
    if (time < TIMER_GOING_TO_EXPIRE_TIME) {
        [timerLabel setTextColor:[Color timerWillExpireColor]];
    }
}
@end
