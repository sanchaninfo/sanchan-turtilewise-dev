//
//  FollowRequestCell.h
//  TurtleWise
//
//  Created by Sunflower on 9/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *requesterNme;
+ (NSString *)cellName;
@property (weak, nonatomic) IBOutlet UIButton *denyButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
-(void)set:(NSString*)Request;
@end
