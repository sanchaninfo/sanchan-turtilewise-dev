//
//  CheckAdvisorCell.m
//  TurtleWise
//
//  Created by Irfan Gul on 23/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "NewUserInfoController.h"
#import "SubscribeController.h"
#import "CheckAdvisorCell.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "Answer.h"
#import "Color.h"
#import "Keys.h"
#import "Font.h"

@implementation CheckAdvisorCell

+(NSString *)cellName{
    return NSStringFromClass([CheckAdvisorCell class]);
}

- (void)awakeFromNib {
    _imageViewAdvisor.layer.cornerRadius = CGRectGetWidth(_imageViewAdvisor.frame)/2.0f;
    _imageViewAdvisor.clipsToBounds = YES;
    _btnUsername.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
    [linkAttributes setObject:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    [linkAttributes setObject:[Color mediumBlueColor] forKey:(NSString *)kCTForegroundColorAttributeName];

    _lblReasonAndWhyMe.linkAttributes = linkAttributes;
    
    
    NSMutableDictionary *activelinkAttributes = [NSMutableDictionary dictionary];
    [activelinkAttributes setObject:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    [activelinkAttributes setObject:[Color mediumBlueColor] forKey:(NSString *)kCTForegroundColorAttributeName];
    
    _lblReasonAndWhyMe.activeLinkAttributes = activelinkAttributes;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)setAnswer:(Answer *)answer andQuestionType:(QuestionType)questionType{
    
    _answer = answer;
    [self setReason:_answer.reason andWhyMe:_answer.whyChooseMe];
    
    [_imageViewAdvisor setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[_answer.advisor profileImageUrl]]]
                             placeholderImage:[UIImage imageNamed:@"user-placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 [_imageViewAdvisor setImage:image];
                             } failure:nil];
    [self.btnUsername setTitle:[_answer.advisor getDisplayName] forState:UIControlStateNormal];
    [self.lblLevel setText:[NSString stringWithFormat:@"Level %@",_answer.advisor.level]];
    [self resetFeedbackView];
    [self showSelectedFeedback];
    if(questionType == QuestionTypeComplex){
        [self setAnswerTextForComplexQuestion];
    }
    else{
        [self hideAnswerDescriptionLabel];
    }
}
- (void)configureChatViews:(BOOL)isAdvisorAddedToChat{
    
    //reset
    [self.btnAvailableForChat setEnabled:YES];
    [self.btnAvailableForChat setSelected:NO];
    
    //set
    [self.chatAvailabilityIndicator setText:_answer.advisor.chatAvailability ? @"Available for chat" : @"Unavailable for chat"];
    
    if(_answer.advisor.chatAvailability)
        [self.chatAvailabilityIndicator setTextColor:[Color greenColor]];
    else
        [self.chatAvailabilityIndicator setTextColor:[Color greyColor666]];
    
    [self.btnAvailableForChat setEnabled:_answer.advisor.chatAvailability];
    if(isAdvisorAddedToChat)
        [self.btnAvailableForChat setSelected:YES];
}

-(void)setAnswerTextForComplexQuestion{
    NSString *answerHeading = @"Answer: ";
    NSString *answerText = [NSString stringWithFormat:@"%@%@",answerHeading,_answer.answerText];
    
    NSDictionary *attributesLight = @{  NSFontAttributeName            : [Font lightFontWithSize:18],
                                        NSForegroundColorAttributeName : [Color greyColor666] };
    
    
    UIFont *boldFont = [Font boldFontWithSize:18];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:answerText
                                                                                  attributes:attributesLight];
    
    [attrString addAttribute:NSFontAttributeName
                       value:boldFont
                       range:[answerText rangeOfString:answerHeading]];
    
    [self.lblAnswer setText:[attrString mutableCopy]];
}

-(void)hideAnswerDescriptionLabel{
    self.lblAnswer.hidden = YES;
    self.lblReasonAndWhyMeTopSpaceConstraint.constant = -22;
    [self layoutIfNeeded];
}

-(void)setReason:(NSString *)reason andWhyMe:(NSString *)whyMe
{
    NSDictionary *attributesLight = @{  NSFontAttributeName            : [Font lightFontWithSize:18],
                                        NSForegroundColorAttributeName : [Color greyColor666] };
    
    UIFont *boldFont = [Font boldFontWithSize:18];
    UIFont *italicFont = [Font italicFontWithSize:18];

    NSString *reasonPrefix = @"Reason: ";
    NSString *whyPickMePrefix = @"Why Choose me: ";
    NSString *subscribeText = @"Must be Subscribed to View";
    
    if([StringUtils isEmptyOrNull:reason]){
        reason = subscribeText;
    }
    
    NSString *reasonAndWhyMeText = @"";
    if (![StringUtils isEmptyOrNull:whyMe]) {
        reasonAndWhyMeText = [reasonAndWhyMeText stringByAppendingString:[NSString stringWithFormat:@"%@%@\n\n", whyPickMePrefix, whyMe]];
    }
    reasonAndWhyMeText = [reasonAndWhyMeText stringByAppendingString:[NSString stringWithFormat:@"%@%@", reasonPrefix, reason]];
    
    [_lblReasonAndWhyMe setText:reasonAndWhyMeText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:reasonAndWhyMeText
                                                                                      attributes:attributesLight];
        
        [attrString addAttribute:NSFontAttributeName
                           value:boldFont
                           range:[reasonAndWhyMeText rangeOfString:reasonPrefix]];
        
        [attrString addAttribute:NSFontAttributeName
                           value:boldFont
                           range:[reasonAndWhyMeText rangeOfString:whyPickMePrefix]];
        
        [attrString addAttribute:NSFontAttributeName
                           value:italicFont
                           range:[reasonAndWhyMeText rangeOfString:subscribeText]];
        
        return attrString;
    }];
    
    [_lblReasonAndWhyMe setDelegate:self];
    [_lblReasonAndWhyMe addLinkToURL:[NSURL URLWithString:@"controller://subscribe"]
                           withRange:[reasonAndWhyMeText rangeOfString:subscribeText]];
}
-(void)disableNewChatFlow{
    self.btnAvailableForChat.hidden = YES;
}

#pragma mark - Actions
- (IBAction)onAvailableForChat:(id)sender{
//    [self.btnAvailableForChat setSelected:!self.btnAvailableForChat.isSelected];
    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(addSelectedAdvisor:toChat:)])
//        [self.delegate addSelectedAdvisor:_answer.advisor toChat:self.btnAvailableForChat.isSelected];
    
    if (_delegate && [_delegate respondsToSelector:@selector(didSelected:advisor:indexPath:)]) {
        [_delegate didSelected:![(UIButton *)sender isSelected] advisor:_answer.advisor indexPath:_indexPath];
    }
}

- (IBAction)onUserName:(id)sender{
    NewUserInfoController *controller = [NewUserInfoController new];
    controller.advisorId = _answer.advisor.advisorID;
    [self.navController pushViewController:controller animated:YES];
}

-(IBAction)onFeedbackOptionSelect:(id)sender{
    
    if([self feedbackAlreadyGiven])
        return;
    
    UIButton *button = (UIButton*)sender;
    for(UIButton *btn in self.feedBackButtons){
        [btn setSelected:NO];
        if(btn == button){
            [btn setSelected:YES];
        }
    }
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(didGiveFeedback:forAnswer:)]){
        [self.delegate didGiveFeedback:[self getSelectedFeedback:[button tag]] forAnswer:_answer];
    }
}

-(void)resetFeedbackView{
    for(UIButton *btn in self.feedBackButtons){
        [btn setSelected:NO];
    }
}

-(void)showSelectedFeedback{
    
    if([self feedbackAlreadyGiven])
        return;
    
    for(UIButton *btn in self.feedBackButtons){
        [btn setSelected:NO];
    }
    
    if([StringUtils isEmptyOrNull:_answer.seekersFeedback])
        return;
    
    NSInteger tag = 0;
    
    if([_answer.seekersFeedback isEqualToString:KEY_RATING_THUMBS_DOWN])
        tag = 1;
    
    else if ([_answer.seekersFeedback isEqualToString:KEY_RATING_THANKS])
        tag = 2;
    
    UIButton *btn = [[self.feedBackButtons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag == %d",tag]] firstObject];
    [btn setSelected:YES];
    
}

-(BOOL)feedbackAlreadyGiven{
    for(UIButton *btn in self.feedBackButtons){
        if([btn isSelected])
            return YES;
    }
    return NO;
}

-(NSString*)getSelectedFeedback:(NSInteger)selectedBtnTag{
    switch (selectedBtnTag) {
        case 0:
            return KEY_RATING_THUMBS_UP;
        
        case 1:
            return KEY_RATING_THUMBS_DOWN;
            
        case 2:
            return KEY_RATING_THANKS;
    }
    return KEY_RATING_THUMBS_UP;
}

#pragma mark - TTTAttributedLabelDelegate
- (void)attributedLabel:(__unused TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    if(self.delegate && [self.delegate respondsToSelector:@selector(didTapOnSubscribeLink:)])
        [self.delegate didTapOnSubscribeLink:self];
}

@end
