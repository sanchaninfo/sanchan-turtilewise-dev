//
//  SummaryTextCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SummaryTextCell.h"
#import "BrandedPage.h"
@implementation SummaryTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setData:(BrandedPage*)page {
    
    [self.lblSummaryText setText:page.summary ? page.summary : @"No Content"];
    fbLink = page.facebookLink;
    twitterLink = page.twitterLink;
    linkedInLink = page.linkedInLink;
    
    if ([page.facebookLink length] == 0) {
        _fbWidth.constant = 0;
        _fbHeight.constant = 0;
    }
    if ([page.twitterLink length] == 0) {
        _twitterWidth.constant = 0;
        _twitterHeight.constant = 0;
    }
    if ([page.linkedInLink length] == 0) {
        _linkedinWidth.constant = 0;
        _linkedinHeight.constant = 0;
    }
    
    if ([page.facebookLink length] == 0 && [page.twitterLink length] == 0 && [page.linkedInLink length] == 0) {
        _lblTop.constant = 0;
    }
}
- (IBAction)facebookTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: fbLink]];
}
- (IBAction)twitterTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: twitterLink]];
}
- (IBAction)linkedInTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: linkedInLink]];
}

@end
