//
//  ChatOptionsTableCell.m
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "ChatOptionsTableCell.h"
#import "AsyncImageView.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "Advisor.h"



@implementation ChatOptionsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setAdvisor:(Advisor *)advisor{
    [_userProfileImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[advisor profileImageUrl]]]
                             placeholderImage:[UIImage imageNamed:@"user-placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 [_userProfileImage setImage:image];
                             } failure:nil];
    
    [self.lblUserName setText:[advisor getDisplayName]];
    [self.lblUserLevel setText:[NSString stringWithFormat:@"Level %d",[advisor.level intValue]]];
}
- (IBAction)addAdvisorGroup:(UIButton *)sender {
    
    [self.delegate selectedAdvisorIndex:sender.tag];
    
}

@end
