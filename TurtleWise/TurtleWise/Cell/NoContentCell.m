//
//  NoContentCell.m
//  TurtleWise
//
//  Created by Sunflower on 9/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "NoContentCell.h"

@implementation NoContentCell

- (void) setData:(NSString *)data {
    [_lable setText:data];
}
@end
