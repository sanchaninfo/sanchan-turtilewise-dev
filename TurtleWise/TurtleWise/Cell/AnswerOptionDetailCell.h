//
//  AnswerOptionDetailCell.h
//  TurtleWise
//
//  Created by Ajdal on 1/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerOptionDetailCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel *titleLabel;
@property(nonatomic,weak) IBOutlet UILabel *detailLabel;

+(NSString *)cellName;
-(void)setTitle:(NSString*)title andDetail:(NSString*)detail;
@end
