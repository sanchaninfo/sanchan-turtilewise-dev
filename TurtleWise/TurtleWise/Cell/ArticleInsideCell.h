//
//  ArticleInsideCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleInsideCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgArticle;
@property (weak, nonatomic) IBOutlet UILabel *txtTitle;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
-(void) setImage:(NSString*)url withTitle:(NSString*)title;
@end
