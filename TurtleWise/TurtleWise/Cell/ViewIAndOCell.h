//
//  ViewIAndOCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BrandedPage;

@interface ViewIAndOCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowers;
@property (weak, nonatomic) IBOutlet UIButton *checkIconButton;

@property (weak, nonatomic) IBOutlet UIButton *followButton;
-(void) setData:(BrandedPage*)page;
@end
