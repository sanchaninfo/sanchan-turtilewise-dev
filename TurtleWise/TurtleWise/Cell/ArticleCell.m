//
//  ArticleCellCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ArticleCell.h"
#import "ArticleInsideCell.h"
#import "ManageBrandedPageView.h"

@interface ArticleCell () < UITableViewDataSource, UITableViewDelegate >
@property(nonatomic, readonly) ManageBrandedPageView *controller;
@end

@implementation ArticleCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setData:(NSArray*)articlesFromParent {
    articles = [articlesFromParent copy];
    self.articlesTableView.estimatedRowHeight = 81.0;
    [self.articlesTableView setRowHeight:UITableViewAutomaticDimension];
    [self.articlesTableView reloadData];
    [self viewDidLayoutSubviews];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return articles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)viewDidLayoutSubviews {
    
    [self.articlesTableView layoutIfNeeded];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.articlesTableView.contentSize.height);
    
    [self setNeedsLayout];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellInside";
    ArticleInsideCell *cell = (ArticleInsideCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ArticleInsideCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString* imgUrl = [[articles objectAtIndex:indexPath.row] valueForKey:@"image"];
    NSString* title = [[articles objectAtIndex:indexPath.row] valueForKey:@"title"];
    
    [cell setImage:imgUrl withTitle:title];
    
    cell.deleteButton.tag = indexPath.row;
    cell.editButton.tag = indexPath.row;
    
    [cell.editButton addTarget:self.controller action:NSSelectorFromString(@"innerEditButtonTapped:") forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton addTarget:self.controller action:NSSelectorFromString(@"innerDeleteButtonTapped:") forControlEvents:UIControlEventTouchUpInside];
    

    [self viewDidLayoutSubviews];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* url = [[articles objectAtIndex:indexPath.row] valueForKey:@"link"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
}


@end
