//
//  EventsCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "EventsCell.h"
#import "EventsInsideCell.h"
#import "ManageBrandedPageView.h"

@interface EventsCell () < UITableViewDataSource, UITableViewDelegate >
@property(nonatomic, readonly) ManageBrandedPageView *controller;
@end

@implementation EventsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setData:(NSArray*)eventsFromOut {
    //    [articles initWithArray:articlesFromParent];
    events = [eventsFromOut copy];
    self.eventsTableview.estimatedRowHeight = 81.0;
    [self.eventsTableview setRowHeight:UITableViewAutomaticDimension];
    [self.eventsTableview reloadData];
    [self viewDidLayoutSubviews];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return events.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)viewDidLayoutSubviews {
    
    [self.eventsTableview layoutIfNeeded];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.eventsTableview.contentSize.height);
    
    [self setNeedsLayout];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellInside";
    EventsInsideCell *cell = (EventsInsideCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EventsInsideCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString* imgUrl = [[events objectAtIndex:indexPath.row] valueForKey:@"image"];
    NSString* title = [[events objectAtIndex:indexPath.row] valueForKey:@"title"];
    NSString* date = [[events objectAtIndex:indexPath.row] valueForKey:@"date"];
    
    [cell setImage:imgUrl withTitle:title andDate:date];
    
    cell.btnEdit.tag = indexPath.row;
    cell.btnDelete.tag = indexPath.row;
    [cell.btnEdit addTarget:self.controller action:NSSelectorFromString(@"innerEventEditButtonTapped:") forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete addTarget:self.controller action:NSSelectorFromString(@"innerEventDeleteButtonTapped:") forControlEvents:UIControlEventTouchUpInside];
    
    [self viewDidLayoutSubviews];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* url = [[events objectAtIndex:indexPath.row] valueForKey:@"link"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
}

@end
