//
//  FollowersCell.h
//  TurtleWise
//
//  Created by Sunflower on 9/7/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *followerName;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
+ (NSString *)cellName;
-(void)set:(NSString*)name;
@end
