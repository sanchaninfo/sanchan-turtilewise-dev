//
//  RatingAdvisorsCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Advisor;
@protocol RatingAdvisorsCellDelegate;
@interface RatingAdvisorsCell : UITableViewCell

//Outlets
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsUp;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnThanks;
@property (weak, nonatomic) id <RatingAdvisorsCellDelegate> delegate;

//Actions
- (IBAction)voteUp:(id)sender;
- (IBAction)voteDown:(id)sender;
- (IBAction)giveThanks:(id)sender;

//Setter
- (void)set:(Advisor *)advisor;

@end

@protocol RatingAdvisorsCellDelegate <NSObject>

- (void)rateAdvisor:(NSString *)advisorId as:(NSString *)advisorRating;
- (void)removeRatingForAdvisor:(NSString *)advisorId;

@end