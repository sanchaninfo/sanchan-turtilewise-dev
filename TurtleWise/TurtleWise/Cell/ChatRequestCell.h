//
//  ChatRequestCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Advisor;

@interface ChatRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLevel;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;

- (void)setData:(Advisor *)advisor;
- (void)customizeLastCell;

@end
