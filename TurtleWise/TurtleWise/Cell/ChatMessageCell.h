//
//  ChatMessageCell.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TableViewCell.h"
#import "ChatMessage.h"

@class Label;
@interface ChatMessageCell : TableViewCell

@property (nonatomic, strong) ChatMessage *chatMessage;

@property (weak, nonatomic) IBOutlet Label *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (weak, nonatomic) IBOutlet UIImageView *tipImage;

+(NSString *)myCellName;
+(NSString *)cellName;

- (void)set:(ChatMessage *)chatMessage;
- (void)markMessageSent;

@end
