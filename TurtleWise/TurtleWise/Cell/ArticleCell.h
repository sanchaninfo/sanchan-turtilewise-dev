//
//  ArticleCellCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell {
    NSArray * articles;
}
@property (weak, nonatomic) IBOutlet UITableView *articlesTableView;
-(void) setData:(NSArray*)articlesFromParent;
@end
