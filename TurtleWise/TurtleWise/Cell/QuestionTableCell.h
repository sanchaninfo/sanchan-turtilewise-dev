//
//  QuestionTableCell.h
//  TurtleWise
//
//  Created by Irfan Gul on 08/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TableViewCell.h"
#import "MZTimerLabel.h"
#import "BaseEntity.h"
@class Question;
@class Answer;
@class Label;

@protocol QuestionTableCellDelegate <NSObject>

-(void)removeClosedQuestionFromList:(Question*)question;

@end

@interface QuestionTableCell : TableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *questionImage;
@property (weak, nonatomic) IBOutlet Label *questionTitle;
@property (weak, nonatomic) IBOutlet MZTimerLabel *questionTime;
@property (weak, nonatomic) IBOutlet UILabel *lblResponseCounter;
@property (weak, nonatomic) IBOutlet UILabel *lblStaticText;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPeriod;

@property (weak, nonatomic) id <QuestionTableCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timerLabelHeightConstraint;

+(NSString *)cellName;
+(CGFloat)cellHeight;

-(void)set:(BaseEntity *)answer;
-(void)hideResponseCounter;
-(void)hideQuestionTimer;
-(void)resetTimerLabel;
@end
