//
//  UserProfileCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileCell;

@interface UserProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UITextView *txtViewContent;

- (void)set:(ProfileCell *)profileCell;

@end
