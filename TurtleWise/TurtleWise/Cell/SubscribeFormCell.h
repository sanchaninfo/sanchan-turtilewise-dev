//
//  SubscribeFormCell.h
//  TurtleWise
//
//  Created by Irfan Gul on 15/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TableViewCell.h"

typedef NS_ENUM(NSUInteger, SubscribeFieldType) {
    SubscribeFieldTypeFull,
    SubscribeFieldTypeDouble,
    SubscribeFieldTypeHalf,
};

@interface SubscribeFormCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFieldName;
@property (weak, nonatomic) IBOutlet UITextField *txtField;

@property (weak, nonatomic) IBOutlet UITextField *leftHalfField;
@property (weak, nonatomic) IBOutlet UITextField *rightHalfField;

@property (assign, nonatomic) SubscribeFieldType fieldType;

+ (CGFloat)cellHeight;

@end
