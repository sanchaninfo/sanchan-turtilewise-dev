//
//  EventsCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsCell : UITableViewCell {
    NSArray * events;
}
@property (weak, nonatomic) IBOutlet UITableView *eventsTableview;
-(void) setData:(NSArray*)eventsFromOut;
@end
