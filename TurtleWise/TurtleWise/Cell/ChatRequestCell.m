//
//  ChatRequestCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "ChatRequestCell.h"
#import "AsyncImageView.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "Advisor.h"
#import "Utility.h"
#import "Color.h"
#import "Font.h"

#define TEXT_LEVEL @"Level"
#define TEXT_TOTAL_COST @"$30.00"
#define TEXT_TOTAL @"Total"

@interface ChatRequestCell ()

- (void)applyAttributesOnUserLevelTxt;

@end

@implementation ChatRequestCell

#pragma mark - Life Cycle Methods

- (void)customizeLastCell
{
    [_userProfileImage setHidden:YES];
    [_lblUserName setHidden:YES];
    
    [_lblCost setText:TEXT_TOTAL_COST];
    [_lblCost setTextColor:[Color mediumBlueColor]];
    
    [_lblUserLevel setText:TEXT_TOTAL];
    [_lblUserLevel setTextColor:[Color mediumBlueColor]];
    [_lblUserLevel setFont:[Font lightFontWithSize:14]];
}

#pragma mark - Setter

- (void)setData:(Advisor *)advisor
{
    [_userProfileImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[advisor profileImageUrl]]]
                             placeholderImage:[UIImage imageNamed:@"user-placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 [_userProfileImage setImage:image];
                             } failure:nil];
    [_lblUserName setText:[advisor getDisplayName]];
    [_lblUserLevel setText:[NSString stringWithFormat:@"Level %ld", (long)[[advisor level]integerValue]]];
    [self applyAttributesOnUserLevelTxt];
}

- (void)applyAttributesOnUserLevelTxt
{
    [_lblUserLevel setAttributedText:[Utility applyAttributes:@{ NSFontAttributeName : [Font lightFontWithSize:14] }
                                                  onSubstring:TEXT_LEVEL
                     withParentString:[_lblUserLevel text]andAttributes:@{ NSFontAttributeName : [Font mediumFontWithSize:14] }]];
}

@end
