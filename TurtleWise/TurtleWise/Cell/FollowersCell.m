//
//  FollowersCell.m
//  TurtleWise
//
//  Created by Sunflower on 9/7/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "FollowersCell.h"

@implementation FollowersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_removeButton.layer setBorderWidth:0.8];
    [_removeButton.layer setBorderColor:([UIColor darkGrayColor].CGColor)];
    [_removeButton.layer setCornerRadius:10];
    // Initialization code
}

+ (NSString *)cellName{
    return NSStringFromClass([FollowersCell class]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)set:(NSString*)name {
    
    
    [_followerName setText:name];
}

@end
