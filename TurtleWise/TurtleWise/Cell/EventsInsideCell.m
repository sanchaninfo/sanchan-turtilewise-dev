//
//  EventsInsideCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "EventsInsideCell.h"
#import "BrandedPageManager.h"

@implementation EventsInsideCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setImage:(NSString*)url withTitle:(NSString*)title andDate:(NSString*)date {
    
    if ([[BrandedPageManager sharedManager] isOwn]) {
        [_btnEdit setHidden:NO];
        [_btnDelete setHidden:NO];
    } else {
        [_btnEdit setHidden:YES];
        [_btnDelete setHidden:YES];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage* img = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imgEvent setImage:img];
        });
        
    });
    
    self.lblEventName.text = title;
    self.lblEventDate.text = date;
}

@end
