//
//  RatingAdvisorsCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "RatingAdvisorsCell.h"
#import "AsyncImageView.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "Advisor.h"
#import "Color.h"

#define RATE_THUMBS_UP @"up"
#define RATE_THUMBS_DOWN @"down"
#define RATE_THANKS @"thanks"

@interface RatingAdvisorsCell ()

@property(nonatomic, strong) Advisor *advisor;

@end

@implementation RatingAdvisorsCell

#pragma mark - Life Cycle

- (void)awakeFromNib
{
    // Initialization code
    [_btnThanks setTitleColor:[Color lightGreenColor] forState:UIControlStateSelected];
}

#pragma mark - Setter

- (void)set:(Advisor *)advisor
{
    _advisor = advisor;
    
    [_imgViewUserProfile setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[advisor profileImageUrl]]]
                             placeholderImage:[UIImage imageNamed:@"user-placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 [_imgViewUserProfile setImage:image];
                             } failure:nil];
    [_lblUserName setText:[_advisor getDisplayName]];
}

#pragma mark - IBActions

- (IBAction)voteUp:(id)sender
{
    [sender setSelected:![sender isSelected]];
    
    [_btnThumbsDown setSelected:NO];
    [_btnThanks setSelected:NO];
    
    ([sender isSelected]) ? [self rateAdvisor:RATE_THANKS] : [self removeRating];
}

- (IBAction)voteDown:(id)sender
{
    [sender setSelected:![sender isSelected]];
    
    [_btnThumbsUp setSelected:NO];
    [_btnThanks setSelected:NO];
    
    ([sender isSelected]) ? [self rateAdvisor:RATE_THANKS] : [self removeRating];
}

- (IBAction)giveThanks:(id)sender
{
    [sender setSelected:![sender isSelected]];
    
    [_btnThumbsDown setSelected:NO];
    [_btnThumbsUp setSelected:NO];
    
    ([sender isSelected]) ? [self rateAdvisor:RATE_THANKS] : [self removeRating];
}

#pragma mark - Helper Methods

- (void)rateAdvisor:(NSString *)rating
{
    if (_delegate && [_delegate respondsToSelector:@selector(rateAdvisor:as:)])
    {
        [_delegate rateAdvisor:[_advisor advisorID] as:rating];
    }
}

- (void)removeRating
{
    if (_delegate && [_delegate respondsToSelector:@selector(removeRatingForAdvisor:)])
    {
        [_delegate removeRatingForAdvisor:[_advisor advisorID]];
    }
}

@end
