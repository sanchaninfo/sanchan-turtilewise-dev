//
//  CheckAdvisorCell.h
//  TurtleWise
//
//  Created by Irfan Gul on 23/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCell.h"
#import "TTTAttributedLabel.h"

@class Advice;
@class Answer;
@class Question;
@class Advisor;
@protocol CheckAdvisorCellDelegate;

@interface CheckAdvisorCell : TableViewCell<TTTAttributedLabelDelegate>

@property (weak, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) Answer *answer;
@property (strong, nonatomic) NSIndexPath * indexPath;
@property (weak, nonatomic) IBOutlet UIButton *btnUsername;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAdvisor;
@property (weak, nonatomic) IBOutlet UILabel *lblAnswer;
@property (weak, nonatomic) IBOutlet UILabel *lblLevel;
@property (weak, nonatomic) IBOutlet UIButton *btnAvailableForChat;
@property (weak, nonatomic) IBOutlet UILabel *chatAvailabilityIndicator;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblReasonAndWhyMe;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblReasonAndWhyMeTopSpaceConstraint;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *feedBackButtons;

@property (assign, nonatomic) id<CheckAdvisorCellDelegate> delegate;

-(void)setAnswer:(Answer *)answer andQuestionType:(QuestionType)questionType;

-(void)configureChatViews:(BOOL)isAdvisorAddedToChat;

-(void)disableNewChatFlow;

+(NSString *)cellName;
@end

@protocol CheckAdvisorCellDelegate <NSObject>


- (void)didTapOnSubscribeLink:(CheckAdvisorCell *)cell;
- (void)addSelectedAdvisor:(Advisor*)advisor toChat:(BOOL)add;
- (void)didSelected:(BOOL)selected advisor:(Advisor*)advisor indexPath:(NSIndexPath *)indexPath;
- (void)didGiveFeedback:(NSString*)feedback forAnswer:(Answer*)answer;

@end