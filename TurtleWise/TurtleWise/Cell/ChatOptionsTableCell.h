//
//  ChatOptionsTableCell.h
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedAdvisorDelegate

-(void)selectedAdvisorIndex:(NSInteger)index;
    
@end

@class Advisor;

@interface ChatOptionsTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLevel;
@property (weak, nonatomic) IBOutlet UIButton *addAdvisor;
@property (assign, nonatomic) id<SelectedAdvisorDelegate>delegate;


- (void)setAdvisor:(Advisor *)advisor;
@end
