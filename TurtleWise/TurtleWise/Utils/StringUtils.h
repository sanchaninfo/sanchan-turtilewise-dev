//
//  StringUtils.h
//  Guardian
//
//  Created by mohsin on 10/17/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject

+(BOOL)isEmptyOrNull:(NSString*)value;

+(NSString*)validateForNull:(NSString*)value;

+(BOOL)validateEmail:(NSString*)candidate;

+(NSString *) getStringFromInt:(int) integer;

+(BOOL)compareString:(NSString *)targetString withString:(NSString *)otherString;
+(BOOL)compareStringIgnorCase:(NSString *)targetString withString:(NSString *)otherString;

+ (NSString *)sentenceCapitalizedString:(NSString *)targetString;

+ (NSString *)realSentenceCapitalizedString:(NSString *)targetString;

+ (NSString *)removeSuffix:(NSString *)suffix from:(NSString *)targetString;

+ (NSString*)getEncodedString:(NSString*)string;

+ (NSString*)base64forData:(NSData*)theData;

+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData withDefault:(NSString *)defaultImageName;

+ (BOOL) validateURL: (NSString *) candidate;

@end
