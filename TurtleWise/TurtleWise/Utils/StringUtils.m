//
//  StringUtils.m
//  Guardian
//
//  Created by mohsin on 10/17/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import "StringUtils.h"

@implementation StringUtils

+(BOOL)isEmptyOrNull:(NSString*)value{
    if([value isKindOfClass:[NSNull class]] || value.length == 0)
        return YES;
    
    return NO;
}

+(NSString*)validateForNull:(NSString*)value{
    if([self isEmptyOrNull:value])
        return nil;
    
    return value;
}

+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL) validateURL: (NSString *) candidate {
    NSString *urlRegex = @"^(http|https)://.*$";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
    
    return [urlTest evaluateWithObject:candidate];
}


+(NSString *) getStringFromInt:(int) integer{

    return [NSString stringWithFormat:@"%d", integer];
}

+(BOOL)compareString:(NSString *)targetString withString:(NSString *)otherString
{
    return ([targetString isEqualToString:otherString]);
}

+(BOOL)compareStringIgnorCase:(NSString *)targetString withString:(NSString *)otherString
{
    if (![targetString length] || ![otherString length])
    {
        return NO;
    }
    return ([targetString caseInsensitiveCompare:otherString] == NSOrderedSame);
}

+ (NSString *)sentenceCapitalizedString:(NSString *)targetString
{
    if (![targetString length])
    {
        return [NSString string];
    }
    
    NSString *uppercase = [[targetString substringToIndex:1] uppercaseString];
    NSString *lowercase = [[targetString substringFromIndex:1] lowercaseString];
    return [uppercase stringByAppendingString:lowercase];
}


+ (NSString *)realSentenceCapitalizedString:(NSString *)targetString
{
    __block NSMutableString *mutableSelf = [NSMutableString stringWithString:targetString];
    [targetString enumerateSubstringsInRange:NSMakeRange(0, [targetString length])
                                     options:NSStringEnumerationBySentences
                                  usingBlock:^(NSString *sentence, NSRange sentenceRange, NSRange enclosingRange, BOOL *stop) {
                                      [mutableSelf replaceCharactersInRange:sentenceRange withString:[StringUtils sentenceCapitalizedString:sentence]];
                          }];
    return [NSString stringWithString:mutableSelf]; // or just return mutableSelf.
}

+ (NSString *)removeSuffix:(NSString *)suffix from:(NSString *)targetString
{
    NSString *newString = [targetString copy];
    if ([targetString hasSuffix:suffix])
    {
        newString = [targetString substringToIndex:[targetString length] - [suffix length]];
    }
    
    return newString;
}
+(NSString*)getEncodedString:(NSString*)string{
    
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (__bridge CFStringRef) string,
                                                                                                    NULL,
                                                                                                    CFSTR(":/?&=;+!@#$()~',*"),
                                                                                                    kCFStringEncodingUTF8));
    return encodedString;
}

+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData withDefault:(NSString *)defaultImageName {
    if (strEncodeData) {
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        return [UIImage imageWithData:data];
    }
    return [UIImage imageNamed:defaultImageName];
}

@end
