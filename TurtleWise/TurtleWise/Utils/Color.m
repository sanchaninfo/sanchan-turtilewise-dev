//
//  Color.m
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "HexColor.h"
#import "Color.h"

@implementation Color

+(UIColor*)backgroundColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)greenColor
{
    return [UIColor colorWithRed:(102/255.0) green:(153/255.0) blue:0 alpha:1];
}

+ (UIColor *)greyColor666
{
    return [UIColor colorWithRed:(102/255.0) green:(102/255.0) blue:(102/255.0) alpha:1];
}

+ (UIColor *)greyColor999
{
    return [UIColor colorWithRed:(153.0/255.0) green:(153.0/255.0) blue:(153.0/255.0) alpha:1];
}

+ (UIColor *)lightGreenColor
{
    return [UIColor colorWithRed:(142/255.0) green:(196/255.0) blue:(77/255.0) alpha:1];
}

+ (UIColor *)lightGreenShade
{
    return [HXColor colorWithHexString:@"#98CF67"];
}

+ (UIColor *)mediumBlueColor
{
    return [UIColor colorWithRed:(32/255.0) green:(97/255.0) blue:(186/255.0) alpha:1];
}

+ (UIColor *)blueTagColor{
    return [UIColor colorWithRed:(40.0/255.0) green:(119.0/255.0) blue:(198.0/255.0) alpha:1];
}

+ (UIColor *)timerWillExpireColor{
    return [UIColor redColor];
}
+ (UIColor *)timerNormalColorForCell{
    return [UIColor colorWithRed:(106.0/255.0) green:(106.0/255.0) blue:(106.0/255.0) alpha:1];
}
+ (UIColor *)timerNormalColorForDetail{
    return [UIColor colorWithRed:(209.0/255.0) green:(209.0/255.0) blue:(209.0/255.0) alpha:1];
}
@end
