//
//  NotificationPanel.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/19/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "NotificationPanel.h"
#import "StringUtils.h"
#import "MFSideMenu.h"
#import "TSMessage.h"

@implementation NotificationPanel

+ (void)showInController:(UIViewController *)targetController message:(NSString *)message withOnClickAction:(void (^)())clickAction
{
    if ([StringUtils isEmptyOrNull:message])
    {
        return;
    }
    
    if ([targetController isKindOfClass:[MFSideMenuContainerViewController class]])
    {
        targetController = [(UINavigationController *)[(MFSideMenuContainerViewController *)targetController centerViewController] topViewController];
    }
    
    [TSMessage showNotificationInViewController:targetController title:message callback:clickAction buttonCallback:^{
        [TSMessage dismissActiveNotification];
    }];
}

@end
