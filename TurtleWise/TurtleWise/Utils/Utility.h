//
//  Utility.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/16/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (NSAttributedString *)getAttributedTags:(NSString *)normalText;

+ (NSAttributedString *)applyFont:(UIFont *)otherFont onSubString:(NSString *)subString withParentString:(NSString *)parentString withOtherAttributes:(NSDictionary *)attributes;

+ (NSAttributedString *)applyAttributes:(NSDictionary *)bolderAttributes onSubstring:(NSString *)subString withParentString:(NSString *)parentString andAttributes:(NSDictionary *)otherAttributes;

+ (void)playNotificationSound;

+ (BOOL)isAgeBetweenBounds:(NSString *)plainText;

+ (NSNumber *)getNumberFromString:(NSString *)string;

@end
