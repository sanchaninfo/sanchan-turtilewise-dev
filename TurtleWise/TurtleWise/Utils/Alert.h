//
//  Alert.h
//  Guardian
//
//  Created by mohsin on 10/22/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseController;

typedef void(^AlertCompletionHandler)(void);

@interface Alert : NSObject

+(void)show:(NSString*)title andMessage:(NSString*)message;
+(void)show:(NSString*)title andMessage:(NSString*)message andDelegate:(id<UIAlertViewDelegate>)theDelegate;

+(void)show:(NSString*)title andMessage:(NSString *)message boldText:(NSString *)boldText;
+(void)show:(NSString*)title andMessage:(NSString *)message boldText:(NSString *)boldText onDismiss:(AlertCompletionHandler)dismiss;
+(void)showInController:(BaseController *)controller withTitle:(NSString*)title message:(NSString *)message onDismiss:(AlertCompletionHandler)dismiss;

+(void)show:(NSError *)error;
+(void)show:(NSString *)title andMessage:(NSString *)message cancelButtonTitle:(NSString *)cancelButton otherButtonTitles:(NSString *)otherButtonTitles andDelegate:(id<UIAlertViewDelegate>)theDelegate;
+(void)show:(NSString *)title andMessage:(NSString *)message cancelButtonTitle:(NSString *)cancelButton otherButtonTitles:(NSString *)otherButtonTitles tag:(NSInteger) tag andDelegate:(id<UIAlertViewDelegate>)theDelegate;
@end
