//
//  NotificationPanel.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/19/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationPanel : NSObject

+ (void)showInController:(UIViewController *)targetController message:(NSString *)message withOnClickAction:(void (^)())clickAction;

@end