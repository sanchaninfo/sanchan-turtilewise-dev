//
//  Font.h
//  wusup
//
//  Created by mohsin on 3/11/14.
//  Copyright (c) 2014 SocialRadar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Font : NSObject

+ (CGFloat)getHeight:(NSString*)text andFont:(UIFont *)font andWidth:(int)width;

+ (UIFont *)lightFontWithSize:(int)size;

+ (UIFont *)regularFontWithSize:(int)size;

+ (UIFont *)boldFontWithSize:(int)size;

+ (UIFont *)mediumFontWithSize:(int)size;

+ (UIFont *)italicFontWithSize:(int)size;

+ (UIFont *)navButtonFont;
@end
