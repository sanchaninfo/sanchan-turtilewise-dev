//
//  UserDefaults.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enum.h"
#import "UserLevel.h"
#define KEY_USER_ROLE @"UserRole"

@class User;
@class UserProfile;
@class UserStats;

@interface UserDefaults : NSObject

+ (UserRole)getUserRole;
+ (void)setUserRole:(UserRole)userRole;

+ (void)setUserProfile:(UserProfile *)profile;
+ (UserProfile *)getUserProfile;

+ (void)setLevelTable:(NSArray *)levels;
+ (NSArray *)getLevelTable;

+ (void)setCharities:(NSArray *)charities;
+ (NSArray *)getCharities;

+ (void)setLevelBucks:(NSArray *)levelBucks;
+ (NSArray *)getLevelBucks;

+ (void)setLevelAmount:(NSArray *)levelAmount;
+ (NSArray *)getLevelAmount;

+ (LoginFlowType)loginFlowType;
+ (void)setLoginFlowType:(LoginFlowType)loginFlow;

+ (NSString *)getAccessToken;
+ (void)setAccessToken:(NSString *)accessToken;

+ (BOOL)isUserLogin;

+ (NSString *)getUserID;
+ (void)setUserID:(NSString *)userID;

+ (NSString *)getPageID;
+ (void)setPageID:(NSString *)pageID;

+ (UserStats *)getUserStats;
+ (void)setUserStats:(UserStats *)stats;

+ (NSString *)getPushToken;
+ (void)savePushToken:(NSString *)pushToken;

+ (BOOL)shouldCheckForUnratedChats;
+ (void)setShouldCheckForUnratedChats:(BOOL)shouldCheck;

+ (NSDate *)getLastLogin;
+ (void)setLastLogin:(NSDate *)lastLogin;

+ (void)clearDefaults;

#pragma mark - Remember Me
+ (void)saveLoginEmail:(NSString*)email;
+ (NSString*)getLoginEmail;

+ (void)saveLoginPassword:(NSString*)email;
+ (NSString*)getLoginPassword;

+ (void)clearSavedLoginCredentials;

+ (BOOL)isPremiumUser;

+ (BOOL) showAlertForIncompleteProfile:(UserProfile *) profile;

+ (void)followPrivate:(NSString*)pageId;

+ (void)followPublic:(NSString*)pageId;

+ (void)unfollow:(NSString*)pageId;
@end
