//
//  Color.h
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Color : UIColor

+ (UIColor*)backgroundColor;
+ (UIColor *)greenColor;
+ (UIColor *)greyColor666;
+ (UIColor *)greyColor999;
+ (UIColor *)mediumBlueColor;
+ (UIColor *)lightGreenColor;
+ (UIColor *)lightGreenShade;
+ (UIColor *)blueTagColor;
+ (UIColor *)timerWillExpireColor;
+ (UIColor *)timerNormalColorForCell;
+ (UIColor *)timerNormalColorForDetail;
@end
