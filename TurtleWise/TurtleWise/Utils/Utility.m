//
//  Utility.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/16/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "UserDefaults.h"
#import "Utility.h"
#import "Color.h"
#import "Font.h"

#define MAX_AGE 80
#define MIN_AGE 18

#define NOTIFICATION_SOUND @"notification-alert-sound"

@implementation Utility

+ (NSAttributedString *)getAttributedTags:(NSString *)normalText
{
    NSString *tags = [normalText stringByAppendingString:@" +ADD"];
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName            :[Font regularFontWithSize:15],
                                 NSForegroundColorAttributeName :[Color greenColor]
                                 };
    
    return [Utility applyFont:[Font boldFontWithSize:15]  onSubString:@"+ADD" withParentString:tags withOtherAttributes:attributes];
}

+ (NSAttributedString *)applyFont:(UIFont *)otherFont onSubString:(NSString *)subString withParentString:(NSString *)parentString withOtherAttributes:(NSDictionary *)attributes
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithString:parentString attributes:attributes];
    
    NSRange range = [parentString rangeOfString:subString];
    [attrStr addAttribute:NSFontAttributeName value:otherFont range:range];
    
    return attrStr;
}

+ (NSAttributedString *)applyAttributes:(NSDictionary *)bolderAttributes onSubstring:(NSString *)subString withParentString:(NSString *)parentString andAttributes:(NSDictionary *)otherAttributes
{
     NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:parentString attributes:otherAttributes];

    [attrStr addAttributes:bolderAttributes range:[parentString rangeOfString:subString]];
    
    return attrStr;
}

+ (void)playNotificationSound
{
//    NSString *soundPath = [[NSBundle mainBundle] pathForResource:NOTIFICATION_SOUND ofType:@"wav"];
//    SystemSoundID soundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
//    AudioServicesPlaySystemSound (soundID);
}

+ (BOOL)isAgeBetweenBounds:(NSString *)plainText
{
    if ([plainText containsString:@"-"])
    {
        NSArray *age = [plainText componentsSeparatedByString:@"-"];
        
        NSInteger lowerLimit = [[age objectAtIndex:0] integerValue];
        NSInteger upperLimit = [[age objectAtIndex:1] integerValue];
        
        if (upperLimit == 0)
        {
             return (lowerLimit > MAX_AGE || lowerLimit < MIN_AGE);
        }
        
        if (lowerLimit > upperLimit)
        {
            return YES;
        }
        
        return (upperLimit > MAX_AGE || lowerLimit < MIN_AGE);
    }
    else
    {
        int age = [plainText intValue];
        
        return (age > MAX_AGE || age < MIN_AGE);
    }
    
    return NO;
}

+ (NSNumber *)getNumberFromString:(NSString *)string
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;

    return [f numberFromString:string];
}

@end
