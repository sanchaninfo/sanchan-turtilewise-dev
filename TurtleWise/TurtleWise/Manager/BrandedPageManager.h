//
//  BrandedPageManager.h
//  TurtleWise
//
//  Created by Sunflower on 8/23/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BrandedPage;

@interface BrandedPageManager : NSObject {
    BrandedPage *brandedPage;
    NSString *pageId;
    BOOL isOwn;
}
@property(nonatomic, retain) BrandedPage *brandedPage;
@property(nonatomic) BOOL isOwn;
@property(nonatomic) NSString * pageId;

+ (id) sharedManager;
- (void) initWithBrandedPage:(BrandedPage *)brandedPageDetail;
- (NSDictionary*) getDictionaryFromBrandedPage:(BrandedPage*)page;

- (void) setPageIdForBrandedPage:(NSString *)pageId;

- (void) clearPageId;

- (NSString*) getPageIdForBrandedpage;
@end
