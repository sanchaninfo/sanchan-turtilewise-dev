//
//  BrandedPageManager.m
//  TurtleWise
//
//  Created by Sunflower on 8/23/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BrandedPageManager.h"
#import "UserDefaults.h"
#import "BrandedPage.h"

@implementation BrandedPageManager

@synthesize brandedPage;
@synthesize pageId;
@synthesize isOwn;

+ (id) sharedManager {
    static BrandedPageManager *manager = nil;
    static dispatch_once_t onceToekn;
    dispatch_once(&onceToekn, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void) initWithBrandedPage:(BrandedPage *)brandedPageDetail {
    brandedPage = brandedPageDetail;
}

- (void) dealloc {
}


- (NSDictionary*) getDictionaryFromBrandedPage:(BrandedPage*)page {
    
    NSDictionary * socialParam = @{
                                   KEY_FACEBOOK:[page facebookLink]?[page facebookLink]:@"",
                                   KEY_TWITTER:[page twitterLink]?[page twitterLink]:@"",
                                   KEY_LINKEDIN:[page linkedInLink]?[page linkedInLink]:@""
                                   };
    
    NSArray * approved = [page approved];
    NSArray * pending = [page pending];
    
    NSDictionary * members = [NSDictionary dictionaryWithObjectsAndKeys:
                              approved, KEY_APPROVED,
                              pending, KEY_PENDING, nil];
    
    NSArray * articles = [page articles]?[page articles]:@[];
    NSArray * events = [page events]?[page events]:@[];
    
    NSDictionary * params = @{
                              KEY_OWNER:[UserDefaults getUserID],
                              KEY_TYPE:[page type],
                              KEY_NAME:[page name],
                              KEY_PRIVACY:[page privacy]?[page privacy]:@"private",
                              KEY_BRANDED_SUMMARY:[page summary]?[page summary]:@"",
                              KEY_SOCIAL:socialParam,
                              KEY_ARTICLES:articles,
                              KEY_EVENTS:events,
                              KEY_MEMBERS:members
                              };
    
    return params;
}

- (void) setPageIdForBrandedPage:(NSString *)pageIdd {
    pageId = pageIdd;
    isOwn = NO;
}

- (void) clearPageId{
    if ([[UserDefaults getPageID] length] == 0) {
        pageId = NULL;
    } else {
        pageId = [UserDefaults getPageID];
    }
    isOwn = YES;
}

- (NSString*) getPageIdForBrandedpage{
    return pageId;
}



@end
