//
//  ProfileWizardManager.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseController;
@class BaseProfile;
@class AskQuestion;

@interface TWWizardManager : NSObject

@property(nonatomic, assign) NSInteger currentStep;
@property(nonatomic, strong) BaseProfile *profile;
@property(nonatomic, strong) NSMutableDictionary *createBrandedPage;
@property(nonatomic, strong) AskQuestion *question;

@property(nonatomic, assign) BOOL shouldSaveWizardOnDone;

- (id)initWithPresentingController:(BaseController *)presentingController;
- (id)initWithPresentingController:(BaseController *)presentingController fromStep:(NSInteger )startStep;
- (BOOL)isLastStep;
- (void)startProfileWizard;
- (void)startAdvisorWizard;
- (void)next;
- (void)previous;
- (void)finish;

@end
