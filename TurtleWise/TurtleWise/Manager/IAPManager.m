//
//  IAPManager.m
//  TurtleWise
//
//  Created by Waleed Khan on 6/1/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//
// This Class is a Wrapper to Apple In App Purchases (IAP).

#import "EnvironmentConstants.h"
#import "IAPManager.h"

#define PRODUCT_IDENTIFIERS @[IAP_MONTHLY_SUBSCRIPTION, IAP_SINGLE_QUESTION] //Add Products ere...

@interface IAPManager ()

@property (nonatomic, copy) IAPProductsResponseBlock requestProductsBlock;
@property (nonatomic, copy) IAPbuyProductCompleteResponseBlock buyProductCompleteBlock;
@property (nonatomic, copy) resoreProductsCompleteResponseBlock restoreCompletedBlock;
@property (nonatomic, copy) checkReceiptCompleteResponseBlock checkReceiptCompleteBlock;

- (void)initalize;

//Product Transactions
- (void)completeTransaction:(SKPaymentTransaction *)transaction;
- (void)failedTransaction:(SKPaymentTransaction *)transaction;
- (void)restoreTransaction:(SKPaymentTransaction *)transaction;
- (void)buyProduct:(SKProduct *)productIdentifier onCompletion:(IAPbuyProductCompleteResponseBlock)completion;

//Helper Methods
- (BOOL)hasProductsLoaded;

@end

@implementation IAPManager

#pragma mark - Life Cycle Methods

+ (IAPManager *)sharedInstance
{
    static IAPManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (id)init
{
    if (self = [super init])
    {
        [self initalize];
    }
    
    return self;
}

- (void)initalize
{
    
}

#pragma mark - Request Products

- (void)requestProductsWithCompletion:(IAPProductsResponseBlock)completion
{
    if (_products)
    {
        if(_requestProductsBlock)
        {
            _requestProductsBlock (NULL, NULL);
        }
        
        return;
    }
    
    _request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:PRODUCT_IDENTIFIERS]];
    [_request setDelegate:self];
    
    _requestProductsBlock = completion;
    
    [_request start];
}

#pragma mark - SKProductsRequestDelegate Methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    _products = [response products];
    _request = NULL;
    
    if(_requestProductsBlock)
    {
        _requestProductsBlock (request, response);
    }
}

#pragma mark - Buy Products 

- (void)buyProduct:(SKProduct *)productIdentifier onCompletion:(IAPbuyProductCompleteResponseBlock)completion
{
    _buyProductCompleteBlock = completion;
    _restoreCompletedBlock = NULL;
    
    SKPayment *payment = [SKPayment paymentWithProduct:productIdentifier];
    
    if ([SKPaymentQueue defaultQueue])
    {
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}

- (void)buyMonthlySubsciptionWithCompletion:(IAPbuyProductCompleteResponseBlock)completion
{
    if (![self hasProductsLoaded])
    {
        return;
    }
    
    [self buyProduct:[self getProductForIdentifier:IAP_MONTHLY_SUBSCRIPTION]onCompletion:completion];
}

- (void)buyOneQuestionSubsctiptionWithCompletion:(IAPbuyProductCompleteResponseBlock)completion
{
    if (![self hasProductsLoaded])
    {
        return;
    }
    
    [self buyProduct:[self getProductForIdentifier:IAP_SINGLE_QUESTION] onCompletion:completion];
}

#pragma mark - Reciept Validation Method

- (void)checkReceipOnCompletion:(checkReceiptCompleteResponseBlock)completion
{
    [self checkReceipt:[self getReceiptData] AndSharedSecret:nil onCompletion:completion];
}

- (void)checkReceipt:(NSData *)receiptData AndSharedSecret:(NSString *)secretKey onCompletion:(checkReceiptCompleteResponseBlock)completion
{
    //TODO: Verify Recipt Here by a POST call.
}

#pragma mark - Restore Products

- (void)restoreProductsWithCompletion:(resoreProductsCompleteResponseBlock)completion
{
    //clear it
    self.buyProductCompleteBlock = nil;
    
    self.restoreCompletedBlock = completion;
 
    if ([SKPaymentQueue defaultQueue])
    {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    
    else
    {
        NSLog(@"Cannot get the default Queue");
    }
}

#pragma mark - SKPaymentTransactionObserver Methods

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                
            default:
                break;
        }
    }
}

#pragma mark - Product Transactions Update Methods

- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    if ([SKPaymentQueue defaultQueue])
    {
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
    
    if(_buyProductCompleteBlock)
    {
        _buyProductCompleteBlock(transaction);
    }
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if ([[transaction error] code] != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@ %ld", transaction.error.localizedDescription,(long)transaction.error.code);
    }
    
    if ([SKPaymentQueue defaultQueue])
    {
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
        if(_buyProductCompleteBlock)
        {
            _buyProductCompleteBlock(transaction);
        }
    }
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{    
    if ([SKPaymentQueue defaultQueue])
    {
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        
        if(_buyProductCompleteBlock)
        {
            _buyProductCompleteBlock(transaction);
        }
    }
}

#pragma mark - Helper Method

- (NSData *)getReceiptData
{
    return [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
}

- (BOOL)hasProductsLoaded
{
    if ((_products && [_products count] != 0))
    {
        return YES;
    }
    
    NSLog(@"IAP Manager: Products Not Requested");
    
    return NO;
}

- (SKProduct *)getProductForIdentifier:(NSString *)productIdentifier
{
    SKProduct *singleQuestionProduct;
    
    for (SKProduct *product in _products)
    {
        if ([[product productIdentifier] isEqualToString:productIdentifier])
        {
            singleQuestionProduct = product;
            
            break;
        }
    }
    
    return singleQuestionProduct;
}

@end
