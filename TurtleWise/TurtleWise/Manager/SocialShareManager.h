//
//  SocialShareManager.h
//  TurtleWise
//
//  Created by Ajdal on 3/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialShareManager : NSObject
+(void)shareOnFB;
+(void)shareOnLinkedin:(UIViewController*)presentingController;
+(void)shareOnGooglePlus:(UIViewController*)presentingController;
+(void)shareOnTwitter:(UIViewController*)presentingController;
@end
