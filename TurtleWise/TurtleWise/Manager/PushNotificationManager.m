//
//  PushNotificationManager.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/29/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "QuestionListingController.h"
#import "PushNotificationManager.h"
#import "NewGiveAnswerController.h"
#import "NotificationPanel.h"
#import "AnswerController.h"
#import "SocketIOManager.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "AppDelegate.h"
#import "ChatMessage.h"
#import "MFSideMenu.h"
#import "Constant.h"
#import "Keys.h"

#define KEY_NOTIFICATION_EVENT @"event"

#define SEEKER      @"se"
#define ADVISOR     @"ad"
#define RESPONSES   @"responses"
#define ANSWER      @"answer"
#define CHAT        @"chat"
#define GROUP       @"group"


@interface PushNotificationManager ()

@property(nonatomic) UIApplicationState applicationState;

@property(nonatomic, strong) NSDictionary *payload;
@property(nonatomic, strong) NSUserActivity *userActivity;
@property(nonatomic, weak) id < PushNotificationManagerDelegate > delegate;

//Private Methods
+ (NSString *)deviceTokenToString:(NSData*)deviceToken;

- (void)onQuestionRecieved;
- (void)onAnswerRecieved;
- (void)onNewMessageRecieved;
- (void)onBeginAndEndChat;
- (void)onSubscriptionEnded;

- (void)showRecievedAnswerForQuestion:(NSString *)questionId;
- (void)showRecievedQuestion:(NSString *)questionId;
- (void)showNewMessageRecivedForConversation:(BOOL)isConversationActive;
- (void)showChatWithId:(NSString *)chatId;
- (void)showSubscriptionEnded;

@end

@implementation PushNotificationManager

#pragma mark - Life Cycle Methods

- (id)initWithPayload:(NSDictionary *)payload andDelegate:(id)delegate
{
    if (self == [super init])
    {
        _payload = payload;
        _delegate = delegate;
        
        _applicationState = [[UIApplication sharedApplication] applicationState];
    }
    
    return self;
}

- (id)initWithUserActivity:(NSUserActivity *)userActivity andDelegate:(id)delegate
{
    if (self == [super init])
    {
        _userActivity = userActivity;
        _delegate = delegate;
        
        _applicationState = [[UIApplication sharedApplication] applicationState];
    }
    
    return self;
}

#pragma mark - Register

+ (void)registerForPushNotification
{
    UIApplication *application = [UIApplication sharedApplication];
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil]];
    [application registerForRemoteNotifications];
}

#pragma mark - Device Token Management

+ (void)storeDeviceToken:(NSData *)deviceToken
{
    [UserDefaults savePushToken:[PushNotificationManager deviceTokenToString:deviceToken]];
}

+ (NSString *)deviceTokenToString:(NSData *)deviceToken
{
    return [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
}

#pragma mark - Push Notification Routes Handling

- (void)handleRoutesForNotification
{
    NSString *notificationEventName = [_payload objectForKey:KEY_NOTIFICATION_EVENT];
    
    if ([notificationEventName isEqualToString:NOTIFICATION_TYPE_RECIEVED_QUESTION])
    {
        [self onQuestionRecieved];
    }
    
    else if([notificationEventName isEqualToString:NOTIFICATION_TYPE_RECIEVED_ANSWER])
    {
        [self onAnswerRecieved];
    }
    
    else if([notificationEventName isEqualToString:NOTIFICATION_TYPE_RECIEVED_NEW_MESSAGE])
    {
        [self onNewMessageRecieved];
    }
    
    else if([notificationEventName isEqualToString:NOTIFICATION_TYPE_RECIEVED_CHAT_INVITATION] || [notificationEventName isEqual:NOTIFICATION_TYPE_CHAT_ENDED])
    {
        [self onBeginAndEndChat];
    }
    
    else if([notificationEventName isEqualToString:NOTIFICATION_TYPE_UPDATE_PROFILE])
    {
        [self getUserProfile];
    }
    
    else if([notificationEventName isEqualToString:NOTIFICATION_TYPE_END_SUBSCRIPTION])
    {
        [self onSubscriptionEnded];
    }
    
    [self getUserStats];
}

#pragma mark - User Activity Routes Handling

- (void)handleRoutesForUserActivity {
    NSString * emailLink = _userActivity.webpageURL.absoluteString;
    
    if ([self linkUrl:emailLink containsAttributeName:@"resetPassword"]) {
        NSString * token = [self getIdFromLinkUrl:emailLink];
        [self showResetPassword:token];
        return;
    }
    if ([self linkUrl:emailLink containsAttributeName:@"unsubscribe"]) {
        NSString * token = [self getIdFromLinkUrl:emailLink];
        [self showEmailNotificationSettings:token];
        return;
    }

    if ([self linkUrl:emailLink containsAttributeName:ADVISOR])
        [UserDefaults setUserRole:UserRoleAdvisor];
    if ([self linkUrl:emailLink containsAttributeName:SEEKER])
        [UserDefaults setUserRole:UserRoleSeeker];
    
    NSString * infoId = [self getIdFromLinkUrl:emailLink];
    if ([self linkUrl:emailLink containsAttributeName:RESPONSES])
        [self showRecievedAnswerForQuestion:infoId];
    if ([self linkUrl:emailLink containsAttributeName:ANSWER])
        [self showRecievedQuestion:infoId];
    if ([self linkUrl:emailLink containsAttributeName:CHAT])
        [self showChatWithId:infoId];
    
    NSString * paramString = [[self getParamStringFromLinkUrl:emailLink] stringByAppendingString:@"&action=linkClicked"];
    [self performSelector:@selector(trackEventWithParams:) withObject:paramString afterDelay:1];
}

#pragma mark -

- (BOOL)linkUrl:(NSString *)linkUrl containsAttributeName:(NSString *)attributeName {
    NSArray * components = [linkUrl componentsSeparatedByString:@"/"];
    return [components containsObject:attributeName];
}

- (NSString *)getIdFromLinkUrl:(NSString *)linkUrl {
    NSRange range = [self getEndOfRouteRangeFromLinkUrl:linkUrl];
    if (range.location == NSNotFound)
        return @"";
    
    NSString * routeString = [linkUrl substringToIndex:range.location];
    
    NSArray * components = [routeString componentsSeparatedByString:@"/"];
    return [StringUtils isEmptyOrNull:[components lastObject]]? @"":[components lastObject];
}

- (NSString *)getParamStringFromLinkUrl:(NSString *)linkUrl {
    NSRange range = [self getEndOfRouteRangeFromLinkUrl:linkUrl];
    if (range.location == NSNotFound)
        return @"";
    
    return [linkUrl substringWithRange:NSMakeRange(range.location + range.length, linkUrl.length - range.location - range.length)];
}

- (NSRange)getEndOfRouteRangeFromLinkUrl:(NSString *)linkUrl {
    NSRange range = [linkUrl rangeOfString:@"/?"];
    if (range.location == NSNotFound)
        range = [linkUrl rangeOfString:@"?"];
    if (range.location == NSNotFound)
        range = NSMakeRange(0, 0);
    return range;
}

#pragma mark -

- (void)onQuestionRecieved
{
    if (_applicationState == UIApplicationStateActive)
    {
        [self showInAppNotificationWithMessage:[_payload objectForKey:KEY_API_MESSAGE] withOnClickAction:^{
            [self showRecievedQuestion:[_payload objectForKey:KEY_QUESTION_ID]];
        }];
        
        return;
    }
    
    [self showRecievedQuestion:[_payload objectForKey:KEY_QUESTION_ID]];
}

- (void)onAnswerRecieved
{
    if (_applicationState == UIApplicationStateActive)
    {
        [self showInAppNotificationWithMessage:[_payload objectForKey:KEY_API_MESSAGE] withOnClickAction:^{
             [self showRecievedAnswerForQuestion:[_payload objectForKey:KEY_QUESTION_ID]];
        }];
        
        return;
    }
    
    [self showRecievedAnswerForQuestion:[_payload objectForKey:KEY_QUESTION_ID]];
}

- (void)onNewMessageRecieved
{    
    NSString *conversationId = [_payload objectForKey:KEY_CHAT_ID];
    
    if (_applicationState == UIApplicationStateActive && [conversationId isEqualToString:[APP_DELEGATE activeConversationId]]) //TODO: Extract this in New Method
    {
        [self showNewMessageRecivedForConversation:YES];
     
        return;
    }
    
    if (_applicationState == UIApplicationStateInactive)
    {
        [self showNewMessageRecivedForConversation:NO];
        
        return;
    }
    
    [self showInAppNotificationWithMessage:[_payload objectForKey:KEY_API_MESSAGE] withOnClickAction:^{
        [self showNewMessageRecivedForConversation:NO];
    }];
}

- (void)onBeginAndEndChat
{
    NSString *chatId = [_payload objectForKey:KEY_CHAT_ID];
    
    if (!chatId)
    {
        return;
    }
    
    if (_applicationState == UIApplicationStateActive)
    {
        [self showInAppNotificationWithMessage:[_payload objectForKey:KEY_API_MESSAGE] withOnClickAction:^{
            [self showChatWithId:chatId];
        }];
        
        return;
    }
    
    [self showChatWithId:chatId];
}

- (void)onSubscriptionEnded
{
    if (_applicationState == UIApplicationStateActive && [APP_DELEGATE isShowingHome])
    {
        [self showSubscriptionEnded];
        
        return;
    }
    
    if (_applicationState == UIApplicationStateActive)
    {
        [self showInAppNotificationWithMessage:[_payload objectForKey:KEY_API_MESSAGE] withOnClickAction:^{
            [self showSubscriptionEnded];
        }];
        
        return;
    }
    
    [self showSubscriptionEnded];
}

#pragma mark - Push Notification Navigation

- (void)showNewMessageRecivedForConversation:(BOOL)isConversationActive
{
    //Create Message Entity from Payload
    ChatMessage *chatMessage = [ChatMessage new];
    [chatMessage setForNotificationPayload:_payload];

    if (isConversationActive)
    {
        [self markMessageUnreadForChatId:[chatMessage conversationId]];
    }
    
    //Call To Delegate Method
    if (_delegate && [_delegate respondsToSelector:@selector(recievedNewMessage:isChatActive:)])
    {
        [_delegate recievedNewMessage:chatMessage isChatActive:isConversationActive];
    }
}

- (void)showChatWithId:(NSString *)chatId
{
    if (_delegate && [_delegate respondsToSelector:@selector(recievedNewChat:)])
    {
        [_delegate recievedNewChat:chatId];
    }
}

- (void)showRecievedQuestion:(NSString *)questionId
{
    [UserDefaults setUserRole:UserRoleAdvisor];
    
    if ([StringUtils isEmptyOrNull:questionId])
    {
        return;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(recievedQuestionWithId:)])
    {
        [_delegate recievedQuestionWithId:questionId];
    }
}

- (void)showRecievedAnswerForQuestion:(NSString *)questionId
{
    [UserDefaults setUserRole:UserRoleSeeker];
    
    if ([StringUtils isEmptyOrNull:questionId])
    {
        return;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(recievedAnswerForQuestion:)])
    {
        [_delegate recievedAnswerForQuestion:questionId];
    }
}

- (void)showSubscriptionEnded
{
    if (_delegate && [_delegate respondsToSelector:@selector(recievedEndSubscription)])
    {
        [_delegate recievedEndSubscription];
    }
}

- (void)showResetPassword:(NSString *)token
{
    [APP_DELEGATE proceedToResetPassword:token];
}

- (void)showEmailNotificationSettings:(NSString *)token
{
    [APP_DELEGATE proceedToEmailNotificationSettings:token];
}

#pragma mark - In App Notification

- (void)showInAppNotificationWithMessage:(NSString *)message withOnClickAction:(void (^)())clickAction
{
    [NotificationPanel showInController:[[APP_DELEGATE navigationController] topViewController] message:message withOnClickAction:clickAction];
}


#pragma mark - Service Calls

- (void)getUserStats
{
    BaseController *topController = [self getTopController];
    
    if (topController)
    {
        [topController getUserStats];
    }
}

- (void)getUserProfile
{
    BaseController *topController = [self getTopController];
    
    if (topController)
    {
        [topController getUserProfile];
    }
}

- (void)trackEventWithParams:(NSString *)paramString {
    BaseController *topController = [self getTopController];
    
    if (topController)
    {
        [topController trackEvent:paramString];
    }
}

#pragma mark - Socket Event 

- (void)markMessageUnreadForChatId:(NSString *)chatId
{
    [[SocketIOManager instance] markMessageUnreadOnConversation:chatId];
}

- (BaseController *)getTopController
{
    UINavigationController *navigationController = [APP_DELEGATE navigationController];
    BaseController *topController = (BaseController *)[navigationController topViewController];
    
    if ([topController isKindOfClass:[MFSideMenuContainerViewController class]])
    {
        topController = (BaseController *) [(UINavigationController *)[(MFSideMenuContainerViewController *)topController centerViewController] topViewController];
    }
    
    return topController;
}

@end
