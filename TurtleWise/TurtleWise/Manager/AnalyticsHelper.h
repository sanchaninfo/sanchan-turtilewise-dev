//
//  AnalyticsHelper.h
//  SnapAndStyle
//
//  Created by Waleed Khan on 9/29/14.
//  Copyright (c) 2014 sana. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GAITrackedViewController;

@interface AnalyticsHelper : NSObject

+ (void) startAnalyticsSession;
+ (void) endSession;
+ (void) logScreeen:(GAITrackedViewController *)controller;
+ (void) logScreeen:(GAITrackedViewController *)controller AndTitle:(NSString *)title;
+ (void) logEvent:(GAITrackedViewController *)controller category:(NSString *)category action:(NSString *)action;
+ (void) logEvent:(GAITrackedViewController *)controller category:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

@end
