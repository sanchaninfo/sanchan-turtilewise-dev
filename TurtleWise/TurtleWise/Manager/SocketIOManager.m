//
//  SocketIOManager.m
//  TurtleWise
//
//  Created by Waleed Khan on 4/25/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

@import SocketIO;

#import "EnvironmentConstants.h"
#import "SocketIOManager.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Keys.h"


/*#if DEVELOPMENT
 #import "TW_QA-Swift.h"
 
 #elif STAGING
 #import "TW_STG-Swift.h"
 
 #elif PRODUCTION
 #import "TW_PROD-Swift.h"
 
 #endif*/

@interface SocketIOManager ()

@property (strong, nonatomic) SocketIOClient *socket;

- (void)addHandlers;
- (void)pushNoficationEventRecieved:(NSArray *)data;

@end

@implementation SocketIOManager

#pragma mark - Life Cycle

+ (SocketIOManager *)instance;
{
    static SocketIOManager *sharedMyManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    
    return sharedMyManager;
}

#pragma mark - Connection Handling

- (void)connectScocket
{
    if (_socket)
    {
        [self disconnectScocket];
    }
    
    if (![UserDefaults isUserLogin])
    {
        return;
    }
    
    _socket = [[SocketIOClient alloc] initWithSocketURL:[NSURL URLWithString:SOCKET_SERVER_URL] config:NULL];
    
    [self addHandlers];
    
    [_socket connect];
}

- (void)disconnectScocket
{
    [_socket disconnect];
    [_socket removeAllHandlers];
    
    _socket = NULL;
}

#pragma mark - Helper Methods

- (void)addHandlers
{
    [_socket on:NSStringFromSocketEvent(SocketEventsConnected) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         NSLog(@"Connected");
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsError) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         NSLog(@"Error");
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsRegister) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self registerSocket];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsDisconnect) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         NSLog(@"Disconnected");
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsQuestionRecieved) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsAnswerRecieved) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsRecievedNewMessage) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsRecievedChatInvitation) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsChatEnded) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsUpdateProfile) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
    
    [_socket on:NSStringFromSocketEvent(SocketEventsSubscriptionEnded) callback:^(NSArray *data, SocketAckEmitter *socketAckEmitter)
     {
         [self pushNoficationEventRecieved:data];
     }];
}

- (void)registerSocket
{
    [_socket emit:NSStringFromSocketEvent(SocketEventsRegister)
             with:[NSArray arrayWithObject:@{KEY_TOKEN : [UserDefaults getAccessToken]}]];
}

- (void)pushNoficationEventRecieved:(NSArray *)data
{
    if (!data)
    {
        return;
    }
    
    [APP_DELEGATE handleRoutesForNotificationWithPayload:[data objectAtIndex:0]];
}

- (void)markMessageUnreadOnConversation:(NSString *)conversationId
{
    
    [_socket emit:NSStringFromSocketEvent(SocketEventsReadMessage)
             with:@[@{KEY_CHAT_ID : conversationId}]];
    
}

@end
