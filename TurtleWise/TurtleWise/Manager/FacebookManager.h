//
//  FacebookManager.h
//  FlikShop
//
//  Created by Waaleed Khan on 4/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//


#import <Foundation/Foundation.h>

@class BaseController;

@interface FacebookManager : NSObject

+ (void)loginFromController:(BaseController *)controller WithSuccess:(void (^)(id response))success failure:(void (^)(NSError *error))failure;


@end
