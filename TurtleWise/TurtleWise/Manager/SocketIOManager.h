//
//  SocketIOManager.h
//  TurtleWise
//
//  Created by Waleed Khan on 4/25/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocketIOManager : NSObject

+ (SocketIOManager *)instance;

- (void)connectScocket;
- (void)disconnectScocket;
- (void)markMessageUnreadOnConversation:(NSString *)conversationId;

@end
