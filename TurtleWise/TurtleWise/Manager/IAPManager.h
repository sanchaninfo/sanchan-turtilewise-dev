//
//  IAPManager.h
//  TurtleWise
//
//  Created by Waleed Khan on 6/1/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "BaseEntity.h"

typedef void (^IAPProductsResponseBlock)(SKProductsRequest* request , SKProductsResponse* response);
typedef void (^IAPbuyProductCompleteResponseBlock)(SKPaymentTransaction* transcation);
typedef void (^checkReceiptCompleteResponseBlock)(NSString* response,NSError* error);
typedef void (^resoreProductsCompleteResponseBlock) (SKPaymentQueue* payment,NSError* error);

@interface IAPManager : NSObject < SKProductsRequestDelegate, SKPaymentTransactionObserver >

//Hold Products
@property (nonatomic,strong) NSArray * products;

//For Requests.
@property (nonatomic,strong) SKProductsRequest *request;
@property (nonatomic) BOOL production;

+ (IAPManager *)sharedInstance;

- (void)requestProductsWithCompletion:(IAPProductsResponseBlock)completion;
- (void)restoreProductsWithCompletion:(resoreProductsCompleteResponseBlock)completion;
- (void)checkReceipOnCompletion:(checkReceiptCompleteResponseBlock)completion;

//Buy Products
- (void)buyMonthlySubsciptionWithCompletion:(IAPbuyProductCompleteResponseBlock)completion;
- (void)buyOneQuestionSubsctiptionWithCompletion:(IAPbuyProductCompleteResponseBlock)completion;

//For Recipt
- (NSData *)getReceiptData;

@end
