//
//  NewGiveAnswerController.h
//  TurtleWise
//
//  Created by Ajdal on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
#import "Question.h"

@protocol NewGiveAnswerControllerProtocol <NSObject>

-(void)didSuccessfullyPostAnswer;
-(void)didSelectGiveAnswerControllerTabBarBtn:(TabbarViewType)tabViewType;

@end

@interface NewGiveAnswerController : BaseController


-(id)initWithQuestionId:(NSString *)questionId;
-(void)setQuestion:(Question*)question forReadOnly:(BOOL)readOnly;
-(void)addNextBtn;
-(void)postAnswer:(NSDictionary*)answer;
-(void)onTabbarBtnTap:(TabbarViewType)tabBarViewType;
-(void)openUserInfoController:(NSString*)seekerId;
-(void)showSubscriptionModal;
-(void)checkIfUserIsPremium;

@property(nonatomic,weak) id <NewGiveAnswerControllerProtocol> customDelegate;
@property(assign) BOOL readOnly;

@end
