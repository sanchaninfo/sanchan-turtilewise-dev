//
//  MCQOptionsController.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/2/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@protocol MCQOptionsControllerDelegate <NSObject>

@required
- (void)saveMCQChoices:(NSArray *)choices;

@end

@interface MCQOptionsController : BaseController

- (id)initWithDelegate:(id)delegate;
- (void)backToAskQuestionWithChoices:(NSArray *)choices;

@end
