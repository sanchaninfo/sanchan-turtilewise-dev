//
//  UserProfileController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserProfileController.h"
#import "EditProfileController.h"
#import "UserProfileView.h"
#import "UserDefaults.h"

#define TITLE @"Profile"
#define RIGHT_EDIT_BUTTON_TITLE @"Edit"

@interface UserProfileController ()

- (void)setupNavigationButtons;
- (void)getProfileSucuss:(id)response;

@property(nonatomic, strong) UserProfile *profile;
@property(assign) BOOL showedIncompleteProfileAlert;

@end

@implementation UserProfileController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:TITLE];
    
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeUser)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    
    [self getUserProfile];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupRightNavigationButtonWithName:RIGHT_EDIT_BUTTON_TITLE withOnClickAction:^{
        [self showEditProfileController];
    }];
    
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - Service Call

- (void)getUserProfile
{
    [self showLoader];

    [[service user] getProfileWthSuccess:^(id response)
    {
        [self getProfileSucuss:response];
    }
      andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Call Back Methods

- (void)getProfileSucuss:(id)response
{
    [self hideLoader];
    
    _profile = (UserProfile *)response;
    
    if([UserDefaults showAlertForIncompleteProfile:_profile] && !_showedIncompleteProfileAlert) {
        [Alert show:@"" andMessage:ALERT_INCOMPLETE_PROFILE];
        _showedIncompleteProfileAlert = YES;
    }
    [(UserProfileView *)self.view updateUserProfile:_profile];
}

#pragma mark - Navigation

- (void)showEditProfileController
{
    EditProfileController *editProfileController = [[EditProfileController alloc] initWithProfile:_profile];
    [[self navigationController] pushViewController:editProfileController animated:YES];
}

@end
