//
//  OrganisationAndInfluencerController.m
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "OrganisationAndInfluencerController.h"
#import "OrganisationAndInfluencerView.h"
#import "ManageBrandedPageController.h"
#import "Keys.h"
#import "UserDefaults.h"

#define TITLE @"View Organizations & Influencers"
@interface OrganisationAndInfluencerController ()

@end

@implementation OrganisationAndInfluencerController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:TITLE];
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setAppearenceForHome];
    [self getAllPages];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)loadParticularViewWithArray:(NSArray*)array {
    [(OrganisationAndInfluencerView *)self.view viewWithArray:array];
}

- (void)getAllPages {
    [self showLoader];
    
    [[service brandedPageService] getPageswithSuccess:^(id response) {
        [super hideLoader];
        [self loadParticularViewWithArray:response];
    }];
}

- (void) showManageBrandedPageController {
    ManageBrandedPageController *page = [[ManageBrandedPageController alloc] init];
    [[self navigationController] pushViewController:page animated:YES];
}

- (void) followWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] followPage:params withSuccess:^(id response) {
        [super hideLoader];
        [UserDefaults followPublic:pageId];
        [self getAllPages];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) unfollowWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOUNFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] unfollowPage:params withSuccess:^(id response) {
            [super hideLoader];
            [UserDefaults unfollow:pageId];
            [self getAllPages];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) followRequestWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] requestToFollowPage:params withSuccess:^(id response) {
        [super hideLoader];
        [UserDefaults followPrivate:pageId];
        [self getAllPages];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

@end
