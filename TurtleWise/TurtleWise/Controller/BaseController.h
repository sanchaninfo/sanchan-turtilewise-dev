//
//  BaseController.h
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "Service.h"
#import "GAITrackedViewController.h"

static NSString *string;


@interface BaseController : GAITrackedViewController
{
    Service *service;
}

@property(nonatomic) BOOL shouldPopController;

- (void)loadServices:(NSArray*)services;
- (void)showLoader;
- (void)showLoader:(NSString*)message;
- (void)hideLoader;
- (void)onServiceResponseFailure:(NSError*)error;
- (void)popToViewController:(NSString *)viewController;
- (void)popViewController;
- (void)setAppearenceForAdvisor;
- (void)setAppearenceForSeeker;
- (void)setAppearenceForHome;
- (void)setAppearenceForAddAdvisorWizard;
- (void)leftButtonAction:(id)sender;
- (void)rightButtonAction:(id)sender;
- (void)setupRightNavigationButtonWithName:(NSString*)name withOnClickAction:(NavBarButtonAction)onClickAction;
- (void)setupLeftNavigationButtonWithName:(NSString*)name withOnClickAction:(NavBarButtonAction)onClickAction;
- (void)setupLeftNavigationButtonWithImage:(NSString *)imageName withOnClickAction:(NavBarButtonAction)onClickAction;
- (void)setupRightNavigationButtonWithImage:(NSString *)imageName withOnClickAction:(NavBarButtonAction)onClickAction;
- (void)openHomeController;
- (void)serviceCallFailed:(NSError *)error;
- (void)userLogOut;
- (void)getUserStats;
- (void)getUserProfile;
- (void)trackEvent:(NSString *)paramString;
- (void) setInteractionPopGestureDelegate;

@end
