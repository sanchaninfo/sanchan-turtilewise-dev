//
//  SelectInfluencerOnboardingController.m
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectInfluencerOnboardingController.h"
#import "SelectInfluencerOnboardingView.h"
#import "TWWizardManager.h"
#import "UserProfile.h"
#import "StringUtils.h"
#import "SeekingAdvisor.h"
#import "BrandedPage.h"
#import "UserDefaults.h"

#define TITLE @"Edit Picture"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface SelectInfluencerOnboardingController ()

- (void)setTitle;
- (void)setupNavigationButtons;

@end

@implementation SelectInfluencerOnboardingController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle];
    [self setupNavigationButtons];
}

#pragma mark - Navigation

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO];
    pageManager = [BrandedPageManager sharedManager];
    [(SelectInfluencerOnboardingView *)self.view setBasicInformation:[pageManager brandedPage]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[self view] endEditing:YES];
}

- (void)setTitle
{

    
    [self setTitle:[NSString stringWithFormat:TITLE]];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self popViewController];
    }];
}

#pragma mark - Service Call backs

- (void)saveProfileSucessful:(id)response
{
    [self hideLoader];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)savePageDetail
{
    [self popViewController];
}


- (UIImage *)getSquareImage:(UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(249, 249), NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, 249, 249)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)uploadPageAvatar:(UIImage *)image type:(NSString *)pictureType {
    NSString * avatarString = [StringUtils encodeToBase64String:image];
    
    [super showLoader];
    if ([pictureType isEqualToString:@"avatar"]) {
    
        [[service user] savePageAvatar:avatarString success:^(id response) {
            [super hideLoader];
            [[pageManager brandedPage] setAvatar:image];
            [(SelectInfluencerOnboardingView *)self.view setBasicInformation:[pageManager brandedPage]];
        } andfailure:^(NSError *error) {
            [super serviceCallFailed:error];
        }];
    } else {
    
        [[service user] savePageCover:avatarString success:^(id response) {
            [super hideLoader];
            [[pageManager brandedPage] setCover:image];
            [(SelectInfluencerOnboardingView *)self.view setBasicInformation:[pageManager brandedPage]];
        } andfailure:^(NSError *error) {
            [super serviceCallFailed:error];
        }];
        
    }
   
}


@end
