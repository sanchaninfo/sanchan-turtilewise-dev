//
//  BoardOfAdvisorsViewController.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 12/11/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"


@interface BoardOfAdvisorsController : BaseController


-(void)getBoardofAdvisorsWith:(NSString*)status;


@end
