//
//  AddEventsController.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"
#import "BrandedPageManager.h"
@class TWWizardManager;
@interface AddEventsController : BaseController {
    BrandedPageManager *pageManager;
}
@property(nonatomic) int eventNo;

@property(nonatomic, strong) TWWizardManager *profileWizardManager;
- (void)savePageDetail;
@end
