//
//  TurtleswagController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TurtleSwagDetailsController.h"
#import "TurtleswagController.h"
#import "TurtleswagView.h"
#import "UserDefaults.h"
#import "Constant.h"
#import "Charity.h"

#define TITLE @"Redeem TurtleBucks"
#define IMAGE_AMAZON_GIFT_CARD @"https://s3.amazonaws.com/tw-data-shared/AGC.png"

@interface TurtleswagController ()

- (void)customizeNavigationBarForUserRole:(UserRole)userRole;
- (void)prepareData;

@end

@implementation TurtleswagController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customizeNavigationBarForUserRole:[UserDefaults getUserRole]];
    [self prepareData];
}

- (void)customizeNavigationBarForUserRole:(UserRole)userRole
{
    [self setTitle:TITLE];
    
    NSString *backButtonImage;
    
    if (userRole == UserRoleAdvisor)
    {
        backButtonImage = BACK_BUTTON_WHITE_IMAGE;
        
        [self setAppearenceForAdvisor];
    }
    
    else
    {
        backButtonImage = BACK_BUTTON_BLACK_IMAGE;
        [self setAppearenceForSeeker];
    }
    
    [self setupLeftNavigationButtonWithImage:backButtonImage withOnClickAction:^{
        [[self navigationController] popViewControllerAnimated:YES];
    }];
}

- (void)prepareData
{
    Charity *giftCard = [Charity new];
    [giftCard setTitle:TITLE_AMAZON_GIFT_CARD];
    [giftCard setImageUrl:IMAGE_AMAZON_GIFT_CARD];
    
    NSArray *amazonCardArray = [NSArray arrayWithObjects:giftCard, nil];
    
    [(TurtleswagView *)self.view populateCharityList:[UserDefaults getCharities] andGiftCard:amazonCardArray];
}

#pragma mark - Navigation

- (void)showTurtleSwagDetails:(Charity *)charity
{
    TurtleSwagDetailsController *detailsController = [[TurtleSwagDetailsController alloc] initWithDetails:charity];
    
    [[self navigationController] pushViewController:detailsController animated:YES];
}

@end
