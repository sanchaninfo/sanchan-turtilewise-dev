//
//  SelectPublicOrPrivateController.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectPublicOrPrivateController.h"
#import "SelectPublicOrPrivateView.h"
#import "TWWizardManager.h"
#import "SeekingAdvisor.h"
#import "UserProfile.h"
#import "BrandedPageManager.h"
#import "UserDefaults.h"

#define TITLE @"Step %ld"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface SelectPublicOrPrivateController ()

- (void)setTitle;
- (void)setupNavigationButtons;

@end

@implementation SelectPublicOrPrivateController

#pragma mark - UIViewController Life Cycle

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle];
    [self setupNavigationButtons];
    [self setAppearenceForAddAdvisorWizard];
}

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager
{
    if (self = [super init])
    {
        _profileWizardManager = profileWizardManager;
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO];
    [self setAppearenceForAddAdvisorWizard];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[self view] endEditing:YES];
    
    [(SelectPublicOrPrivateView *)self.view saveWizardData:[_profileWizardManager profile]];
}


- (void)setTitle
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [self setTitle:[NSString stringWithFormat:TITLE, (long)[_profileWizardManager currentStep]]];
    
    if([_profileWizardManager isLastStep] && ![[_profileWizardManager profile] isMemberOfClass:[SeekingAdvisor class]])
        [self setTitle:TITLE_REVIEW_PROFILE];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self previous];
    }];
    
    if (![_profileWizardManager isLastStep])
    {
        [self setupRightNavigationButtonWithImage:NEXT_BUTTON_WHITE_IMAGE withOnClickAction:^{
            [self next];
        }];
    }
}

#pragma mark - Navigation

- (void)next
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [_profileWizardManager next];
}

- (void)previous
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [_profileWizardManager previous];
}

- (void)completeWizard
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    if ([_profileWizardManager shouldSaveWizardOnDone])
    {
        [self saveProfile];
        
        return;
    }
    
    [_profileWizardManager finish];
}

#pragma mark - Service Calls

- (void)saveProfile
{
    
    [[service user] saveProfile:[(UserProfile *)[_profileWizardManager profile] getAsDictionary] withSuccess:^(id response)
     {
         [self saveProfileSucessful:response];
         
     } andfailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

#pragma mark - Service Call backs

- (void)saveProfileSucessful:(id)response
{
    [self hideLoader];
}
@end
