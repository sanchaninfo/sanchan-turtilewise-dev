//
//  UserBasicInfoController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserBasicInfoController.h"
#import "TWWizardManager.h"
#import "UserProfile.h"
#import "StringUtils.h"

@interface UserBasicInfoController ()

@end

@implementation UserBasicInfoController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (UIImage *)getSquareImage:(UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(249, 249), NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, 249, 249)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)updateProfilePhoto:(UIImage *)image {
    NSString * avatarString = [StringUtils encodeToBase64String:image];
    
    [super showLoader];
    [[service user] saveAvatar:avatarString success:^(id response) {
        [super hideLoader];
    } andfailure:^(NSError *error) {
        [super serviceCallFailed:error];
    }];
}

@end
