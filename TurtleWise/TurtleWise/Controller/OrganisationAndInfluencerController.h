//
//  OrganisationAndInfluencerController.h
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface OrganisationAndInfluencerController : BaseController
- (void) showManageBrandedPageController;
- (void) followWithPageId:(NSString*)pageId;

- (void) unfollowWithPageId:(NSString*)pageId;

- (void) followRequestWithPageId:(NSString*)pageId;
@end
