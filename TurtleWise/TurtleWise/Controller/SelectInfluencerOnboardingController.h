//
//  SelectInfluencerOnboardingController.h
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//


//#import "BaseProfileWizardController.h"
#import "BaseController.h"
#import "BrandedPageManager.h"
//@class TWWizardManager;

@interface SelectInfluencerOnboardingController : BaseController {
    BrandedPageManager *pageManager;
}

//@property(nonatomic, strong) TWWizardManager *profileWizardManager;

//- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager;
- (UIImage *)getSquareImage:(UIImage *)image;
- (void)uploadPageAvatar:(UIImage *)image type:(NSString *)pictureType;
- (void)savePageDetail;

@end
