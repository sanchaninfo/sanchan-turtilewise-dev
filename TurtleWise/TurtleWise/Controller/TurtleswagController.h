//
//  TurtleswagController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class Charity;

@interface TurtleswagController : BaseController

- (void)showTurtleSwagDetails:(Charity *)charity;

@end
