//
//  SubmitQuestionController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class SeekingAdvisor;

@interface SubmitQuestionController : BaseController

- (id)initWithSeekingAdvisor:(SeekingAdvisor *)seekingAdvisor;
- (void)askQuestion;
- (void)getAdvisorCountForAskQuestion;

@end
