//
//  RatingAdvisorsController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "RatingAdvisorsController.h"
#import "RatingAdvisorsView.h"
#import "SurveyController.h"
#import "MenuController.h"
#import "Conversation.h"
#import "MFSideMenu.h"
#import "Constant.h"

#define TITLE @"Rate Chat"

#define MESSAGE_RATING_VALIDATION @"Please rate all the Guru(s)."

@interface RatingAdvisorsController ()

@property(nonatomic, strong) NSArray *advisors;
@property(nonatomic, strong) NSString *chatId;

- (void)setupNavigationButtons;
- (void)submitAdvisorsRating;
- (void)goBackToDashboard;

@end

@implementation RatingAdvisorsController

#pragma mark - Life Cycle Methods

- (id)initWithConversation:(Conversation *)conversation
{
    if (self = [super init])
    {
        _advisors = [conversation advisors];
        _chatId = [conversation conversationId];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:TITLE];
    [self setupNavigationButtons];
    [self loadServices:@[@(ServiceTypeChat)]];
    
    [(RatingAdvisorsView *)self.view populateAdvisorsList:_advisors];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
}

#pragma mark - Public Private Methods

- (void)setupNavigationButtons
{    
    [self setupRightNavigationButtonWithName:SUBMIT_BUTTON_TITLE withOnClickAction:^{
        [self submitAdvisorsRating];
    }];
}

#pragma mark - Service Calls

- (void)submitAdvisorsRating
{
    NSArray *advisorRating = [(RatingAdvisorsView *)self.view advisorsRating];
    
    if ([advisorRating count] < [_advisors count])
    {
        [Alert show:ERROR_ALERT_TITLE andMessage:MESSAGE_RATING_VALIDATION];
        
        return;
    }
    
    [self showLoader];
    
    [[service chat] rateAdvisors:advisorRating onChat:_chatId withSuccess:^(id response)
    {
        [self goBackToDashboard];
        
    } andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Navigation

- (void)goBackToDashboard
{
    [self hideLoader];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

@end
