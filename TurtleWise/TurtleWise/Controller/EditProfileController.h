//
//  EditProfileController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@class UserProfile;
@class TWWizardManager;

@interface EditProfileController : BaseController

@property(nonatomic, strong) TWWizardManager *profileWizardManager;

- (id)initWithProfile:(UserProfile *)profile;
- (void)startProfileWizardFromStep:(NSInteger)startStep;
- (void)updatePrivateAttributes:(NSMutableSet *)privateAttributes andOverallPrivacy:(BOOL)isPublic;

@end
