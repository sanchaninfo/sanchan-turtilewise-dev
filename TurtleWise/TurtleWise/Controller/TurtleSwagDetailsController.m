//
//  TurtleSwagDetailsController.m
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TurtleSwagDetailsController.h"
#import "TurtleSwagDetailsView.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "Charity.h"

#define TITLE @"Redeem TurtleBucks"

//Keys
#define KEY_CHARITY_TITLE @"charityTitle"

@interface TurtleSwagDetailsController ()

@property(nonatomic, strong) Charity *charityDetails;

@end
@implementation TurtleSwagDetailsController

#pragma mark - Life Cycle Methods

- (id)initWithDetails:(Charity *)charity
{
    if (self = [super init])
    {
        _charityDetails = charity;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customizeNavigationBarForUserRole:[UserDefaults getUserRole]];
    [self loadServices:@[@(ServiceTypeTurtleSwag)]];
    
    [(TurtleSwagDetailsView *)self.view populateDetails:_charityDetails];
}


- (void)customizeNavigationBarForUserRole:(UserRole)userRole
{
    [self setTitle:TITLE];
    
    NSString *backButtonImage;
    
    if (userRole == UserRoleAdvisor)
    {
        backButtonImage = BACK_BUTTON_WHITE_IMAGE;
        
        [self setAppearenceForAdvisor];
    }
    
    else
    {
        backButtonImage = BACK_BUTTON_BLACK_IMAGE;
        [self setAppearenceForSeeker];
    }
    
    [self setupLeftNavigationButtonWithImage:backButtonImage withOnClickAction:^{
        [[self navigationController] popViewControllerAnimated:YES];
    }];
}

#pragma mark - Service Calls

- (void)redeemAmount:(NSInteger)amount
{
    [self showLoader];
    
    [[service turtleSwag] redeemAmount:amount withSuccess:^(id response)
    {
        [self hideLoader];
        [(TurtleSwagDetailsView *)self.view redeemSucessfull];
        
    } andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

- (void)donateAmount:(NSInteger)amount
{
    [self showLoader];
    
    [[service turtleSwag] donateAmount:@{KEY_AMOUNT : [NSNumber numberWithInteger:amount], KEY_CHARITY_TITLE : [_charityDetails title]} withSuccess:^(id response)
    {
        [self hideLoader];
        [(TurtleSwagDetailsView *)self.view redeemSucessfull];
        
    } andfailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

#pragma mark - Navigation

- (void)goToHome
{
    [APP_DELEGATE proceedToHome];
}

@end
