//
//  LandingController.m
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "LandingController.h"
#import "LandingVideoController.h"

@interface LandingController ()

@end

@implementation LandingController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void) onNavigationToLandingVideoController {
    [self.navigationController pushViewController:[LandingVideoController new] animated:YES];
}

@end
