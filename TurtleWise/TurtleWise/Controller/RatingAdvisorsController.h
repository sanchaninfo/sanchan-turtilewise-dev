//
//  RatingAdvisorsController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class Conversation;

@interface RatingAdvisorsController : BaseController

- (id)initWithConversation:(Conversation *)conversation;

@end
