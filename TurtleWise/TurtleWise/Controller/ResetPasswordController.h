//
//  ResetPasswordController.h
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface ResetPasswordController : BaseController

@property (nonatomic) NSString * resetPasswordToken;

- (void)forgotResetPassword:(NSString *)newPassword;
@end
