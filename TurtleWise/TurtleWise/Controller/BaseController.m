//
//  BaseController.m
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "SubmitQuestionController.h"
#import "LandingController.h"
#import "EnvironmentConstants.h"
#import "SocketIOManager.h"
#import "AnalyticsHelper.h"
#import "BaseController.h"
#import "MenuController.h"
#import "MBProgressHUD.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "BaseView.h"
#import "Service.h"
#import "Alert.h"
#import "Font.h"

#define IMAGE_NAV_BAR_BG_BLACK @"nav-bar-black"
#define IMAGE_NAV_BAR_BG_GREEN @"nav-bar-green"
#define IMAGE_NAV_BAR_BG_YELLOW @"nav-bar-yellow"
#define NO_NAV_BAR_IMAGE nil

@interface BaseController ()<UIAlertViewDelegate, UIGestureRecognizerDelegate>

@property(nonatomic, strong)  NavBarButtonAction rightButtonBlock;
@property(nonatomic, strong)  NavBarButtonAction leftButtonBlock;

- (void)customizeNavigationBarWith:(NSString *)barBgImage;
- (void)setTextColorForNavBarButtons:(UIColor *)textColor;

@end

@implementation BaseController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setScreenName:@""];
    
    [self loadView:[self getViewName]];
    
    [(BaseView *)self.view setNavigationController:self.navigationController];
    [(BaseView *)self.view  viewDidLoad];
    
    [[self navigationItem] setHidesBackButton:YES];
    [self loadServices:@[@(ServiceTypeStats), @(ServiceTypeUser), @(ServiceTypeTrackEvent)]];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _shouldPopController = YES;
    
    [(BaseView *)self.view viewWillAppear];
    
    if ([(BaseView *)[self view] hasTabBar])
    {
        [self getUserStats];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [AnalyticsHelper logScreeen:self];
    [super viewDidAppear:animated];
    
    [(BaseView *)self.view viewDidAppear];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [(BaseView *)self.view viewWillDisappear];
    
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [(BaseView *)self.view viewDidDisappear];
}

-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [(BaseView *)self.view viewDidLayoutSubviews];
}

-(void)throwExceptioin:(NSString*)message
{
    NSLog(@"\n%@\n",message);
    @throw message;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadServices:(NSArray*)services
{
    if(service == nil)
        service = [Service get:self];
    
    [service load:services];
}

-(void)onServiceResponseFailure:(NSError*)error
{
    [self hideLoader];
    [Alert show:@"" andMessage:error.localizedDescription];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL) shouldAutorotate {
    return NO;
}

-(void)showLoader
{
    [MBProgressHUD showHUDAddedTo:[AppDelegate getInstance].window animated:YES];
}

-(void)showLoader:(NSString*)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[AppDelegate getInstance].window animated:YES];
    hud.labelText = message;
}

-(void)hideLoader
{
    [MBProgressHUD hideHUDForView:[AppDelegate getInstance].window animated:YES];
}

-(void)popToViewController:(NSString *)viewController
{
    BaseController *controller = (BaseController *) NSClassFromString(viewController);

    for (UIViewController *viewController in [[self navigationController] viewControllers]) {
        if ([viewController isKindOfClass:[controller class]] ) {
            [self.navigationController popToViewController:viewController animated:YES];
        }
    }
}

-(void) popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UINavigation Bar Customization

- (void)customizeNavigationBarWith:(NSString *)barBgImage
{
    UIImage *bgImage = [UIImage imageNamed:barBgImage];
    
    [[[self navigationController] navigationBar] setBackgroundImage:bgImage forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setBackgroundImage:bgImage forBarMetrics:UIBarMetricsDefault];
}

- (void)setTextColorForNavBarButtons:(UIColor *)textColor
{
    NSDictionary *titleAttributes = @{
                                      NSForegroundColorAttributeName : textColor,
                                      NSFontAttributeName            : [Font boldFontWithSize:17]
                                      };
    [[[self navigationController] navigationBar] setTitleTextAttributes:titleAttributes];
    [[[self navigationController] navigationBar] setTintColor:textColor];
    
    [[[self navigationItem] rightBarButtonItem] setTitleTextAttributes:@{
                                                                         //NSFontAttributeName            : [Font navButtonFont],
                                                                         NSForegroundColorAttributeName : textColor
                                                                        }
                                                              forState:UIControlStateNormal];
    
    [[UINavigationBar appearance] setTitleTextAttributes:titleAttributes];
    [[UINavigationBar appearance] setTintColor:textColor];
}


- (void)setAppearenceForAdvisor
{
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [[self navigationController] setNavigationBarHidden:NO];
    [self customizeNavigationBarWith:IMAGE_NAV_BAR_BG_GREEN];
    [self setTextColorForNavBarButtons:[UIColor whiteColor]];
}


- (void)setAppearenceForSeeker
{
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [[self navigationController] setNavigationBarHidden:NO];
    [self customizeNavigationBarWith:IMAGE_NAV_BAR_BG_YELLOW];
    [self setTextColorForNavBarButtons:[UIColor blackColor]];
}


- (void)setAppearenceForAddAdvisorWizard
{
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [self customizeNavigationBarWith:IMAGE_NAV_BAR_BG_BLACK];
    [self setTextColorForNavBarButtons:[UIColor whiteColor]];
}

- (void)setAppearenceForHome
{
    [self customizeNavigationBarWith:NO_NAV_BAR_IMAGE];
    
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] navigationBar] setBarTintColor:[UIColor blackColor]];
    [self setTextColorForNavBarButtons:[UIColor whiteColor]];
    
    MenuController *menuController = (MenuController *)[[self menuContainerViewController] rightMenuViewController];
    [menuController handleViewStatusBar:self];
}


-(void)setupRightNavigationButtonWithName:(NSString*)name withOnClickAction:(NavBarButtonAction)onClickAction
{
    _rightButtonBlock = onClickAction;
    
    UIBarButtonItem *rigthBarButton = [[UIBarButtonItem alloc] initWithTitle:name
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(rightButtonAction:)];
    
    [rigthBarButton setTitlePositionAdjustment:UIOffsetMake(-10, 0) forBarMetrics:UIBarMetricsDefault];
    
    [[self navigationItem] setRightBarButtonItem:rigthBarButton];
}

-(void)setupLeftNavigationButtonWithName:(NSString*)name withOnClickAction:(NavBarButtonAction)onClickAction
{
    _leftButtonBlock = onClickAction;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithTitle:name
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                     action:@selector(leftButtonAction:)];
    
    [leftBarButton setTitlePositionAdjustment:UIOffsetMake(-10, 0) forBarMetrics:UIBarMetricsDefault];
    [leftBarButton setTitleTextAttributes:@{
                                             NSForegroundColorAttributeName:[UIColor whiteColor],
                                             }
                                  forState:UIControlStateNormal];
    
    [[self navigationItem] setLeftBarButtonItem:leftBarButton];
    
    
}

- (void)setupLeftNavigationButtonWithImage:(NSString *)imageName withOnClickAction:(NavBarButtonAction)onClickAction
{
    UIImage *buttonImage = [UIImage imageNamed:imageName];
    
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:buttonImage forState:UIControlStateNormal];
    [aButton setFrame:CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(leftButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[self navigationItem] setLeftBarButtonItem:backButton];
    
    _leftButtonBlock = onClickAction;
}

- (void)setupRightNavigationButtonWithImage:(NSString *)imageName withOnClickAction:(NavBarButtonAction)onClickAction
{
    UIImage *buttonImage = [UIImage imageNamed:imageName];
    
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:buttonImage forState:UIControlStateNormal];
    [aButton setFrame:CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(rightButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[self navigationItem] setRightBarButtonItem:backButton];
    
    _rightButtonBlock = onClickAction;
}

#pragma mark - Navigation

-(void) setInteractionPopGestureDelegate {
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void)openHomeController
{
    [self.navigationController setViewControllers:@[[APP_DELEGATE getHomeControllerWithSlideMenu]] animated:YES];
}

- (void)userLogOut
{
    [[SocketIOManager instance] disconnectScocket];
    
    [UserDefaults clearDefaults];
    
    [[self navigationController] setViewControllers:@[[LandingController new]] animated:YES];
}

#pragma mark - Actions Nav Bar Buttons

- (void)rightButtonAction:(id)sender
{
    UIBarButtonItem *item = (UIBarButtonItem *)sender;
    
    [item setEnabled:NO];
    
    if (_rightButtonBlock)
    {
        _shouldPopController = YES;
        
        [(BaseView *)[self view] resignFieldsOnView];
        
        _rightButtonBlock();
    }
    
    [self performSelector:@selector(enableRightNavigationBarButtons:) withObject:item afterDelay:0.5];
}

- (void)leftButtonAction:(id)sender
{
    UIBarButtonItem *item = (UIBarButtonItem *)sender;
    
    [item setEnabled:NO];
    
    if (_leftButtonBlock)
    {
        _shouldPopController = NO;
        
        [(BaseView *)[self view] resignFieldsOnView];
        
        _leftButtonBlock();
    }
    
    [self performSelector:@selector(enableLeftNavigationBarButtons:) withObject:item afterDelay:0.5];
}

- (void)enableRightNavigationBarButtons:(UIBarButtonItem *)sender
{
    [sender setEnabled:YES];
}

- (void)enableLeftNavigationBarButtons:(UIBarButtonItem *)sender
{
    [sender setEnabled:YES];
}


#pragma mark - load view

-(NSString*)getViewName
{
    NSString *file = [self.class description];
    if(![file hasSuffix:@"Controller"])
        [self throwExceptioin:@"Invalid class name. Name should end with string 'controller' (e.g. SampleController)"];
    
    return [file stringByReplacingOccurrencesOfString:@"Controller" withString:@"View"];
}

-(void)loadView:(NSString*)nib
{
    if([[NSBundle mainBundle] pathForResource:nib ofType:@"nib"] == nil){
        [self throwExceptioin:[NSString stringWithFormat:@"%@ nib/class not found in project.",nib]];
    }
    
    UINib *nibs    = [UINib nibWithNibName:nib bundle:nil];
    NSArray *array = [nibs instantiateWithOwner:nil options:nil];
    if(array.count == 0){
        [self throwExceptioin:[NSString stringWithFormat:@"%@ nib doesn't have any view (IB error)",nib]];
    }
    
    if(![[array objectAtIndex:0] isKindOfClass:[BaseView class]]){
        [self throwExceptioin:[NSString stringWithFormat:@"%@ nib should be subclass of %@ -> BaseView (IB error).",nib,nib]];
    }
    
    BaseView *view  = (BaseView*)[array objectAtIndex:0];
    view.controller = self;
    self.view       = view;
}

#pragma mark Helper Methods

- (void)serviceCallFailed:(NSError *)error
{
    [self hideLoader];
    
    if ([error code] == STATUS_CODE_FAILED_AUTH)
    {
        [Alert show:@"Session Expired!" andMessage:@"Please login to continue." andDelegate:self];
        
        return;
    }
    
    [Alert show:error];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self userLogOut];
}

#pragma mark - Gesture Recognizer Delegate

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
    
}

#pragma mark - Service Calls

- (void)getUserStats
{
    [[service stats] getStatsWithSuccess:^(id response)
     {
         [self getUserStatsSucessful:response];
         
     } andfailure:^(NSError *error)
     {
     }];
}

- (void)getUserStatsSucessful:(UserStats *)response
{
    [UserDefaults setUserStats:response];
    
    [(BaseView *)self.view updateStatsOnTabBar:response];
}

- (void)getUserProfile
{
    [[service user] getProfileWthSuccess:^(UserProfile *userProfile)
     {
         [UserDefaults setUserProfile:userProfile];
         NSLog(@"got Profile");
         
     } andfailure:^(NSError *error)
     {
     }];
}

- (void)trackEvent:(NSString *)paramString
{
    [[service trackEvent] trackEvent:paramString];
}

@end
