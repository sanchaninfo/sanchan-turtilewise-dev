//
//  HomeController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/4/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//
@import AirshipKit;
#import "QuestionListingController.h"
#import "AskQuestionController.h"
#import "AskQuestionOneController.h"
#import "SubscribeController.h"
#import "ChattingController.h"
#import "ConfigResponse.h"
#import "HomeController.h"
#import "MenuController.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "UserProfile.h"
#import "UserStats.h"
#import "HomeView.h"

#define LOGO_IMAGE_NAME @"home-app-logo"

@interface HomeController () < UIAlertViewDelegate, SubscribeControllerDelegate >

- (void)addAppLogo;
- (void)openSideMenu;
- (void)getUserProfile;
- (void)getProfileSuccessful:(UserProfile *)response;

@end

@implementation HomeController

#pragma mark - Overide Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addAppLogo];
    [self sendDeviceDetails];
    [[self navigationController] setNavigationBarHidden:NO];
    
    [self setupRightNavigationButtonWithImage:HAMBURGER_BUTTON_IMAGE_WHITE withOnClickAction:^{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
    }];
    
    [super loadServices:@[@(ServiceTypeUser), @(ServiceTypeChat)]];
    
    [self getLevelTable];
}

-(void)sendDeviceDetails{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://go.urbanairship.com/api/named_users/associate"]];
    urlRequest.HTTPMethod = @"POST";
    NSLog(@"push token: %@",[UserDefaults getPushToken]);
    NSLog(@"channel Id: %@",[UAirship push].channelID);
    NSLog(@"User Id: %@",[UserDefaults getUserID]);
    NSData *data = [NSJSONSerialization dataWithJSONObject:@{@"channel_id": [UAirship push].channelID,@"device_type": @"ios",
                                                             @"named_user_id": [UserDefaults getUserID]} options:NSJSONWritingPrettyPrinted error:nil];
    
    urlRequest.HTTPBody = data;
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/vnd.urbanairship+json; version=3" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:[NSString stringWithFormat:@"Basic RS00c2lrQjZTQU9ET0ZhYnhIYTJCdzppdm1ybUhTU1JXdXZDWkQ1MEM0QWZn"] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error == nil)
        {
            NSLog(@"%@",response.description);
        }
        else
        {
            NSLog(@"%@",error.localizedDescription);
        }
    }];
    [task resume];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    [self getUserProfile];
    
    [APP_DELEGATE setIsShowingHome:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [APP_DELEGATE setIsShowingHome:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)addAppLogo
{
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 161, 44)];
    
    UIImageView *appLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 161, 32)];
    [appLogo setImage:[UIImage imageNamed:LOGO_IMAGE_NAME]];
    [containerView addSubview:appLogo];
    
    UIBarButtonItem *appLogoItem =[[UIBarButtonItem alloc] initWithCustomView:containerView];
    
    [self.navigationItem setLeftBarButtonItem:appLogoItem];
}

-(void)getLevelTable{
    [service.user getLevelTableOnSuccess:^(ConfigResponse *configResponse)
    {
        [UserDefaults setLevelTable:[configResponse levelsTable]];
        [UserDefaults setCharities:[configResponse charities]];
        [UserDefaults setLevelBucks:[configResponse levelBucks]];
        [UserDefaults setLevelAmount:[configResponse levelAmount]];
        
    } andfailure:^(NSError *error)
    {
        //: Nothing to Do.
    }];
}

- (void)updateUserRole
{
    [(HomeView *)[self view] updateViewForUserRoleChange:[UserDefaults getUserRole]];
}

- (void)updateSlideMenu
{
    [(MenuController *) [[self menuContainerViewController] rightMenuViewController] updateMenu];
}

- (void)subsciptionEnded
{
    [APP_DELEGATE setShowCheckForSubscriptionExpiray:YES];
    
    UserProfile *userProfile = [UserDefaults getUserProfile];
    [userProfile setIsUserPremium:NO];
    
    [UserDefaults setUserProfile:userProfile];
    
    [(HomeView *)self.view showSubscriptionEndedAlert];
}

#pragma mark - Service Calls

- (void)getUserProfile
{
    [[service user] getProfileWthSuccess:^(id response)
     {
         [self getProfileSuccessful:response];
     }
    andfailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

#pragma mark - Service Callbacks

- (void)getProfileSuccessful:(UserProfile *)userProfile
{
    [UserDefaults setUserProfile:userProfile];
    [(HomeView *) self.view updateViewForUserProfile:userProfile];
    [(MenuController *) [[self menuContainerViewController] rightMenuViewController] updateBrandedpage];
    if([UserDefaults loginFlowType] == LoginFlowTypeEmail && [APP_DELEGATE homeControllerShouldShowEmailActivationAlert] && !userProfile.emailVerified)
    {
        _showingValidationAlert = YES;
        [Alert show:TITLE_ALERT_ACCOUNT_ACTIVATION andMessage:MESSAGE_ACCOUNT_ACTIVATION andDelegate:self];
        
        return;
    }
    
    if (_shouldShowChatAlert)
    {
        [(HomeView *)[self view] checkForUnratedChats];
    }
}

- (void)rateAllChatsThanks
{
    [[service chat] rateAllChatsWithSuccess:^(id response) {} andFailure:^(NSError *error) {}];
}

#pragma mark - Navigation

- (void)proceedToAskQuestion
{
    [[self navigationController] pushViewController:[AskQuestionOneController new] animated:YES];
}

- (void)showQuestionListingController
{
    QuestionListingController *controller = [[QuestionListingController alloc] initWithListType:TabbarViewTypeQuestion andUserRole:[UserDefaults getUserRole]];
    [[self navigationController] pushViewController:controller animated:YES];
}

- (void)showAnswerListingController
{
    QuestionListingController *controller = [[QuestionListingController alloc] initWithListType:TabbarViewTypeAnswer andUserRole:[UserDefaults getUserRole]];
    [[self navigationController] pushViewController:controller animated:YES];
}

- (void)showChatListingController
{
    QuestionListingController *controller = [[QuestionListingController alloc] initWithListType:TabbarViewTypeChat andUserRole:[UserDefaults getUserRole]];
    [[self navigationController] pushViewController:controller animated:YES];
}

- (void)askForUnratedChat:(long)unratedChatCount
{
    [self showChatListingController];
}

- (void)openSideMenu
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (void)switchToRole:(UserRole)userRole
{
    [UserDefaults setUserRole:userRole];
    [self updateUserRole];
    [self updateSlideMenu];
}

- (void)showSubscriptionModel
{
    SubscribeController *subscriptionController = [SubscribeController new];
    [subscriptionController hideSingleSubscriptionOption];
    
    [subscriptionController setDelegate:self];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:subscriptionController];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void) offerAdviseAfterChangingRole {
    [(HomeView *)[self view] updateViewForUserRoleChange:UserRoleAdvisor];
    [self showQuestionListingController];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([APP_DELEGATE homeControllerShouldShowEmailActivationAlert])
    {
        _showingValidationAlert = NO;
        [APP_DELEGATE setHomeControllerShouldShowEmailActivationAlert:NO];
        
        [(HomeView *)[self view] checkForUnratedChats];
        
        return;
    }
    
    [self userLogOut];
}

#pragma mark - Subscription Model Delegate Methods

- (void)didSubscribeSuccessfully
{
    [(HomeView *)self.view markUserPremium];
}

@end
