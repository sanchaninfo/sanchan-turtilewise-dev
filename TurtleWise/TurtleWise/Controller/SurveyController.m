//
//  SurveyController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 7/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TurtleswagController.h"
#import "SurveyController.h"

@interface SurveyController ()

@end

@implementation SurveyController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

- (void)goToTurtleSwags
{
    [[self navigationController] pushViewController:[TurtleswagController new] animated:YES];
}

@end
