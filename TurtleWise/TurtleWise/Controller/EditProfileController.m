//
//  EditProfileController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "EditProfileController.h"
#import "AboutMeController.h"
#import "AnalyticsHelper.h"
#import "TWWizardManager.h"
#import "EditProfileView.h"
#import "UserProfile.h"
#import "UserDefaults.h"
#import "BrandedPageManager.h"

#define TITLE @"Edit Profile"
#define RIGHT_SAVE_BUTTON @"Save"
#define WIZARD_STEP_FOR_ABOUT_ME -1

@interface EditProfileController ()

@property(nonatomic, strong) UserProfile *profile;
@property(nonatomic, assign) BOOL shouldUpdateProfile;

- (void)setupNavigationButtons;
- (void)saveProfile;
- (void)saveProfileSucessful;

@end

@implementation EditProfileController

#pragma mark - Life Cycle Methods

- (id)initWithProfile:(UserProfile *)profile
{
    if (self = [super init])
    {
        _profile = profile;
        _shouldUpdateProfile = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super loadServices:@[@(ServiceTypeUser)]];
    
    [self setTitle:TITLE];
    [self setupNavigationButtons];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    
    if (_shouldUpdateProfile)
    {
        _profile = (UserProfile *)[_profileWizardManager profile];
        _shouldUpdateProfile = NO;
    }
    
    [(EditProfileView *)self.view setProfile:_profile];
    
    [[self navigationController] setNavigationBarHidden:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

- (void)showAboutMeController
{
    AboutMeController *aboutMeController = [[AboutMeController alloc] initWithProfileWizardManager:_profileWizardManager];
    
    [aboutMeController setFlowType:AboutMeViewFlowTypeEditProfile];
    
    [[self navigationController] pushViewController:aboutMeController animated:YES];
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupRightNavigationButtonWithName:RIGHT_SAVE_BUTTON withOnClickAction:^{
        [self savePage];
    }];
    
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)updatePrivateAttributes:(NSMutableSet *)privateAttributes andOverallPrivacy:(BOOL)isPublic
{
    [_profile setPrivateAttributes:privateAttributes];
    [_profile updateProfilePrivacy:isPublic];
}

#pragma mark - Wizard handling

- (void)startProfileWizardFromStep:(NSInteger)startStep
{
    _profileWizardManager = [[TWWizardManager alloc] initWithPresentingController:self fromStep:startStep];
    [_profileWizardManager setProfile:_profile];
    
    _shouldUpdateProfile = YES;
    
    if (startStep == WIZARD_STEP_FOR_ABOUT_ME)
    {
        [self showAboutMeController];
        return;
    }
    
    [_profileWizardManager startProfileWizard];
}

#pragma mark - Service Calls

- (void)saveProfile
{
    [(EditProfileView *)self.view updatePrivacy];
    
    [[service user] saveProfile:[_profile getAsDictionary] withSuccess:^(id response)
     {
         [self saveProfileSucessful];
         
     } andfailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

- (void)savePage {
    [self showLoader];
    if ([_profileWizardManager createBrandedPage].count != 0 && [[_profile pages] count] == 0) {
        [[service user] savePage:[_profileWizardManager createBrandedPage] withSuccess:^(id response)
         {
             if ([UserDefaults getPageID].length != 0) {
                 [_profile setPages:[NSArray arrayWithObjects:[UserDefaults getPageID], nil]];
             }
             [self saveProfile];
             
         } andfailure:^(NSError *error)
         {
             [self serviceCallFailed:error];
         }];
    } else {
        [self saveProfile];
    }
}

#pragma mark - Service Call Back

- (void)saveProfileSucessful
{
    [self hideLoader];
    [self popViewController];
    
    [AnalyticsHelper logEvent:self category:CATEGORY_PROFILE action:EVENT_PROFILE_UPDATED];
}

@end
