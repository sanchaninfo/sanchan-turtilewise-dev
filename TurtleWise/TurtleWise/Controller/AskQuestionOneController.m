//
//  AskQuestionOneController.m
//  TurtleWise
//
//  Created by Sunflower on 9/4/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "AskQuestionOneController.h"
#import "AskQuestionOneView.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "AskQuestionController.h"

@interface AskQuestionOneController ()

@end

@implementation AskQuestionOneController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:ASK_SCREEN_TITLE];
    
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [self popViewController];
    }];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor blackColor]];
    [self getAllPages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setAppearenceForSeeker];
    
}

- (void)loadParticularViewWithArray:(NSArray*)array {
    [(AskQuestionOneView *) self.view viewWithArray:array];
}

- (void)getAllPages {
    [self showLoader];
    
    NSMutableArray *getList = [[[UserDefaults getUserProfile] following] mutableCopy];
    if ([[UserDefaults getPageID] length] > 0) {
        [getList addObject:[UserDefaults getPageID]];
    }
    
    [[service brandedPageService] getApprovedList:getList withSuccess:^(id response) {
        [super hideLoader];
        [self loadParticularViewWithArray:response];
    }];
}

- (void)startQuestionTextWithDictionary:(NSMutableDictionary*)dic;
{
    AskQuestionController *questionController = [[AskQuestionController alloc] initWithAnswereDictionary:dic];
    
    [self.navigationController pushViewController:questionController animated:YES];
}

@end
