//
//  NewGiveAnswerController.m
//  TurtleWise
//
//  Created by Ajdal on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewGiveAnswerController.h"
#import "NewUserInfoController.h"
#import "EnvironmentConstants.h"
#import "SubscribeController.h"
#import "NewGiveAnswerView.h"
#import "AnalyticsHelper.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "Constant.h"

#define ERROR_MESSAGE_QUESTION_EXPIRED @"Question has been Expired."
#define ERROR_MESSAGE_QUESTION_CLOSED @"Question has been Closed."
#define ERROR_MESSAGE_ALREADY_ANSWERED @"You have already answered this question."

@interface NewGiveAnswerController ()<SubscribeControllerDelegate>{
    Question *_question;
    NSString *_seekerName;
}

@property(nonatomic, strong) NSString *questionId;
@property(nonatomic, assign) BOOL openedThroughNotification;

- (void)getQuestionById;

@end

@implementation NewGiveAnswerController

- (id)initWithQuestionId:(NSString *)questionId
{
    if (self == [super init])
    {
        _questionId = questionId;
        _openedThroughNotification = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:MY_ANSWER_SCREEN_TITLE];
    [self addNextBtn];
    
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [(NewGiveAnswerView*)self.view onBackNavBtnTap];
    }];
    
    [super loadServices:@[@(ServiceTypeQuestion), @(ServiceTypeAnswer), @(ServiceTypeUser), @(ServiceTypeTrackEvent)]];
    
    [self getQuestionById];
    [super trackEvent:[NSString stringWithFormat:TRACK_EVENT_ANSWER_PAGE_OPENED, _questionId? _questionId:_question.questionId, [UserDefaults getUserID]]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForAdvisor];
    if(_openedThroughNotification)
        [self getQuestionDetails];
}

-(void)addNextBtn{
    
    [self setupRightNavigationButtonWithImage:NEXT_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [(NewGiveAnswerView*)self.view onNextNavBtnTap];
    }];
}
-(void)setQuestion:(Question *)question forReadOnly:(BOOL)readOnly{
    _question = question;
    _readOnly = readOnly;
    [(NewGiveAnswerView*)self.view setQuestion:question];
    
    if (readOnly){
        [self getAnswerWithQuestionId:question.questionId];
        return;
    }
    
    [self getQuestionDetails];
}

-(void)getQuestionDetails{
    [service.question getQuestionDetails:_questionId ? _questionId : _question.questionId onSuccess:^(NSString *seekerName) {
        _seekerName = seekerName;
    } andFailure:^(NSError *error) {
        NSLog(@"");
    }];
}

-(void)onTabbarBtnTap:(TabbarViewType)tabBarViewType{
    if(self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectGiveAnswerControllerTabBarBtn:)]){
        [self.customDelegate didSelectGiveAnswerControllerTabBarBtn:tabBarViewType];
    }
}
-(void)openUserInfoController:(NSString*)seekerId{
    NewUserInfoController *controller = [NewUserInfoController new];
    controller.advisorId = seekerId;
    controller.isSeeker = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showSubscriptionModal{
    SubscribeController *cont = [[SubscribeController alloc] initWithQuestion:_question];
    [cont setDelegate:self];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cont];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - Service Call
-(void)postAnswer:(NSDictionary*)answer {
    [super showLoader];
    [service.answer postAnswerForQuestion:_question.questionId andParams:answer onSuccess:^(id response) {
        [super hideLoader];
        
        [Alert showInController:self withTitle:@"Success!" message:[response get] onDismiss:^{
            if(self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSuccessfullyPostAnswer)]){
                [self.customDelegate didSuccessfullyPostAnswer];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [AnalyticsHelper logEvent:self category:CATEGORY_ANSWER action:EVENT_SUBMIT];
    } andFailure:^(NSError *error)
    {
        [super hideLoader];
        NSString * errorMessage = [NSString stringWithFormat:@"%@",[error localizedDescription]];
        [Alert showInController:self withTitle:TITLE_ALERT_ERROR message:errorMessage onDismiss:^{
            if (error.code == STATUS_CODE_FAILED_NOT_FOUND) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }];
}

- (void)getQuestionById
{
    [super showLoader];
    
    [[service question] getQuestionForId:_questionId ? _questionId : _question.questionId withSuccess:^(id response)
     {
         [self getQuestionByIdSucessfull:response];
     }
      andFailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

- (void)getAnswerWithQuestionId:(NSString *)questionId
{
    [(NewGiveAnswerView *)self.view hideQuestionTimer];
    
    [self showLoader];
    [service.question getAnswerWithQuestionId:questionId onSuccess:^(id response)
     {
         [self hideLoader];
         [(NewGiveAnswerView *)self.view setupUIForAdvisorAnswerView:response];
     }
      andFailure:^(NSError *error)
     {
         [super serviceCallFailed:error];
     }];
}

- (void)checkIfUserIsPremium
{
    [self showLoader];
    
    [[service user] getProfileWthSuccess:^(UserProfile *userProfile)
    {
        [self gotUserProfileSuccess:userProfile];
        
    } andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Callbacks

- (void)getQuestionByIdSucessfull:(Question *)question
{
    [super hideLoader];
    
    _question = question;
    
    if ([_question isExpired] && _openedThroughNotification)
    {
        [self showAlert:ERROR_MESSAGE_QUESTION_EXPIRED];
        return;
    }
    
    if ([_question status] == QuestionStatusClosed && _openedThroughNotification)
    {
        [self showAlert:ERROR_MESSAGE_QUESTION_CLOSED];
        return;
    }
    
    if (![_question canAnswer] && _openedThroughNotification)
    {
        [self showAlert:ERROR_MESSAGE_ALREADY_ANSWERED];
        return;
    }
    
    [(NewGiveAnswerView*)self.view setQuestion:_question];
}

- (void)gotUserProfileSuccess:(UserProfile *)userProfile
{
    [self hideLoader];
    
    //Save Updated Profile
    [UserDefaults setUserProfile:userProfile];
    
    if (![userProfile isUserPremium])
    {
        [self showSubscriptionModal];
        
        return;
    }
    
    [(NewGiveAnswerView*)self.view preparePostAnswerCall];
}

#pragma mark - SubscriptionController Delegate
-(void)didSubscribeSuccessfully{
    _question.isSubscribed = YES;
    [(NewGiveAnswerView*)self.view preparePostAnswerCall];
    
}

#pragma mark - Helper Methods

- (void)showAlert:(NSString *)message
{
    [Alert showInController:self withTitle:TITLE_ALERT_ERROR message:message onDismiss:^{
        [self popViewController];
    }];
}


@end
