//
//  ChatRequestController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/22/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"
@class Question;

@protocol ChatRequestControllerDelegate <NSObject>

-(void)didStartNewChat;

@end

@interface ChatRequestController : BaseController

@property(nonatomic,weak) id <ChatRequestControllerDelegate> delegate;

- (void)setAdvisors:(NSArray *)advisors forQuestion:(Question *)question;


@end
