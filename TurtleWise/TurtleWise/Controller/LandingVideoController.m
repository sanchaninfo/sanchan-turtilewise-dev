//
//  LandingVideoController.m
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "LandingVideoController.h"
#import "LandingVideoView.h"
#import "SocialLoginController.h"
#import "AppDelegate.h"

@interface LandingVideoController ()<UIGestureRecognizerDelegate>

@end

@implementation LandingVideoController

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [super setInteractionPopGestureDelegate];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self.view
                                             selector:@selector(MPMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self.view name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [(LandingVideoView *) self.view resetVideo];
}

-(void) onNavigationToSocialLoginController {
    [self.navigationController pushViewController:[SocialLoginController new] animated:YES];
}

@end
