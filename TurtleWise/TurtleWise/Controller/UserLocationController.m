//
//  UserLocationController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/7/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "SelectEthnicReligionController.h"
#import "UserLocationController.h"
#import "UserLocationView.h"
#import "NSArray+Utils.h"
#import "StringUtils.h"

#define TITLE @"Step 2"
#define COUNTRIES_FILE_NAME @"Countries"
#define STATES_FILE_NAME    @"States"
#define CASE_EXCEPTION_COUNTRY_NAME @"united states"

@interface UserLocationController ()

@end

@implementation UserLocationController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:TITLE];
}

- (NSArray *)getAllCountries
{
    NSArray * arrOfCountries = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:COUNTRIES_FILE_NAME ofType:PLIST_EXTENSION]];
    return [arrOfCountries sortArray:arrOfCountries orderAscending:YES];
}

- (NSArray *)getAllStates
{
    NSArray * arrOfStates = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:STATES_FILE_NAME ofType:PLIST_EXTENSION]];
    return [arrOfStates sortArray:arrOfStates orderAscending:YES];
}

- (BOOL)shouldEnableStatesForCountry:(NSString *)countryName
{
    if (countryName.length > 0 && [[countryName lowercaseString] isEqualToString:CASE_EXCEPTION_COUNTRY_NAME]) {
        return YES;
    }
    return NO;
}

- (NSInteger)getIndexForCountry:(NSString *)countryName
{
    __block NSInteger index = 0;
    countryName = [StringUtils isEmptyOrNull:countryName]?CASE_EXCEPTION_COUNTRY_NAME:countryName;
    NSArray * arrOfCountries = [self getAllCountries];
    [arrOfCountries enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if ([[obj lowercaseString] isEqualToString:[countryName lowercaseString]])
         {
             stop = false;
             index = idx;
         }
     }];
    return index;
}

- (NSInteger)getIndexForState:(NSString *)stateName
{
    __block NSInteger index = 0;
    NSArray * arrOfCountries = [self getAllStates];
    [arrOfCountries enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if ([[obj lowercaseString] isEqualToString:[stateName lowercaseString]])
         {
             stop = false;
             index = idx;
         }
     }];
    return index;
}
@end
