//
//  SelectSummaryController.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectSummaryController.h"
#import "SelectSummaryView.h"
#import "BrandedPage.h"
#import "UserDefaults.h"

#define TITLE @"Edit Summary"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface SelectSummaryController ()

- (void)setTitle;
- (void)setupNavigationButtons;
@end

@implementation SelectSummaryController

#pragma mark - UIViewController Life Cycle

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
    // Do any additional setup after loading the view.
    
    [self setTitle];
    [self setupNavigationButtons];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForHome];
    
    [[self navigationController] setNavigationBarHidden:NO];
    pageManager = [BrandedPageManager sharedManager];
    [(SelectSummaryView *)self.view updateData:[pageManager brandedPage]];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[self view] endEditing:YES];
}


- (void)setTitle
{
    
    [self setTitle:[NSString stringWithFormat:TITLE]];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - Navigation

#pragma mark - Service Calls

- (void)savePageDetail
{
    
    [(SelectSummaryView *)self.view saveWizardData:[pageManager brandedPage]];
    
    NSDictionary * params = [pageManager getDictionaryFromBrandedPage:[pageManager brandedPage]];;
    [self showLoader];
    [[service brandedPageService] updatePage:params withPageId:[UserDefaults getPageID] withSuccess:^(id response) {
        [super hideLoader];
        [self popViewController];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Call backs

- (void)saveProfileSucessful:(id)response
{
    [self hideLoader];
}

@end
