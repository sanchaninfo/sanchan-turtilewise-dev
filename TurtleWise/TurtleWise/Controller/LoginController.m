//
//  LoginController.m
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "AboutMeController.h"
#import "AboutMeController.h"
#import "AnalyticsHelper.h"
#import "LoginController.h"
#import "SocketIOManager.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "LoginView.h"

@class UserProfile;

@interface LoginController ()

- (void)loginSucessful:(id)response;

@end

@implementation LoginController

#pragma mark - Life Cycle Methods

- (id)initWithUserName:(NSString *)username andPassword:(NSString *)password
{
    self = [super init];
    
    if (self)
    {
        [(LoginView *)[self view] setUsername:username andPassword:password];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeAuth)]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

- (void)openHomeController
{
    [UserDefaults setShouldCheckForUnratedChats:YES];
    [self.navigationController setViewControllers:@[[APP_DELEGATE getHomeControllerWithSlideMenu]] animated:YES];
}

- (void)openForgotPasswordControllerWithEmail:(NSString *)email {
    ForgotPasswordController * controller = [ForgotPasswordController new];
    [controller setEmail:email];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Service Calls

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password
{
    [self showLoader];
    
    [[service auth] loginUserViaEmail:email andPassword:password withSuccess:^(id response)
    {
        [self loginSucessful:response];
    }
     andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Callbacks

- (void)loginSucessful:(id)response
{
    [self hideLoader];
    
    [UserDefaults setLoginFlowType:LoginFlowTypeEmail];
    [UserDefaults setUserRole:UserRoleAdvisor];
    [AnalyticsHelper logEvent:self
                     category:CATEGORY_AUTHORIZATION
                       action:EVENT_SIGNIN
                        label:LABEL_EMAIL value:NULL];

    [[SocketIOManager instance] connectScocket];
    
    [APP_DELEGATE setHomeControllerShouldShowEmailActivationAlert:YES];

    [self openHomeController];
}

- (void)serviceCallFailed:(NSError *)error
{
    [self hideLoader];
    [Alert show:error];
}

@end
