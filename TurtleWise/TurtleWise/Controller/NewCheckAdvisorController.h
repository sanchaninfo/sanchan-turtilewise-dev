//
//  CheckAdvisorControllerNewViewController.h
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
#import "AnswerPagedResponse.h"
#import "TabbarView.h"
#import "Question.h"
#import "Answer.h"

@interface NewCheckAdvisorController : BaseController{
    NSString *_sortBy;
    Question *_question;
}
@property(nonatomic,weak) id tabBarDelegate;

-(void)setupNavigationButtons;
-(void)setSortOrder:(NSString*)sortBy;
-(void)setQuestion:(Question*)question;
-(void)setAnswerMeta:(AnswerPagedResponse*)answerMeta andSelectTab:(NSInteger)tab;
-(void)loadAnswersWithSearchOption:(NSInteger)selectedTab andReset:(BOOL)reset;
-(void)closeQuestion;
-(void)postFeedback:(NSString*)feedback forAnswer:(Answer*)answer;
-(void)showChatConfirmationControllerWithAdvisors:(NSArray*)advisors;
-(BOOL)canStartNewChat;
-(void)showSubscriptionModal;
-(void)checkAvailablityFromService:(Advisor *)advisor indexPath:(NSIndexPath *)indexPath;
@end
