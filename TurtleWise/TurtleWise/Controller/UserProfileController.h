//
//  UserProfileController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface UserProfileController : BaseController

- (void)getUserProfile;

@end
