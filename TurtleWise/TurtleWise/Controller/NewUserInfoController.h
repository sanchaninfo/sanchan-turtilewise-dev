//
//  NewUserInfoController.h
//  TurtleWise
//
//  Created by Usman Asif on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface NewUserInfoController : BaseController

@property (nonatomic, strong) NSString * advisorId;
@property (nonatomic) BOOL isSeeker;
@end
