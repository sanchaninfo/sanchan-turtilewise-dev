//
//  TurtleSwagDetailsController.h
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@class Charity;

@interface TurtleSwagDetailsController : BaseController

- (id)initWithDetails:(Charity *)charity;
- (void)redeemAmount:(NSInteger)amount;
- (void)donateAmount:(NSInteger)amount;
- (void)goToHome;

@end
