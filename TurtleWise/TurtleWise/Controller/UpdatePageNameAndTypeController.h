//
//  UpdatePageNameAndTypeController.h
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
#import "BrandedPageManager.h"

@interface UpdatePageNameAndTypeController : BaseController {
    BrandedPageManager *pageManager;
}
- (void)savePageDetail;
@end
