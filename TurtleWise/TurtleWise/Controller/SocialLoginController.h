//
//  SocialLoginController.h
//  TurtleWise
//
//  Created by Irfan Gul on 02/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface SocialLoginController : BaseController

- (void)startFacebookLoginProcess;
- (void)startLinkedInLoginProcess;
- (void)openRegisterController;
- (void)openLoginController;

@end
