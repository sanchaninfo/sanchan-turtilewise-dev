//
//  SelectSignUpAsViewController.m
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectSignUpAsController.h"
#import "SelectSignUpAsView.h"
#import "TWWizardManager.h"
#import "SeekingAdvisor.h"
#import "UserProfile.h"
#import "UserDefaults.h"

#define TITLE @"Step %ld"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface SelectSignUpAsController ()

@end

@implementation SelectSignUpAsController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
