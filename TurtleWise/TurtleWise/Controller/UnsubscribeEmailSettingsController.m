//
//  UnsubscribeEmailSettingsController.m
//  TurtleWise
//
//  Created by Usman Asif on 11/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UnsubscribeEmailSettingsController.h"
#import "UnsubscribeEmailSettingsView.h"
#import "UserDefaults.h"

@implementation UnsubscribeEmailSettingsController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeSettings)]];
    [self unsubscribeEmailSettings];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - Service Call
- (void)unsubscribeEmailSettings {
    [super showLoader];
    [service.settings unsubscribeEmailSettings:@{KEY_TOKEN : _unsubscribeToken} withSuccess:^(id response) {
        [super hideLoader];
        [(UnsubscribeEmailSettingsView *)self.view updateDoneButtonTitle:[UserDefaults isUserLogin]];
    } andfailure:^(NSError *error) {
        [super hideLoader];
        [super serviceCallFailed:error];
    }];
}

@end