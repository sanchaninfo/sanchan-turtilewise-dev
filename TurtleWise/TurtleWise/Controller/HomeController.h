//
//  HomeController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/4/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface HomeController : BaseController

@property(nonatomic) BOOL showingValidationAlert;
@property(nonatomic) BOOL shouldShowChatAlert;

- (void)updateSlideMenu;
- (void)proceedToAskQuestion;
- (void)updateUserRole;
- (void)showQuestionListingController;
- (void)showAnswerListingController;
- (void)showChatListingController;
- (void)askForUnratedChat:(long)unratedChatCount;
- (void)rateAllChatsThanks;
- (void)showSubscriptionModel;
- (void)subsciptionEnded;

@end
