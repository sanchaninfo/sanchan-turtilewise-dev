//
//  LoginController.h
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface LoginController : BaseController

- (id)initWithUserName:(NSString *)username andPassword:(NSString *)password;
- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password;
- (void)openHomeController;
- (void)openForgotPasswordControllerWithEmail:(NSString *)email;

@end
