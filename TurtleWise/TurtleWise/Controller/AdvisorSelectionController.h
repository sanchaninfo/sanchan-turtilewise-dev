//
//  QuestionTypeController.h
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class TWWizardManager;

@interface AdvisorSelectionController : BaseController

@property(nonatomic, strong) TWWizardManager *profileWizardManager;

- (id)initWithQuestion:(NSString *)question andAnswerer:(NSMutableDictionary*)dic;
- (void)startWizardForAdvisorAttributes;
- (void)proceedToSubmitQuestion;
- (void)saveGuruTags:(NSArray *)guruTags;
- (void)proceedToSelectChoicesForMCQ;
- (void)clearMCQOptionsController;
- (void)requestSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray * tags))success;
- (void)requestToSaveTag:(NSString *)tag;
@end
