//
//  BaseProfileWizardController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"
#import "BaseProfileWizardView.h"
#import "TWWizardManager.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "SeekingAdvisor.h"
#import "BrandedPageManager.h"

#define TITLE @"Step %ld"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface BaseProfileWizardController ()

- (void)setTitle;
- (void)setupNavigationButtons;

@end

@implementation BaseProfileWizardController

#pragma mark - Life Cycle Methods

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager
{
    if (self = [super init])
    {
        _profileWizardManager = profileWizardManager;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeUser)]];
    
    [self setTitle];
    [self setupNavigationButtons];
    [self setAppearenceForAddAdvisorWizard];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO];
    [self setAppearenceForAddAdvisorWizard];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[self view] endEditing:YES];
    
    [(BaseProfileWizardView *)self.view saveWizardData:[_profileWizardManager profile]];
}

- (void)setTitle
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [self setTitle:[NSString stringWithFormat:TITLE, (long)[_profileWizardManager currentStep]]];
    
    if([_profileWizardManager isLastStep] && ![[_profileWizardManager profile] isMemberOfClass:[SeekingAdvisor class]])
        [self setTitle:TITLE_REVIEW_PROFILE];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self previous];
    }];
    
    if (![_profileWizardManager isLastStep])
    {
        [self setupRightNavigationButtonWithImage:NEXT_BUTTON_WHITE_IMAGE withOnClickAction:^{
            [self next];
        }];
    }
}

#pragma mark - Navigation

- (void)next
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [_profileWizardManager next];
}

- (void)previous
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    [_profileWizardManager previous];
}

- (void)completeWizard
{
    if (!_profileWizardManager)
    {
        return;
    }
    
    if ([_profileWizardManager shouldSaveWizardOnDone])
    {
        [self saveProfile];
        
        return;
    }
    
    [_profileWizardManager finish];
}

#pragma mark - Service Calls

- (void)saveProfile
{
    [self showLoader];
    [[service user] saveProfile:[(UserProfile *)[_profileWizardManager profile] getAsDictionary] withSuccess:^(id response)
     {
         [self saveProfileSucessful:response];
         
     } andfailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

#pragma mark - Service Call backs

- (void)saveProfileSucessful:(id)response
{
    [self hideLoader];
    
    [self openHomeController];
}

@end
