//
//  ManageBrandedPageController.m
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ManageBrandedPageController.h"
#import "ManageBrandedPageView.h"
#import "SelectSummaryController.h"
#import "SelectInfluencerOnboardingController.h"
#import "UpdatePageNameAndTypeController.h"
#import "AddArticlesController.h"
#import "AddEventsController.h"
#import "AskQuestionController.h"
#import "UserDefaults.h"
#import "BrandedPage.h"

#define TITLE @"Manage Branded Page"
@interface ManageBrandedPageController ()
- (void)setupNavigationButtons;

@end

@implementation ManageBrandedPageController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:TITLE];
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
    pageManager = [BrandedPageManager sharedManager];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setAppearenceForHome];
    [self getBrandedPageDetails];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)loadManageBrnadedView {
    [(ManageBrandedPageView *)self.view brandedPageWith:[pageManager brandedPage]];
}

- (void) getBrandedPageDetails {
    [self showLoader];
    [[service brandedPageService] getPageWithPageId:[pageManager getPageIdForBrandedpage] withSuccess:^(id response) {
        {
            [super hideLoader];
            [pageManager initWithBrandedPage:(BrandedPage *)response];
            [self loadManageBrnadedView];
        }
    } andfailure:^(NSError *error) {
        {
            [self serviceCallFailed:error];
        }
    }];
}

- (void)savePageDetail {
    [(ManageBrandedPageView *)self.view getPrivacy:[pageManager brandedPage]];
    
    NSDictionary * params = [pageManager getDictionaryFromBrandedPage:[pageManager brandedPage]];
    
    [self showLoader];
    [[service brandedPageService] updatePage:params withPageId:[UserDefaults getPageID] withSuccess:^(id response) {
        [super hideLoader];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void)showSummaryController
{
    SelectSummaryController *summaryController = [[SelectSummaryController alloc] init];
    [[self navigationController] pushViewController:summaryController animated:YES];
}

- (void) showInfluencerOnboardingController {
    SelectInfluencerOnboardingController *influencerController = [[SelectInfluencerOnboardingController alloc] init];
    [[self navigationController] pushViewController:influencerController animated:YES];
}

- (void) showUpdateNameANdTypeController {
    UpdatePageNameAndTypeController *nameController = [[UpdatePageNameAndTypeController alloc] init];
    [[self navigationController] pushViewController:nameController animated:YES];
}

- (void) showArticleControllerWithNumber:(int)articleNoFromOUt {
    AddArticlesController *article = [[AddArticlesController alloc] init];
    article.articleNo = articleNoFromOUt;
    [[self navigationController] pushViewController:article animated:YES];
}

-(void) deleteArticleWithNumber:(int)articleNo {
    [[[pageManager brandedPage] articles] removeObjectAtIndex:articleNo];
    [self savePageDetail];
}

- (void) showEventControllerWithNumber:(int)eventNoFromOut {
    AddEventsController *event = [[AddEventsController alloc] init];
    event.eventNo = eventNoFromOut;
    [[self navigationController] pushViewController:event animated:YES];
}

-(void) deleteEventWithNumber:(int)eventNo {
    [[[pageManager brandedPage] events] removeObjectAtIndex:eventNo];
    [self savePageDetail];
}

- (void) deletePage {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             [UserDefaults getPageID], KEY_PAGE_DELETE_ID,
                             nil];
    [self showLoader];
    [[service brandedPageService] deletePage:params withSuccess:^(id response) {
        [super hideLoader];
        [[BrandedPageManager sharedManager] clearPageId];
        [UserDefaults setPageID:NULL];
        [self.navigationController popViewControllerAnimated:YES];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void)startQuestionTextWithDictionary:(NSMutableDictionary*)dic;
{
    AskQuestionController *questionController = [[AskQuestionController alloc] initWithAnswereDictionary:dic];
    
    [self.navigationController pushViewController:questionController animated:YES];
}

- (void) followWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] followPage:params withSuccess:^(id response) {
        [super hideLoader];
        [UserDefaults followPublic:pageId];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) unfollowWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOUNFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] unfollowPage:params withSuccess:^(id response) {
        [super hideLoader];
        [UserDefaults unfollow:pageId];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) followRequestWithPageId:(NSString*)pageId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             pageId, KEY_TOFOLLOW,
                             nil];
    [self showLoader];
    [[service brandedPageService] requestToFollowPage:params withSuccess:^(id response) {
        [super hideLoader];
        [UserDefaults followPrivate:pageId];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) getFindIdsWithString:(NSString*)isFrom {
    
    NSMutableArray* isFromArray;
    
    if ([isFrom isEqualToString:@"pending"]) {
        isFromArray = [[pageManager brandedPage] pending];
    } else if ([isFrom isEqualToString:@"approved"]) {
        isFromArray = [[pageManager brandedPage] approved];
    }
    
    [self showLoader];
    [[service brandedPageService] findPendingId:isFromArray withSuccess:^(id response) {
        [self hideLoader];
        if ([isFrom isEqualToString:@"pending"]) {
            [(ManageBrandedPageView *)self.view showRequestView:response];
        } else if ([isFrom isEqualToString:@"approved"]) {
            [(ManageBrandedPageView *)self.view showFollowersView:response];
        }
    }];
}

- (void) toUnFollow:(NSString*)userId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[pageManager brandedPage] pageId], KEY_TOUNFOLLOW,
                             userId, KEY_USER_IDs, nil];
    [self showLoader];
    [[service brandedPageService] unfollowPage:params withSuccess:^(id response) {
        [super hideLoader];
        [(ManageBrandedPageView *)self.view removesubview];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

- (void) toFollow:(NSString*)userId {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[pageManager brandedPage] pageId], KEY_TOFOLLOW,
                             userId, KEY_USER_IDs, nil];
    
    [self showLoader];
    [[service brandedPageService] followPage:params withSuccess:^(id response) {
        [super hideLoader];
        [(ManageBrandedPageView *)self.view removesubview];
        [self getBrandedPageDetails];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}



@end
