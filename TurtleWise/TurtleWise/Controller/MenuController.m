//
//  MenuController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//


#import "LandingController.h"
#import "UserProfileController.h"
#import "EnvironmentConstants.h"
#import "SocketIOManager.h"
#import "HomeController.h"
#import "MenuController.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "MenuView.h"

@interface MenuController ()

- (BaseController *)getTopViewController;
- (UINavigationController *)getCentreNavigationController;

@end

@implementation MenuController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeAuth)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    //Nothing to Do.
}

#pragma mark - Public Method

- (void)updateMenu
{
    [(MenuView *)self.view setUserRole:[UserDefaults getUserRole]];
    [(MenuView *)[self view] updateUserRole];
}

- (void)updateBrandedpage {
    [(MenuView *)[self view] updateBrandedPage];
}

- (void)handleViewStatusBar:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[HomeController class]] ||
        [viewController isKindOfClass:[UserProfileController class]])
    {
        [[self view] setBackgroundColor:[UIColor blackColor]];
    }
    else
    {
        [[self view] setBackgroundColor:[UIColor whiteColor]];
    }
}

- (void)pushController:(UIViewController *)viewController
{
    [self handleViewStatusBar:viewController];
    
    BaseController *topViewController = [self getTopViewController];
    
    if (![topViewController isKindOfClass:[viewController class]])
    {
        [[self getCentreNavigationController] pushViewController:viewController animated:NO];
    }
    
    if ([topViewController isKindOfClass:[HomeController class]])
    {
        [(HomeController *)topViewController updateUserRole];
    }
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

- (void)openRedeemInSafari {
    NSString * urlString = [NSString stringWithFormat:URL_REDEEM_PAGE,
                            BASE_URL_REDEEM_PAGE,
                            [UserDefaults getAccessToken],
                            [UserDefaults getUserID]];
    
    NSURL * url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

#pragma mark - Helper Methods

- (BaseController *)getTopViewController
{
    return (BaseController *)[[self getCentreNavigationController] topViewController];
}

- (UINavigationController *)getCentreNavigationController
{
    return (UINavigationController *)self.menuContainerViewController.centerViewController;
}

#pragma mark - Service Calls

- (void)userLogOut
{
    [self showLoader];
    
    [[service auth] logoutUserWithSuccess:^(id response)
    {
        [self handleLogoutService];
    }
       andFailure:^(NSError *error)
     {
        [self handleLogoutService];
    }];

}

#pragma mark - Service Call Back

- (void)handleLogoutService
{
    [self hideLoader];
    [UserDefaults clearDefaults];
    
    [[SocketIOManager instance] disconnectScocket];
    
    [[self getCentreNavigationController] setViewControllers:@[[LandingController new]]];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

@end


