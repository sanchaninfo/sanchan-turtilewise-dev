//
//  CheckAdvisorControllerNewViewController.m
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewCheckAdvisorController.h"
#import "QuestionListingController.h"
#import "ChatRequestController.h"
#import "SubscribeController.h"
#import "NewCheckAdvisorView.h"
#import "ChattingController.h"
#import "AnalyticsHelper.h"
#import "UserDefaults.h"
#import "AnswerOption.h"
#import "Conversation.h"
#import "StringUtils.h"
#import "Constant.h"

@interface NewCheckAdvisorController ()<ChatRequestControllerDelegate,SubscribeControllerDelegate>{
    AnswerPagedResponse *_answersMeta;
    AnswerPagedResponse *_answers;
    BOOL _createdNewChat;
    BOOL _isPurchasingForChat;
}

@end

@implementation NewCheckAdvisorController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeAnswer),@(ServiceTypeChat)]];
    _answers = [AnswerPagedResponse new];
    [self.navigationItem setTitle:THEIR_ANSWER_SCREEN_TITLE];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setAppearenceForSeeker];
    [self setupNavigationButtons];
    if(_createdNewChat) {
        [[(NewCheckAdvisorView*)self.view tableView] reloadData];
    }
    
    _isPurchasingForChat = NO;
}

-(void)setupNavigationButtons{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    if (_question.status != QuestionStatusClosed && ![_question isExpired]) {
        [self setupRightNavigationButtonWithName:RIGHT_BAR_BUTTON_TITLE_CLOSE_QUESTION withOnClickAction:^{
            [(NewCheckAdvisorView *)self.view showCloseAlert];
        }];
    }
    
    if([(NewCheckAdvisorView*)self.view getSelectedAdvisors].count){
        [self setupRightNavigationButtonWithName:@"Chat" withOnClickAction:^{
            [self handleInitateChat];
        }];
    }
}

-(void)setQuestion:(Question*)question{
    _question = question;
    [(NewCheckAdvisorView*)self.view setQuestion:question];
}

- (void)handleInitateChat
{
    if ([_question isSubscribed])
    {
//        [self showChatConfirmationControllerWithAdvisors:[(NewCheckAdvisorView*)self.view getSelectedAdvisors]];
        
        
        if([self getAdvisorIds].count > MAXIMUM_CHAT_USERS)
        {
            [Alert show:TITLE_ALERT_ERROR andMessage:MESSAGE_MAXIMUM_CHAT_USERS];
            
            return;
        }
        
        [self showLoader];
        [service.chat initiateChatWithAdvisors:[self getAdvisorIds] forQuestion:_question.questionId
                                     onSuccess:^(Conversation *conversation) {
                                         [self getChatDetails:conversation];
                                     }
                                    andFailure:^(NSError *error) {
                                        [self serviceCallFailed:error];
                                    }];
        return;
    }
    
    _isPurchasingForChat = YES;
    [self showSubscriptionModal];
}

-(void)setAnswerMeta:(AnswerPagedResponse*)answerMeta andSelectTab:(NSInteger)tab{
    _answersMeta = answerMeta;
    [(NewCheckAdvisorView*)self.view selectTab:tab];
}
-(void)showChatConfirmationControllerWithAdvisors:(NSArray*)advisors{
    ChatRequestController *controller = [[ChatRequestController alloc] init];
    controller.delegate = self;
    [controller setAdvisors:advisors forQuestion:_question];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)popToAnswerController
{
    NSArray * viewControllers = [self.navigationController viewControllers];
    if (viewControllers.count >= 3)
    {
        UIViewController * controller = [viewControllers objectAtIndex:viewControllers.count-3];
        if ([controller isKindOfClass:[QuestionListingController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            [(QuestionListingController *)controller refreshOnCloseQuestionSuccess];
        }
    }
}

- (void)closeQuestion
{
    [super showLoader];
    if(!service.question)
        [super loadServices:@[@(ServiceTypeQuestion)]];
    [service.question closeQuestion:@{KEY_QUESTION_ID:_question.questionId} onSuccess:^(id response)
     {
         [super hideLoader];
         [self popToAnswerController];
     } andfailure:^(NSError *error)
     {
         [super hideLoader];
         [super serviceCallFailed:error];
     }];
}

-(BOOL)canStartNewChat{
    if(_question.isExpired || _question.status == QuestionStatusClosed)
        return NO;
    return YES;
}
-(void)showSubscriptionModal{
    SubscribeController *cont = [[SubscribeController alloc] initWithQuestion:_question];
    [cont setDelegate:self];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cont];
    [self presentViewController:nav animated:YES completion:nil];
}

- (NSArray *)getAdvisorIds
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(Advisor *advisor in [(NewCheckAdvisorView*)self.view getSelectedAdvisors])
    {
        [array addObject:[advisor advisorID]];
    }
    
    return array;
}

#pragma mark -- SubscriptionController Delegate
-(void)didSubscribeSuccessfully{
    
    _question.isSubscribed = YES;
    if (_isPurchasingForChat)
    {
        _isPurchasingForChat = NO;
        
        [self showChatConfirmationControllerWithAdvisors:[(NewCheckAdvisorView*)self.view getSelectedAdvisors]];
        return;
    }
    
    [self showLoader];
    [self loadAnswersWithSearchOption:[(NewCheckAdvisorView*)self.view getSelectedTab] andReset:YES];
}

#pragma mark -- ChatRequestController Delegate

-(void)didStartNewChat{
    _createdNewChat = YES;
    //[self.navigationItem setRightBarButtonItem:nil];
}

#pragma mark - Navigation

- (void)showChattingScreenWithConversation:(Conversation *)conversation withMessage:(NSString *)message
{
    ChattingController *chattingController = [[ChattingController alloc] initWithConversation:conversation withMessage:message];
    chattingController.popTwiceOnBackBtnTap = YES;
    [[self navigationController] pushViewController:chattingController animated:YES];
}

#pragma mark - Service Calls
-(void)setSortOrder:(NSString*)sortBy{
    _sortBy = sortBy;
}
-(void)loadAnswersWithSearchOption:(NSInteger)selectedTab andReset:(BOOL)reset{
    
    if(reset)
        [_answers reset];
    else{
        if(![self selectedOptionHasMoreAnswers:selectedTab])
            return;
    }
    
    if(!_answers.hasMoreData){
        return;
    }
    
    if(_answers.isLoadingData)
        return;
    
    
    NSString *selectedOption = @"";
    
    ([_answersMeta answerOptions].count) ? selectedOption = [(AnswerOption*)[[_answersMeta answerOptions] objectAtIndex:selectedTab] name] : @"";

    [service.answer getAnswersForQuestion:_answersMeta.questionID andParams:[self getParamsForServiceCall:selectedOption] onSuccess:^(id response) {
        [self hideLoader];
        [(NewCheckAdvisorView*)self.view setAnswers:_answers.dataArray];
    } andFailure:^(NSError *error) {
        [self hideLoader];
        [(NewCheckAdvisorView*)self.view showError:_answers.dataArray.count ? @"" : @"Unknown error occured."];//Don't show this error on loadMore call failure.
    } response:_answers];
}

-(BOOL)selectedOptionHasMoreAnswers:(NSInteger)selectedTab{
    
    if(_answersMeta.answerOptions.count == 0)
        return NO;
    
    AnswerOption *answerOption = [[_answersMeta answerOptions] objectAtIndex:selectedTab];
    if(answerOption.count > [_answers dataArray].count)
        return YES;
    
    return NO;
}


-(NSDictionary*)getParamsForServiceCall:(NSString*)searchOption{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{KEY_PAGE_SIZE : [NSNumber numberWithInt:_answers.pageSize],
                                                                                    KEY_SKIP : [NSNumber numberWithInt:_answers.skip],
                                                                                    KEY_ANSWERS_SEARCH_OPTION : [StringUtils getEncodedString:searchOption]}];
    
    if(_sortBy)
        [params setObject:_sortBy forKey:@"sort"];
    
    return params;
}
-(void)postFeedback:(NSString*)feedback forAnswer:(Answer*)answer{
    [service.answer postSeekersFeedbackToAdvisorsAnswer:feedback forQuestion:_question.questionId andAnswer:answer];
}

-(void)getChatDetails:(Conversation*)conversation
{
    [service.chat getConversationDetails:conversation.conversationId onSuccess:^(id response) {
        [self hideLoader];
        [self showChattingScreenWithConversation:response withMessage:conversation.message];
        _createdNewChat = YES;
        
        [AnalyticsHelper logEvent:self category:CATEGORY_CHAT action:EVENT_CHAT_INITIATED];
        
    } andFailure:^(NSError *error)
     {
         [self serviceCallFailed:error];
     }];
}

- (void)checkAvailablityFromService:(Advisor *)advisor indexPath:(NSIndexPath *)indexPath {
    //[super showLoader];
    [service.chat checkAvailablityForAdvisor:advisor.advisorID forQuestion:_question.questionId withSuccess:^(id response) {
        [super hideLoader];
        [(NewCheckAdvisorView *)self.view addAdvisorToChatPanel:advisor];
        [(NewCheckAdvisorView *)self.view updateAvailableForChat:YES atIndexPath:indexPath];
        [self setupNavigationButtons];
    } andFailure:^(NSError *error) {
        [super hideLoader];
        [Alert show:error];
        [(NewCheckAdvisorView *)self.view removeAdvisorFromChatPanel:advisor];
        [(NewCheckAdvisorView *)self.view updateAvailableForChat:NO atIndexPath:indexPath];
        [self setupNavigationButtons];
    }];
}

@end
