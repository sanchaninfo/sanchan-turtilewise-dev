//
//  AnswerController.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class Question;
@protocol TabBarBtnTapEvent <NSObject>

-(void)didSelectAnswerControllerTabBarBtn:(TabbarViewType)tabViewType;
-(void)loadConversationsForQuestion:(NSString*)questionId;

@end

@interface AnswerController : BaseController

@property(nonatomic,weak) id <TabBarBtnTapEvent> customDelegate;
@property(nonatomic,assign) BOOL isArchived;

- (id)initWithQuestionId:(NSString *)questionId;
-(void)loadAnswerMeta;
-(void)setQuestion:(Question*)question;
-(void)setIsArchived:(BOOL)isArchived;
-(void)goToAdvisorListingControllerWithSearchOption:(NSInteger)option;
-(void)archiveQuestionById;
@end
