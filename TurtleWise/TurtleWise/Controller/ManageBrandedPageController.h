//
//  ManageBrandedPageViewController.h
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
#import "BrandedPageManager.h"
//#import "BrandedPage.h"

@interface ManageBrandedPageController : BaseController {
    BrandedPageManager *pageManager;
}

//@property(nonatomic, strong) BrandedPageManager *pageManager;

- (void)loadManageBrnadedView;
- (void)showSummaryController;
- (void)showInfluencerOnboardingController;
- (void) showUpdateNameANdTypeController;
- (void)savePageDetail;
- (void) showArticleControllerWithNumber:(int)articleNoFromOut;
- (void) deleteArticleWithNumber:(int)articleNo;
- (void) showEventControllerWithNumber:(int)eventNoFromOut;
-(void) deleteEventWithNumber:(int)eventNo;

- (void) deletePage;
- (void)startQuestionTextWithDictionary:(NSMutableDictionary*)dic;
//@property(nonatomic, strong) BrandedPage *pageDetail;
- (void) followWithPageId:(NSString*)pageId;

- (void) unfollowWithPageId:(NSString*)pageId;

- (void) followRequestWithPageId:(NSString*)pageId;
- (void) getFindIdsWithString:(NSString*)isFrom;
- (void) toUnFollow:(NSString*)userId;
- (void) toFollow:(NSString*)userId;
@end
