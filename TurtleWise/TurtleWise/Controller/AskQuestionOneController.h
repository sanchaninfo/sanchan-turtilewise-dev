//
//  AskQuestionOneController.h
//  TurtleWise
//
//  Created by Sunflower on 9/4/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface AskQuestionOneController : BaseController
- (void)startQuestionTextWithDictionary:(NSMutableDictionary*)dic;
@end
