//
//  SurveyController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 7/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface SurveyController : BaseController

- (void)goToTurtleSwags;

@end
