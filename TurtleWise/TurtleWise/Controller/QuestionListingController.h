//
//  SeekerController.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"
#import "QuestionsResponsePaged.h"
#import "ConversationsPagedResponse.h"
@class Question;
@class Answer;
@class Conversation;

@interface QuestionListingController : BaseController{
    QuestionsResponsePaged *_questions;
    ConversationsPagedResponse *_conversations;
    TabbarViewType _listType;
    UserRole _userRole;
    
}

@property(nonatomic,assign) BOOL isArchived;

-(TabbarViewType)getListType;
-(void)setupWithListType:(TabbarViewType)type;
-(UserRole)getUserRole;
-(void)loadMoreConversations;
-(void)loadMoreQuestions;
-(void)loadQuestionsAndReset:(BOOL)reset;
-(void)loadAnswersAndReset:(BOOL)reset;
-(void)loadConversationsAndReset:(BOOL)reset;
-(void)openAnswerControllerWithQuestion:(Question*)question;
-(void)openGiveAnswerControllerWithQuestion:(Question*)question isReadOnly:(BOOL)readOnly;
-(id)initWithListType:(TabbarViewType)listType andUserRole:(UserRole)userRole;
-(void)closeQuestion:(NSString*)questionId;
-(void)deleteQuestions:(NSArray*)questions;
-(void)refreshOnCloseQuestionSuccess;
-(void)openChattingControllerForConversation:(Conversation *)conversation;

@end
