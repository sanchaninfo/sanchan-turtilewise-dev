//
//  ResetPasswordController.m
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ResetPasswordController.h"
#import "EnvironmentConstants.h"
#import "Keys.h"

@interface ResetPasswordController ()

@end

@implementation ResetPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeSettings)]];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Service Calls

- (void)forgotResetPassword:(NSString *)newPassword
{
    [super showLoader];
    
    [[service settings] forgotResetPassword:@{KEY_TOKEN : _resetPasswordToken,
                                              KEY_NEW_PASSWORD : newPassword,
                                              KEY_CONFIRM_NEW_PASSWORD : newPassword}
                                withSuccess:^(id response) {
        [super hideLoader];
        [Alert show:EMPTY_STRING andMessage:[response get]];
        [self.navigationController popViewControllerAnimated:YES];
    } andfailure:^(NSError *error) {
        [super hideLoader];
        if (error.code == STATUS_CODE_FAILED_AUTH || error.code == STATUS_CODE_FAILED_NOT_FOUND) {
            [Alert show:EMPTY_STRING andMessage:@"This link has expired. Please click on \"Forgot Password\" link on sign-in screen to receive the link again."];
            return;
        }
        [Alert show:error];
    }];
}

@end
