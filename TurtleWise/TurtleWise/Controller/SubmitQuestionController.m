//
//  SubmitQuestionController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubmitQuestionController.h"
#import "SubmitQuestionView.h"
#import "AnalyticsHelper.h"
#import "HomeController.h"
#import "SeekingAdvisor.h"
#import "UserDefaults.h"
#import "MFSideMenu.h"
#import "Constant.h"
#import "AskQuestionController.h"
#import "HomeController.h"
#import "UINavigationController+PushNotificationRoutes.h"

//Keys
#define KEY_QUESTION_CONTENT @"content"
#define KEY_QUESTION_OPTIONS @"options"

#define TITLE_ALERT_CONFIRMATION @"Are you sure?"
#define MESSAGE_ALERT_CONFIRMATION @"You will loose all your progress."
#define TITLE_OFFER_ADVICE @"Offer Advice"
#define TITLE_ASK_QUESTION @"Ask a Question"
#define TITLE_CLOSE @"Close"

#define ALERT_CANCEL_QUESTION 1
#define ALERT_SUCCESS_QUESTION 2

@interface SubmitQuestionController ()< UIAlertViewDelegate >

@property(nonatomic, weak) SubmitQuestionView *view;
@property(nonatomic, strong) SeekingAdvisor *seekingAdvisor;

@property(nonatomic, assign) BOOL hasServiceFailed;
@property(nonatomic, strong) NSString *advisorCount;

- (void)cancelQuestion;
- (void)setupNavigationButtons;
- (void)askQuestionSucessful:(id)response;

@end

@implementation SubmitQuestionController

@dynamic view;

#pragma mark - Life Cycle Methods

- (id)initWithSeekingAdvisor:(SeekingAdvisor *)seekingAdvisor
{
    if (self = [super init])
    {
        _seekingAdvisor = seekingAdvisor;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super loadServices:@[@(ServiceTypeQuestion)]];
    
    [[self view] setData:_seekingAdvisor];
    [self setTitle:ASK_SCREEN_TITLE];
    [self setupNavigationButtons];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForSeeker];
}

#pragma mark - Private Method

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [[self navigationController] popViewControllerAnimated:YES];
    }];
    
    [self setupRightNavigationButtonWithName:LEFT_BUTTON_TITLE withOnClickAction:^{
        [self cancelQuestion];
    }];
}

- (NSDictionary *)getParamsForAskQuestionCall
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setObject:[_seekingAdvisor questionType] forKey:KEY_TYPE];
    [params setObject:[_seekingAdvisor question] forKey:KEY_QUESTION_CONTENT];
    [params setObject:[_seekingAdvisor guruTags] forKey:KEY_SEARCH_TAGS];
    [params setObject:[_seekingAdvisor searchOptions] ? [_seekingAdvisor searchOptions] : @"anyone" forKey:KEY_SEARCH_OPTIONS];
    [params setObject:[_seekingAdvisor getAsDictionary] forKey:KEY_SEARCH_ATTRIBUTES];
    [params setObject:[_seekingAdvisor questionOptions] forKey:KEY_QUESTION_OPTIONS];
    
    NSDictionary * answerer = [NSDictionary dictionaryWithObjectsAndKeys:
                               [_seekingAdvisor list], KEY_LIST,
                               [_seekingAdvisor pageName], KEY_PAGENAME,
                               [_seekingAdvisor who], KEY_WHO,
                               nil];
    
    if (answerer.count > 0) {
        [params setObject:answerer forKey:KEY_ANSWERER];
    }
    
    
    return params;
}

- (NSDictionary *)getParamsForAdvisorCountCall
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setObject:[_seekingAdvisor guruTags] forKey:KEY_SEARCH_TAGS];
    [params setObject:[_seekingAdvisor searchOptions] forKey:KEY_SEARCH_OPTIONS];
    [params setObject:[_seekingAdvisor getAsDictionary] forKey:KEY_SEARCH_ATTRIBUTES];
    
    NSDictionary * answerer = [NSDictionary dictionaryWithObjectsAndKeys:
                               [_seekingAdvisor list], KEY_LIST,
                               [_seekingAdvisor pageName], KEY_PAGENAME,
                               [_seekingAdvisor who], KEY_WHO,
                               nil];
    
    if (answerer.count > 0) {
        [params setObject:answerer forKey:KEY_ANSWERER];
    }
    
    return params;
}

- (void)cancelQuestion
{
    [Alert show:TITLE_ALERT_CONFIRMATION andMessage:MESSAGE_ALERT_CONFIRMATION cancelButtonTitle:@"Yes" otherButtonTitles:@"No" tag:ALERT_CANCEL_QUESTION andDelegate:self];
}

#pragma mark - Serice Calls

- (void)askQuestion
{
    [self showLoader];
    
    [[service question] askQuestionWithParams:[self getParamsForAskQuestionCall] withSuccess:^(id response)
     {
         [self askQuestionSucessful:response];
     }
                                   andfailure:^(NSError *error)
     {
         _hasServiceFailed = YES;
         
         [self serviceCallFailed:error];
     }];
}

- (void)getAdvisorCountForAskQuestion
{
    if ([[_seekingAdvisor who] isEqualToString:@"owner"]) {
        [self getAdvisorCountSucess:[NSNumber numberWithInt:1]];
    } else {
        [self showLoader];
        
        [[service question] getAdvisorCountForAskQuestion:[self getParamsForAdvisorCountCall] onSuccess:^(id response)
         {
             [self getAdvisorCountSucess:response];
         }
                                               andFailure:^(NSError *error)
         {
             _hasServiceFailed = YES;
             [self serviceCallFailed:error];
         }];
    }
    
}

#pragma mark - Service Call backs

- (void)askQuestionSucessful:(id)response
{
    [self hideLoader];
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Success!" message:[response get] delegate:self cancelButtonTitle:TITLE_CLOSE otherButtonTitles:TITLE_OFFER_ADVICE, TITLE_ASK_QUESTION, nil];
    alertview.tag = ALERT_SUCCESS_QUESTION;
    [alertview show];
    
    [AnalyticsHelper logEvent:self category:CATEGORY_QUESTION action:EVENT_SUBMIT];
}


- (void)getAdvisorCountSucess:(id)response
{
    [self hideLoader];
    
    _advisorCount = [NSString stringWithFormat:@"%@", response];
    
    [[self view] showAdvisorCountForConfirmation:_advisorCount];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == ALERT_CANCEL_QUESTION)
        [self handleCancelQuestionAlert:buttonIndex];
    else
        [self handleSubmitQuestionSuccessAlert:buttonIndex];
}

#pragma mark - Alert Action methods

-(void) handleCancelQuestionAlert:(NSInteger) buttonIndex {
    if (buttonIndex)
    {
        return;
    }
    
    if (_hasServiceFailed)
    {
        [self userLogOut];
        
        _hasServiceFailed = NO;
        return;
    }
    
    [self openHomeController];
}

-(void) handleSubmitQuestionSuccessAlert:(NSInteger) buttonIndex {
    if(buttonIndex == 0)
        [self openHomeController];
    if(buttonIndex == 1)
        [self openQuestionListing];
    else
        [self popToAskQuestion];
}

-(void) popToAskQuestion {
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if([controller isKindOfClass:[AskQuestionController class]]) {
            AskQuestionController *askController = (AskQuestionController *) controller;
            askController.clearQuestion = YES;
            [self.navigationController popToViewController:askController animated:YES];
        }
    }
}

-(void) openQuestionListing {
    [self.navigationController proceedToQuestionListWithRoleChange:YES];
}

@end
