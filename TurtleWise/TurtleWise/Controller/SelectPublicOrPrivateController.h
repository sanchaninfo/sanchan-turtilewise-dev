//
//  SelectPublicOrPrivateController.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"


@class TWWizardManager;
@interface SelectPublicOrPrivateController : BaseController

@property(nonatomic, strong) TWWizardManager *profileWizardManager;

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager;
- (void)next;

@end
