//
//  BaseProfileWizardController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@class TWWizardManager;

@interface BaseProfileWizardController : BaseController

@property(nonatomic, strong) TWWizardManager *profileWizardManager;

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager;
- (void)completeWizard;
- (void)next;


@end
