//
//  AnswerController.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AnswerController.h"
#import "AnswerView.h"
#import "Question.h"
#import "AnswerPagedResponse.h"
#import "NewCheckAdvisorController.h"

@interface AnswerController (){
    Question *_question;
    AnswerPagedResponse *_answerStats;
}

@property(nonatomic, strong) NSString *questionId;

- (void)getQuestionById;

@end

@implementation AnswerController

- (id)initWithQuestionId:(NSString *)questionId
{
    if (self == [super init])
    {
        _questionId = questionId;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super loadServices:@[@(ServiceTypeAnswer), @(ServiceTypeQuestion)]];
    [super viewDidLoad];
    
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setTitle:THEIR_ANSWER_SCREEN_TITLE];
    
    if (_questionId)
    {
        [self getQuestionById];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForSeeker];
}

-(void)setQuestion:(Question*)question{
    _question = question;
    [(AnswerView*)self.view setQuestion:_question];
}
-(void)setIsArchived:(BOOL)isArchived
{
    _isArchived = isArchived;
    [(AnswerView*)self.view setArchived:_isArchived];

}

#pragma mark - Load Answer Meta

-(void)loadAnswerMeta{
    
    if (!_question)
        return;
    
    _answerStats = [AnswerPagedResponse new];
    
    [service.answer getAnswersForQuestion:_question.questionId andParams:[self getParamsForServiceCall:@""] onSuccess:^(id response) {
        _answerStats.questionID = _question.questionId;
        [_question setIsSubscribed:[_answerStats isUserSubscribed]];
        [(AnswerView*)self.view setAnswerOptions:_answerStats.answerOptions andTotalCount:_answerStats.totalCount];
    } andFailure:^(NSError *error) {
        [(AnswerView*)self.view showError:[error localizedDescription]];
    } response:_answerStats];

}
-(NSDictionary*)getParamsForServiceCall:(NSString*)searchOption{
    return @{KEY_PAGE_SIZE : [NSNumber numberWithInt:_answerStats.pageSize],
             KEY_SKIP : [NSNumber numberWithInt:_answerStats.skip],
             KEY_ANSWERS_SEARCH_OPTION : searchOption};
}

#pragma mark - Navigation
-(void)goToAdvisorListingControllerWithSearchOption:(NSInteger)option{
    NewCheckAdvisorController *controller = [NewCheckAdvisorController new];
    [controller setTabBarDelegate:[[(AnswerView*)self.view tabBar] delegate]];
    [controller setQuestion:_question];
    [controller setAnswerMeta:_answerStats andSelectTab:option];
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Service Calls

- (void)getQuestionById
{
    [self showLoader];
    
    [[service question] getQuestionForId:_questionId withSuccess:^(id response)
    {
        [self hideLoader];
        
        _question = (Question *)response;
        
        [(AnswerView*)self.view setQuestion:_question];
 
        [self loadAnswerMeta];
    }
     andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

-(void)archiveQuestionById
{
    [self showLoader];
    
    [[service question]archiveQuestion:_question.questionId WithParams:nil withSuccess:^(id response) {
        [self hideLoader];
        NSString *message = response[@"data"][@"message"];
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Success" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [controller addAction:action];
        [self presentViewController:controller animated:YES completion:nil];
    } andfailure:^(NSError *error) {
         [self serviceCallFailed:error];
    }];
}

@end
