//
//  AskQuestionController.m
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AdvisorSelectionController.h"
#import "AskQuestionController.h"
#import "HomeController.h"
#import "AskQuestionView.h"

@interface AskQuestionController ()
@property(nonatomic, strong) NSMutableDictionary *answerer;
@end

@implementation AskQuestionController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:ASK_SCREEN_TITLE];

    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [self popViewController];
    }];
    
    [self setupRightNavigationButtonWithName:LEFT_BUTTON_TITLE withOnClickAction:^{
        [self popViewController];
    }];
    
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor blackColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForSeeker];
    if(_clearQuestion) {
        [(AskQuestionView *) self.view clearQuestion];
        _clearQuestion = NO;
    }
}

- (id)initWithAnswereDictionary:(NSMutableDictionary*)dic {
    if (self = [super init])
    {
        _answerer = dic;
    }
    
    return self;
}

#pragma mark - Navigation

- (void)startAdvisorSelectionForQuestion:(NSString *)question;
{
    AdvisorSelectionController *questionTypeController = [[AdvisorSelectionController alloc] initWithQuestion:question andAnswerer:_answerer];
    
    [self.navigationController pushViewController:questionTypeController animated:YES];
}

@end
