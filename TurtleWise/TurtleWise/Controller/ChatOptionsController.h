//
//  ChatOptionsController.h
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
@class Conversation;
@interface ChatOptionsController : BaseController

-(void)setConversation:(Conversation*)conversation;
-(void)endChat;
-(void)showUserProfileController:(NSString*)advisorId;
-(void)addAdvisorWith:(NSString*)advisorID;

@end
