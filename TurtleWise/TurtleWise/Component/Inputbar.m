//
//  Inputbar.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/24/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//


#import "Inputbar.h"
#import "HPGrowingTextView.h"

#define RIGHT_BUTTON_SIZE 45
#define LEFT_BUTTON_SIZE 45

#define SEND_BUTTON_NORMAL_IMAGE @"send-message-btn-normal"
#define SEND_BUTTON_SELECTED_IMAGE @"send-message-btn-disabled"

@interface Inputbar() <HPGrowingTextViewDelegate>

@property(nonatomic, strong) HPGrowingTextView *textView;
@property(nonatomic, strong) UIButton *rightButton;
@property(nonatomic, strong) UIButton *leftButton;

@end


@implementation Inputbar

#pragma mark - Life Cycle Methods
- (id)init
{
    self = [super init];
    if (self)
    {
        [self addContent];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        [self addContent];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self addContent];
    }
    return self;
}

#pragma mark Creating View

- (void)addContent
{
    [self addTextView];
    [self addRightButton];
    
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

- (void)addTextView
{
    CGSize size = [self frame].size;
    _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(15,
                                                                    5,
                                                                    size.width - RIGHT_BUTTON_SIZE - 35,
                                                                    size.height)];
    [_textView setIsScrollable:NO];
    [_textView setContentInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    
    [_textView setMinNumberOfLines:1];
    [_textView setMaxNumberOfLines:6];

    [_textView setPlaceholder:@"Type a message..."];
    
    [_textView setFont:[UIFont systemFontOfSize:15.0f]];
    [_textView setDelegate:self];
    [[_textView internalTextView] setScrollIndicatorInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    [_textView setBackgroundColor:[UIColor whiteColor]];
    
    [_textView setKeyboardType:UIKeyboardTypeDefault];
    [_textView setReturnKeyType:UIReturnKeyDefault];
    [_textView setEnablesReturnKeyAutomatically:YES];
    
    [[_textView layer] setCornerRadius:5.0];
    [[_textView layer] setBorderWidth:0.5];
    [[_textView layer] setBorderColor:[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:205.0/255.0 alpha:1.0].CGColor];
    
    _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self addSubview:_textView];
}

- (void)addRightButton
{
    CGSize size = [self frame].size;
    
    _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(size.width - RIGHT_BUTTON_SIZE - 5, 7, 30.0, 30.0)];
    
    [_rightButton setImageEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [_rightButton setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin];
    [_rightButton setSelected:YES];
    [_rightButton addTarget:self action:@selector(didPressRightButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [_rightButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_NORMAL_IMAGE] forState:UIControlStateNormal];
    [_rightButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_SELECTED_IMAGE] forState:UIControlStateSelected];
    
    [self addSubview:_rightButton];
}

#pragma mark - Helper Methods

- (NSString *)text
{
    return [_textView text];
}

- (void)dismissTextField
{
    [_textView resignFirstResponder];
}

#pragma mark - Delegate

- (void)didPressRightButton:(UIButton *)sender
{
    if (self.rightButton.isSelected) return;
    
    if (_customDelegate && [_customDelegate respondsToSelector:@selector(inputbarDidPressRightButton:)])
    {
        [self.customDelegate inputbarDidPressRightButton:self];
        [_textView setText:@""];
    }
}

#pragma mark - TextViewDelegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
    CGRect r = self.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    self.frame = r;
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(inputbarDidChangeHeight:)])
    {
        [self.customDelegate inputbarDidChangeHeight:self.frame.size.height];
    }
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
    NSString *text = [growingTextView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([text isEqualToString:@""])
        [self.rightButton setSelected:YES];
    else
        [self.rightButton setSelected:NO];
}


@end
