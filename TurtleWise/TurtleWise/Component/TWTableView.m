//
//  TWTableView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/28/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "ProfileAttributeCell.h"
#import "OrderedDictionary.h"
#import "NSArray+Utils.h"
#import "StringUtils.h"
#import "TWTableView.h"
#import "TextField.h"

#define CELL_IDENTIFIER @"ProfileAttributeCell"

#define ROW_HEIGHT 77.0
#define MAX_ROWS_ALLOWED 5
#define MIN_ROWS_VISIBLE 3

@interface TWTableView () < UITableViewDataSource, UITableViewDelegate, ProfileAttributeCellDelegate >

@property(nonatomic, strong) MutableOrderedDictionary *dataSet;
@property(nonatomic, assign) NSInteger currentRows;

- (void)disableAddingMoreFields;

@end

@implementation TWTableView

#pragma mark - Life cycle Methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initalize];
}

- (void)initalize
{
    _dataSet = [MutableOrderedDictionary new];
    
    _minRowsVisible = MIN_ROWS_VISIBLE;
    _maxRowsAllowed = MAX_ROWS_ALLOWED;
    
    [self setDelegate:self];
    [self setDataSource:self];
}

- (void)addRow
{
    _currentRows += 1;
    
    [self insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_currentRows - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    [self disableAddingMoreFields];
}

- (void)reset
{
    [_dataSet removeAllObjects];
    
    [self reloadData];
}

#pragma mark - Data Set Management

- (void)prepareDataSet:(NSArray *)inputData
{
    if (!inputData)
    {
        _currentRows = _minRowsVisible;
        
        return;
    }
    
    _currentRows = 0;
    
    [_dataSet removeAllObjects];
    
    [inputData enumerateObjectsUsingBlock:^(NSString *object, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if (![StringUtils isEmptyOrNull:object])
         {
             [_dataSet setObject:object forKey:[NSIndexPath indexPathForRow:idx inSection:0]];
             
             _currentRows += 1;
         }
     }];
    
    if (_currentRows < _minRowsVisible)
    {
        _currentRows = _minRowsVisible;
    }
    
    [self disableAddingMoreFields];
    [self reloadData];
}

- (NSArray *)getDataSetAsArray
{
    NSMutableArray *arrayDataSet = [NSMutableArray new];
    
    [[_dataSet allValues] enumerateObjectsUsingBlock:^(NSString *object, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if (![StringUtils isEmptyOrNull:object])
         {
             [arrayDataSet addObject:object];
         }
     }];
    
    return arrayDataSet;
}

#pragma mark - Helper Methods

- (void)disableAddingMoreFields
{
    if (!_addMoreButton)
    {
        return;
    }
    
    if (_currentRows == _maxRowsAllowed)
    {
        [_addMoreButton setAlpha:0.5];
        [_addMoreButton setUserInteractionEnabled:NO];
    }
}

#pragma mark - TableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _currentRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileAttributeCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:CELL_IDENTIFIER owner:self options:nil];
        
        cell = (ProfileAttributeCell *) [topLevelObjects objectAtIndex:0];
        [cell setDelegate:self];
    }
    
    if ([_dataSet objectForKey:indexPath])
    {
        [cell set:[_dataSet objectForKey:indexPath]];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}

#pragma mark - Profile Attribute Cell Delegate

- (void)saveDataForCell:(ProfileAttributeCell *)cell
{
    NSIndexPath *cellIndexPath = [self indexPathForCell:cell];
    
    if (cellIndexPath)
    {
        [_dataSet setObject:[[cell textField] text] forKey:cellIndexPath];
    }
}

@end
