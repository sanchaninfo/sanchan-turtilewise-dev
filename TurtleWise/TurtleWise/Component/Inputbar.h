//
//  Inputbar.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/24/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InputbarDelegate;

@interface Inputbar : UIToolbar

@property(nonatomic, weak) id<InputbarDelegate>customDelegate;

- (NSString *)text;
- (void)dismissTextField;

@end

@protocol InputbarDelegate <NSObject>

@optional

- (void)inputbarDidPressRightButton:(Inputbar *)inputbar;
- (void)inputbarDidChangeHeight:(CGFloat)newHeight;

@end
