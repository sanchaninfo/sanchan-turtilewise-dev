//
//  TWTableView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/28/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "TPKeyboardAvoidingTableView.h"
#import <UIKit/UIKit.h>


@interface TWTableView : TPKeyboardAvoidingTableView

@property(nonatomic, weak) UIButton *addMoreButton;
@property(nonatomic, assign) NSInteger minRowsVisible;
@property(nonatomic, assign) NSInteger maxRowsAllowed;

- (void)addRow;
- (void)prepareDataSet:(NSArray *)inputData;
- (void)reset;
- (NSArray *)getDataSetAsArray;

@end
