//
//  TWChatTableView.h
//  TurtleWise
//
//  Created by Waleed Khan on 3/24/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatMessage;
@protocol TWChatTableViewDelegate;

@interface TWChatTableView : UITableView

@property(nonatomic, weak) id<TWChatTableViewDelegate> viewDelegate;

- (void)scrollToBottomAnimated:(BOOL)animated;
- (void)prepareChatMessages:(NSArray *)chatMessages;
- (void)addMessage:(ChatMessage *)message;
- (void)addActivityIndicator;
- (void)updateMessage:(ChatMessage *)message;

@end

@protocol TWChatTableViewDelegate <NSObject>

- (void)loadMoreMessages;

@end