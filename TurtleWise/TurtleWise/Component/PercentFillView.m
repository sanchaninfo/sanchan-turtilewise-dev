//
//  PercentFillView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/14/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "PercentFillView.h"
#import "HexColor.h"

#define COLOR_LIGHT_GREY @"#CCCCCC"
#define COLOR_DARK_GREY @"#333333"

@implementation PercentFillView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

-(void)drawRect:(CGRect)rect
{
    float percentDone = _fillPercentage / 100.0;
    
    CGRect upperRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width * percentDone, rect.size.height );
    CGRect lowerRect = CGRectMake(rect.origin.x + (rect.size.width * percentDone), rect.origin.y , rect.size.width *(1-percentDone), rect.size.height );
    
    [[HXColor colorWithHexString:COLOR_DARK_GREY] set];
    UIRectFill(upperRect);
    [[HXColor lightGrayColor] set];
    UIRectFill(lowerRect);
}

@end
