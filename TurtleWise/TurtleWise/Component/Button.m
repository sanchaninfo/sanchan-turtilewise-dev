//
//  Button.m
//  Guardian
//
//  Created by mohsin on 10/18/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import "Button.h"
#import "Constant.h"

@implementation Button

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    [[self titleLabel] setAdjustsFontSizeToFitWidth:YES];
}

@end
