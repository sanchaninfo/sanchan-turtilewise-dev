//
//  QuestionTypeView.m
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TPKeyboardAvoidingScrollView.h"
#import "AdvisorSelectionController.h"
#import "AdvisorSelectionView.h"
#import "TWWizardManager.h"
#import "SeekingAdvisor.h"
#import "TLTagsControl.h"
#import "UserDefaults.h"
#import "Constant.h"
#import "Utility.h"
#import "Color.h"
#import "Font.h"

#define ALERT_MESSAGE_MISSING_ADVISOR @"Please select Guru Type."
#define ALERT_MESSAGE_MISSING_QUESTION_TYPE @"Please select Question Type."
#define ALERT_MESSAGE_MISSING_MCQ_OPTIONS @"Please select atleast 2 MCQ Options."
#define ALERT_MESSAGE_MISSING_SEARCH_OPTIONS @"Profile attributes are required when \"Custom\" Guru option is selected."
#define MESSAGE_ALERT_CONFIRMATION @"Your Question will be sent to %@ Gurus!\n\nYou can expand your field of Gurus by clicking cancel to modify your search criteria."

#define QUESTION_TYPE_COMPLEX @"complex"

#define ADVISOR_TYPE_BUTTONS_START_TAG 200
#define ADVISOR_TYPE_BUTTONS_END_TAG 202

#define QUESTION_TYPE_BUTTONS_START_TAG 300
#define QUESTION_TYPE_BUTTONS_END_TAG 302

@interface AdvisorSelectionView () < TLTagsControlDelegate >

@property(nonatomic, strong) NSString *advisorSearchOption;
@property(nonatomic, strong) NSString *questionType;

@property(nonatomic, assign) BOOL isAdvisorTypeLikeMe;

@property(nonatomic, readonly) AdvisorSelectionController *controller;

- (void)resetAttributeTags;
- (void)updateWizardTags;
- (void)confirgureTagsView;
- (void)selectButton:(NSInteger)senderButtonTag fromStartTag:(int)startTag toEndTag:(int)endTag;

- (BOOL)isAdvisorTypeSelected;
- (BOOL)isQuestionTypeSelectd;
- (void)saveSelections;

@end


@implementation AdvisorSelectionView

@dynamic controller;

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [self confirgureTagsView];
}

- (void)viewWillAppear
{
    [self updateWizardTags];
}


- (void)viewWillDisappear
{
    [self endEditing:YES];
}

#pragma mark - Tag Managment Methods

- (void)confirgureTagsView
{
    [_attributtesTagView setControlDelegate:self];
    [_attributtesTagView setMode:TLTagsControlModeList];
    
    [_guruTagsView setControlDelegate:self];
    [_guruTagsView setTagPlaceholder:GURU_TAG_PLACEHOLDER];
    [_guruTagsView setMode:TLTagsControlModeEdit];
}

- (void)updateWizardTags
{
    [_attributtesTagView setTags:[(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] attributeHashTags]];
    [_attributtesTagView reload];
}

- (void)resetAttributeTags
{
    [(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] resetAllTags];
    [_attributtesTagView reload];
}

#pragma mark - Validations

- (BOOL)isAdvisorTypeSelected;
{
    return [self isOptionSelectedFrom:ADVISOR_TYPE_BUTTONS_START_TAG to:ADVISOR_TYPE_BUTTONS_END_TAG];
}

- (BOOL)isQuestionTypeSelectd
{
    return [self isOptionSelectedFrom:QUESTION_TYPE_BUTTONS_START_TAG to:QUESTION_TYPE_BUTTONS_END_TAG];
}

- (BOOL)mcqOptionsAvailable
{
    if (![_questionType isEqualToString:QUESTION_TYPE_MCQ])
    {
        return YES;
    }
    
    return [[(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] questionOptions] count] >= 2;
}

- (BOOL)searchOptionsAdded
{
    return [[(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] getAsDictionary] count] > 0;
}

- (BOOL)isAdvisorTypeCustom
{
    return [(UIButton *)[self viewWithTag:ADVISOR_TYPE_BUTTONS_END_TAG] isSelected];
}

- (void)setupHiddenUI {
    isOwner = YES;
    _viewToQue.priority = 500;
    _seekToQue.priority = 999;
    _guruView.alpha = 0;
}

- (void)setupShowUI {
    isOwner = NO;
    _viewToQue.priority = 999;
    _seekToQue.priority = 500;
    _guruView.alpha = 100;
}

#pragma mark - Action Handlers

- (IBAction)onSeekAdvice:(id)sender //TODO: REFACTOR THIS.
{
    if (![self isAdvisorTypeSelected] && !isOwner)
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:ALERT_MESSAGE_MISSING_ADVISOR];
        return;
    }
    
    if (![self isQuestionTypeSelectd])
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:ALERT_MESSAGE_MISSING_QUESTION_TYPE];
        return;
    }
    
    if (![self mcqOptionsAvailable])
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:ALERT_MESSAGE_MISSING_MCQ_OPTIONS];
        return;
    }
    
    if ([self isAdvisorTypeCustom] && ![self searchOptionsAdded])
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:ALERT_MESSAGE_MISSING_SEARCH_OPTIONS];
        return;
    }
    [self getAdvisorCountForAskQuestion];
}

- (void)getAdvisorCountForAskQuestion
{
    SeekingAdvisor *seekingAdvisor = [[SeekingAdvisor alloc] initWithUserProfile:[UserDefaults getUserProfile]];

    if ([[seekingAdvisor who] isEqualToString:@"owner"]) {
        [self  getAdvisorCountSucess:[NSNumber numberWithInt:1]];
    } else {
        [[self controller] showLoader];
        
        QuestionService *question = [[QuestionService alloc]init];
        [question getAdvisorCountForAskQuestion:[self  getParamsForAdvisorCountCall] onSuccess:^(id response)
         {
             [self  getAdvisorCountSucess:response];
         }
                                               andFailure:^(NSError *error)
         {
             [[self controller] serviceCallFailed:error];
         }];
    }
    
}
-(void)getAdvisorCountSucess:(id)count
{
    [[self controller] hideLoader];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Your question will be sent to %@ Gurus!\n\nYou can expand your field of Gurus by clicking cancel to modify your search criteria.",count] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *continueAction = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self saveSelections];
        [[self controller] saveGuruTags:[_guruTagsView tags]];
        [[self controller] proceedToSubmitQuestion];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:continueAction];
    [alertController addAction:cancelAction];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}
- (NSDictionary *)getParamsForAdvisorCountCall
{
    SeekingAdvisor *seekingAdvisor = [[SeekingAdvisor alloc] initWithUserProfile:[UserDefaults getUserProfile]];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setObject:[seekingAdvisor guruTags] forKey:KEY_SEARCH_TAGS];
    if([seekingAdvisor searchOptions] != nil)
        [params setObject:[seekingAdvisor searchOptions] forKey:KEY_SEARCH_OPTIONS];
    else
        [params setObject:@"anyone" forKey:KEY_SEARCH_OPTIONS];
    [params setObject:[seekingAdvisor getAsDictionary] forKey:KEY_SEARCH_ATTRIBUTES];
    
    NSDictionary * answerer = [NSDictionary dictionaryWithObjectsAndKeys:
                               [seekingAdvisor list], KEY_LIST,
                               [seekingAdvisor pageName], KEY_PAGENAME,
                               [seekingAdvisor who], KEY_WHO,
                               nil];
    if (answerer.count > 0) {
        [params setObject:answerer forKey:KEY_ANSWERER];
    }
    
    return params;
}


- (IBAction)setAdvisorType:(id)sender
{
    NSInteger senderTag = [sender tag];
    
    _advisorSearchOption = [[sender titleLabel] text];
    
    [self selectButton:senderTag fromStartTag:ADVISOR_TYPE_BUTTONS_START_TAG toEndTag:ADVISOR_TYPE_BUTTONS_END_TAG];
    
    switch (senderTag)
    {
        case ADVISOR_TYPE_BUTTONS_START_TAG:
                [self chooseSomeOneLikeMeAdvisor];
            break;
            
        case ADVISOR_TYPE_BUTTONS_END_TAG:
                [self chooseCustomAdvisor];
            break;
            
        default:
                [self chooseAnyAdvisor];
            break;
    }
}

- (IBAction)selectQuestionType:(id)sender
{
    NSInteger senderTag = [sender tag];

    [self selectButton:senderTag fromStartTag:QUESTION_TYPE_BUTTONS_START_TAG toEndTag:QUESTION_TYPE_BUTTONS_END_TAG];
    
    _questionType = [@[QUESTION_TYPE_MCQ, QUESTION_TYPE_COMPLEX, QUESTION_TPYE_YES_NO] objectAtIndex:senderTag % 300];
    
    if (senderTag == QUESTION_TYPE_BUTTONS_START_TAG)
    {
        [[self controller] proceedToSelectChoicesForMCQ];
        
        return;
    }
    
    [[self controller] clearMCQOptionsController];
}

- (IBAction)addGuruTagButton:(UIButton *)sender
{
    [_guruTagsView addTag];
}

#pragma mark - IBAction Helpers

- (void)chooseSomeOneLikeMeAdvisor
{
    _isAdvisorTypeLikeMe = YES;
    
    SeekingAdvisor *previousValue = (SeekingAdvisor *)[[[self controller] profileWizardManager] profile];
    SeekingAdvisor *seekingAdvisor = [[SeekingAdvisor alloc] initWithUserProfile:[UserDefaults getUserProfile]];
    
    [seekingAdvisor setQuestion:[previousValue question]];
    [seekingAdvisor setQuestionOptions:[previousValue questionOptions]];
    
    [[[self controller] profileWizardManager] setProfile:seekingAdvisor];
    
    [_attributtesTagView setTags:[seekingAdvisor attributeHashTags]];
    [_attributtesTagView reload];
}

- (void)chooseCustomAdvisor
{
    if (_isAdvisorTypeLikeMe)
    {
        [self resetAttributeTags];
        
        _isAdvisorTypeLikeMe = NO;
    }
    
    [[self controller] startWizardForAdvisorAttributes];
}

- (void)chooseAnyAdvisor
{
    _isAdvisorTypeLikeMe = NO;
    
    [self resetAttributeTags];
}

#pragma mark - Helper Methods

- (BOOL)isOptionSelectedFrom:(int)startOption to:(int)endOption
{
    for (int i = startOption; i <= endOption; i++)
    {
        UIButton *iterateButton = (UIButton *)[self viewWithTag:i];
        
        if ([iterateButton isSelected])
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)saveSelections
{    
    [(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] setSearchOptions:[[_advisorSearchOption lowercaseString]
                                                                                            stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    [(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] setQuestionType:_questionType];
}

- (void)selectButton:(NSInteger)senderButtonTag fromStartTag:(int)startTag toEndTag:(int)endTag
{
    for (int i = startTag; i <= endTag; i++)
    {
        if (i == senderButtonTag)
        {
            UIButton *sender = (UIButton *)[self viewWithTag:senderButtonTag];
            [sender setSelected:YES];
            
            continue;
        }
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

#pragma mark - TagsView Delegate

- (void)tagRemoved:(NSString *)removedTag with:(TLTagsControl *)tagsControl
{
    _isAdvisorTypeLikeMe = NO;
    
    if (tagsControl == _attributtesTagView)
    {
        UIButton *someOneLikeMeButton = (UIButton *)[self viewWithTag:ADVISOR_TYPE_BUTTONS_START_TAG];
        if (someOneLikeMeButton.isSelected)
        {
            [someOneLikeMeButton setSelected:NO];
            UIButton *customButton = (UIButton *)[self viewWithTag:ADVISOR_TYPE_BUTTONS_END_TAG];
            [customButton setSelected:YES];
            
            _advisorSearchOption = [[customButton titleLabel] text];
        }
        
        [(SeekingAdvisor *)[[[self controller] profileWizardManager] profile] resetValueForTag:removedTag];
    }
}

- (void)getSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray *))successBlock
{
    [(AdvisorSelectionController *)self.controller requestSuggestedTagsFor:tag success:^(NSArray *tags) {
        if (successBlock) {
            successBlock(tags);
        }
    }];
}

-(void)tagsControlRequestToSave:(NSString *)tag
{
    [(AdvisorSelectionController *)self.controller requestToSaveTag:tag];
}

@end
