//
//  UserTagsView.m
//  TurtleWise
//
//  Created by Waleed Khan on 1/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TPKeyboardAvoidingScrollView.h"
#import "UserTagsController.h"
#import "TLTagsControl.h"
#import "UserTagsView.h"
#import "UserProfile.h"
#import "Constant.h"
#import "Color.h"

#define EXPLORER_TAG_PLACEHOLDER @"Explorer Tag"
@interface UserTagsView () < TLTagsControlDelegate >

- (void)confirgureTagsView;

- (IBAction)addExplorerTagButton:(UIButton *)sender;
- (IBAction)addGuruTagButton:(UIButton *)sender;
@end
@implementation UserTagsView

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self confirgureTagsView];
}

- (void)confirgureTagsView
{
    [_explorerTagsView setMode:TLTagsControlModeEdit];
    [_explorerTagsView setTagPlaceholder:EXPLORER_TAG_PLACEHOLDER];
    [_explorerTagsView setControlDelegate:self];
    
    [_guruTagsView setMode:TLTagsControlModeEdit];
    [_guruTagsView setTagPlaceholder:GURU_TAG_PLACEHOLDER];
    [_guruTagsView setControlDelegate:self];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_explorerTagsView setTags:[NSMutableArray arrayWithArray:[profile explorerTags]]];
    [_explorerTagsView reload];
    
    [_guruTagsView setTags:[NSMutableArray arrayWithArray:[profile guruTags]]];
    [_guruTagsView reload];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setExplorerTags:[_explorerTagsView tags]];
    [profile setGuruTags:[_guruTagsView tags]];
}


#pragma mark - TagsView Delegate

- (void)getSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray *))successBlock
{
    [(UserTagsController *)self.controller requestSuggestedTagsFor:tag success:^(NSArray *tags) {
        if (successBlock) {
            successBlock(tags);
        }
    }];
}

- (void)tagsControlRequestToSave:(NSString *)tag
{
    [(UserTagsController *)self.controller requestToSaveTag:tag];
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)addExplorerTagButton:(UIButton *)sender
{
    [_explorerTagsView addTag];
}
- (IBAction)addGuruTagButton:(UIButton *)sender
{
    [_guruTagsView addTag];
}
@end
