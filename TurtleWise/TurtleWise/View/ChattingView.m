//
//  ChattingView.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "ChattingController.h"
#import "DAKeyboardControl.h"
#import "TWChatTableView.h"
#import "ChattingView.h"
#import "UserDefaults.h"
#import "ChatMessage.h"
#import "DateUtils.h"
#import "Inputbar.h"
#import "HexColor.h"

#define BLUE_BUTTON_COLOR @"#0079C8"

@interface ChattingView () <InputbarDelegate, TWChatTableViewDelegate>

@property(nonatomic, strong) NSMutableArray *chatMessages;

- (void)addKeyboardPanning;
- (void)updateInputBarFrame:(CGRect)keyboardFrame;
- (void)updateContainerViewBottomConstraint:(CGFloat)bottomConstraint;
- (void)keyboardOpneing:(CGRect)keyboardFrame;
- (void)keyboardClosing:(CGRect)keyboardFrame;

@end

@implementation ChattingView

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{ 
    [_inputBar setCustomDelegate:self];
    [_tableViewChat setViewDelegate:self];
   
    _chatMessages = [NSMutableArray new];
    
    [self addKeyboardPanning];
}

- (void)setChatMessages:(NSMutableArray *)chatMessages
{
    [chatMessages removeObjectsInArray:_chatMessages];
    
    [_tableViewChat prepareChatMessages:chatMessages];
    [_chatMessages addObjectsFromArray:chatMessages];
}

#pragma mark - IBActions

- (IBAction)goToRateAdvisors:(id)sender
{
    [(ChattingController *)[self controller] proceedToRateAdvisors];

    [_btnRateAdvisors setHidden:YES];
}

#pragma mark - Helper Methods

- (void)markChatClosed
{
    [_inputBar setHidden:YES];
}

- (void)markChatUnrated
{
    [_btnRateAdvisors setHidden:NO];
}

#pragma mark - Keyboard Handling Methods

- (void)addKeyboardPanning
{
    __weak ChattingView *weakSelf = self;
    
    [self setKeyboardTriggerOffset:[_inputBar frame].size.height];
    [self addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing)
     {
         [weakSelf updateInputBarFrame:keyboardFrameInView];
         
         if (opening)
         {
             [weakSelf keyboardOpneing:keyboardFrameInView];
         }
         
         if (closing)
         {
             [weakSelf keyboardClosing:keyboardFrameInView];
         }
     }];
}

- (void)updateInputBarFrame:(CGRect)keyboardFrame
{
    CGRect toolBarFrame = [_inputBar frame];
    toolBarFrame.origin.y = keyboardFrame.origin.y - toolBarFrame.size.height;
    [_inputBar setFrame:toolBarFrame];
}

- (void)updateContainerViewBottomConstraint:(CGFloat)bottomConstraint
{
    [_containerViewBottomConstraint setConstant:bottomConstraint];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)keyboardOpneing:(CGRect)keyboardFrame
{
    [self updateContainerViewBottomConstraint:keyboardFrame.size.height];
 
    [_tableViewChat scrollToBottomAnimated:YES];
}

- (void)keyboardClosing:(CGRect)keyboardFrame
{
    [self updateContainerViewBottomConstraint:0];
}

#pragma mark - Helper Methods

- (void)enableActivityIndicator:(BOOL)shouldEnable
{
    if (shouldEnable)
    {
        [_tableViewChat addActivityIndicator];
        
        return;
    }
    
    [_tableViewChat setTableHeaderView:NULL];
}

- (void)inputbarResign
{
    [_inputBar dismissTextField];
}

- (void)updateChatForNewMessage:(ChatMessage *)newChatMessage
{
    [_tableViewChat updateMessage:newChatMessage];
}

#pragma mark - InputBar Delegate Methods

- (void)inputbarDidChangeHeight:(CGFloat)newHeight
{
    [_inputBarHeightConstraint setConstant:newHeight];
    self.keyboardTriggerOffset = newHeight;
    
    [self updateConstraints];
}

- (void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    ChatMessage *chatMessage = [ChatMessage new];
    
    [chatMessage setMessageDate:[NSDate new]];
    [chatMessage setMessage:[inputbar text]];
    [chatMessage setIsMyMessage:YES];
    [chatMessage setMessageStatus:ChatMessageStatusSending];
    [chatMessage setMessageIdBeforeSending:[NSString stringWithFormat:@"%@", [DateUtils getUnixTimeStamp:[NSDate new]]]];
    
    [chatMessage setMessageSender:[MessageSender new]];
    [[chatMessage messageSender] setUserRole:[UserDefaults getUserRole]];
    
    [_tableViewChat addMessage:chatMessage];
    
    [(ChattingController *)self.controller sendMessage:[chatMessage message] withTemporaryId:[chatMessage messageIdBeforeSending]];
}

#pragma mark - TWChatTableView Delegate Methods

- (void)loadMoreMessages
{
    [(ChattingController *)self.controller loadMoreMessages];
}

@end
