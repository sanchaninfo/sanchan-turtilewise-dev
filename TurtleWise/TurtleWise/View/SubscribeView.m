//
//  SubscribeView.m
//  TurtleWise
//
//  Created by Irfan Gul on 09/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubscribeController.h"
#import "SubscribeView.h"

@implementation SubscribeView

#pragma mark - IBActions

- (IBAction)buyMonthlySubscription:(id)sender
{
    [(SubscribeController *)self.controller buyMonthlySubscription];
}

- (IBAction)buyThisQuestionSubscription:(id)sender
{
    [(SubscribeController *)self.controller buyThisQuestionSubscription];
}

#pragma mark - Helper Methods

- (void)hideSingleQuestionSubscription
{
    [_btnSingleQuestionSubscription setHidden:YES];
}

@end
