//
//  FollowRequestsView.m
//  TurtleWise
//
//  Created by Sunflower on 9/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "FollowRequestsView.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "FollowRequestCell.h"
#import "StringUtils.h"
#import "ManageBrandedPageView.h"

@interface FollowRequestsView (){
    NSArray *requests;
}
@end

@implementation FollowRequestsView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UINib *nib = [UINib nibWithNibName:@"FollowRequestsView" bundle:nil];
        NSArray *array = [nib instantiateWithOwner:nil options:nil];
        self = [array lastObject];
        self.frame = frame;
    }
    return self;
}

- (void) getData:(NSArray*)req {
    requests = req;
    [self setupTableView];
}

-(void)setupTableView{
    [self.requestTableView setEstimatedRowHeight:93.0];
    [self.requestTableView setRowHeight:UITableViewAutomaticDimension];
    [self.requestTableView registerNib:[UINib nibWithNibName:[FollowRequestCell cellName]bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[FollowRequestCell cellName]];
    [_requestTableView reloadData];
}

#pragma mark - UITableView Delegate/DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return requests.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FollowRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:[FollowRequestCell cellName]];
    NSDictionary* attributes = [ParserUtils object:[requests objectAtIndex:indexPath.row] key:KEY_ATTRIBUTES];
    NSString *firstName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_FIRST_NAME]];
    NSString *lastName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_LAST_NAME]];
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    [cell set:fullName];
    
    cell.acceptButton.tag = (NSInteger)[ParserUtils stringValue:[requests objectAtIndex:indexPath.row] key:KEY_USER_ID];
    cell.denyButton.tag = (NSInteger)[ParserUtils stringValue:[requests objectAtIndex:indexPath.row] key:KEY_USER_ID];
    
    [cell.acceptButton addTarget:(ManageBrandedPageView*)self.superview action:NSSelectorFromString(@"acceptTapped:") forControlEvents:UIControlEventTouchUpInside];
    [cell.denyButton addTarget:(ManageBrandedPageView*)self.superview action:NSSelectorFromString(@"denyTapped:") forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 5)];
    header.backgroundColor = [UIColor whiteColor];
    return header;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (IBAction)closeTapped:(id)sender {
    [self removeFromSuperview];
}

@end
