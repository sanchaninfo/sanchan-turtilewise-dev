//
//  HomeView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/4/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class Label;
@class UserStats;

@interface HomeView : BaseView

//Properties and Outlets
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLevel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet UIButton *btnAskQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnUserRoleChange;
@property (weak, nonatomic) IBOutlet UIButton *btnUserMode;

@property (weak, nonatomic) IBOutlet UILabel *questionBubble;
@property (weak, nonatomic) IBOutlet UILabel *answerBubble;
@property (weak, nonatomic) IBOutlet UILabel *chatBubble;

@property (weak, nonatomic) IBOutlet UILabel *lblTurtlePoints;
@property (weak, nonatomic) IBOutlet UILabel *lblTurtleBucks;

@property (weak, nonatomic) IBOutlet UILabel *lblThumbsUp;
@property (weak, nonatomic) IBOutlet UILabel *lblThumbsDown;
@property (weak, nonatomic) IBOutlet UILabel *lblThanks;

- (void)updateViewForUserRoleChange:(UserRole)userRole;
- (void)updateViewForUserProfile:(UserProfile *)profile;
- (void)checkForUnratedChats;
- (void)markUserPremium;
- (void)showSubscriptionEndedAlert;


//Actions
- (IBAction)askQuestion:(id)sender;
- (IBAction)changeUserRole:(id)sender;
- (IBAction)showQuestionList:(id)sender;
- (IBAction)showChatList:(id)sender;
- (IBAction)showTurtleBucksInfoView:(id)sender;
- (IBAction)showSubscriptionModel:(id)sender;

@end
