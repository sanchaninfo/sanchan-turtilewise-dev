//
//  RegistrationView.m
//  iOSTemplate
//
//  Created by mohsin on 11/5/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "RegistrationController.h"
#import "RegistrationView.h"
#import "FacebookManager.h"
#import "LoginController.h"
#import "StringUtils.h"
#import "TextField.h"
#import "Alert.h"
#import "Font.h"

#define ERROR_MESSAGE_PASSWORD_DONOT_MATCH @"Password do not match."
#define ERROR_MESSAGE_IN_VALID_PASSWORD @"Password must be between 6 to 32 characters."

#define TITLE_TERM_OF_USE @"Terms of Service"
#define TITLE_PRIVACY_POLICIES @"Privacy Policy"

@interface RegistrationView () < UIWebViewDelegate, UIScrollViewDelegate >

- (void)initUI;

@end

@implementation RegistrationView

#pragma mark - Life Cycle Methods

-(void)viewDidLoad
{
    [self initUI];
}

- (void)initUI
{
    _submitButton.layer.cornerRadius = floor(_submitButton.frame.size.height/4);
    _submitButton.clipsToBounds = YES;
    
    [self setupAgreeTextWebView];
    
    [self setSwipeToBackGesture];
}

#pragma mark - 

- (void)setupAgreeTextWebView
{
    NSString * termsAndPolicyHtml = [[NSBundle mainBundle] pathForResource:@"TermsAndPolicyText.html" ofType:nil];
    NSString * termsAndPolicyText = [NSString stringWithContentsOfFile:termsAndPolicyHtml encoding:NSUTF8StringEncoding error:nil];
    
    [_agreeTextWebView loadHTMLString:termsAndPolicyText baseURL:nil];
    [_agreeTextWebView.scrollView setScrollEnabled:NO];
    [_agreeTextWebView.scrollView setDelegate:self];
    [_agreeTextWebView.scrollView setBounces:NO];
}

#pragma mark - IBActions

- (IBAction)onSubmit:(id)sender
{
    if ([self validateFields])
    {
        [(RegistrationController *)self.controller signUpWithEmail:[_txtFieldEmail text] andPassword:[_txtFieldPassword text] confirmPassword:[_txtFieldConfirmPassword text]];
    }
}

#pragma mark - Validation

- (BOOL)validateFields
{
    NSString *email = [_txtFieldEmail text];
    NSString *password = [_txtFieldPassword text];
    NSString *confirmPassword = [_txtFieldConfirmPassword text];
    
    if ([StringUtils isEmptyOrNull:email])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL];
        return NO;
    }
    
    if ([StringUtils isEmptyOrNull:password])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_PASSWORD];
        return NO;
    }
    
    if (![StringUtils validateEmail:email])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL_ADDRESS];
        return NO;
    }
    
    //FIXME: Disable For First Sprint.
    //FIXME: Re-enable In Third Sprint.
    
    if (![StringUtils compareString:password withString:confirmPassword])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_PASSWORD_DONOT_MATCH];
        return NO;
    }
    
    if ([password length] < 6 || [password length] > 32)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_IN_VALID_PASSWORD];
        return NO;
    }
        
    return YES;
}

#pragma mark UIWebView delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasSuffix:@"termsOfService"])
    {
        [(RegistrationController *)self.controller openInAppLink:URL_TERMS_AND_CONDITIONS andTitle:TITLE_TERM_OF_USE];

        return NO;
    }
    if ([request.URL.absoluteString hasSuffix:@"privacyPolicy"])
    {
        [(RegistrationController *)self.controller openInAppLink:URL_PRIVACY_POLICY andTitle:TITLE_PRIVACY_POLICIES];

        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitUserSelect='none';"];
}
#pragma mark
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return nil;
}

@end
