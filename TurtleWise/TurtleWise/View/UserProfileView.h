//
//  UserProfileView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class UserProfile;
@class PercentFillView;

@interface UserProfileView : BaseView

@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet PercentFillView *viewProfileComplete;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileComplete;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLevel;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;

- (void)updateUserProfile:(UserProfile *)profile;

@end
