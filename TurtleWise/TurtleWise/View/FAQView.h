//
//  FAQView.h
//  TurtleWise
//
//  Created by Anum Amin on 3/21/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface FAQView : BaseView<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
