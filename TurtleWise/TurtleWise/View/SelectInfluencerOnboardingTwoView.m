//
//  SelectInfluencerOnboardingTwoView.m
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectInfluencerOnboardingController.h"
#import "SelectInfluencerOnboardingView.h"
#import "StringUtils.h"
#import "Constant.h"
#import "BrandedPage.h"

#define IMAGE_BUTTON_START_TAG 100
#define IMAGE_BUTTON_END_TAG 101

@interface SelectInfluencerOnboardingView ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(nonatomic, strong) UIImagePickerController *imagePickerController;
@property(nonatomic) BOOL ignorUpdateInfo;
@property(nonatomic) BOOL isProfileButtonSelected;

@end

@implementation SelectInfluencerOnboardingView

#pragma mark - Life Cycle Methods

-(void)viewDidLoad
{
    [super viewDidLoad];
    _isProfileButtonSelected = true;
}
#pragma mark - Override Methods

- (void)showImagePickerForSource:(UIImagePickerControllerSourceType)source
{
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.sourceType = source;
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = YES;
    
    [self.controller presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)saveWizardData:(BrandedPage *)brandedPage
{

    [brandedPage setCover :[_userProfileImage imageForState:UIControlStateNormal]];
    [brandedPage setAvatar:[_userCoverImage imageForState:UIControlStateNormal]];

}

- (void)setBasicInformation:(BrandedPage *)brandedPage
{
    [_userProfileImage setImage:[brandedPage avatar] forState:UIControlStateNormal];
    [_userCoverImage setImage:[brandedPage cover] forState:UIControlStateNormal];
}

#pragma mark - IBActions

- (IBAction)changeUserImage:(UIButton*)sender
{
    _ignorUpdateInfo = YES;
    
    if (sender.tag == IMAGE_BUTTON_START_TAG)
        _isProfileButtonSelected = true;
    else
        _isProfileButtonSelected = false;
    
    [self showImagePickerForSource:UIImagePickerControllerSourceTypePhotoLibrary];
    
    
}

- (IBAction)submitWizard:(id)sender{
    
    [(SelectInfluencerOnboardingController *)self.controller savePageDetail];
    
    
}


# pragma mark -
# pragma mark UIImagePickerController Delegate Methods

- (void)hideImagePicker
{
    [_imagePickerController dismissViewControllerAnimated:YES completion:^{
        _ignorUpdateInfo = NO;
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    if (!editedImage) {
        [self hideImagePicker];
        return;
    }
    
    editedImage = [(SelectInfluencerOnboardingController *)self.controller getSquareImage:editedImage];
    
    if (_isProfileButtonSelected) {
        
        [(SelectInfluencerOnboardingController *)self.controller uploadPageAvatar:editedImage type: @"avatar"];
    } else {
        [(SelectInfluencerOnboardingController *)self.controller uploadPageAvatar:editedImage type: @"cover"];
    }
    
    [self hideImagePicker];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self hideImagePicker];
}






@end
