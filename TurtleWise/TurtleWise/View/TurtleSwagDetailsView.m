//
//  TurtleSwagDetailsView.m
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TurtleSwagDetailsController.h"
#import "ActionSheetStringPicker.h"
#import "TurtleSwagDetailsView.h"
#import "UIView+UIViewRect.h"
#import "AsyncImageView.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "TextField.h"
#import "UserLevel.h"
#import "Constant.h"
#import "Charity.h"
#import "Label.h"

#define TITLE_PICKER @"Choose TurtleBucks"

#define MESSAGE_ALERT_INSUFFIECENT_BUCKS @"You donot have sufficient TurtleBucks."
#define MESSAGE_ALERT_NO_BUCKS_ADDED @"Please add TurtleBucks."
#define TITLE_ALERT_SUCESSFULL_REDEMPTION @"Thank You!"
#define MESSAGE_SUCCESSFULL_DONATION @"You have redeemed your hard earned Turtle Bucks by donating to charity of your selection. You will receive a confirmation email shortly."
#define MESSAGE_SUCESSFULL_REDEMPTION @"You have successfully redeemed your TurtleBucks for Amazon Gift Card. You will receive a confirmation email shortly."


@interface TurtleSwagDetailsView () < UIAlertViewDelegate >

@property(nonatomic, assign) NSInteger selectedBucks;
@property(nonatomic, assign) NSInteger equivalentAmount;
@property(nonatomic, assign) BOOL isRedeemingGiftCard;

@end

@implementation TurtleSwagDetailsView

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    _isRedeemingGiftCard = NO;
    
    [[_displayImage layer] setBorderWidth:0.5];
    [[_displayImage layer] setBorderColor:[UIColor lightGrayColor].CGColor];
}

- (void)populateDetails:(Charity *)charityDetails
{
    [_title setText:[charityDetails title]];
    [_details setText:[charityDetails details]];
    [_displayImage setImageURL:[NSURL URLWithString:[charityDetails imageUrl]]];
    
    if ([[charityDetails title] isEqualToString:TITLE_AMAZON_GIFT_CARD])
    {
        [self handleViewForAmazonGiftCard:charityDetails];
    }
}

#pragma mark - IBActions

- (IBAction)chooseAmount:(id)sender
{
    [self showPicker];
}

- (IBAction)submitRedemption:(id)sender
{
    if ([self validate])
    {
        (_isRedeemingGiftCard) ? [(TurtleSwagDetailsController *)self.controller redeemAmount:_equivalentAmount] :
                                 [(TurtleSwagDetailsController *)self.controller donateAmount:_equivalentAmount];
    }
}

#pragma mark - Helper Methods

- (void)showPicker
{    
    [ActionSheetStringPicker showPickerWithTitle:TITLE_PICKER
                                            rows:[UserDefaults getLevelBucks]
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
     {
         [self setAmount:selectedIndex andValue:selectedValue];
     }
                                     cancelBlock:NULL
                                          origin:self];
}

- (void)setAmount:(NSInteger)selectedIndex andValue:(NSString *)selectedValue
{
    _equivalentAmount = [[[UserDefaults getLevelAmount] objectAtIndex:selectedIndex] integerValue];
    _selectedBucks = [selectedValue integerValue];
    
    [_btnDollarAmount setTitle:[NSString stringWithFormat:@"%ld TB", (long)_selectedBucks] forState:UIControlStateNormal];
    [_fieldBucks setText:[NSString stringWithFormat:@"%ld $", (long)_equivalentAmount]];
}

- (BOOL)validate
{
    if (_selectedBucks == 0)
    {
        [Alert show:ERROR_ALERT_TITLE andMessage:MESSAGE_ALERT_NO_BUCKS_ADDED];
        
        return NO;
    }
    
    UserProfile *userProfile = [UserDefaults getUserProfile];
    
    if ([userProfile bucks] < _selectedBucks)
    {
        [Alert show:ERROR_ALERT_TITLE andMessage:MESSAGE_ALERT_INSUFFIECENT_BUCKS];
        
        return NO;
    }
    
    return YES;
}

- (void)handleViewForAmazonGiftCard:(Charity *)charityDetails
{
    _isRedeemingGiftCard = YES;
    
    [_displayImage setImageURL:[NSURL URLWithString:[charityDetails imageUrl]]];
    
    [_dollarBtnTopConstraint setConstant:178.0];
    [_containerViewHeightConstraint setConstant:320.0];
    
    [self layoutIfNeeded];
    [self updateConstraints];
}

- (void)redeemSucessfull
{
    [Alert show:TITLE_ALERT_SUCESSFULL_REDEMPTION andMessage:((_isRedeemingGiftCard) ? MESSAGE_SUCESSFULL_REDEMPTION : MESSAGE_SUCCESSFULL_DONATION) andDelegate:self];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [(TurtleSwagDetailsController *)self.controller goToHome];
}

@end

