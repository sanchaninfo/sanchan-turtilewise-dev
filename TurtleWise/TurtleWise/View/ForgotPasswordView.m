//
//  ForgotPasswordView.m
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "ForgotPasswordView.h"
#import "StringUtils.h"
#import "TextField.h"

@interface ForgotPasswordView ()

@property (weak, nonatomic) IBOutlet TextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@end

@implementation ForgotPasswordView

-(void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)initWithEmail:(NSString *)email {
    [_txtFieldEmail setText:email];
}

#pragma mark - Private Methods

- (void)initUI {
    _btnSubmit.layer.cornerRadius = floor(_btnSubmit.frame.size.height/4);
    _btnSubmit.clipsToBounds = YES;
    
    [super setSwipeToBackGesture];
}

#pragma mark - IBActions

- (IBAction)onClickSubmit {
    if ([self validateFields]) {
        [(ForgotPasswordController *)self.controller forgotPasswordForEmail:_txtFieldEmail.text];
    }
}

#pragma -  Validators

- (BOOL)validateFields{
    NSString *email = [_txtFieldEmail text];
    
    if ([StringUtils isEmptyOrNull:email])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL];
        return NO;
    }
        
    if (![StringUtils validateEmail:email])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL_ADDRESS];
        return NO;
    }
    
    [_txtFieldEmail resignFirstResponder];    
    return YES;
}

@end
