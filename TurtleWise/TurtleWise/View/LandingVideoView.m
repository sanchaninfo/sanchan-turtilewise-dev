//
//  LandingVideoView.m
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "LandingVideoView.h"
#import "LandingVideoController.h"

#define YOUTUBE_VID_ID @"w2kgfmwXESg"

@implementation LandingVideoView

#pragma mark - Actions

-(void) awakeFromNib {
    [super awakeFromNib];

    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayVideo)];
    [self.imgBackground addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayVideo)];
    [self.imgPlay addGestureRecognizer:tap2];
  
    [self preparePlayer];
}

- (IBAction)onSkipVideo:(id)sender {
    
    if(_playerController.playbackState != MPMoviePlaybackStatePlaying)
        [(LandingVideoController *)self.controller onNavigationToSocialLoginController];
    else
        [_playerController stop];
}

- (IBAction)onPlayVideo {
    _imgBackground.hidden = _imgPlay.hidden = YES;
    _imgBackground.userInteractionEnabled = _imgPlay.userInteractionEnabled = NO;
    [_playerController play];
}

#pragma mark - Methods

-(void) preparePlayer {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WelcomeVid" ofType:@"mp4"];
    NSURL *url = [NSURL fileURLWithPath:path];
    
    _playerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    _playerController.controlStyle = MPMovieControlStyleFullscreen;
    _playerController.fullscreen = YES;
    [self insertSubview:_playerController.view belowSubview:_imgBackground];
    [self setVideoConstraints];
}

-(void) loadVideo {
    NSString*  embedHTML = [NSString stringWithFormat:@"\
                            <html>\
                            <body style='margin:0px;padding:0px;background-color:black;'>\
                            <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script>\
                            <script type='text/javascript'>\
                            function onYouTubeIframeAPIReady()\
                            {\
                            ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})\
                            }\
                            function onPlayerReady(a)\
                            { \
                            a.target.playVideo(); \
                            }\
                            </script>\
                            <iframe id='playerId' type='text/html' width='%f' height='%f' src='http://www.youtube.com/embed/%@?enablejsapi=1&rel=0&playsinline=1&autoplay=1&controls=1' frameborder='0' style='background-color:black;'>\
                            </body>\
                            </html>", _webview.frame.size.width, _webview.frame.size.height, YOUTUBE_VID_ID];
    
    [_webview setAllowsInlineMediaPlayback:YES];
    [_webview setMediaPlaybackRequiresUserAction:NO];
    [_webview loadHTMLString:embedHTML baseURL: nil];
}

-(void) resetVideo {
    _imgBackground.hidden = _imgPlay.hidden = NO;
    _imgBackground.userInteractionEnabled = _imgPlay.userInteractionEnabled = YES;
    [_playerController stop];
    
    NSString *emptyBody = @"<html>\
    <body style='margin:0px;padding:0px;background-color:black;'>\
    </body>\
    </html>";
    [_webview loadHTMLString:emptyBody baseURL: nil];
}

-(void) setVideoConstraints {
    
    _playerController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                        [NSLayoutConstraint constraintWithItem:_playerController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0],
                        [NSLayoutConstraint constraintWithItem:_playerController.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0],
                        [NSLayoutConstraint constraintWithItem:_playerController.view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0],
                        [NSLayoutConstraint constraintWithItem:_playerController.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]]];
    [self layoutIfNeeded];
}

-(void) stopVideo {
    
    [_playerController stop];
}

#pragma mark - MPMoviePlayer

- (void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    _imgBackground.userInteractionEnabled = _imgPlay.userInteractionEnabled = YES;
    [(LandingVideoController *)self.controller onNavigationToSocialLoginController];
    
}

@end
