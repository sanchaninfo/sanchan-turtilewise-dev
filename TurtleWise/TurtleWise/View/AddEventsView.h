//
//  AddEventsView.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"
@class TextField;
@class Button;
@class BrandedPage;

@interface AddEventsView : BaseProfileWizardView {
    int eventNoInside;
}

@property (weak, nonatomic) IBOutlet TextField *txtTitle;
@property (weak, nonatomic) IBOutlet TextField *txtLink;
@property (weak, nonatomic) IBOutlet TextField *txtImage;
@property (weak, nonatomic) IBOutlet Button *btnDate;


- (void)updateData:(BrandedPage *)page withArticleNo:(int)eventNo;
- (IBAction)submitWizard:(id)sender;
- (BOOL)validateFields;
- (void)saveWizardData:(BrandedPage *)page;

@end
