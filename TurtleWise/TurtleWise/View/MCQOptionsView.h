//
//  MCQOptionsView.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/2/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class TWTableView;

@interface MCQOptionsView : BaseView

//Outlets
@property (weak, nonatomic) IBOutlet TWTableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMore;

- (NSArray *)getChoices;

//Actions
- (IBAction)submitChoices:(id)sender;
- (IBAction)addMore:(id)sender;

- (void)clearOptions;

@end
