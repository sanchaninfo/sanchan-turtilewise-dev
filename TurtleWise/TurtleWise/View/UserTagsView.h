//
//  UserTagsView.h
//  TurtleWise
//
//  Created by Waleed Khan on 1/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TLTagsControl;

@interface UserTagsView : BaseProfileWizardView

@property (weak, nonatomic) IBOutlet TLTagsControl *explorerTagsView;
@property (weak, nonatomic) IBOutlet TLTagsControl *guruTagsView;

- (IBAction)submitWizard:(id)sender;

@end
