//
//  SelectHobbiesView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TWTableView;

@interface SelectHobbiesView : BaseProfileWizardView

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnAddMore;
@property (weak, nonatomic) IBOutlet TWTableView *tableView;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)addMore:(id)sender;

@end
