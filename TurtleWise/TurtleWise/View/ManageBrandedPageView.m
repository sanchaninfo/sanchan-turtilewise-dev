//
//  ManageBrandedPageView.m
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ManageBrandedPageView.h"
#import "BrandedPage.h"
#import "AppDelegate.h"
#import "ManageBrandedPageController.h"
#import "SummaryTextCell.h"
#import "ArticleCell.h"
#import "EventsCell.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "BrandedPageManager.h"
#import "NoContentCell.h"
#import "FollowRequestsView.h"
#import "FollowersView.h"

@interface ManageBrandedPageView () < UITableViewDataSource, UITableViewDelegate >
@property(nonatomic, strong) NSArray *tableHeaderData;

@end

@implementation ManageBrandedPageView
-(void)viewDidLoad {
    [super viewDidLoad];
    _tableHeaderData = [NSArray new];
    [self.tableview registerNib:[UINib nibWithNibName:@"AccordionHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:kAccordionHeaderViewReuseIdentifier];
    self.tableview.estimatedRowHeight = 64.0;
    [self.tableview setRowHeight:UITableViewAutomaticDimension];
    
}

-(void)viewWillAppear {
    [super viewWillAppear];
    [self setupUI];
}

-(void)viewWillDisappear {
    [self removesubview];
}

- (void) setupUI {
    [[_imgUserProfile layer] setCornerRadius:CGRectGetWidth([_imgUserProfile frame])/2.0f];
    [_imgUserProfile setClipsToBounds:YES];
    _tableHeaderData = [NSArray arrayWithObjects:@"Summary", @"Published Articles", @"Events", nil];
}

- (void) sethiddenUI {
    if ([[BrandedPageManager sharedManager] isOwn]) {
        _pictureEditButton.hidden = NO;
        _nameEditButton.hidden = NO;
        self.privacySegment.hidden = NO;
        _deleteButton.hidden = NO;
        [_deleteButton setTitle:@"DELETE BRANDED PAGE" forState:UIControlStateNormal];
        _followersHeight.constant = 25;
        _privacySegmentHeight.constant = 28;
    } else {
        _pictureEditButton.hidden = YES;
        _nameEditButton.hidden = YES;
        self.privacySegment.hidden = YES;
        [_deleteButton setTitle:@"ASK A QUESTION" forState:UIControlStateNormal];
        if ([[[UserDefaults getUserProfile] following] containsObject:pageBranded.pageId]) {
            _deleteButton.hidden = NO;
        } else {
            _deleteButton.hidden = YES;
        }
        
        _followersHeight.constant = 0;
        _privacySegmentHeight.constant = 0;
    }
}

- (void) setFollowButtonUI {
    if ([[BrandedPageManager sharedManager] isOwn]) {
        int no = (int)[[pageBranded pending] count];
        NSString* title = [NSString stringWithFormat:@"%d Requests",no];
        [_followButton setTitle:title forState:UIControlStateNormal];
        [_followButton setImage:[UIImage imageNamed:@"tw-follow-icon"] forState:UIControlStateNormal];
        [_followButton setBackgroundColor:[UIColor colorWithRed:214.0/255.0 green:61.0/255.0 blue:61.0/255.0 alpha:1.0]];
    } else {
        if ([[[UserDefaults getUserProfile] following] containsObject:pageBranded.pageId]) {
            [_followButton setTitle:@"Following" forState:UIControlStateNormal];
            [_followButton setImage:[UIImage imageNamed:@"tw-following-icon"] forState:UIControlStateNormal];
            [_followButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0 green:126.0/255.0 blue:205.0/255.0 alpha:1.0]];
        } else if ([[[UserDefaults getUserProfile] pending] containsObject:pageBranded.pageId]) {
            [_followButton setTitle:@"Pending" forState:UIControlStateNormal];
            [_followButton setImage:[UIImage imageNamed:@"tw-follow-icon"] forState:UIControlStateNormal];
            [_followButton setBackgroundColor:[UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0]];
        } else {
            [_followButton setTitle:@"Follow" forState:UIControlStateNormal];
            [_followButton setImage:[UIImage imageNamed:@"tw-follow-icon"] forState:UIControlStateNormal];
            [_followButton setBackgroundColor:[UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0]];
        }
    }
}

- (void) getPrivacy:(BrandedPage *)pageDetail {
    
    if (self.privacySegment.selectedSegmentIndex == 0) {
        [pageDetail setPrivacy:@"public"];
    } else {
        [pageDetail setPrivacy:@"private"];
    }
}

- (void) brandedPageWith:(BrandedPage *) pageDetail {
    
    pageBranded = pageDetail;
    [self sethiddenUI];
    [self setFollowButtonUI];
    
    [_lblName setText:[pageDetail.name capitalizedString]];
    if ([pageDetail.type isEqualToString:@"influencer"]) {
        [_lblType setTextColor:[UIColor colorWithRed:255.0/255.0 green:153.0/255.0 blue:0.0/255.0 alpha:1.0]];
    } else {
        [_lblType setTextColor:[UIColor colorWithRed:153.0/255.0 green:204.0/255.0 blue:102.0/255.0 alpha:1.0]];
    }
    [_lblType setText:[pageDetail.type capitalizedString]];
    
    
    
    NSString* followersButtonTitle = [NSString stringWithFormat:@"%d followers",(int)[[pageDetail approved] count]];
    NSMutableAttributedString *underlineTitle = [[NSMutableAttributedString alloc] initWithString:followersButtonTitle];
    [underlineTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, [underlineTitle length])];
    [_followersButton setAttributedTitle:underlineTitle forState:UIControlStateNormal];
    
    if([pageDetail.privacy isEqualToString:@"public"]) {
        self.privacySegment.selectedSegmentIndex = 0;
    } else if ([pageDetail.privacy isEqualToString:@"private"]) {
        self.privacySegment.selectedSegmentIndex = 1;
    }
    if ([pageDetail avatar]) {
        [_imgUserProfile setImage:[pageDetail avatar]];
    }
    if([pageDetail cover]) {
        [_imgCoverpic setImage:[pageDetail cover]];
    }
    [self.tableview  reloadData];
    [self viewDidLayoutSubviews];
}

#pragma mark - <UITableViewDataSource> / <UITableViewDelegate> -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kDefaultAccordionHeaderViewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return [self tableView:tableView heightForHeaderInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    if([indexPath section] == 0) {
        SummaryTextCell *cell = (SummaryTextCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SummaryTextCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        [cell setData:pageBranded];
        
        return cell;
    } else if([indexPath section] == 1) {
        
        if (![[BrandedPageManager sharedManager] isOwn] &&
            [pageBranded.privacy isEqualToString:@"private"] &&
            ![[[UserDefaults getUserProfile] following] containsObject:pageBranded.pageId]) {
            NoContentCell *cell = (NoContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoContentCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:@"Private"];
            return cell;
        } else if([pageBranded articles].count == 0) {
            NoContentCell *cell = (NoContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoContentCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:@"No Content"];
            return cell;
        } else {
            ArticleCell *cell = (ArticleCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ArticleCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:[pageBranded articles]];
            return cell;
        }
    } else if ([indexPath section] == 2) {
        
        if (![[BrandedPageManager sharedManager] isOwn] &&
            [pageBranded.privacy isEqualToString:@"private"] &&
            ![[[UserDefaults getUserProfile] following] containsObject:pageBranded.pageId]) {
            NoContentCell *cell = (NoContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoContentCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:@"Private"];
            return cell;
        } else if([pageBranded events].count == 0) {
            NoContentCell *cell = (NoContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoContentCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:@"No Content"];
            return cell;
        } else {
            EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(!cell) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            [cell setData:[pageBranded events]];
            return cell;
        }
        
        
    }
    return nil;
}

-(void)viewDidLayoutSubviews {
    [[self tableview] reloadData];
    [self.tableview layoutIfNeeded];
    self.tableHeight.constant = self.tableview.contentSize.height;
    [self setNeedsLayout];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    AccordionHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kAccordionHeaderViewReuseIdentifier];
    [cell set:[_tableHeaderData objectAtIndex:section]];
    cell.editButton.tag = section;
    [cell.editButton addTarget:self action:@selector(editButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (IBAction)pictureEditTapped:(id)sender {
    [(ManageBrandedPageController*)self.controller showInfluencerOnboardingController];
}
- (IBAction)editNameAndTypeTapped:(id)sender {
    [(ManageBrandedPageController*)self.controller showUpdateNameANdTypeController];
}
- (IBAction)chagedSwitch:(id)sender {
}
- (IBAction)deleteButtonTapped:(id)sender {
    
    if ([[BrandedPageManager sharedManager] isOwn]) {
        UIAlertView *alertArticle = [[UIAlertView alloc]
                                     initWithTitle:@"Confirm!"
                                     message:@"Do you want to Delete?"
                                     delegate: self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles:@"OK", nil];
        alertArticle.tag = 101;
        [alertArticle show];
    } else {
        
        
        
        
        NSMutableDictionary *answerer = [[NSMutableDictionary alloc] init];
        NSMutableArray *list = [[NSMutableArray alloc] init];
        [answerer setObject:[pageBranded name] forKey:KEY_PAGENAME];
        [answerer setValue:@"owner" forKey:KEY_WHO];
        [list addObject:pageBranded.ownerId];
        [answerer setObject:list forKey:KEY_LIST];
        [(ManageBrandedPageController*)self.controller startQuestionTextWithDictionary:answerer];
    }
    
    
    
    
}
- (IBAction)privacyTapped:(id)sender {
    [(ManageBrandedPageController*)self.controller savePageDetail];
}
- (IBAction)followButtonTapped:(id)sender {
    
    if ([[BrandedPageManager sharedManager] isOwn]) {
        int no = (int)[[pageBranded pending] count];
        if (no > 0) {
            [(ManageBrandedPageController*)self.controller getFindIdsWithString:@"pending"];
        }
    } else {
        if ([[_followButton.titleLabel text] isEqualToString:@"Follow"]) {
            if ([pageBranded.privacy isEqualToString:@"public"]) {
                [(ManageBrandedPageController*)self.controller followWithPageId:pageBranded.pageId];
                
            } else {
                [(ManageBrandedPageController*)self.controller followRequestWithPageId:pageBranded.pageId];
            }
        } else {
            [(ManageBrandedPageController*)self.controller unfollowWithPageId:pageBranded.pageId];
        }
    }
}

- (void) showRequestView:(NSArray*)array {
    CGRect frame = [UIScreen mainScreen].bounds;
    FollowRequestsView *reqView = [[FollowRequestsView alloc] initWithFrame:frame];
    [reqView getData:array];
    [self addSubview:reqView];
}

- (void) showFollowersView:(NSArray*)array {
    CGRect frame = [UIScreen mainScreen].bounds;
    FollowersView *folView = [[FollowersView alloc] initWithFrame:frame];
    [folView getData:array];
    [self addSubview:folView];
}

#pragma mark - <FZAccordionTableViewDelegate> -

- (void)tableView:(FZAccordionTableView *)tableView willOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
}

- (void)tableView:(FZAccordionTableView *)tableView didOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    [self viewDidLayoutSubviews];
}

- (void)tableView:(FZAccordionTableView *)tableView willCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
}

- (void)tableView:(FZAccordionTableView *)tableView didCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    [self viewDidLayoutSubviews];
}

- (void) editButtonTapped:(UIButton *)sender {
    if(sender.tag == 0)
    {
        [(ManageBrandedPageController*)self.controller showSummaryController];
    } else if(sender.tag == 1)
    {
        if(pageBranded.articles.count < 3) {
            [(ManageBrandedPageController*)self.controller showArticleControllerWithNumber:-1];
        } else {
            [Alert show:@"Error" andMessage:@"Can not add More than 3 Articles"];
        }
        
    } else if (sender.tag == 2)
    {
        if(pageBranded.events.count < 3) {
            [(ManageBrandedPageController*)self.controller showEventControllerWithNumber:-1];
        } else {
            [Alert show:@"Error" andMessage:@"Can not add More than 3 Events"];
        }
    }
}

- (void) innerEditButtonTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller showArticleControllerWithNumber:(int)sender.tag];
}

- (void) innerDeleteButtonTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller deleteArticleWithNumber:(int)sender.tag];
}

- (void) innerEventEditButtonTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller showEventControllerWithNumber:(int)sender.tag];
}

- (void) innerEventDeleteButtonTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller deleteEventWithNumber:(int)sender.tag];
}

-(void)removesubview {
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[FollowRequestsView class]] || [view isKindOfClass:[FollowersView class]]) {
            [view removeFromSuperview];
        }
    }
}
- (IBAction)followersTapped:(id)sender {
    int no = (int)[[pageBranded approved] count];
    if (no > 0) {
        [(ManageBrandedPageController*)self.controller getFindIdsWithString:@"approved"];
    }
}

- (void) acceptTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller toFollow:[NSString stringWithFormat:@"%@", sender.tag]];
}

- (void) denyTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller toUnFollow:[NSString stringWithFormat:@"%@", sender.tag]];
}

-(void) removeButtonTapped:(UIButton *)sender {
    [(ManageBrandedPageController*)self.controller toUnFollow:[NSString stringWithFormat:@"%@", sender.tag]];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0)
    {
    }else if(buttonIndex == 1)
    {
        [(ManageBrandedPageController*)self.controller deletePage];
    }
}


@end
