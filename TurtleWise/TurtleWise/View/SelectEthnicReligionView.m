//
//  SelectEthnicReligionView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectEthnicReligionController.h"
#import "SelectEthnicReligionView.h"
#import "ActionSheetStringPicker.h"
#import "StringUtils.h"
#import "TextField.h"
#import "Constant.h"
#import "UserProfile.h"

#define TAG_HETERO @"heterosexual"
#define TAG_HOMO @"homosexual"
#define TAG_BI @"bisexual"

#define PICKER_TITLE_ETHNICITY @"Ethnicity"
#define PICKER_TITLE_POLITICAL_AFFLILIATION @"Political Affiliation"
#define PICKER_TITLE_RELIGION @"Religion"

#define PICKER_VALUES_ETHNICITY @[@"White American", @"Black Or African American", @"Native American And Alaska Native", @"Asian American", @"Native Hawaiian And Other Pacific Islander", @"Hispanic Or Latino", @"Other"]
#define PICKER_VALUES_POLITICAL_AFFLILIATION @[@"Democrat", @"Republican", @"Independent", @"Other"]
#define PICKER_VALUES_RELIGION @[@"Christian", @"Islamic", @"Catholic", @"Hindu", @"Agnostic", @"Atheist", @"Jewish", @"Buddhist", @"Anglican", @"Sikh", @"Latter Day Saint", @"Seventh Day Adventist",  @"Other"]

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 102

@interface SelectEthnicReligionView ()

@property(nonatomic, strong) NSString *tagForOrientation;
@property(nonatomic, strong) NSArray *possibleTags;

- (void)updateOrientation:(NSString *)orientation;
- (NSInteger)getIndexOf:(NSString *)object inArray:(NSArray *)array;
- (void)showPickerWithTitle:(NSString *)title values:(NSArray *)values intitalValue:(NSInteger)initalSelection andResultButton:(UIButton *)resultButton;
@end

@implementation SelectEthnicReligionView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tagForOrientation = EMPTY_STRING;
    
    _possibleTags = @[TAG_HETERO, TAG_HOMO, TAG_BI];
}


#pragma mark - Helper Methods

- (void)saveTags
{
    //TODO: Will Implemente Later
}

- (void)updateOrientation:(NSString *)orientation
{
    _tagForOrientation = [orientation lowercaseString];
    
    int tag = 0;
    
    if ([StringUtils compareString:_tagForOrientation withString:TAG_HETERO])
    {
        tag = OPTION_BUTTONS_START_TAG;
    }
    
    if ([StringUtils compareString:_tagForOrientation withString:TAG_HOMO])
    {
        tag = 101;
    }
    
    if ([StringUtils compareString:_tagForOrientation withString:TAG_BI])
    {
        tag = OPTION_BUTTONS_END_TAG;
    }
    
    if (tag)
    {
        [(UIButton *)[self viewWithTag:tag] setSelected:YES];
    }
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_btnEthnicity setTitle:[profile ethnicity] forState:UIControlStateNormal];
    [_btnReligion setTitle:[profile religion] forState:UIControlStateNormal];
    [_btnPolicticalAffliation setTitle:[profile politicalAffiliation] forState:UIControlStateNormal];
    
    [self updateOrientation:[profile orientation]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [[_btnReligion titleLabel] text];
    
    [profile setOrientation:[StringUtils sentenceCapitalizedString:_tagForOrientation]];
    [profile setReligion:[[_btnReligion titleLabel] text]];
    [profile setEthnicity:[[_btnEthnicity titleLabel] text]];
    [profile setPoliticalAffiliation:[[_btnPolicticalAffliation titleLabel] text]];
}

#pragma mark - Helper Methods

- (void)showPickerWithTitle:(NSString *)title values:(NSArray *)values intitalValue:(NSInteger)initalSelection andResultButton:(UIButton *)resultButton
{
    [ActionSheetStringPicker showPickerWithTitle:title
                                            rows:values
                                initialSelection:initalSelection
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        [self setTitle:selectedValue onButton:resultButton];
    }
                                     cancelBlock:NULL
                                          origin:self];
}

- (NSInteger)getIndexOf:(NSString *)object inArray:(NSArray *)array
{
    BOOL identicalStringFound = [array indexOfObject:object] != NSNotFound;
    
    if (identicalStringFound)
    {
        return [array indexOfObject:object];
    }
    
    return 0;
}

- (void)setTitle:(NSString *)title onButton:(UIButton *)selectedButton
{
    [selectedButton setTitle:title forState:UIControlStateNormal];
}

#pragma mark - IBActions

- (IBAction)setOrientation:(id)sender
{
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            _tagForOrientation = ([sender isSelected]) ? [_possibleTags objectAtIndex:[sender tag] % OPTION_BUTTONS_START_TAG] :
                                                         EMPTY_STRING;
            
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)onPoliticalAff:(id)sender
{
    [self showPickerWithTitle:PICKER_TITLE_POLITICAL_AFFLILIATION
                       values:PICKER_VALUES_POLITICAL_AFFLILIATION
                 intitalValue:[self getIndexOf:[[(UIButton *)sender titleLabel] text] inArray:PICKER_VALUES_POLITICAL_AFFLILIATION]
              andResultButton:sender];
}

- (IBAction)chooseReligion:(id)sender
{
    [self showPickerWithTitle:PICKER_TITLE_RELIGION
                       values:PICKER_VALUES_RELIGION
                 intitalValue:[self getIndexOf:[[(UIButton *)sender titleLabel] text] inArray:PICKER_VALUES_RELIGION]
              andResultButton:sender];
}

- (IBAction)chooseEhnicity:(id)sender
{
    [self showPickerWithTitle:PICKER_TITLE_ETHNICITY
                       values:PICKER_VALUES_ETHNICITY
                 intitalValue:[self getIndexOf:[[(UIButton *)sender titleLabel] text] inArray:PICKER_VALUES_ETHNICITY]
              andResultButton:sender];
}

@end
