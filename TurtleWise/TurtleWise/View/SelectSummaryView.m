//
//  SelectSummaryView.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectSummaryView.h"

#import "StringUtils.h"
#import "Constant.h"
#import "BrandedPage.h"

@interface SelectSummaryView ()
@property(nonatomic, readonly) SelectSummaryController *controller;
@property(nonatomic, strong) NSString *summaryStr;

@end

@implementation SelectSummaryView

@dynamic controller;
#pragma mark - UIView Life Cycle

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _summaryStr = EMPTY_STRING;
}

- (void)updateData:(BrandedPage *)pageDetail
{    
    [_summaryView setText:[pageDetail summary]];
    [_fbLinkView setText:[pageDetail facebookLink]];
    [_twitterLinkView setText:[pageDetail twitterLink]];
    [_linkedInLinkView setText:[pageDetail linkedInLink]];
}

- (void)saveWizardData:(BrandedPage *)pageDetail
{
    [pageDetail setSummary:[_summaryView text]];
    [pageDetail setFacebookLink:[_fbLinkView text]];
    [pageDetail setTwitterLink:[_twitterLinkView text]];
    [pageDetail setLinkedInLink:[_linkedInLinkView text]];
    
}
- (IBAction)submitWizard:(id)sender
{
    if (self.validateFields) {
        [self.controller savePageDetail];
    }
}

- (BOOL)validateFields
{
    NSString *fblink = [_fbLinkView text];
    NSString *twitterlink = [_twitterLinkView text];
    NSString *linkedinlink = [_linkedInLinkView text];
    
    if (![StringUtils validateURL:fblink] && fblink.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    if (![StringUtils validateURL:twitterlink] && twitterlink.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    if (![StringUtils validateURL:linkedinlink] && linkedinlink.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    return YES;
}


@end
