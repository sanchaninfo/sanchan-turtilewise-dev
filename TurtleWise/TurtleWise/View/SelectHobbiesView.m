//
//  SelectHobbiesView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "SelectHobbiesView.h"
#import "TWTableView.h"
#import "UserProfile.h"

@interface SelectHobbiesView ()

@end

@implementation SelectHobbiesView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableView setAddMoreButton:_btnAddMore];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_tableView prepareDataSet:[profile hobbies]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setHobbies:[_tableView getDataSetAsArray]];
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)addMore:(id)sender
{
    [_tableView addRow];
}

@end

