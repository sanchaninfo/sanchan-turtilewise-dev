//
//  ChatOptionsView.m
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ChatOptionsView.h"
#import "Conversation.h"
#import "Question.h"
#import "Constant.h"
#import "Color.h"
#import "UserDefaults.h"
#import "ChatOptionsController.h"
#import "ChatOptionsTableCell.h"
#import "Advisor.h"
#import "Font.h"


#define kChatEnded @"Chat Ended"
#define CELL_NIB_NAME @"ChatOptionsTableCell"

@interface ChatOptionsView ()<MZTimerLabelDelegate,SelectedAdvisorDelegate>{
    Conversation *_conversation;
    NSArray *_guruPanel;
}
@end

@implementation ChatOptionsView

#pragma mark --

-(void)awakeFromNib{
    [self.tableView registerNib:[UINib nibWithNibName:CELL_NIB_NAME bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CELL_REUSE_IDENTIFIER];
    
    
    if([UserDefaults getUserRole] == UserRoleAdvisor){
        self.endChatBtn.hidden = YES;
    }

}

-(void)configureTableHeader{
    CGFloat messageHeight = [Font getHeight:_conversation.question.questionText andFont:[Font lightFontWithSize:17.0] andWidth:[UIScreen mainScreen].bounds.size.width-70];
    self.questionDetailsHeader.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, messageHeight+150);
    self.tableView.tableHeaderView = self.questionDetailsHeader;
    self.guruPanelStaticLabel.hidden = (_guruPanel.count == 0);
}

-(void)viewWillAppear{
    //[self configureTimerLabel];
}

-(void)setConversation:(Conversation*)conversation{
    _conversation = conversation;
    _guruPanel = [self getGuruPanel];
    self.questionTitle.text = _conversation.question.questionText;
    [self.questionImage setImage:[self getImageWithQuestionType:_conversation.question.questionType]];
    [self configureTableHeader];
    [self configureTimerLabel];
}

-(NSArray*)getGuruPanel{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(Advisor *adv in _conversation.advisors){
        if([UserDefaults getUserRole] == UserRoleAdvisor && [adv.advisorID isEqualToString:[UserDefaults getUserID]])//I'm the advisor.
            continue;
        [array addObject:adv];
    }
    return array;
}


- (UIImage *)getImageWithQuestionType:(QuestionType )questionType
{
    switch (questionType){
        case QuestionTypeYN:
            return [UIImage imageNamed:@"question-type-icon-yes-no"];
            
        case QuestionTypeComplex:
            return [UIImage imageNamed:@"question-type-icon-complex"];
            
        default:
            return [UIImage imageNamed:@"question-type-icon-multiple"];
    }
}
-(void)configureTimerLabel{
    [self.chatTime reset];
    [self.chatTime setHidden:NO];
    
    if (_conversation.status == ConversationStatusClosed){
        [self updateViewForClosedChat];
        return;
    }
    
    [self.chatTime setTimerType:MZTimerLabelTypeTimer];
    [self.chatTime setShouldCountBeyondHHLimit:YES];
    [self.chatTime setCountDownTime:[_conversation getRemainingTime]];
    [self.chatTime startWithEndingBlock:^(NSTimeInterval countTime){
        _conversation.status = ConversationStatusClosed;
        [self updateViewForClosedChat];
    }];
    
    [self.chatTime setDelegate:self];
}

-(void)updateViewForClosedChat{
    [self.remainingChatTimeStaticLabel setText:kChatEnded];
    [self.remainingChatTimeStaticLabel setTextColor:[Color blackColor]];
    self.remainingTimeLblHorizontalConstraint.constant = 0;
    self.tableViewBottomSpaceConstraint.constant = -70;
    self.chatTime.hidden = YES;
    
    [self layoutIfNeeded];
    self.endChatBtn.hidden = YES;
}

#pragma mark - MZTimer delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType{
    if (time < TIMER_GOING_TO_EXPIRE_TIME) {
        [timerLabel setTextColor:[Color timerWillExpireColor]];
    }
}
#pragma mark - UITableView DataSource/Delegate

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatOptionsTableCell *chatOptionsCell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER];
    chatOptionsCell.delegate = self;
    chatOptionsCell.addAdvisor.tag = indexPath.row;
    
    [chatOptionsCell setAdvisor:[_guruPanel objectAtIndex:indexPath.row]];
    
    return chatOptionsCell;
}

-(void)selectedAdvisorIndex:(NSInteger)index{
    [(ChatOptionsController*)self.controller addAdvisorWith:[(Advisor*)[_guruPanel objectAtIndex:index] advisorID]];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [(ChatOptionsController*)self.controller showUserProfileController:[(Advisor*)[_guruPanel objectAtIndex:indexPath.row] advisorID]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _guruPanel.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 157.0f;
}
#pragma mark - Actions
-(IBAction)onEndChatBtnTap:(id)sender{
    [self showEndChatConfirmationAlert];
}
-(void)showEndChatConfirmationAlert{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:NULL
                                                                         message:@"Are you sure you want to end this chat?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [(ChatOptionsController*)self.controller endChat];
                                                }];
    [controller addAction:ok];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"NO"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [self.tableView setEditing:NO];
                                                    }];
    [controller addAction:cancel];
    
    [self.controller presentViewController:controller animated:YES completion:nil];
}
@end
