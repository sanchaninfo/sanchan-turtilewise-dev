//
//  RegistrationView.h
//  iOSTemplate
//
//  Created by mohsin on 11/5/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "BaseView.h"

@class TextField;

@interface RegistrationView : BaseView

@property (weak, nonatomic) IBOutlet TextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet TextField *txtFieldPassword;
@property (weak, nonatomic) IBOutlet TextField *txtFieldConfirmPassword;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *scorllContentView;
@property (weak, nonatomic) IBOutlet UIWebView *agreeTextWebView;

-(IBAction)onSubmit :(id)sender;

@end
