//
//  TurtleBucksInfoView.m
//  TurtleWise
//
//  Created by Ajdal on 6/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TurtleBucksInfoView.h"
#import "TurtleBucksInfoCell.h"
#import "UserDefaults.h"

@interface TurtleBucksInfoView (){
    NSArray *_levels;
}
@end

@implementation TurtleBucksInfoView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UINib *nib = [UINib nibWithNibName:@"TurtleBucksInfoView" bundle:nil];
        NSArray *array = [nib instantiateWithOwner:nil options:nil];
        self = [array lastObject];
        self.frame = frame;
    }
    return self;
}

-(void)awakeFromNib{
    _levels = [UserDefaults getLevelTable];
    [self setupTableView];
}

-(void)setupTableView{
    [self.table setEstimatedRowHeight:93.0];
    [self.table setRowHeight:UITableViewAutomaticDimension];
    [self.table registerNib:[UINib nibWithNibName:[TurtleBucksInfoCell cellName]bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[TurtleBucksInfoCell cellName]];
}

#pragma mark - UITableView Delegate/DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _levels.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TurtleBucksInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[TurtleBucksInfoCell cellName]];
    [cell set:[_levels objectAtIndex:indexPath.row]];
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 5)];
    header.backgroundColor = [UIColor whiteColor];
    return header;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
#pragma mark - Actions
-(IBAction)onCloseBtnTap:(id)sender{
    [self removeFromSuperview];
}

@end
