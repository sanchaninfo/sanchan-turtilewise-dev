//
//  SelectSummaryView.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "SelectSummaryController.h"
@interface SelectSummaryView : BaseView
@property (weak, nonatomic) IBOutlet UITextView *summaryView;
@property (weak, nonatomic) IBOutlet UITextField *fbLinkView;
@property (weak, nonatomic) IBOutlet UITextField *twitterLinkView;
@property (weak, nonatomic) IBOutlet UITextField *linkedInLinkView;

- (IBAction)submitWizard:(id)sender;
- (void)updateData:(BrandedPage *)pageDetail;
- (void)saveWizardData:(BrandedPage *)pageDetail;
- (BOOL)validateFields;
@end
