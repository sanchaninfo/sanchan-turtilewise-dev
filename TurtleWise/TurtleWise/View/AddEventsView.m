//
//  AddEventsView.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "AddEventsView.h"
#import "AddEventsController.h"
#import "StringUtils.h"
#import "Constant.h"
#import "UserProfile.h"
#import "TextField.h"
#import "Button.h"
#import "ActionSheetDatePicker.h"
#import "BrandedPage.h"
#import "DateUtils.h"

#define PICKER_TITLE_DOB @"Event Date"
@interface AddEventsView ()
@property(nonatomic, readonly) AddEventsController *controller;
@end

@implementation AddEventsView
@dynamic controller;
#pragma mark - UIView Life Cycle

-(void)viewDidLoad
{
    [super viewDidLoad];
    
}

#pragma mark - Override Methods

- (void)updateData:(BrandedPage *)page withArticleNo:(int)eventNo
{
    eventNoInside = eventNo;
    if (eventNo != -1) {
        [_txtTitle setText:[[[page events] objectAtIndex:eventNo] valueForKey:@"title"]];
        [_txtLink setText:[[[page events] objectAtIndex:eventNo] valueForKey:@"link"]];
        [_txtImage setText:[[[page events] objectAtIndex:eventNo] valueForKey:@"image"]];
        [_btnDate setTitle:[[[page events] objectAtIndex:eventNo] valueForKey:@"date"] forState:UIControlStateNormal];
    }
}

- (void)saveWizardData:(BrandedPage *)page
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    if(eventNoInside == -1){
        [dic setValue:_txtTitle.text forKey:@"title"];
        [dic setValue:_txtLink.text forKey:@"link"];
        [dic setValue:_txtImage.text forKey:@"image"];
        [dic setValue:[_btnDate.titleLabel text] forKey:@"date"];
        if([page events].count > 0) {
            [[page events] addObject:dic];
        } else {
            [page setEvents:[NSMutableArray arrayWithObjects:dic, nil]];
        }
    } else {
        [dic addEntriesFromDictionary:[[page events] objectAtIndex:eventNoInside]];
        [dic setValue:_txtTitle.text forKey:@"title"];
        [dic setValue:_txtLink.text forKey:@"link"];
        [dic setValue:_txtImage.text forKey:@"image"];
        [dic setValue:[_btnDate.titleLabel text] forKey:@"date"];
        [[page events] replaceObjectAtIndex:eventNoInside withObject:dic];
    }
    
}
- (IBAction)submitWizard:(id)sender
{
    if ([self validateFields]) {
        [self.controller savePageDetail];
    }
}


#pragma mark - Public/Private Methods

- (void)resignFieldsOnView
{
    [_txtTitle resignFirstResponder];
    [_txtLink resignFirstResponder];
    [_txtImage resignFirstResponder];
}

- (IBAction)openDatePicker:(id)sender
{
    [self endEditing:YES];
    [ActionSheetDatePicker showPickerWithTitle:PICKER_TITLE_DOB
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[DateUtils dateFromString:[[_btnDate titleLabel] text]]
                                   minimumDate:[NSDate date]
                                   maximumDate:nil
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
     {
         [self setDateOfBirth:selectedDate];
     }
                                   cancelBlock:NULL
                                        origin:self];
}

- (void)setDateOfBirth:(NSDate *)selectedDate;
{
    if ([selectedDate compare:[NSDate date]] == NSOrderedAscending)
    {
        [Alert show:@"" andMessage:@"Event Date must not be past date."];
        return;
    }
    [_btnDate setTitle:[DateUtils stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (BOOL)validateFields
{
    NSString *title = [_txtTitle text];
    NSString *image = [_txtImage text];
    
    if ([StringUtils isEmptyOrNull:title])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:MESSAGE_ALERT_EMPTY_FIELD];
        return NO;
    }
    
    if (![StringUtils validateURL:image] && image.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    return YES;
}


@end
