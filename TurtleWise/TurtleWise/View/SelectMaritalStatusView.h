//
//  SelectMaritialStatusView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"
#import "TextField.h"

@interface SelectMaritalStatusView : BaseProfileWizardView

@property (weak, nonatomic) IBOutlet TextField *fieldKids;
@property (weak, nonatomic) IBOutlet TextField *fieldSiblings;
@property (weak, nonatomic) IBOutlet UIStepper *stepperKids;
@property (weak, nonatomic) IBOutlet UIStepper *stepperSiblings;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)onSteppingKids:(UIStepper *)sender;
- (IBAction)onSteppingSiblings:(UIStepper *)sender;
- (IBAction)setMaritalStatus:(id)sender;

- (BOOL)areFieldsValid;

@end
