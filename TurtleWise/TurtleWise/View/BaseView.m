//
//  BaseView.m
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "BaseController.h"
#import "UserStats.h"
#import "BaseView.h"

@interface BaseView ()<UIGestureRecognizerDelegate>

@property(nonatomic, strong) UISwipeGestureRecognizer *swipeRight;

- (UISwipeGestureRecognizer *)getRightSwipeGestureWithSelector:(SEL)selector andDirection:(UISwipeGestureRecognizerDirection)direction;

@end

@implementation BaseView

@synthesize controller;

#pragma mark - Init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(void) notImplemented
{
    [Alert show:@"" andMessage:@"Coming soon!"];
}

#pragma mark - UIView Life Cycle

-(void) viewDidLoad{}
-(void) viewDidAppear{}
-(void) viewWillAppear{}
-(void) viewWillDisappear {}
-(void) viewDidDisappear{}
-(void) viewDidLayoutSubviews {}
-(void) resignFieldsOnView{}

#pragma mark - Swipe Gesture Handling

- (UISwipeGestureRecognizer *)getRightSwipeGestureWithSelector:(SEL)selector andDirection:(UISwipeGestureRecognizerDirection)direction
{
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:selector];
    
    [swipeRight setDelegate:self];
    [swipeRight setDirection:direction];
    
    return swipeRight;
    
}

- (void) setSwipeToBackGesture
{
    // Adding the swipe gesture on image view
    [self addGestureRecognizer:[self getRightSwipeGestureWithSelector:@selector(handleNavigationRightSwipe)
                                                         andDirection:UISwipeGestureRecognizerDirectionRight]];
}

- (void)removeGesture
{
    for (UIGestureRecognizer *gestures in [self gestureRecognizers])
    {
        [self removeGestureRecognizer:gestures];
    }
}

- (void) setWizardSwipeGestures
{
    [self addGestureRecognizer:[self getRightSwipeGestureWithSelector:@selector(handleLeftSwipe)
                                                         andDirection:UISwipeGestureRecognizerDirectionLeft]];
    
    [self addGestureRecognizer:[self getRightSwipeGestureWithSelector:@selector(handleWizardRightSwipe)
                                                         andDirection:UISwipeGestureRecognizerDirectionRight]];
}


#pragma mark - Swipe Gesture Actions

- (void)handleNavigationRightSwipe
{
    [[[self controller] navigationController] popViewControllerAnimated:YES];
}

- (void)handleWizardRightSwipe
{
    [(BaseController *)[self controller] leftButtonAction:[[[self controller] navigationItem] leftBarButtonItem]];
}


- (void)handleLeftSwipe
{
    [(BaseController *)[self controller] rightButtonAction:[[[self controller] navigationItem] rightBarButtonItem]];
}

#pragma mark - Tab Bar and Notification

-(BOOL) hasTabBar
{
    return NO;
}

-(void) updateStatsOnTabBar:(UserStats *)userStats
{
    //TODO: Will be overidded...
}

@end
