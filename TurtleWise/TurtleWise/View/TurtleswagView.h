//
//  TurtleswagView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface TurtleswagView : BaseView < UITableViewDataSource, UITableViewDelegate >

@property (weak, nonatomic) IBOutlet UILabel *lblHeading;

@property (weak, nonatomic) IBOutlet UITableView *turtleswagList;

@property(nonatomic, strong) NSArray *catalogItems;
@property(nonatomic, strong) NSArray *beneficiaryList;

- (void)populateCharityList:(NSArray *)charityList andGiftCard:(NSArray *)giftCardList;

@end
