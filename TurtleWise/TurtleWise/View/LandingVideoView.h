//
//  LandingVideoView.h
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface LandingVideoView : BaseView

@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlay;
@property (strong, nonatomic) MPMoviePlayerController *playerController;

-(void) resetVideo;

@end
