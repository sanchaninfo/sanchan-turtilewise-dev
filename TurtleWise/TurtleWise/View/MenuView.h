//
//  MenuView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface MenuView : BaseView

@property (weak, nonatomic) IBOutlet UIButton *btnSwitchRoles;
@property (weak, nonatomic) IBOutlet UILabel *lblUserRole;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *manageBrandedPageHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblManagePage;

@property(nonatomic) UserRole userRole;

- (void)updateUserRole;
- (void)updateBrandedPage;

- (IBAction)switchUserRole:(id)sender;
- (IBAction)showUserProfile:(id)sender;
- (IBAction)showTurtleSwag:(id)sender;
- (IBAction)showHelp:(id)sender;
- (IBAction)logout:(id)sender;
- (IBAction)showSettings:(id)sender;
- (IBAction)showFAQ:(id)sender;
- (IBAction)showManageBrandedPage:(id)sender;
- (IBAction)showBoardofAdvisors:(id)sender;
- (IBAction)whoIAdvise:(id)sender;


@end
