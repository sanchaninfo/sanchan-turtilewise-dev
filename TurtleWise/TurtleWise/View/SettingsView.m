//
//  SettingsView.m
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "SettingsController.h"
#import "SettingsView.h"
#import "StringUtils.h"
#import "TextField.h"
#import "Settings.h"
#import "Alert.h"
#import "Font.h"
#import "UserDefaults.h"

@implementation SettingsView

#pragma mark - Life Cycle

- (void)viewDidLoad{
    [self setupUI];
}

- (void)setupUI{
    
    [self hideResendEmailViews];
    
    if([UserDefaults loginFlowType] == LoginFlowTypeSocial){
        self.pendingVerificationStaticLabelTopSpace.constant = -310;
        [self.changePasswordFields setValue:@(YES) forKey:@"hidden"];
        [self layoutIfNeeded];
    }
}

#pragma mark -

- (void)resetTxtFieldsOnSuccess
{
    [_txtFieldOldPassword setText:@""];
    [_txtFieldNewPassword setText:@""];
    [_txtFieldConfirmPassword setText:@""];
}

- (void)updateViewWithSettings:(Settings *)settings
{
    [_switchQuestionNotificationSetting setOn:settings.shouldNotifyOnQuestion];
    [_switchAnswerNotificationSetting setOn:settings.shouldNotifyOnAnswer];
    if(!settings.emailVerified){
        [self showResendEmailViews];
    }
    
    BOOL shouldSendDailyDigest = [settings shouldSendEmailDigest];
    
    [_switchDailyEmailDigest setOn:shouldSendDailyDigest];
    [self handleDailyDigestToggle:shouldSendDailyDigest];
    
    [_switchQuestionNotificationSetting setEnabled:!shouldSendDailyDigest];
    [_switchAnswerNotificationSetting setEnabled:!shouldSendDailyDigest];
}

-(void)hideResendEmailViews{
    [self.resendEmailViews setValue:@(YES) forKey:@"hidden"];
    self.emailNotificationsStaticLabelTopSpace.constant = -140;

    [UIView animateWithDuration:0.0 animations:^{
        [self layoutIfNeeded];
    }];
}

-(void)showResendEmailViews{
    
    if([UserDefaults loginFlowType] == LoginFlowTypeSocial)
        return;
    
    [self.resendEmailViews setValue:@(NO) forKey:@"hidden"];
    self.emailNotificationsStaticLabelTopSpace.constant = 30;
    [UIView animateWithDuration:0 animations:^{
        [self layoutIfNeeded];
    }];
}

- (void)onVerificationEmailResent{
}

- (void)handleDailyDigestToggle:(BOOL)isEnabled
{
    if (isEnabled)
    {
        [_switchAnswerNotificationSetting setOn:NO];
        [_switchQuestionNotificationSetting setOn:NO];
    }
    
    [_switchAnswerNotificationSetting setEnabled:!isEnabled];
    [_switchQuestionNotificationSetting setEnabled:!isEnabled];
}

- (NSDictionary *)getParamsForSaveSettings
{
    NSDictionary * params = @{KEY_EMAIL:@{
                                      KEY_QUESTION_RECEIVED : [NSNumber numberWithBool:[_switchQuestionNotificationSetting isOn]],
                                      KEY_ANSWER_RECEIVED: [NSNumber numberWithBool:[_switchAnswerNotificationSetting isOn]],
                                      KEY_DAILY_DIGEST : [NSNumber numberWithBool:[_switchDailyEmailDigest isOn]]
                                      }
                              };
    return params;
}

#pragma mark - IBActions

-(IBAction)onResendEmailBtnTap {
    [(SettingsController*)self.controller resendAccountVerificationEmail];
}

- (IBAction)onDailyEmailDigestToggle:(id)sender {
    [self handleDailyDigestToggle:[(UISwitch *)sender isOn]];
}

- (IBAction)onChangePassword:(UIButton *)sender {
    [self endEditing:YES];
    
    if ([self validateFields]) {
        [(SettingsController *)self.controller resetOldPassword:[_txtFieldOldPassword text] withPassword:[_txtFieldNewPassword text]];
    }
}

- (IBAction)onUpdateNotifications:(UIButton *)sender {    
    [(SettingsController *)self.controller updateSettingsWithParams:[self getParamsForSaveSettings]];
}

#pragma mark - Validation

- (BOOL)validateFields
{
    NSString *oldPassword = [_txtFieldOldPassword text];
    NSString *newPassword = [_txtFieldNewPassword text];
    NSString *confirmPassword = [_txtFieldConfirmPassword text];
    
    if ([StringUtils isEmptyOrNull:oldPassword])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_OLD_PASSWORD];
        return NO;
    }
    
    if ([StringUtils isEmptyOrNull:newPassword])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_NEW_PASSWORD];
        return NO;
    }
    
    if ([StringUtils isEmptyOrNull:confirmPassword])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_CONFIRM_PASSWORD];
        return NO;
    }
    
    if (![StringUtils compareString:newPassword withString:confirmPassword])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_PASSWORD_DONOT_MATCH];
        return NO;
    }
    
    if ([newPassword length] < 6 || [newPassword length] > 32)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_IN_VALID_PASSWORD];
        return NO;
    }
    
    return YES;
}

@end
