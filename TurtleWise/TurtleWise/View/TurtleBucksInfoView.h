//
//  TurtleBucksInfoView.h
//  TurtleWise
//
//  Created by Ajdal on 6/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TurtleBucksInfoView : UIView<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,weak) IBOutlet UITableView *table;

-(IBAction)onCloseBtnTap:(id)sender;
@end
