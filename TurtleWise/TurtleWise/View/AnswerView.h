//
//  AnswerView.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"
#import "Label.h"
#import "MZTimerLabel.h"
#import "TLTagsControl.h"
@class TabbarView;
@class Question;

@interface AnswerView : BaseView

-(void)setAnswerOptions:(NSArray*)answerOptions andTotalCount:(int)count;
-(void)showError:(NSString*)error;

@property (weak,nonatomic) IBOutlet TabbarView *tabBar;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak,nonatomic) IBOutlet UIImageView *questionImage;
@property (weak,nonatomic) IBOutlet Label *questionTitle;
@property (weak,nonatomic) IBOutlet MZTimerLabel *questionTime;
@property (weak, nonatomic) IBOutlet UILabel *lblRemaining;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPeriod;

@property (weak,nonatomic) IBOutlet UIView *fourChoiceQuestionView;
@property (weak,nonatomic) IBOutlet UIView *threeChoiceQuestionView;
@property (weak,nonatomic) IBOutlet UIView *yesNoQuestionView;

@property(nonatomic,weak) IBOutlet UITableView *tableViewAnswerOptionDetails;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property(nonatomic,strong) IBOutletCollection(UIView) NSArray *statsView;

#pragma mark - Answer Choice Buttons
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsTwoChoiceQuestion;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsThreeChoiceQuestion;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsFourChoiceQuestion;

@property(nonatomic,weak) IBOutlet NSLayoutConstraint *answerButtonsHolderHeightConstraint;

#pragma mark - Graph Bars
@property(nonatomic,weak) IBOutlet UIView *optionBarATwoChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarBTwoChoiceMCQ;

@property(nonatomic,weak) IBOutlet UIView *optionBarAThreeChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarBThreeChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarCThreeChoiceMCQ;

@property(nonatomic,weak) IBOutlet UIView *optionBarAFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarBFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarCFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UIView *optionBarDFourChoiceMCQ;

#pragma mark - Percentages
@property(nonatomic,weak) IBOutlet UILabel *percentageATwoChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageBTwoChoiceMCQ;

@property(nonatomic,weak) IBOutlet UILabel *percentageAThreeChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageBThreeChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageCThreeChoiceMCQ;

@property(nonatomic,weak) IBOutlet UILabel *percentageAFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageBFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageCFourChoiceMCQ;
@property(nonatomic,weak) IBOutlet UILabel *percentageDFourChoiceMCQ;

#pragma mark - Stats View Height Constraints
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *statsViewHeightConstraintTwoChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *statsViewHeightConstraintThreeChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *statsViewHeightConstraintFourChoiceQuestion;

#pragma mark - Graph Height Constraint
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphATwoChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphBTwoChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphAThreeChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphBThreeChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphCThreeChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphAFourChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphBFourChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphCFourChoiceQuestion;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraintGraphDFourChoiceQuestion;

#pragma mark - Tags View
@property(nonatomic,weak) IBOutlet TLTagsControl *advisorTagsView;
@property(nonatomic,weak) IBOutlet TLTagsControl *guruTagsView;
@property(nonatomic,assign) BOOL archived;

#pragma mark - Actions
-(IBAction)didTapAnswerChoiceButton:(id)sender;
-(IBAction)onCheckAdvisorsBtnTap:(id)sender;
-(IBAction)onViewChatsBtnTap:(id)sender;

-(void)setQuestion:(Question*)question;
@end
