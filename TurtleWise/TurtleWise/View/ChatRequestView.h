//
//  ChatRequestView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/22/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface ChatRequestView : BaseView < UITableViewDataSource, UITableViewDelegate >

@property (weak, nonatomic) IBOutlet UIView *tabBarContainerView;
@property (weak, nonatomic) IBOutlet UITableView *chatRequestList;

- (void)populateAdvisorsList:(NSArray *)advisors;

@end
