//
//  NewCheckAdvisorView.m
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewCheckAdvisorView.h"
#import "TabbarView.h"
#import "NewCheckAdvisorController.h"
#import "CheckAdvisorCell.h"

#define kSortBy @"Sort by:"
#define KSortOptionDateAnswered @"Newest"
#define KSortOptionChatAvailability @"Chat Availability"
#define KSortOptionLevel @"Level"
#define KSortOptionThumbsUp @"Thumbs up"

@interface NewCheckAdvisorView() <CheckAdvisorCellDelegate>

@end

@implementation NewCheckAdvisorView{
    UIButton *_selectedTab;
    NSArray *_answers;
    Question *_question;
    NSMutableArray *_advisorsForChat;
}

-(void)viewDidLoad{
    [self setupUI];
}

- (void)viewWillAppear {
    _advisorsForChat = nil;
}

-(NSArray*)getSelectedAdvisors{
    return _advisorsForChat;
}
-(void)setupUI{
    
    [self.tableView registerNib:[UINib nibWithNibName:[CheckAdvisorCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[CheckAdvisorCell cellName]];
    self.tableView.estimatedRowHeight = 307;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    
//    [_tabbar enableTab:YES tabbarType:TabbarViewTypeQuestion];
//    [_tabbar enableTab:YES tabbarType:TabbarViewTypeChat];
    [_tabbar setSelectedTab:TabbarViewTypeAnswer];
    _tabbar.delegate = [(NewCheckAdvisorController*)self.controller tabBarDelegate];
}

-(void)setQuestion:(Question*)question{
    _question = question;

    switch (_question.answerOptions.count) {
            
        case 0:
            [self layoutViewForComplexQuestion];
            break;
            
        case 2:
            [self layoutViewForTwoChoiceQuestion:_question.answerOptions];
            break;
            
        case 3:
            [self layoutViewForThreeChoiceQuestion:_question.answerOptions];
            break;
            
        case 4:
            [self layoutViewForFourChoiceQuestion:_question.answerOptions];
            break;
            
        default:
            break;
    }
    
}

-(void)setAnswers:(NSArray *)answers{
    [self.activityIndicator stopAnimating];
    _answers = answers;
    
    if(_answers.count == 0){
        [self showError:@"No response against selected option."];
    }
    else{
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
    }
    
}
#pragma mark -- UI Setup

-(void)layoutViewForComplexQuestion{
    self.buttonsHolderHeightConstraint.constant = 0;
    self.buttonsHolder.hidden = YES;
    [self layoutIfNeeded];
    
    [self setupAnswerOptionView:-1];
}

-(void)layoutViewForTwoChoiceQuestion:(NSArray*)answerOptions{
    [self.twoChoiceQuestionView setHidden:NO];
    [self.threeChoiceQuestionView setHidden:YES];
    [self.fourChoiceQuestionView setHidden:YES];
    
    for(UIButton *btn in self.answerOptionButtonsTwoChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
}
-(void)layoutViewForThreeChoiceQuestion:(NSArray*)answerOptions{
    [self.twoChoiceQuestionView setHidden:YES];
    [self.threeChoiceQuestionView setHidden:NO];
    [self.fourChoiceQuestionView setHidden:YES];
    
    for(UIButton *btn in self.answerOptionButtonsThreeChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
}
-(void)layoutViewForFourChoiceQuestion:(NSArray*)answerOptions{
    [self.twoChoiceQuestionView setHidden:YES];
    [self.threeChoiceQuestionView setHidden:YES];
    [self.fourChoiceQuestionView setHidden:NO];
    
    for(UIButton *btn in self.answerOptionButtonsFourChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
}
-(NSString*)getMCQBtnTileForAnswerAtIndex:(NSInteger)index{
    if(_question.questionType == QuestionTypeYN){
        if(index == 0)
            return @"YES";
        else
            return @"NO";
    }
    else if (_question.questionType == QuestionTypeMultipleChoice){
        switch (index) {
            case 0:
                return @"A";;
                
            case 1:
                return @"B";
                
            case 2:
                return @"C";
                
            case 3:
                return @"D";
                
            default:
                break;
        }
    }
    return @"";
}

#pragma mark --

-(void)showError:(NSString *)errorString{
    [self.activityIndicator stopAnimating];
    [self.errorLabel setHidden:NO];
    [self.errorLabel setText:errorString];
}

-(void)selectTab:(NSInteger)tag{
    if(_question.questionType == QuestionTypeComplex){
        [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:YES];
        return;
    }
    
    
    NSArray *answerButtons = self.answerOptionButtonsTwoChoiceQuestion;
    
    if(_question.answerOptions.count == 3)
        answerButtons = self.answerOptionButtonsThreeChoiceQuestion;
    else if (_question.answerOptions.count == 4)
        answerButtons = self.answerOptionButtonsFourChoiceQuestion;
    
    UIButton *btn = [[answerButtons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag == %d",tag]] firstObject];
    [self onAnswerTabSelection:btn];
}

-(void)setupAnswerOptionView:(NSInteger)selectedTab{
    
    if(_question.questionType == QuestionTypeMultipleChoice){
        self.answerOptionDetail.text = [_question.answerOptions objectAtIndex:selectedTab];
        return;
    }
    self.answerOptionDetailView.hidden = YES;
    self.tableViewTopMargin.constant = -44;
    [self layoutIfNeeded];
    
}

- (void)showCloseAlert{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:NULL
                                                                         message:@"Are you sure, you want to close this question?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [(NewCheckAdvisorController *)self.controller closeQuestion];
                                                }];
    [controller addAction:ok];
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"NO"
                                                      style:UIAlertActionStyleDefault
                                                    handler:nil];
    [controller addAction:cancel];
    
    
    [self.controller presentViewController:controller animated:YES completion:nil];
}

-(NSInteger)getSelectedTab{
    return [_selectedTab tag];
}

#pragma mark - Override Methods

- (BOOL)hasTabBar
{
    return YES;
}

- (void)updateStatsOnTabBar:(UserStats *)userStats
{
    if (!_tabbar)
    {
        return;
    }
    
    [_tabbar updateNotificationValues:userStats];
}

#pragma mark - Actions
-(void)resetView{
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height) animated:NO];
    [self.errorLabel setHidden:YES];
    [self.tableView setHidden:YES];
    [self.activityIndicator startAnimating];
}

-(void)onAnswerTabSelection:(UIButton*)btn{
    [self resetView];
    [_selectedTab setSelected:NO];
    [btn setSelected:YES];
    _selectedTab = btn;
    [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:YES];
    [self setupAnswerOptionView:[btn tag]];
    
}

-(IBAction)onAnswerChoiceBtnSelection:(id)sender{
    if((UIButton*)sender == _selectedTab)
        return;
    [self onAnswerTabSelection:(UIButton*)sender];
}

#pragma mark - UITableview DataSource/Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _answers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CheckAdvisorCell *cell = [tableView dequeueReusableCellWithIdentifier:[CheckAdvisorCell cellName]];
    
    Answer *answer = (Answer*)[_answers objectAtIndex:indexPath.row];
    [cell setAnswer:answer andQuestionType:_question.questionType];
    [cell configureChatViews:[self isAdvisorAddedToChat:answer.advisor]];
    [cell setNavController:self.controller.navigationController];
    [cell setDelegate:self];
    [cell setIndexPath:indexPath];
    [[cell btnAvailableForChat] setHidden:(!answer.advisorAvailableForChat || ![(NewCheckAdvisorController*)self.controller canStartNewChat])];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == _answers.count-1){
        [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:NO];
    }
}


-(BOOL)isAdvisorAddedToChat:(Advisor*)advisor{
    BOOL isAdded = NO;
    
    NSArray *filteredArray = [_advisorsForChat filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"advisorID == %@",advisor.advisorID]];
    
    if(filteredArray.count)
        isAdded = YES;
    
    return isAdded;
}
-(IBAction)showSortOptions{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kSortBy
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alert addAction:[self getSortActionDateAnswered]];

    //[alert addAction:[self getSortActionLevel]];
    [alert addAction:[self getSortActionThumbsUp]];
    [alert addAction:[self getSortActionChatAvailability]];
    [alert addAction:[self getSortActionCancel]];
    
    [self.controller presentViewController:alert animated:YES completion:nil];
}
#pragma mark - Sort Options
-(UIAlertAction*)getSortActionDateAnswered{
    return [UIAlertAction actionWithTitle:KSortOptionDateAnswered
                                    style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self resetView];
                                      [self.sortByButton setTitle:[NSString stringWithFormat:@"%@ %@",kSortBy,KSortOptionDateAnswered]];
                                      [(NewCheckAdvisorController*)self.controller setSortOrder:nil];
                                      [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:YES];
                                  }];
}
-(UIAlertAction*)getSortActionChatAvailability{
    return [UIAlertAction actionWithTitle:KSortOptionChatAvailability style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action){
                                      [self resetView];
                                      [self.sortByButton setTitle:[NSString stringWithFormat:@"%@ %@",kSortBy,KSortOptionChatAvailability]];
                                      [(NewCheckAdvisorController*)self.controller setSortOrder:@"chatAvailability"];
                                      [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:YES];
                                      
                                  }];
}
-(UIAlertAction*)getSortActionLevel{
    return [UIAlertAction actionWithTitle:KSortOptionLevel style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      
                                      [Alert show:@"" andMessage:@"Coming soon!"];
                                  }];
}
-(UIAlertAction*)getSortActionThumbsUp{
    return [UIAlertAction actionWithTitle:KSortOptionThumbsUp style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      
                                      [self resetView];
                                      [self.sortByButton setTitle:[NSString stringWithFormat:@"%@ %@",kSortBy,KSortOptionThumbsUp]];
                                      [(NewCheckAdvisorController*)self.controller setSortOrder:@"up"];
                                      [(NewCheckAdvisorController*)self.controller loadAnswersWithSearchOption:[_selectedTab tag] andReset:YES];
                                  }];
}
-(UIAlertAction*)getSortActionCancel{
    return [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction * action) {}];
}
#pragma mark - CheckAdvisorCell Delegate
-(void)didTapOnSubscribeLink:(CheckAdvisorCell *)cell{
    [(NewCheckAdvisorController*)self.controller showSubscriptionModal];
}

- (void)didSelected:(BOOL)selected advisor:(Advisor *)advisor indexPath:(NSIndexPath *)indexPath {
    if (!_advisorsForChat) {
        _advisorsForChat = [NSMutableArray new];
    }
    
    if (selected) {
        [(NewCheckAdvisorController*)self.controller checkAvailablityFromService:advisor indexPath:indexPath];
    }
    else {
        [self removeAdvisorFromChatPanel:advisor];
        [self updateAvailableForChat:NO atIndexPath:indexPath];
        [(NewCheckAdvisorController*)self.controller setupNavigationButtons];
    }
}

//- (void)addSelectedAdvisor:(Advisor*)advisor toChat:(BOOL)add{
//    if(!_advisorsForChat)
//        _advisorsForChat = [[NSMutableArray alloc] init];
//    
//    if(add)
//        [(NewCheckAdvisorController*)self.controller checkAvailablityFromService:advisor];
//    else {
//        [self removeAdvisorFromChatPanel:advisor];
//        [(NewCheckAdvisorController*)self.controller setupNavigationButtons];
//    }
//}

- (void)didGiveFeedback:(NSString*)feedback forAnswer:(Answer*)answer{
    [(NewCheckAdvisorController*)self.controller postFeedback:feedback forAnswer:answer];
}

- (void)updateAvailableForChat:(BOOL)selected atIndexPath:(NSIndexPath *)indexPath {
    CheckAdvisorCell * cell = [_tableView cellForRowAtIndexPath:indexPath];
    [[cell btnAvailableForChat] setSelected:selected];
}

-(void)addAdvisorToChatPanel:(Advisor*)advisor{
    Advisor *adv = [[_advisorsForChat filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"advisorID == %@",advisor.advisorID]] firstObject];
    if(adv)
        return;
    
    [_advisorsForChat addObject:advisor];
}

-(void)removeAdvisorFromChatPanel:(Advisor*)advisor{
    Advisor *adv = [[_advisorsForChat filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"advisorID == %@",advisor.advisorID]] firstObject];
    [_advisorsForChat removeObject:adv];
}
@end
