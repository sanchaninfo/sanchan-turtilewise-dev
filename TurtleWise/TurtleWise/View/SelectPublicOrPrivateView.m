//
//  SelectPublicOrPrivateView.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "SelectPublicOrPrivateView.h"
#import "SelectPublicOrPrivateController.h"
#import "StringUtils.h"
#import "Constant.h"
#import "UserProfile.h"

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 101

@interface SelectPublicOrPrivateView ()

@property(nonatomic, strong) NSString *publicOrPrivate;

@end

@implementation SelectPublicOrPrivateView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _publicOrPrivate = EMPTY_STRING;
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    _publicOrPrivate = [profile publicOrPrivate];
    
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        UIButton *buttonToSelect = (UIButton *)[self viewWithTag:i];
        
        if ([StringUtils compareString:[[buttonToSelect titleLabel] text] withString:[profile publicOrPrivate]])
        {
            [buttonToSelect setSelected:YES];
        }
    }
    
}


- (IBAction)selectFromPublicOrPrivateOption:(id)sender
{
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            _publicOrPrivate = ([sender isSelected]) ? [[sender titleLabel] text] : EMPTY_STRING;
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setPublicOrPrivate:_publicOrPrivate];
}

- (IBAction)submitWizard:(id)sender
{
    
    [self completeWizard];
}

#pragma mark - IBActions

- (IBAction)skipThisStep:(id)sender{
    
    [(SelectPublicOrPrivateController *)self.controller next];
    
}



@end
