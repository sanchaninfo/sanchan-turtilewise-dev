//
//  LoginView.m
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "LoginController.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "TextField.h"
#import "LoginView.h"
#import "Alert.h"

@interface LoginView ()

- (void)initUI;

@end

@implementation LoginView

#pragma mark - Life Cycle Methods

-(void)viewDidLoad{ 
    [super viewDidLoad];
    [self populateLoginFieldsIfRequired];
    [self initUI];
}

- (void)initUI{
    _btnSubmit.layer.cornerRadius = floor(_btnSubmit.frame.size.height/4);
    _btnSubmit.clipsToBounds = YES;
    
    [self setSwipeToBackGesture];
}

- (void)setUsername:(NSString *)username andPassword:(NSString *)password{
    [_txtFieldEmail setText:username];
    [_txtFieldPassword setText:password];
}

-(void)populateLoginFieldsIfRequired{
    if([StringUtils isEmptyOrNull:[UserDefaults getLoginEmail]] || [StringUtils isEmptyOrNull:[UserDefaults getLoginPassword]])
        return;
    
    [self.rememberMeBtn setSelected:YES];
    [self setUsername:[UserDefaults getLoginEmail] andPassword:[UserDefaults getLoginPassword]];
}

#pragma mark IBActions

- (IBAction)onSubmit:(id)sender{
    if ([self validateFields])
    {
        [(LoginController *)self.controller loginWithEmail:[_txtFieldEmail text] andPassword:[_txtFieldPassword text]];
        if(self.rememberMeBtn.isSelected){
            [UserDefaults saveLoginEmail:[_txtFieldEmail text]];
            [UserDefaults saveLoginPassword:[_txtFieldPassword text]];
        }
        else
            [UserDefaults clearSavedLoginCredentials];
    }
}

-(IBAction)onRememberMeBtnTap:(id)sender{
    UIButton *rememberMeBtn = (UIButton*)sender;
    [rememberMeBtn setSelected:!rememberMeBtn.selected];
}

- (IBAction)onClickForgotPassword {
    [(LoginController *)self.controller openForgotPasswordControllerWithEmail:[_txtFieldEmail text]];
}

#pragma -  Validators

- (BOOL)validateFields{
    NSString *email = [_txtFieldEmail text];
    
    if ([StringUtils isEmptyOrNull:email])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL];
        return NO;
    }
    
    if ([StringUtils isEmptyOrNull:[_txtFieldPassword text]])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_PASSWORD];
        return NO;
    }
    
    if (![StringUtils validateEmail:email])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_EMAIL_ADDRESS];
        return NO;
    }
    
    [_txtFieldEmail resignFirstResponder];
    [_txtFieldPassword resignFirstResponder];
    
    return YES;
}

@end