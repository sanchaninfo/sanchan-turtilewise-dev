//
//  UpdatePageNameAndTypeView.m
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "UpdatePageNameAndTypeView.h"
#import "BrandedPage.h"
#import "TextField.h"
#import "StringUtils.h"

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 101

#define TITLE_VALIDATION @"Required!"
#define MESSAGE_ALERT_VALIDATION @"Name is required!"
#define MESSAGE_ALERT_BUTTON_VALIDATION @"Type is required!"
#define DONE_BUTTON_TITLE_VALIDATION @"oops!"
#define DONE_BUTTON_MESSAGE_ALERT_VALIDATION @"Page already created!"

@interface UpdatePageNameAndTypeView ()
@property(nonatomic, readonly) UpdatePageNameAndTypeController *controller;
@end

@implementation UpdatePageNameAndTypeView
@dynamic controller;

-(void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Override Methods

- (void)updateData:(BrandedPage *)pageDetail
{
    [_txtName setText:[pageDetail name]];
}

- (void)saveWizardData:(BrandedPage *)pageDetail
{
    if (![StringUtils isEmptyOrNull:_txtName.text])
    {
        [pageDetail setName:_txtName.text];
    }
    
}

- (IBAction)submitWizard:(id)sender
{
    [self.controller savePageDetail];
}

@end
