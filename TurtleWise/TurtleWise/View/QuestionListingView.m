//
//  SeekerView.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "QuestionListingController.h"
#import "QuestionListingView.h"
#import "QuestionTableCell.h"
#import "AnswerController.h"
#import "UserDefaults.h"
#import "TabbarView.h"
#import "UserStats.h"
#import "Question.h"
#import "ChatListTableCell.h"

@interface QuestionListingView ()<UITableViewDataSource,UITableViewDelegate, TabbarViewDelegate,QuestionTableCellDelegate>

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *selectedQuestions;
@property (nonatomic, assign) BOOL refreshListOnAppear;
@property (nonatomic) UserRole userRole;
@property (nonatomic,strong) NSString *userID;

@end

@implementation QuestionListingView

-(void)viewDidLoad{
    [self setupUI];
    _userID = [UserDefaults getUserID];
    _selectedQuestions = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear
{
     _userRole = [(QuestionListingController *)[self controller] getUserRole];
     [_tabBar setSelectedTab:[(QuestionListingController *)self.controller getListType]];
}

- (void)viewDidAppear
{
    [_tabBar updateNotificationValues:[UserDefaults getUserStats]];
}

-(void)setupUI{    
    [_tableView registerNib:[UINib nibWithNibName:[QuestionTableCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[QuestionTableCell cellName]];
    [_tableView registerNib:[UINib nibWithNibName:[ChatListTableCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[ChatListTableCell cellName]];
    [_tableView setEstimatedRowHeight:[QuestionTableCell cellHeight]];
    [_tableView setRowHeight:UITableViewAutomaticDimension];
    
    _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [self setupTabbar];
}

- (void)setupTabbar{
    _tabBar.delegate = self;

    [_tabBar enableTab:YES tabbarType:TabbarViewTypeQuestion];
    [_tabBar enableTab:YES tabbarType:TabbarViewTypeAnswer];
    [_tabBar enableTab:YES tabbarType:TabbarViewTypeChat];
}

#pragma mark - Override Methods

- (BOOL)hasTabBar{
    return YES;
}

- (void)updateStatsOnTabBar:(UserStats *)userStats{
    if (!_tabBar)
        return;
    
    [_tabBar updateNotificationValues:userStats];
}


-(void)populateTableView:(NSArray *)array{
    [self.activityIndicator stopAnimating];
    [self hideTableFooter];
    self.dataArray = [array mutableCopy];
    self.errorLabel.hidden = array.count;
    self.tableView.hidden = !self.errorLabel.hidden;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadTable];
    });
}

- (NSString *)titleForRowAction:(NSIndexPath *)indexPath{
    UserRole role = [(QuestionListingController*)self.controller getUserRole];
    if (role == UserRoleAdvisor)
        return @"Delete";
    return @"Close";
}

- (void)onTableEditAction:(UITableViewRowAction *)action forRowAtIndexPath:(NSIndexPath *)indexPath{
    UserRole role = [(QuestionListingController*)self.controller getUserRole];

    UIAlertController *controller = (role == UserRoleSeeker) ? [self getCloseQuestionConfirmation:indexPath] : [self getDeleteQuestionConfirmation:indexPath];
    
    [self.controller presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Alerts
-(UIAlertController*)getCloseQuestionConfirmation:(NSIndexPath*)indexPath{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:NULL
                                                                         message:@"Are you sure you want to close this question?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    NSString * questionId = [(Question *)self.dataArray[indexPath.row] questionId];
                                                    [self closeQuestion:questionId];
                                                }];
    [controller addAction:ok];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"NO"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [self.tableView setEditing:NO];
                                                        //[self onEditBtnTap];
                                                    }];
    [controller addAction:cancel];
    
    return controller;
}
-(UIAlertController*)getDeleteQuestionConfirmation:(NSIndexPath*)indexPath{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:NULL
                                                                         message:@"Are you sure you want to delete this question?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    if (indexPath)
                                                    {
                                                        [_selectedQuestions addObject:self.dataArray[indexPath.row]];
                                                    }
                                                    
                                                    [self deleteSelectedQuestions];
                                                }];
    [controller addAction:ok];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"NO"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        //[self.tableView setEditing:NO];
                                                        [self onEditBtnTap];
                                                    }];
    [controller addAction:cancel];
    
    return controller;
}


#pragma mark --
- (void)closeQuestion:(NSString *)questionId{
    [(QuestionListingController *)self.controller closeQuestion:questionId];
}
-(void)deleteSelectedQuestions{
    
    if(_selectedQuestions.count == 0)
        return;
    
    NSMutableArray *questionIds = [[NSMutableArray alloc] init];
    for(Question *ques in _selectedQuestions){
        [questionIds addObject:ques.questionId];
    }
    [(QuestionListingController*)self.controller deleteQuestions:questionIds];
}

- (void)closeQuestionSuccess{
    //Update Stats
    UserStats *userStats = [UserDefaults getUserStats];
    [userStats markActiveQuestionInActive];
    
    [_tabBar updateNotificationValues:userStats];
    [_tableView setEditing:NO];
}

-(void)deleteQuestionsSuccess{
    [_selectedQuestions removeAllObjects];
    [self.tableView setEditing:NO];
    [self setToolbarHidden:YES];
    [self.controller setupRightNavigationButtonWithName:@"Edit" withOnClickAction:^{
        [self onEditBtnTap];
    }];
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
    
    UserStats *userStats = [UserDefaults getUserStats];
    [userStats decreaseUnAnsweredQuestionCount];
    
    [_tabBar updateNotificationValues:userStats];
}

- (void)setToolbarHidden:(BOOL)hidden
{
    if (hidden)
    {
        [self.toolbarBottomMargin setConstant:-self.toolbar.frame.size.height];
        return;
    }
    [self.toolbarBottomMargin setConstant:0.0];
}

#pragma mark - Actions
-(void)onEditBtnTap{
    
    [self bringSubviewToFront:self.toolbar];
    UserRole userRole = [(QuestionListingController*)self.controller getUserRole];
    self.deleteBarBtn.title = userRole == UserRoleAdvisor ? @"Delete" : @"Close";
    
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
    [self setToolbarHidden:!self.tableView.isEditing];
    [self.controller setupRightNavigationButtonWithName:self.tableView.isEditing ? @"Cancel" : @"Edit" withOnClickAction:^{
        [self onEditBtnTap];
    }];

    [_selectedQuestions removeAllObjects];
    self.deleteBarBtn.enabled = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self reloadTable];
    }];
}

-(IBAction)onDeleteBtnTap{
    UIAlertController *controller = [self getDeleteQuestionConfirmation:NULL];
    [controller setMessage:@"Are you sure you want to delete these questions?"];
    
    [self.controller presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Table Footer
-(void)addTableFooterWithActivityIndicator{
    if(self.dataArray.count == 0)
        return;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.center = footerView.center;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    [footerView addSubview:activityIndicator];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.tableFooterView = footerView;
}

-(void)hideTableFooter{
    self.tableView.tableFooterView = nil;
}

#pragma mark - UITableView DataSource and Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    
    if(_tabBar.selectedTab == TabbarViewTypeChat){
        cell = [self getChatTableCellForRowAtIndexPath:indexPath];
    }
    else{
        cell = [self getQuestionTableCellForRowAtIndexPath:indexPath];
    }
    
    
    if(tableView.isEditing){
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    }
    else
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)reloadTable{
    [self.tableView reloadData];
}

-(UITableViewCell*)getChatTableCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    ChatListTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[ChatListTableCell cellName]];
    if(![[self.dataArray objectAtIndex:indexPath.row] isKindOfClass:[Conversation class]])
        return cell;//To tackle the bug when user switches between tabs very quickly.
    
    Conversation *conversation = self.dataArray[indexPath.row];
    [cell setParticipantList:[self getChatParticipantsAsCommaSeparatedString:[conversation getParticipantsForUserRole:_userRole andUserId:_userID]]];
    [cell set:conversation forRole:_userRole];
    
    return cell;
}
-(UITableViewCell*)getQuestionTableCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QuestionTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[QuestionTableCell cellName]];
//    cell.delegate = self;
    [cell set:self.dataArray[indexPath.row]];
    
    if (_userRole == UserRoleAdvisor)
        [cell hideResponseCounter];
    
    if (([(QuestionListingController *)self.controller getListType] ==  TabbarViewTypeAnswer) && (_userRole == UserRoleAdvisor))
        [cell hideQuestionTimer];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.tabBar.selectedTab == TabbarViewTypeChat)
        [self chatListingDidSelectRowAtIndexPath:indexPath];
    else
        [self questionListingDidSelectRowAtIndexPath:indexPath];
    
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!tableView.isEditing)
        return;
    
    [_selectedQuestions removeObject:[self.dataArray objectAtIndex:indexPath.row]];
    if(_selectedQuestions.count == 0)
        self.deleteBarBtn.enabled = NO;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == self.dataArray.count-1 && !tableView.isEditing){
        
        if(_tabBar.selectedTab == TabbarViewTypeChat)
            [(QuestionListingController*)self.controller loadMoreConversations];
        else
            [(QuestionListingController*)self.controller loadMoreQuestions];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_tabBar.selectedTab == TabbarViewTypeChat)
        return NO;
    
    
    UserRole userRole = [(QuestionListingController*)self.controller getUserRole];
    Question * question = [self.dataArray objectAtIndex:indexPath.row];

    if ((userRole == UserRoleSeeker && question.status != QuestionStatusClosed && ![question isExpired]) || (userRole == UserRoleAdvisor && _tabBar.selectedTab == TabbarViewTypeQuestion))
        return YES;
    
    return NO;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

    // DON'T REMOVE THIS METHOD.
    
    // All tasks are handled by blocks defined in editActionsForRowAtIndexPath, however iOS8 requires this method to enable editing
    
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *actions = [NSMutableArray array];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:[self titleForRowAction:indexPath] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self onTableEditAction:action forRowAtIndexPath:indexPath];
    }];
    
    deleteAction.backgroundColor = APP_GREY_COLOR;
    [actions addObject:deleteAction];
    
    return actions;
}

#pragma mark - Cell Tap
-(void)chatListingDidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [(QuestionListingController *)self.controller openChattingControllerForConversation:[_dataArray objectAtIndex:[indexPath row]]];
}


-(void)questionListingDidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.tableView.isEditing){
        [_selectedQuestions addObject:[self.dataArray objectAtIndex:indexPath.row]];
        self.deleteBarBtn.enabled = YES;
        return;
    }
    
    //Forcing the timer to stop for the selected question
    QuestionTableCell *cell = (QuestionTableCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if([UserDefaults getUserRole] == UserRoleAdvisor){
        [(QuestionListingController*)self.controller openGiveAnswerControllerWithQuestion:[self.dataArray objectAtIndex:indexPath.row]
                                                                               isReadOnly:(_tabBar.selectedTab == TabbarViewTypeAnswer)];
        [cell resetTimerLabel];
        return;
    }
    
    Question *question = [self.dataArray objectAtIndex:[indexPath row]];
    
    if (![question isRead]){
        [question setIsRead:YES];
        
        UserStats *userStats = [UserDefaults getUserStats];
        [userStats markUnReadQuestionRead];
    }
    
    
    
    [(QuestionListingController*)self.controller openAnswerControllerWithQuestion:[self.dataArray objectAtIndex:indexPath.row]];
    [cell resetTimerLabel];
    
    
}
-(void)resetTableView{
    [self.errorLabel setHidden:YES];
    [self.tableView setEditing:NO];
    [self.dataArray removeAllObjects];
    [self reloadTable];
    self.tableView.hidden = YES;
    [self.activityIndicator startAnimating];
}
-(void)didSelectTabBarBtn:(TabbarViewType)tabViewType{
    [self.controller.navigationController popToViewController:(QuestionListingController*)self.controller animated:NO];
    [_tabBar setSelectedTab:tabViewType];
}

#pragma mark - QuestionTableCell Delegate
-(void)removeClosedQuestionFromList:(Question*)question{//This delegate method will only be fired in case of Guru.
    [self.dataArray removeObject:question];
    [self.tableView reloadData];
}

#pragma mark - TabbarView Delegate
-(void)tabbar:(TabbarView *)tabbar didSelectTab:(TabbarViewType)tabViewType
{
    [self resetTableView];
    [self setToolbarHidden:YES];
    
    [(QuestionListingController *)self.controller setupWithListType:tabViewType];
}

#pragma mark -- 
-(NSString*)getChatParticipantsAsCommaSeparatedString:(NSArray*)participants{
    
    
    NSMutableString *commaSeparatedNameList = [[NSMutableString alloc] init];
    
    for(int index=0;index<participants.count;index++){
        [commaSeparatedNameList appendString:[participants objectAtIndex:index]];
        if(index != participants.count-1)
            [commaSeparatedNameList appendString:@", "];
    }
    
    return commaSeparatedNameList;
}
@end
