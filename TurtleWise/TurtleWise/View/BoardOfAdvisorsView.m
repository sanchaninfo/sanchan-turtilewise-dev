//
//  BoardOfAdvisorsView.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 12/11/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BoardOfAdvisorsView.h"
#import "BoardOfAdvisorsController.h"
#import "BoardOfAdvisorCell.h"

@implementation BoardOfAdvisorsView

-(void)getAdvisorsList{
    
    [(BoardOfAdvisorsController*)[self controller] getBoardofAdvisorsWith:@"approved"];
    
    [self.listofAdvisorsView registerNib:[UINib nibWithNibName:@"BoardOfAdvisorCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listofAdvisors.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BoardOfAdvisorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
   
    NSDictionary *advisorDict = _listofAdvisors[indexPath.row];
    NSDictionary *dict = advisorDict[@"advisor"];
    
    cell.nameLabel.text = [[NSString stringWithFormat:@"%@ %@",dict[@"firstName"],dict[@"lastName"]] capitalizedString];
    cell.answeredLabel.text = [NSString stringWithFormat:@"Answered\n\n%@",dict[@"questionsAnswered"]];
    cell.thumbsUp.text = [NSString stringWithFormat:@"%@",dict[@"rating"][@"up"]];
    cell.thumbsDown.text = [NSString stringWithFormat:@"%@",dict[@"rating"][@"down"]];
    return cell;
}
@end
