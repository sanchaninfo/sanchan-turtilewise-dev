//
//  RatingAdvisorsView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "RatingAdvisorsController.h"
#import "RatingAdvisorsView.h"
#import "RatingAdvisorsCell.h"
#import "TabbarView.h"
#import "Constant.h"

#define TABLE_CELL_HEIGHT 55.0
#define CELL_NIB_NAME @"RatingAdvisorsCell"

#define KEY_ADVISOR_ID @"advisorId"
#define KEY_ADVISOR_RATING @"rating"

@interface RatingAdvisorsView () < RatingAdvisorsCellDelegate >

@property(nonatomic, strong) NSArray *advisors;
@property(nonatomic, strong) NSMutableArray *advisorsRating;

- (void)setupTableView;

@end

@implementation RatingAdvisorsView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    _advisorsRating = [NSMutableArray new];
    
    [self setupTableView];
}

- (void)populateAdvisorsList:(NSArray *)advisors
{
    _advisors = [[NSArray alloc] initWithArray:advisors];
    
    [_advisorList reloadData];
}

- (NSMutableArray *)advisorsRating
{
    return _advisorsRating;
}

#pragma mark - Set Up View Methods

- (void)setupTableView
{
    [_advisorList registerNib:[UINib nibWithNibName:CELL_NIB_NAME bundle:nil] forCellReuseIdentifier:CELL_REUSE_IDENTIFIER];
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_advisors count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RatingAdvisorsCell *chatRequestCell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER];
    
    [chatRequestCell set:[_advisors objectAtIndex:[indexPath row]]];
    [chatRequestCell setDelegate:self];
    
    return chatRequestCell;
}

#pragma mark - UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_CELL_HEIGHT;
}

#pragma mark - RatingAdvisorsCellDelegate Methods

- (void)rateAdvisor:(NSString *)advisorId as:(NSString *)rating
{
    //Remove Previous Rating if Exist.
    [self removeRatingForAdvisor:advisorId];
    
    NSDictionary *advisorRating = @{
                                    KEY_ADVISOR_ID : advisorId,
                                    KEY_ADVISOR_RATING : rating
                                    };
    
    [_advisorsRating addObject:advisorRating];
}

- (void)removeRatingForAdvisor:(NSString *)advisorId
{
    [_advisorsRating enumerateObjectsUsingBlock:^(NSDictionary *advisorRating, NSUInteger idx, BOOL * _Nonnull stop)
     {
         if ([advisorId isEqualToString:[advisorRating objectForKey:KEY_ADVISOR_ID]])
         {
             [_advisorsRating removeObjectAtIndex:idx];
         }
     }];
}

@end
