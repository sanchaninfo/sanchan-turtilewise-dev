//
//  SelectEthnicReligionView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TextField;

@interface SelectEthnicReligionView : BaseProfileWizardView

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnReligion;
@property (weak, nonatomic) IBOutlet UIButton *btnEthnicity;
@property (weak, nonatomic) IBOutlet UIButton *btnPolicticalAffliation;


//Actions
- (IBAction)setOrientation:(id)sender;
- (IBAction)submitWizard:(id)sender;
- (IBAction)onPoliticalAff:(id)sender;
- (IBAction)chooseReligion:(id)sender;
- (IBAction)chooseEhnicity:(id)sender;

- (void)saveTags;

@end
