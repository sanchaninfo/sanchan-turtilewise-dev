//
//  UserBasicInfoView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TextField;
@class Button;

@interface UserBasicInfoView : BaseProfileWizardView

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *imageUserProfile;
@property (weak, nonatomic) IBOutlet TextField *txtFieldFirstName;
@property (weak, nonatomic) IBOutlet TextField *txtFieldLastName;
@property (weak, nonatomic) IBOutlet Button *btnDOB;

//Actions
- (IBAction)changeUserProfile:(id)sender;
- (IBAction)setUserGender:(id)sender;
- (IBAction)submitWizard:(id)sender;
- (IBAction)openDatePicker:(id)sender;

- (BOOL)areFieldsValid;

@end
