//
//  AddArticlesView.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "AddArticlesView.h"
#import "AddArticlesController.h"
#import "StringUtils.h"
#import "Constant.h"
#import "UserProfile.h"
#import "BrandedPage.h"
#import "Article.h"
#import "TextField.h"

@interface AddArticlesView ()
@property(nonatomic, readonly) AddArticlesController *controller;
@end

@implementation AddArticlesView
@dynamic controller;

#pragma mark - UIView Life Cycle

-(void)viewDidLoad
{
    [super viewDidLoad];
    
}

#pragma mark - Override Methods

- (void)updateData:(BrandedPage *)page withArticleNo:(int)articleNo
{
    articleNoInside = articleNo;
    if (articleNo != -1) {
        [_txtTitle setText:[[[page articles] objectAtIndex:articleNo] valueForKey:@"title"]];
        [_txtLink setText:[[[page articles] objectAtIndex:articleNo] valueForKey:@"link"]];
        [_txtImage setText:[[[page articles] objectAtIndex:articleNo] valueForKey:@"image"]];
    }
    
}

- (void)saveWizardData:(BrandedPage *)page
{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if(articleNoInside == -1){
        [dic setValue:_txtTitle.text forKey:@"title"];
        [dic setValue:_txtLink.text forKey:@"link"];
        [dic setValue:_txtImage.text forKey:@"image"];
        if([page articles].count > 0) {
            [[page articles] addObject:dic];
        } else {
            [page setArticles:[NSMutableArray arrayWithObjects:dic, nil]];
        }
        
    } else {
        [dic addEntriesFromDictionary:[[page articles] objectAtIndex:articleNoInside]];
        [dic setValue:_txtTitle.text forKey:@"title"];
        [dic setValue:_txtLink.text forKey:@"link"];
        [dic setValue:_txtImage.text forKey:@"image"];
        [[page articles] replaceObjectAtIndex:articleNoInside withObject:dic];
    }
    
}
- (IBAction)submitWizard:(id)sender
{
    if ([self validateFields]) {
        [self.controller savePageDetail];
    }
    
}


#pragma mark - Public/Private Methods

- (void)resignFieldsOnView
{
    [_txtTitle resignFirstResponder];
    [_txtLink resignFirstResponder];
    [_txtImage resignFirstResponder];
}

- (BOOL)validateFields
{
    NSString *title = [_txtTitle text];
    NSString *link = [_txtLink text];
    NSString *image = [_txtImage text];
    
    if ([StringUtils isEmptyOrNull:title])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:MESSAGE_ALERT_EMPTY_FIELD];
        return NO;
    }
    
    if (![StringUtils validateURL:link] && link.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    if (![StringUtils validateURL:image] && image.length > 0)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:TITLE_ALERT_INVALID_URL];
        return NO;
    }
    
    return YES;
}




@end
