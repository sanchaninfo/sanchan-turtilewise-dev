//
//  AddArticlesView.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "AddArticlesController.h"
@class TextField;
@interface AddArticlesView : BaseView {
    int articleNoInside;
}

@property (weak, nonatomic) IBOutlet TextField *txtTitle;
@property (weak, nonatomic) IBOutlet TextField *txtLink;
@property (weak, nonatomic) IBOutlet TextField *txtImage;
- (void)updateData:(BrandedPage *)page withArticleNo:(int)articleNo;
- (IBAction)submitWizard:(id)sender;
- (BOOL)validateFields;
- (void)saveWizardData:(BrandedPage *)page;
@end
