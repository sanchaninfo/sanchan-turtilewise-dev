//
//  SelectSignUpAsView.m
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//


#import "SelectSignUpAsController.h"
#import "SelectSignUpAsView.h"
#import "SelectInfluencerOnboardingController.h"
#import "StringUtils.h"
#import "Constant.h"
#import "UserProfile.h"
#import "TWWizardManager.h"
#import "UserDefaults.h"
#import "BrandedPage.h"

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 101

#define TITLE_VALIDATION @"Required!"
#define MESSAGE_ALERT_VALIDATION @"Name is required!"
#define MESSAGE_ALERT_BUTTON_VALIDATION @"Type is required!"
#define DONE_BUTTON_TITLE_VALIDATION @"oops!"
#define DONE_BUTTON_MESSAGE_ALERT_VALIDATION @"Page already created!"

@interface SelectSignUpAsView ()

@property(nonatomic, strong) NSString *signUpAsTag;

@end

@implementation SelectSignUpAsView

#pragma mark - Life Cycle Methods

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _signUpAsTag = EMPTY_STRING;
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    _signUpAsTag = [profile signUpAs];
    
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        UIButton *buttonToSelect = (UIButton *)[self viewWithTag:i];
        
        if ([StringUtils compareString:[[buttonToSelect titleLabel] text] withString:[profile signUpAs]])
        {
            [buttonToSelect setSelected:YES];
        }
    }
    
    [_fieldName setText:[profile name]];
}

- (void)saveWizardData:(BaseProfile *)profile
{
    //Will be Implemented by the Sub Class
}

- (void)saveWizardDataPage:(NSMutableDictionary *)page
{
    [page setValue:[_signUpAsTag lowercaseString] forKey:KEY_TYPE];
    
    NSString *nameOfBrandedPage = [_fieldName text];
    
    if (![StringUtils isEmptyOrNull:nameOfBrandedPage])
    {
        [page setValue:nameOfBrandedPage forKey:KEY_NAME];
    }
    [page setValue:[UserDefaults getUserID] forKey:KEY_OWNER];

}

#pragma mark - IBActions

- (IBAction)selectSignUpAs:(id)sender
{
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            _signUpAsTag = ([sender isSelected]) ? [[sender titleLabel] text] : EMPTY_STRING;
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)submitWizard:(id)sender
{
    NSString *pageIDStr = [UserDefaults getPageID];
    if ([pageIDStr isEqualToString:@""]){
        
        if (![self areFieldsValid])
        {
            return;
        }
        [self addPage];
        
    } else {
    
        [Alert show:DONE_BUTTON_TITLE_VALIDATION andMessage:DONE_BUTTON_MESSAGE_ALERT_VALIDATION];

    }
    
}

#pragma mark - IBActions

- (IBAction)skipThisStep:(id)sender{
    
    // [(SelectSignUpAsController *)self.controller next];

}


#pragma mark - Public/Private Methods

- (void)resignFieldsOnView
{
    [_fieldName resignFirstResponder];
}

- (BOOL)areFieldsValid
{

    if ([StringUtils isEmptyOrNull:_signUpAsTag]) {
        
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_BUTTON_VALIDATION];
        
        return NO;
    }
    
    if (![_fieldName isValid])
    {
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_VALIDATION];
        
        return NO;
    }
    
    return YES;
}




@end
