//
//  LoginView.h
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

@class TextField;
@interface LoginView : BaseView

@property (weak, nonatomic) IBOutlet UILabel *lblLoginType;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet TextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet TextField *txtFieldPassword;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

@property (weak, nonatomic) IBOutlet UIButton *rememberMeBtn;

-(IBAction)onRememberMeBtnTap:(id)sender;


-(void)setUsername:(NSString *)username andPassword:(NSString *)password;

@end
