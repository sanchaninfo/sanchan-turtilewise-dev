//
//  AskQuestionOneView.h
//  TurtleWise
//
//  Created by Sunflower on 9/4/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
@class BrandedPage;

@interface AskQuestionOneView : BaseView {
    NSMutableArray * influencerArray;
    NSMutableArray * organisationArray;
    BrandedPage *selectedBrandedPage;
    NSMutableDictionary *answers;
}

@property (weak, nonatomic) IBOutlet UIButton *communityImg;
@property (weak, nonatomic) IBOutlet UIButton *influencerImg;
@property (weak, nonatomic) IBOutlet UIButton *organizationImg;
@property (weak, nonatomic) IBOutlet UILabel *chooseAnswererLabel;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *influencerConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioHeight;
@property (weak, nonatomic) IBOutlet UICollectionView *pageCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionHeight;

@property (weak, nonatomic) IBOutlet UILabel *sendToLable;

@property (weak, nonatomic) IBOutlet UIButton *sendToInfluencer;
@property (weak, nonatomic) IBOutlet UIButton *sendToGrpMembers;
-(void) viewWithArray:(NSArray*)array;
@end
