//
//  NewUserInfoView.h
//  TurtleWise
//
//  Created by Usman Asif on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class UserProfile;

@interface NewUserInfoView : BaseView

@property (weak, nonatomic) IBOutlet UILabel *lblLevel;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserDp;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsUp;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsDown;
@property (weak, nonatomic) IBOutlet UILabel *lblThanks;
@property (weak, nonatomic) IBOutlet UILabel *lblThumbsUp;
@property (weak, nonatomic) IBOutlet UILabel *lblThumbsDown;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionsAnswered;
@property (weak, nonatomic) IBOutlet UILabel *lblChats;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberSince;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

- (void)setUser:(UserProfile *)profile;
@end
