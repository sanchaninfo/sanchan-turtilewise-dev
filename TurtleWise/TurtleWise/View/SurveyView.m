//
//  SurveyView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 7/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SurveyController.h"
#import "SurveyView.h"
#import "Constant.h"

#define PLACE_HOLDER_TEXT @"How was your TurtleWise chat experience for this question?"

#define TITLE_ALERT @"Thank You!"
#define MESSAGE_ALERT @"\nCongratulations,\n\nYou've earned 30 TurtleBucks"
#define TITLE_ALERT_BUTTON @"Checkout TurtleSwag"

@implementation SurveyView

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:PLACE_HOLDER_TEXT])
    {
        [textView setText:EMPTY_STRING];
        [textView setTextColor:[UIColor blackColor]];
    }
    
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:EMPTY_STRING])
    {
        [textView setText:PLACE_HOLDER_TEXT];
        
        [textView setTextColor:[UIColor lightGrayColor]];
    }
    
    [textView resignFirstResponder];
}

#pragma mark - IBActions

- (IBAction)submitSurvey:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:TITLE_ALERT
                                message:MESSAGE_ALERT
                               delegate:self
                      cancelButtonTitle:TITLE_ALERT_BUTTON
                      otherButtonTitles:nil]
     show];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [(SurveyController *)[self controller] goToTurtleSwags];
}

@end
