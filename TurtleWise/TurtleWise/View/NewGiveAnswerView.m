//
//  NewGiveAnswerView.m
//  TurtleWise
//
//  Created by Ajdal on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewGiveAnswerView.h"
#import "NewGiveAnswerController.h"
#import "StringUtils.h"
#import "AnswerOption.h"
#import "AnswerOptionDetailCell.h"
#import "Color.h"
#import "Keys.h"
#import "Answer.h"
#import "UserProfile.h"
#import "UserDefaults.h"
#import "UIImageView+AFNetworking.h"

#define kTagUndefined -1
#define kDefaultBottomViewHeight 377
#define kChatAvailabilityViewHeight 287
#define kComplexAnswerViewHeight 417
#define kSubmitButtonHeight 62

#define kTagThumbsUp 0
#define kTagThumbsDown 1
#define kTagThanks 2

#define QUESTION_TEXT_LIMIT 255

@interface NewGiveAnswerView ()<TabbarViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,MZTimerLabelDelegate>{
    Question *_question;
    NSString *_answer;
}

@end

@implementation NewGiveAnswerView
#pragma mark --
-(void)awakeFromNib{
    self.answerState = AnswerViewStateChoice;
    [self configureChatRewardLabel];
    self.premiumFeatureStaticLabel.hidden = [UserDefaults isPremiumUser];
    [self addConstraintsToContentView];
    //[self addKeyboardNotificationObservers];
    [self addTapGestureToDismissKeyboard];
}

- (void)viewDidLoad
{
    [self addTabBar];
    [[_imgUser layer] setCornerRadius:CGRectGetWidth([_imgUser frame])/2.0f];
    [_imgUser setClipsToBounds:YES];
}

- (BOOL)hasTabBar
{
    return YES;
}

- (void)updateStatsOnTabBar:(UserStats *)userStats{
    if (!_tabBar)
    {
        return;
    }
    
    [_tabBar updateNotificationValues:userStats];
}

-(void)configureChatRewardLabel{
    NSInteger myLevel = [UserDefaults getUserProfile].level;
    NSArray *levelTable = [UserDefaults getLevelTable];
    
    
    UserLevel *lvl = [[levelTable filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"level == %ld",myLevel]] firstObject];
    
    [self.chatRewardLabel setText:[NSString stringWithFormat:@"(Earn %ld TB if selected)",(long)lvl.chatReward]];
}

-(void)setQuestion:(Question *)question{
    _question = question;
    [self.questionImage setImage:[self getImageWithQuestionType:_question.questionType]];
    [self.questionTitle setText:question.questionText];
    [self setTimer];
    [self setupAnswerView];
    [self setupAnswerChoicesTableView];
    [self updateBottomView];

    if(![StringUtils isEmptyOrNull:[_question.seeker getDisplayNameOrTurtleIdentifier]]){
        [self configureSeekerNameBtn];
    }
    
}

-(void)configureSeekerNameBtn{
    [self.seekerNameBtn setTitle:[NSString stringWithFormat:@"%@",[_question.seeker getDisplayNameOrTurtleIdentifier]] forState:UIControlStateNormal];
    [self.seekerNameBtn.titleLabel setTextColor:[Color blueTagColor]];
    
    [_imgUser setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_question.seeker.imageURL]]
                             placeholderImage:[UIImage imageNamed:@"user-placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 [_imgUser setImage:image];
                             } failure:nil];

}

-(void)setTimer{
    [self.questionTime reset];
    if (_question.status == QuestionStatusClosed || [_question isExpired])
    {
        [self onTimerStop];
        return;
    }
    [self.questionTime setTimerType:MZTimerLabelTypeTimer];
    [self.questionTime setShouldCountBeyondHHLimit:YES];
    [self.questionTime setCountDownTime:[_question getRemainingTime]];
    [self.questionTime startWithEndingBlock:^(NSTimeInterval countTime) {
        _question.status = QuestionStatusClosed;
        [self onTimerStop];
    }];
    [self.questionTime setDelegate:self];
}

-(void)onTimerStop{
    [self.lblCurrentPeriod setHidden:YES];
    [self.questionTime setText:TITLE_TIMER_CLOSED];
    [self.questionTime setTextColor:[Color timerNormalColorForDetail]];
    [self.lblRemaining setText:@""];
}

-(void)preparePostAnswerCall
{
    if([self isFeedBackProvided]){
        [(NewGiveAnswerController*)self.controller postAnswer:[self getParametersForServiceCall]];
    }
}

#pragma mark - UI Setup
-(void)setupAnswerView{
    
    if(_question.questionType == QuestionTypeComplex){
        self.answerViewComplex.tag = 0;
        self.answerViewMCQ.tag = kTagUndefined;
        [self updateBottomView];
        return;
    }

    for(NSString *answerOption in _question.answerOptions){
        NSInteger index = [_question.answerOptions indexOfObject:answerOption];
        UIButton *btn = [[self.answerOptionButtons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag == %d",index]] firstObject];
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:index] forState:UIControlStateNormal];
        [btn setHidden:NO];
    }
}

- (void)setupUIForAdvisorAnswerView:(Answer *)answer
{
    UIColor * inactiveFieldColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    
    [_reasonTextView setUserInteractionEnabled:NO];
    [_reasonTextView setBackgroundColor:inactiveFieldColor];
    [_whyChooseMeTextView setUserInteractionEnabled:NO];
    [_whyChooseMeTextView setBackgroundColor:inactiveFieldColor];
    [_complexAnswerTextView setUserInteractionEnabled:NO];
    [_complexAnswerTextView setBackgroundColor:inactiveFieldColor];
    [_chatRewardLabel setHidden:YES];
    
    [_reasonTextView setText:answer.reason];
    [_whyChooseMeTextView setText:answer.whyChooseMe];
    [_premiumFeatureStaticLabel setHidden:YES];
    
    
    [self setAdvisorAnswer:answer.answerText];
    [self setFeedback:answer.seekersFeedback];
    [self setChatAvailability:answer.advisorAvailableForChat];
    _btnSubmit.hidden = YES;
}

-(void)setChatAvailability:(BOOL)advisorChatAvailability{
    if(advisorChatAvailability){
        [self.chatAvailabilityYes setSelected:YES];
    }
    else{
        [self.chatAvailabilityNo setSelected:YES];
    }
    
    self.chatAvailabilityYes.userInteractionEnabled = self.chatAvailabilityNo.userInteractionEnabled = NO;
}
- (void)setAdvisorAnswer:(NSString *)answer
{
    if (_question.questionType == QuestionTypeComplex)
    {
        [_complexAnswerTextView setText:answer];
        _lblCharCount.hidden = YES;
        return;
    }
    
    for(NSString *answerOption in _question.answerOptions){
        NSInteger index = [_question.answerOptions indexOfObject:answerOption];
        UIButton *btn = [[self.answerOptionButtons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag == %d",index]] firstObject];
        [btn setUserInteractionEnabled:NO];
        if ([StringUtils compareStringIgnorCase:answer withString:answerOption])
        {
            [btn setSelected:YES];
        }
    }
}

-(NSString*)getMCQBtnTileForAnswerAtIndex:(NSInteger)index{
    if(_question.questionType == QuestionTypeYN){
        if(index == 0)
            return @"YES";
        else
            return @"NO";
    }
    else if (_question.questionType == QuestionTypeMultipleChoice){
        switch (index) {
            case 0:
                return @"A";;
            
            case 1:
                return @"B";
                
            case 2:
                return @"C";
            
            case 3:
                return @"D";
                
            default:
                break;
        }
    }
    return @"";
}

-(void)addConstraintsToContentView{
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:0
                                                                         toItem:self
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1.0
                                                                       constant:0];
    [self addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:0
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1.0
                                                                        constant:0];
    [self addConstraint:rightConstraint];
    
    [self layoutIfNeeded];
}
-(void)addTabBar{
    [_tabBar setSelectedTab:TabbarViewTypeAnswer];
    [_tabBar enableTab:YES tabbarType:TabbarViewTypeAnswer];
    [_tabBar setDelegate:self];
}

-(void)hideQuestionTimer
{
    [_lblCurrentPeriod setHidden:YES];
    [_questionTime setHidden:YES];
    [_lblRemaining setText:@""];
}

#pragma mark - Actions
-(IBAction)onAnswerChoiceTap:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    BOOL isSelected = button.isSelected;
    
    
    for(UIButton *btn in self.answerOptionButtons){
        [btn setSelected:NO];
        if(btn == button){
            [btn setSelected:!isSelected];
        }
    }
}
-(IBAction)onChatAvailabilitySelection:(id)sender{
    UIButton *button = (UIButton*)sender;
    [button setSelected:YES];
    
    if(button == self.chatAvailabilityYes){
        [self.chatAvailabilityNo setSelected:NO];
    }
    else{
        [self.chatAvailabilityYes setSelected:NO];
    }
    
    if(!_chatAvailabilityYes.isSelected && !_chatAvailabilityNo.isSelected)
        _chooseMeViewHeightConstraint.constant = 0;
    else
        _chooseMeViewHeightConstraint.constant = _chatAvailabilityNo.isSelected ? 0 : kDefaultBottomViewHeight;
 
    _bottomViewHeightConstraint.constant = kChatAvailabilityViewHeight + _chooseMeViewHeightConstraint.constant + kSubmitButtonHeight + 10 + 20;
    self.scrollview.contentSize = CGSizeMake(CGRectGetWidth(_scrollview.bounds), _questionViewHeightConstraint.constant + _bottomViewHeightConstraint.constant);
    [self layoutIfNeeded];
}

-(IBAction)onSubmitAnswerBtnTap{
    
    if(![self requiredInputPresent])
        return;
    
    if(![UserDefaults isPremiumUser] && self.chatAvailabilityYes.isSelected){
        [(NewGiveAnswerController*)self.controller checkIfUserIsPremium];
        return;
    }

    [self preparePostAnswerCall];
}

-(IBAction)onFeedbackOptionSelect:(id)sender{
    UIButton *button = (UIButton*)sender;
    
    for(UIButton *btn in self.feedBackButtons){
        [btn setSelected:NO];
        if(btn == button){
            [btn setSelected:YES];
        }
    }
}
-(IBAction)onSeekerNameBtnTap:(id)sender{
    [(NewGiveAnswerController*)self.controller openUserInfoController:_question.seeker.userId];
}

#pragma mark - Navigation Actions
-(void)onBackNavBtnTap{
    [self endEditing:YES];
    if(self.answerState == AnswerViewStateChoice){
        [self.controller.navigationController popViewControllerAnimated:YES];
        return;
    }
    if(self.answerState == AnswerViewStateChat)
        [(NewGiveAnswerController*)self.controller addNextBtn];
    
    self.answerState--;
    [self updateBottomView];
}

-(void)onNextNavBtnTap{
    [self endEditing:YES];
    if(![self requiredInputPresent])
        return;
    self.answerState++;
    [self updateBottomView];
    
    if(self.answerState == AnswerViewStateChat) {
        [[(NewGiveAnswerController*)self.controller navigationItem] setRightBarButtonItem:nil];
    }
}

#pragma mark -- Validation
-(BOOL)isFeedBackProvided{
    
//    if(![self isAnswered]){
//        [Alert show:@"" andMessage:@"Please provide a relevant answer."];
//        return NO;
//    }
//    
//    if([StringUtils isEmptyOrNull:self.reasonTextView.text]){
//        [Alert show:@"" andMessage:@"Please provide a relevant reason."];
//        return NO;
//    }
//    
//    if([StringUtils isEmptyOrNull:self.whyChooseMeTextView.text]){
//        [Alert show:@"" andMessage:@"Please provide reason for your selection."];
//        return NO;
//    }
    
    BOOL isFeedbackProvided = NO;
    
    for (UIButton *feedbackButton in _feedBackButtons)
    {
        if ([feedbackButton isSelected])
        {
            isFeedbackProvided = YES;
            break;
        }
    }
    
    if (!isFeedbackProvided)
    {
        [Alert show:@"" andMessage:@"Please provide relevant feedback."];
        return NO;
    }
    
    return YES;
}

-(BOOL)requiredInputPresent{
    
    BOOL valid = YES;
    
    switch (self.answerState) {
        case AnswerViewStateChoice:
            if(![self isAnswered]){
                [Alert show:@"" andMessage:@"Please provide a relevant answer."];
                valid = NO;
                break;
            }
            if([StringUtils isEmptyOrNull:self.reasonTextView.text]){
                [Alert show:@"" andMessage:@"Please provide a relevant reason."];
                valid = NO;
                break;
            }
            break;
            
        case AnswerViewStateChat:
            if(!self.chatAvailabilityYes.isSelected && !self.chatAvailabilityNo.isSelected){
                [Alert show:@"" andMessage:@"Please select an option."];
                valid = NO;
                break;
            }
            if(_chatAvailabilityYes.isSelected && [StringUtils isEmptyOrNull:self.whyChooseMeTextView.text]){
                [Alert show:@"" andMessage:@"Please provide reason for your selection."];
                valid = NO;
                break;
            }
            break;
            
        default:
            break;
    }
    
    return valid;
}

-(BOOL)isAnswered{
    
    if(_question.questionType == QuestionTypeComplex){
        if([StringUtils isEmptyOrNull:self.complexAnswerTextView.text])
            return NO;
        
        _answer = self.complexAnswerTextView.text;
        return YES;
    }
    
    
    BOOL answerOptionSelected = NO;
    for(UIButton *answerBtn in self.answerOptionButtons){
        if([answerBtn isSelected]){
            answerOptionSelected = YES;
            NSString *selectedOption = @"";
            @try {
                selectedOption = [_question.answerOptions objectAtIndex:answerBtn.tag];
            }
            @catch (NSException *exception) {
                selectedOption = @"";
                NSLog(@"No answer options");
            }
            _answer = selectedOption;
            break;
        }
    }
    return answerOptionSelected;
}

-(NSString*)getFeedback{
    UIButton *selectedFeedbackBtn = (UIButton*)[[self.feedBackButtons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == TRUE"]] firstObject];
    
    NSString *feedback = KEY_RATING_THUMBS_UP;
    
    switch (selectedFeedbackBtn.tag) {
        case 0:
            feedback = KEY_RATING_THUMBS_UP;
            break;
        case 1:
            feedback = KEY_RATING_THUMBS_DOWN;
            break;
        case 2:
            feedback = KEY_RATING_THANKS;
            break;
            
        default:
            break;
    }
    
    return feedback;
}

-(void)setFeedback:(NSString *)feedback
{
    int selectedButtonTag = -1;
    if ([StringUtils compareStringIgnorCase:feedback withString:KEY_RATING_THUMBS_UP])
        selectedButtonTag = 0;
    if ([StringUtils compareStringIgnorCase:feedback withString:KEY_RATING_THUMBS_DOWN])
        selectedButtonTag = 1;
    if ([StringUtils compareStringIgnorCase:feedback withString:KEY_RATING_THANKS])
        selectedButtonTag = 2;
    
    for (UIButton * button in _feedBackButtons)
    {
        if (button.tag == selectedButtonTag)
        {
            [button setSelected:YES];
            continue;
        }
        [button setSelected:NO];
        [button setUserInteractionEnabled:NO];
    }
}

-(NSDictionary*)getParametersForServiceCall{
    return @{KEY_ANSWER : _answer,
             KEY_FEEDBACK : [self getFeedback],
             KEY_REASON : self.reasonTextView.text,
             KEY_WHY_CHOOSE_ME : self.whyChooseMeTextView.text,
             KEY_CHAT_AVAILABILITY : self.chatAvailabilityYes.isSelected ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO]};
}


#pragma mark -- 
-(void)updateBottomView{
    
    CGFloat bottomViewHeight = kDefaultBottomViewHeight;
    
    if(self.answerState == AnswerViewStateChoice){
        
         _viewChatAvailability.hidden = _viewChatAvailability.hidden = _viewWhyChat.hidden =
        _btnSubmit.hidden = _answerViewComplex.hidden = YES;
        _viewReason.hidden = _answerViewMCQ.hidden = NO;

        if(_question.questionType != QuestionTypeComplex) {
            
            bottomViewHeight = 60 + 65*(_question.answerOptions.count) + 30;
        }
        if(_question.questionType == QuestionTypeMultipleChoice) {
            
            self.tableViewHeightConstraint.constant = _tableViewAnswerOptionDetails.contentSize.height;
        }
        else
            self.tableViewHeightConstraint.constant = 0;
        
        if(_question.questionType == QuestionTypeComplex) {
            _answerViewMCQ.hidden = YES;
            _answerViewComplex.hidden = NO;
            bottomViewHeight = kComplexAnswerViewHeight;
        }
        
        _reasonViewHeightConstraint.constant = kDefaultBottomViewHeight;
        self.bottomViewHeightConstraint.constant = bottomViewHeight;
        self.scrollview.contentSize = CGSizeMake(CGRectGetWidth(_scrollview.bounds), _questionViewHeightConstraint.constant + _bottomViewHeightConstraint.constant + _reasonViewHeightConstraint.constant + (_question.questionType == QuestionTypeMultipleChoice ? _tableViewHeightConstraint.constant : 0));
    }
    else{
        _answerViewMCQ.hidden = YES;
        _answerViewComplex.hidden = YES;
        _viewWhyChat.hidden = _chatAvailabilityNo.isSelected;
        
        self.tableViewHeightConstraint.constant = 0;
        
        if(!_chatAvailabilityYes.isSelected && !_chatAvailabilityNo.isSelected)
            _chooseMeViewHeightConstraint.constant = 0;
        else
            _chooseMeViewHeightConstraint.constant = _chatAvailabilityNo.isSelected ? 0 : kDefaultBottomViewHeight;
        
        _viewChatAvailability.hidden = NO;
        _btnSubmit.hidden = ((NewGiveAnswerController *)self.controller).readOnly;
        _viewReason.hidden = _answerViewComplex.hidden = YES;
        
        _reasonViewHeightConstraint.constant = 0;
        
        _bottomViewHeightConstraint.constant = kChatAvailabilityViewHeight + _chooseMeViewHeightConstraint.constant + kSubmitButtonHeight + 10 + 20;
        self.scrollview.contentSize = CGSizeMake(CGRectGetWidth(_scrollview.bounds), _questionViewHeightConstraint.constant + _bottomViewHeightConstraint.constant);
    }
    
    [self layoutIfNeeded];
}

#pragma mark - Keyboard Notification
-(void)addKeyboardNotificationObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
-(void)keyboardWillShow:(NSNotification*)notification{;
    self.tableViewBottomMarginConstraint.constant = [self getKeyboardHeight:notification];
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}

-(void)keyboardWillHide:(NSNotification*)notification{
    self.tableViewBottomMarginConstraint.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}

-(CGFloat)getKeyboardHeight:(NSNotification*)notification{
    
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect frame      = [value CGRectValue];
    return frame.size.height;
}

-(void)addTapGestureToDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [tap setNumberOfTapsRequired:1];
    [tap setDelegate:self];
    [self addGestureRecognizer:tap];
}
-(void)dismissKeyboard{
    [self endEditing:YES];
}

#pragma mark - TableView DataSource/Delegate
-(void)setupAnswerChoicesTableView{
    [self.tableViewAnswerOptionDetails registerNib:[UINib nibWithNibName:[AnswerOptionDetailCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[AnswerOptionDetailCell cellName]];
    self.tableViewAnswerOptionDetails.estimatedRowHeight = 65;
    self.tableViewAnswerOptionDetails.rowHeight = UITableViewAutomaticDimension;
    [self.tableViewAnswerOptionDetails setLayoutMargins:UIEdgeInsetsZero];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnswerOptionDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:[AnswerOptionDetailCell cellName]];
    [cell setTitle:[self getMCQBtnTileForAnswerAtIndex:indexPath.row] andDetail:[_question.answerOptions objectAtIndex:indexPath.row]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_question.questionType == QuestionTypeMultipleChoice)
        return _question.answerOptions.count;
       else
           return 0;
}

#pragma mark --
- (UIImage *)getImageWithQuestionType:(QuestionType )questionType
{
    switch (questionType)
    {
        case QuestionTypeYN:
            return [UIImage imageNamed:@"question-type-icon-yes-no"];
            
        case QuestionTypeComplex:
            return [UIImage imageNamed:@"question-type-icon-complex"];
            
        default:
            return [UIImage imageNamed:@"question-type-icon-multiple"];
    }
}

#pragma mark - TabBar Delegate
-(void)tabbar:(TabbarView *)tabbar didSelectTab:(TabbarViewType)tabViewType{
    [(NewGiveAnswerController*)self.controller onTabbarBtnTap:tabViewType];
}

#pragma mark - TextView Delegate

-(void) textViewDidChange:(UITextView *)textView {
    
    if(textView == _complexAnswerTextView)
        _lblCharCount.text = [NSString stringWithFormat:@"%lu/%d char left", QUESTION_TEXT_LIMIT - textView.text.length, QUESTION_TEXT_LIMIT];

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if(text.length > QUESTION_TEXT_LIMIT && textView.text.length == 0) {
        textView.text = [text substringToIndex:QUESTION_TEXT_LIMIT];
        _lblCharCount.text = [NSString stringWithFormat:@"%lu/%d char left", QUESTION_TEXT_LIMIT - textView.text.length, QUESTION_TEXT_LIMIT];
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= QUESTION_TEXT_LIMIT;
}

#pragma mark - MZTimer delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType
{
    if (time < TIMER_GOING_TO_EXPIRE_TIME) {
        [timerLabel setTextColor:[Color timerWillExpireColor]];
    }
}
@end
