//
//  ChatMessage.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UserDefaults.h"
#import "ChatMessage.h"
#import "DateUtils.h"
#import "Keys.h"

//Keys
#define KEY_SENDER_OBJECT @"sender"

#define DATE_IN_STRING_FORMAT @"h:mm a"

@interface ChatMessage ()

@property(nonatomic, strong) NSString *messageTimeString;

@end
@implementation ChatMessage

#pragma mark - Setter Methods

- (void)setForNotificationPayload:(NSDictionary *)input
{
    _conversationId = [ParserUtils stringValue:input key:KEY_CHAT_ID];

    //Message
    _message = [ParserUtils stringValue:input key:KEY_CONTENT];
    _messageIdBeforeSending = [ParserUtils stringValue:input key:KEY_TEMP_MESSAGE_ID];
    
    //Set Message Date and Message Time String.
    [self setMessageDate:[DateUtils getDateFromUnixTimeStamp:[ParserUtils numberValue:input key:KEY_CREATED_AT]]];
    
    //Sender
    _messageSender = [MessageSender new];
    [_messageSender setSenderId:[ParserUtils stringValue:input key:KEY_SENDER_ID]];
    
    _isMyMessage = [[[self messageSender] senderId] isEqualToString:[UserDefaults getUserID]];
}

- (void)set:(NSDictionary *)input withUserId:(NSString *)userId
{
    _message = [ParserUtils stringValue:input key:KEY_API_MESSAGE];
    
    //Set Message Date and Message Time String.
    [self setMessageDate:[DateUtils getDateFromUnixTimeStamp:[ParserUtils numberValue:input key:KEY_CREATED_AT]]];
    
    //Check If Message is Mine.
    _isMyMessage = ([[ParserUtils stringValue:input key:KEY_SENDER_ID] isEqualToString:userId]);
    _messageStatus = ChatMessageStatusSent;
    
    //Prepare Sender
    _messageSender = [MessageSender new];
    [_messageSender set:[ParserUtils object:input key:KEY_SENDER_OBJECT]];
}

#pragma mark - Getter/Setter

- (NSString *)messageTimeString
{
    return _messageTimeString;
}

- (void)setMessageDate:(NSDate *)messageDate
{
    _messageDate = messageDate;
    _messageTimeString = [DateUtils stringFromDate:_messageDate withFormat:DATE_IN_STRING_FORMAT inUTC:NO];
}

@end
