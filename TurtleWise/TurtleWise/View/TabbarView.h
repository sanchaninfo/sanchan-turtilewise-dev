//
//  TabbarView.h
//  TurtleWise
//
//  Created by Irfan Gul on 08/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBView.h"
#import "Enum.h"

@class UserStats;
@protocol TabbarViewDelegate;

@interface TabbarView : IBView

@property (nonatomic, weak) id<TabbarViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;

@property (weak, nonatomic) IBOutlet UIView *separatorLeft;
@property (weak, nonatomic) IBOutlet UIView *separatorRight;

@property (weak, nonatomic) IBOutlet UILabel *lblQuestionBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblAnswerBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblChatBubble;

@property (nonatomic) TabbarViewType selectedTab;

- (void)updateNotificationValues:(UserStats *)userStats;
- (void)enableTab:(BOOL)enabled tabbarType:(TabbarViewType) type;

@end

@protocol TabbarViewDelegate <NSObject>

- (void)tabbar:(TabbarView *)tabbar didSelectTab:(TabbarViewType)tabViewType;

@end