//
//  AskQuestionView.h
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class TextField;

@interface AskQuestionView : BaseView

@property (weak, nonatomic) IBOutlet TextField *inputTextView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

-(void) clearQuestion;

@end
