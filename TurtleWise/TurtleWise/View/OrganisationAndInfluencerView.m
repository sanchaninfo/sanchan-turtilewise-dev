//
//  ViewOrganisationAndInfluencerView.m
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "OrganisationAndInfluencerView.h"
#import "OrganisationAndInfluencerController.h"
#import "ViewIAndOCell.h"
#import "BrandedPage.h"
#import "BrandedPageManager.h"

#define FIRST_VIEW @"Influencer"
#define SECOND_VIEW @"Organisation"

@interface OrganisationAndInfluencerView () < UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout >

@end

@implementation OrganisationAndInfluencerView

-(void)viewDidLoad {
    [super viewDidLoad];
    [self updasteSelectedView:FIRST_VIEW];
    [_collectionview registerNib:[UINib nibWithNibName:@"ViewIAndOCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
}

- (void) updasteSelectedView:(NSString*)view {
    if ([view isEqualToString:FIRST_VIEW]) {
        selectedView = FIRST_VIEW;
        _lblOrganisationSelected.hidden = YES;
        _lblInfluencerSelected.hidden = NO;
        [_lblOrganisation setTextColor:[UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0]];
        [_lblInfluencer setTextColor:[UIColor colorWithRed:22.0/255.0 green:22.0/255.0 blue:22.0/255.0 alpha:1.0]];
    } else {
        selectedView = SECOND_VIEW;
        _lblInfluencerSelected.hidden = YES;
        _lblOrganisationSelected.hidden = NO;
        [_lblInfluencer setTextColor:[UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0]];
        [_lblOrganisation setTextColor:[UIColor colorWithRed:22.0/255.0 green:22.0/255.0 blue:22.0/255.0 alpha:1.0]];
    }
}
- (IBAction)influencerTapped:(id)sender {
    [self updasteSelectedView:FIRST_VIEW];
    [_collectionview reloadData];
}
- (IBAction)organisationTapped:(id)sender {
    [self updasteSelectedView:SECOND_VIEW];
    [_collectionview reloadData];
}

-(void) viewWithArray:(NSArray*)array {
    
    influencerArray = [[NSMutableArray alloc] init];
    organisationArray = [[NSMutableArray alloc] init];
    
    [influencerArray removeAllObjects];
    [organisationArray removeAllObjects];
    
    for (int i = 0; i < array.count; i++) {
        if ([[[array objectAtIndex:i] objectForKey:@"type"] isEqualToString:@"influencer"]) {
            [influencerArray addObject:[array objectAtIndex:i]];
        } else {
            [organisationArray addObject:[array objectAtIndex:i]];
        }
    }
    [_collectionview reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([selectedView isEqualToString:FIRST_VIEW]) {
        return influencerArray.count;
    } else {
        return organisationArray.count;
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat size = collectionView.frame.size.width - 50;
    return CGSizeMake(size/2, size/2);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    ViewIAndOCell *cell = (ViewIAndOCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ViewIAndOCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    if ([selectedView isEqualToString:FIRST_VIEW]) {
        [cell setData:[[BrandedPage alloc] initWithDictionary:[influencerArray objectAtIndex:indexPath.row]]];
    } else {
        [cell setData:[[BrandedPage alloc] initWithDictionary:[organisationArray objectAtIndex:indexPath.row]]];
    }
    [cell.checkIconButton setHidden:YES];
    cell.followButton.tag = indexPath.row;
    [cell.followButton addTarget:self action:@selector(followButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([selectedView isEqualToString:FIRST_VIEW]) {
        [[BrandedPageManager sharedManager] setPageIdForBrandedPage:[[influencerArray objectAtIndex:indexPath.row] valueForKey:@"_id"]];
    } else {
        [[BrandedPageManager sharedManager] setPageIdForBrandedPage:[[organisationArray objectAtIndex:indexPath.row] valueForKey:@"_id"]];
    }
    
    [(OrganisationAndInfluencerController*)self.controller showManageBrandedPageController];
    
}

- (void) followButtonTapped:(UIButton *)sender {
    
    NSString * pageId;
    NSString * privacy;
    
    if ([selectedView isEqualToString:FIRST_VIEW]) {
        pageId = [[influencerArray objectAtIndex:sender.tag] valueForKey:@"_id"];
        privacy = [[influencerArray objectAtIndex:sender.tag] valueForKey:@"privacy"];
    } else {
        pageId = [[organisationArray objectAtIndex:sender.tag] valueForKey:@"_id"];
        privacy = [[organisationArray objectAtIndex:sender.tag] valueForKey:@"privacy"];
    }
    
    if ([[sender.titleLabel text] isEqualToString:@"Follow"]) {
        if ([privacy isEqualToString:@"public"]) {
            [(OrganisationAndInfluencerController*)self.controller followWithPageId:pageId];
        } else {
            [(OrganisationAndInfluencerController*)self.controller followRequestWithPageId:pageId];
        }
    } else {
        [(OrganisationAndInfluencerController*)self.controller unfollowWithPageId:pageId];
    }
}

@end
