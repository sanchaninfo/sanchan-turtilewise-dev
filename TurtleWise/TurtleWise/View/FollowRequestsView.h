//
//  FollowRequestsView.h
//  TurtleWise
//
//  Created by Sunflower on 9/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BaseView.h"

@interface FollowRequestsView : UIView<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *requestTableView;
- (void) getData:(NSArray*)req;
@end
