//
//  SocialLoginView.m
//  TurtleWise
//
//  Created by Irfan Gul on 02/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SocialLoginController.h"
#import "SocialLoginView.h"
#import "Constant.h"
#import "Font.h"

@interface SocialLoginView ()

@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

- (void)initUI;

@end

@implementation SocialLoginView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setSwipeToBackGesture];
    [self initUI];
}

#pragma mark - Private Methods

- (void)initUI
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithString:_signUpBtn.titleLabel.text];
    NSRange boldStrRange = [_signUpBtn.titleLabel.text rangeOfString:@"Sign Up"];
    
    [attrStr addAttribute:NSFontAttributeName
                    value:[Font regularFontWithSize:18.0f]
                    range:NSMakeRange(0, _signUpBtn.titleLabel.text.length)];
    
    [attrStr addAttribute:NSFontAttributeName
                    value:[Font boldFontWithSize:18.0f]
                    range:boldStrRange];
    
    [_signUpBtn.titleLabel setAttributedText:attrStr];
    [_signUpBtn.titleLabel setAdjustsFontSizeToFitWidth:YES];
}

#pragma mark - IBActions

- (IBAction)onLoginIn:(id)sender
{
    [(SocialLoginController *)self.controller openLoginController];
}

- (IBAction)onSignupEmail:(id)sender
{
    [(SocialLoginController *)self.controller openRegisterController];
}

- (IBAction)signInWithFacebook:(id)sender
{
    [(SocialLoginController *)[self controller] startFacebookLoginProcess];
}

- (IBAction)signInWithLinkedIn:(id)sender
{
    [(SocialLoginController *)self.controller startLinkedInLoginProcess];
}

@end
