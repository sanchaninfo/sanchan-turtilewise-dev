//
//  FAQView.m
//  TurtleWise
//
//  Created by Anum Amin on 3/21/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "FAQView.h"
#import "FAQController.h"
#import "EnvironmentConstants.h"

#define FAQ_URL @"faqMobile/faq"

@implementation FAQView

-(void) awakeFromNib {
    [super awakeFromNib];
    _webview.scrollView.bounces = NO;
    [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:
                                                        [BASE_URL_REDEEM_PAGE stringByAppendingString:FAQ_URL]]]];
}

#pragma mark - Webview Delegate

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [(FAQController *)self.controller showLoader];
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [(FAQController *)self.controller hideLoader];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [(FAQController *)self.controller hideLoader];
}

@end
