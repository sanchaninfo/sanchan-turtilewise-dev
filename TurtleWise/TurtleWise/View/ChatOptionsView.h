//
//  ChatOptionsView.h
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "Label.h"
#import "MZTimerLabel.h"
@class Conversation;

@interface ChatOptionsView : BaseView<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UIView *questionDetailsHeader;

@property (weak, nonatomic) IBOutlet UIImageView *questionImage;
@property (weak, nonatomic) IBOutlet Label *questionTitle;
@property (weak, nonatomic) IBOutlet Label *guruPanelStaticLabel;
@property (weak, nonatomic) IBOutlet MZTimerLabel *chatTime;
@property (weak, nonatomic) IBOutlet UILabel *remainingChatTimeStaticLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *endChatBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomSpaceConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remainingTimeLblHorizontalConstraint;

-(IBAction)onEndChatBtnTap:(id)sender;

-(void)setConversation:(Conversation*)conversation;
@end
