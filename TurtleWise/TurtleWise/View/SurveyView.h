//
//  SurveyView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 7/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface SurveyView : BaseView < UITextViewDelegate >

- (IBAction)submitSurvey:(id)sender;

@end
