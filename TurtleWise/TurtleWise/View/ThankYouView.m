//
//  ThankYouView.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ThankYouView.h"
#import "ThankYouController.h"

@interface ThankYouView ()

@end

@implementation ThankYouView

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)submitWizard:(id)sender
{
    
    [self completeWizard];
}



@end
