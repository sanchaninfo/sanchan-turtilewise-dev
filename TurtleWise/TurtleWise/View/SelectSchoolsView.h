//
//  SelectSchoolsView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TWTableView;

@interface SelectSchoolsView : BaseProfileWizardView

//Outlets
@property (weak, nonatomic) IBOutlet TWTableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMore;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)addMore:(id)sender;

@end
