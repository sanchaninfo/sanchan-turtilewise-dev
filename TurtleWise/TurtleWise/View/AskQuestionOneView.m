//
//  AskQuestionOneView.m
//  TurtleWise
//
//  Created by Sunflower on 9/4/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "AskQuestionOneView.h"
#import "ViewIAndOCell.h"
#import "BrandedPage.h"
#import "AskQuestionOneController.h"
#import "UserDefaults.h"

@interface AskQuestionOneView () < UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout >

@end

@implementation AskQuestionOneView

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [_communityImg setImage:[UIImage imageNamed:@"tw-checked"] forState:UIControlStateSelected];
    [_communityImg setImage:[UIImage imageNamed:@"checkbox-unchecked"] forState:UIControlStateNormal];
    
    [_influencerImg setImage:[UIImage imageNamed:@"tw-checked"] forState:UIControlStateSelected];
    [_influencerImg setImage:[UIImage imageNamed:@"checkbox-unchecked"] forState:UIControlStateNormal];
    
    [_organizationImg setImage:[UIImage imageNamed:@"tw-checked"] forState:UIControlStateSelected];
    [_organizationImg setImage:[UIImage imageNamed:@"checkbox-unchecked"] forState:UIControlStateNormal];
    
    [_sendToGrpMembers setImage:[UIImage imageNamed:@"tw-radio-active"] forState:UIControlStateSelected];
    [_sendToGrpMembers setImage:[UIImage imageNamed:@"tw-radio-inactive"] forState:UIControlStateNormal];
    
    [_sendToInfluencer setImage:[UIImage imageNamed:@"tw-radio-active"] forState:UIControlStateSelected];
    [_sendToInfluencer setImage:[UIImage imageNamed:@"tw-radio-inactive"] forState:UIControlStateNormal];
    
    _influencerConstraintHeight.constant = 0;
    _radioHeight.constant = 0;
    
    [_pageCollection registerNib:[UINib nibWithNibName:@"ViewIAndOCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
}

-(void)viewWillAppear {
    [super viewWillAppear];
}

-(void) viewWithArray:(NSArray*)array {
    
    influencerArray = [[NSMutableArray alloc] init];
    organisationArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        if ([[[array objectAtIndex:i] objectForKey:@"type"] isEqualToString:@"influencer"]) {
            [influencerArray addObject:[array objectAtIndex:i]];
        } else {
            [organisationArray addObject:[array objectAtIndex:i]];
        }
    }
}

- (IBAction)communityTapped:(id)sender {
    
    _communityImg.selected = TRUE;
    _influencerImg.selected = FALSE;
    _organizationImg.selected = FALSE;
    
    selectedBrandedPage = NULL;
    
    _influencerConstraintHeight.constant = 0;
}
- (IBAction)influencerTapped:(id)sender {
    _influencerImg.selected = TRUE;
    _communityImg.selected = FALSE;
    _organizationImg.selected = FALSE;
    
    [_sendToLable setText:@"Send to Influencer"];
    
    selectedBrandedPage = NULL;
    _radioHeight.constant = 0;
    
    [_chooseAnswererLabel setText:@"Choose Influencer to Ask"];
    
    if (influencerArray.count > 0) {
        _influencerConstraintHeight.constant = _collectionHeight.constant + 100.0;
    } else {
        _influencerConstraintHeight.constant = 0;
    }
    
    
    [_pageCollection reloadData];
}
- (IBAction)organizationTapped:(id)sender {
    
    _organizationImg.selected = TRUE;
    _influencerImg.selected = FALSE;
    _communityImg.selected = FALSE;
    
    [_sendToLable setText:@"Send to Organization"];
    
    selectedBrandedPage = NULL;
    _radioHeight.constant = 0;
    
    [_chooseAnswererLabel setText:@"Choose Organization to Ask"];
    
    if (organisationArray.count > 0) {
        _influencerConstraintHeight.constant = _collectionHeight.constant + 100.0;
    } else {
        _influencerConstraintHeight.constant = 0;
    }
    
    [_pageCollection reloadData];
}

- (IBAction)sendToInfluencerTapped:(id)sender {
    _sendToInfluencer.selected = YES;
    _sendToGrpMembers.selected = NO;
}
- (IBAction)sendToGroupTapped:(id)sender {
    _sendToInfluencer.selected = NO;
    _sendToGrpMembers.selected = YES;
}

- (BOOL)validateAnswerer {
    if (!_influencerImg.isSelected && !_communityImg.isSelected && !_organizationImg.isSelected) {
        return false;
    } else {
        return true;
    }
    return NULL;
}
- (IBAction)nextTapped:(id)sender {
    answers = [[NSMutableDictionary alloc] init];
    
    if ([self validateAnswerer]) {
        if (_communityImg.isSelected || selectedBrandedPage == NULL) {
            answers = NULL;
        } else {
            [answers setObject:[selectedBrandedPage valueForKey:@"name"] forKey:KEY_PAGENAME];
            NSMutableArray *list1 = [[NSMutableArray alloc] init];
            if (_sendToInfluencer.isSelected) {
                [answers setValue:@"owner" forKey:KEY_WHO];
                [list1 addObject:[selectedBrandedPage valueForKey:@"owner"]];
            } else {
                [answers setValue:@"members" forKey:KEY_WHO];
                NSMutableArray* members = [[NSMutableArray alloc] init];
                members = [[[selectedBrandedPage valueForKey:@"members"] valueForKey:@"approved"] mutableCopy];
                if ([members containsObject:[UserDefaults getUserID]]) {
                    [members removeObject:[UserDefaults getUserID]];
                }
                [list1 addObjectsFromArray:members];
            }
            
            [answers setObject:list1 forKey:KEY_LIST];
        }
        
        [(AskQuestionOneController *)self.controller startQuestionTextWithDictionary:answers];
    } else {
        [Alert show:@"Invalid Entry" andMessage:@"Please Select your Answerer"];
    }
    
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat size = collectionView.frame.size.width - 50;
    return CGSizeMake(size/2, size/2);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([_influencerImg isSelected]) {
        return influencerArray.count;
    } else {
        return organisationArray.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    ViewIAndOCell *cell = (ViewIAndOCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ViewIAndOCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    if ([_influencerImg isSelected]) {
        [cell setData:[[BrandedPage alloc] initWithDictionary:[influencerArray objectAtIndex:indexPath.row]]];
    } else {
        [cell setData:[[BrandedPage alloc] initWithDictionary:[organisationArray objectAtIndex:indexPath.row]]];
    }
    
    if (selectedBrandedPage != NULL) {
        if (_influencerImg.isSelected) {
            if ([[selectedBrandedPage valueForKey:KEY_ID] isEqualToString:[[influencerArray objectAtIndex:indexPath.row] valueForKey:KEY_ID]]) {
                [cell.checkIconButton setSelected:YES];
            } else {
                [cell.checkIconButton setSelected:NO];
            }
        } else if (_organizationImg.isSelected){
            if ([[selectedBrandedPage valueForKey:KEY_ID] isEqualToString:[[organisationArray objectAtIndex:indexPath.row] valueForKey:KEY_ID]]) {
                [cell.checkIconButton setSelected:YES];
            } else {
                [cell.checkIconButton setSelected:NO];
            }
        }
    } else {
        [cell.checkIconButton setSelected:NO];
    }
    
    
    
    [cell.checkIconButton setHidden:NO];
    cell.checkIconButton.tag = indexPath.row;
    [cell.checkIconButton addTarget:self action:@selector(checkTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    _collectionHeight.constant = _pageCollection.contentSize.height;
    _influencerConstraintHeight.constant = _collectionHeight.constant + 100.0;
    [self setNeedsUpdateConstraints];
    return cell;
}

- (void) checkTapped:(UIButton *)sender {
    
    sender.selected = !sender.isSelected;
    
    if (_influencerImg.isSelected && sender.isSelected) {
        selectedBrandedPage = [influencerArray objectAtIndex:sender.tag];
        [_pageCollection reloadData];
    } else if (_organizationImg.isSelected && sender.isSelected){
        selectedBrandedPage = [organisationArray objectAtIndex:sender.tag];
        [_pageCollection reloadData];
    } else {
        selectedBrandedPage = NULL;
        [_pageCollection reloadData];
    }
    
    if (selectedBrandedPage != NULL) {
        
        if ([[selectedBrandedPage valueForKey:KEY_ID] isEqualToString:[UserDefaults getPageID]]) {
            [_sendToInfluencer setEnabled:NO];
        } else {
            [_sendToInfluencer setEnabled:YES];
        }
    }
    
    if (sender.isSelected) {
        _radioHeight.constant = 35;
        _sendToGrpMembers.selected = YES;
        _sendToInfluencer.selected = NO;
    } else {
        _radioHeight.constant = 0;
        _sendToGrpMembers.selected = NO;
        _sendToInfluencer.selected = NO;
    }
    
}

@end
