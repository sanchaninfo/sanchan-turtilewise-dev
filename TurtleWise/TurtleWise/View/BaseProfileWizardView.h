//
//  BaseProfileWizardView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class BaseProfile;

@interface BaseProfileWizardView : BaseView

- (void)completeWizard;
- (void)saveWizardData:(BaseProfile *)profile;
- (void)updateData:(BaseProfile *)profile;
- (void)addPage;
- (void)saveWizardDataPage:(NSMutableDictionary *)page;

@end
