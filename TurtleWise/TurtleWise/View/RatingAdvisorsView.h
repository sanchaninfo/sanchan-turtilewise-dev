//
//  RatingAdvisorsView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface RatingAdvisorsView : BaseView < UITableViewDataSource, UITableViewDataSource >

@property (weak, nonatomic) IBOutlet UITableView *advisorList;

- (void)populateAdvisorsList:(NSArray *)advisors;
- (NSMutableArray *)advisorsRating;

@end
