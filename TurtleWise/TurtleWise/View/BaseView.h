//
//  BaseView.h
//  iOSTemplate
//
//  Created by mohsin on 4/3/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationController+PushNotificationRoutes.h"

@class BaseController;
@class UserStats;

@interface BaseView : UIView

@property(nonatomic,retain) BaseController *controller;
@property(nonatomic, strong) UINavigationController *navigationController;

-(void) setSwipeToBackGesture;
-(void) viewDidLoad;
-(void) viewWillAppear;
-(void) viewDidAppear;
-(void) viewWillDisappear;
-(void) viewDidDisappear;
-(void) viewDidLayoutSubviews;
-(void) resignFieldsOnView;
-(void) removeGesture;
-(void) setWizardSwipeGestures;
-(void) notImplemented;
-(BOOL) hasTabBar;
-(void) updateStatsOnTabBar:(UserStats *)userStats;

@end
