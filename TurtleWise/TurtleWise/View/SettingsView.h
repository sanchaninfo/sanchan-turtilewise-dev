//
//  SettingsView.h
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class TextField;
@class Settings;

@interface SettingsView : BaseView

@property (weak, nonatomic) IBOutlet TextField *txtFieldOldPassword;
@property (weak, nonatomic) IBOutlet TextField *txtFieldNewPassword;
@property (weak, nonatomic) IBOutlet TextField *txtFieldConfirmPassword;

@property (weak, nonatomic) IBOutlet UISwitch *switchQuestionNotificationSetting;
@property (weak, nonatomic) IBOutlet UISwitch *switchAnswerNotificationSetting;
@property (weak, nonatomic) IBOutlet UISwitch *switchDailyEmailDigest;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pendingVerificationStaticLabelTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailNotificationsStaticLabelTopSpace;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *changePasswordFields;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *resendEmailViews;

- (IBAction)onResendEmailBtnTap;
- (IBAction)onDailyEmailDigestToggle:(id)sender;

- (void)resetTxtFieldsOnSuccess;
- (void)updateViewWithSettings:(Settings *)settings;
- (void)onVerificationEmailResent;
@end
