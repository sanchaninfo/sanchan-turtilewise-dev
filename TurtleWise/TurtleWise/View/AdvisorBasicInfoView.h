//
//  SelectAdvisorTypeView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/2/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class NMRangeSlider;

@interface AdvisorBasicInfoView : BaseProfileWizardView

@property (weak, nonatomic) IBOutlet NMRangeSlider *ageRestrictionSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeRestriction;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)advisorTypeSelection:(id)sender;
- (IBAction)genderSelection:(id)sender;


@end
