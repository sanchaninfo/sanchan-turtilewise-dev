//
//  ForgotPasswordView.h
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface ForgotPasswordView : BaseView

- (void)initWithEmail:(NSString *)email;
@end
