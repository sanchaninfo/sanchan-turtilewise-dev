//
//  SubscribeView.h
//  TurtleWise
//
//  Created by Irfan Gul on 09/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"
#import "Enum.h"

@interface SubscribeView : BaseView

@property (weak, nonatomic) IBOutlet UIButton *btnSingleQuestionSubscription;

- (void)hideSingleQuestionSubscription;

- (IBAction)buyMonthlySubscription:(id)sender;
- (IBAction)buyThisQuestionSubscription:(id)sender;

@end
