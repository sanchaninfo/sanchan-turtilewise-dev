//
//  SelectInfluencerOnboardingTwoView.h
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

//#import "BaseProfileWizardView.h"
//#import "TWWizardManager.h"
#import "BaseView.h"


@interface SelectInfluencerOnboardingView : BaseView

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *userProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *userCoverImage;


//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)skipThisStep:(id)sender;
- (void)setBasicInformation:(BrandedPage *)brandedPage;
- (void)saveWizardData:(BrandedPage *)brandedPage;

@end
