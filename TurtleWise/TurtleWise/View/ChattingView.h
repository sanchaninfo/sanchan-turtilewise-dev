//
//  ChattingView.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class TWChatTableView;
@class Inputbar;

@interface ChattingView : BaseView

@property (weak, nonatomic) IBOutlet TWChatTableView *tableViewChat;
@property (weak, nonatomic) IBOutlet Inputbar *inputBar;
@property (weak, nonatomic) IBOutlet UIButton *btnRateAdvisors;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomConstraint;


@property (weak, nonatomic) IBOutlet UIView *containerView;


- (void)setChatMessages:(NSMutableArray *)chatMessages;
- (void)enableActivityIndicator:(BOOL)shouldEnable;
- (void)updateChatForNewMessage:(ChatMessage *)newChatMessage;
- (void)markChatClosed;
- (void)markChatUnrated;
- (void)inputbarResign;

- (IBAction)goToRateAdvisors:(id)sender;

@end
