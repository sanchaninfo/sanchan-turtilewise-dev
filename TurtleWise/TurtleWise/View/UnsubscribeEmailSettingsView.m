//
//  UnsubscribeEmailSettingsView.m
//  TurtleWise
//
//  Created by Usman Asif on 11/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UnsubscribeEmailSettingsController.h"
#import "UnsubscribeEmailSettingsView.h"
#import "Label.h"

@interface UnsubscribeEmailSettingsView()

@property (weak, nonatomic) IBOutlet Label *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@end

@implementation UnsubscribeEmailSettingsView

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [self setupUI];
}

- (void)setupUI{
    _btnDone.layer.cornerRadius = floor(_btnDone.frame.size.height/4);
    _btnDone.clipsToBounds = YES;

    [self setSwipeToBackGesture];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)updateDoneButtonTitle:(BOOL)isLoggedIn {
    [_lblMessage setHidden:NO];
    [_btnDone setHidden:NO];
    NSString * buttonTitle = isLoggedIn? @"Go back" : @"Go to home";
    [_btnDone setTitle:buttonTitle forState:UIControlStateNormal];
}

#pragma mark - IBActions

-(IBAction)onClickDone {
    [self.controller popViewController];
}

@end
