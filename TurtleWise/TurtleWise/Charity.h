//
//  Charity.h
//  TurtleWise
//
//  Created by Waleed Khan on 7/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface Charity : BaseEntity

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *details;
@property(nonatomic, strong) NSString *imageUrl;

@end
