//
//  AppDelegate.h
//  TurtleWise
//
//  Created by Waaleed Khan on 11/27/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@class LoginController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSString *activeConversationId;

@property (nonatomic) BOOL homeControllerShouldShowEmailActivationAlert;
@property (nonatomic) BOOL showCheckForSubscriptionExpiray;
@property (nonatomic) BOOL isShowingHome;

+(AppDelegate*)getInstance;

- (void)proceedToHome;
- (void)proceedToResetPassword:(NSString *)token;
- (void)proceedToEmailNotificationSettings:(NSString *)token;
- (void)handleRoutesForNotificationWithPayload:(NSDictionary *)payload;
- (UIViewController *)getHomeControllerWithSlideMenu;
-(void) showSocialLoginController;

@end

