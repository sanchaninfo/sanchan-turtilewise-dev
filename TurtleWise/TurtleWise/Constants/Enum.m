//
//  Enums.m
//  TurtleWise
//
//  Created by Waleed Khan on 4/25/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "Enum.h"
#import "Keys.h"

#define SOCKET_EVENT_REGISTER @"register"
#define SOCKET_EVENT_CONNECT @"connect"
#define SOCKET_EVENT_READ_MESSAGE @"readMessage"
#define SOCKET_EVENT_ERROR @"twError"
#define SOCKET_EVENT_DISCONNECT @"disconnect"

#pragma mark - Socket

NSDictionary *SocketEventsDic()
{
    NSDictionary *values = @{
                             @(SocketEventsRegister)               : SOCKET_EVENT_REGISTER,
                             @(SocketEventsQuestionRecieved)       : NOTIFICATION_TYPE_RECIEVED_QUESTION,
                             @(SocketEventsAnswerRecieved)         : NOTIFICATION_TYPE_RECIEVED_ANSWER,
                             @(SocketEventsConnected)              : SOCKET_EVENT_CONNECT,
                             @(SocketEventsRecievedNewMessage)     : NOTIFICATION_TYPE_RECIEVED_NEW_MESSAGE,
                             @(SocketEventsRecievedChatInvitation) : NOTIFICATION_TYPE_RECIEVED_CHAT_INVITATION,
                             @(SocketEventsChatEnded)              : NOTIFICATION_TYPE_CHAT_ENDED,
                             @(SocketEventsReadMessage)            : SOCKET_EVENT_READ_MESSAGE,
                             @(SocketEventsUpdateProfile)          : NOTIFICATION_TYPE_UPDATE_PROFILE,
                             @(SocketEventsError)                  : SOCKET_EVENT_ERROR,
                             @(SocketEventsDisconnect)             : SOCKET_EVENT_DISCONNECT,
                             @(SocketEventsSubscriptionEnded)      : NOTIFICATION_TYPE_END_SUBSCRIPTION
                             };
    return values;
}


NSString *NSStringFromSocketEvent(SocketEvents type)
{
    NSDictionary *dic = SocketEventsDic();
    return dic[@(type)];
}

SocketEvents SocketEventsFromNSString(NSString *string)
{
    NSDictionary *dic = SocketEventsDic();
    return [[[dic allKeysForObject:string] lastObject] integerValue];
}
