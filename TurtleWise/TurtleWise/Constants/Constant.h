//
//  Constant.h
//  wusup
//
//  Created by Mohsin on 3/6/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "Enum.h"

#define ACCESS_TOKEN @"gfd"
#define KEY_ACCESS_TOKEN @"access_token"

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)


#define REGULAR_FONT        @"Roboto-Regular"
#define BOLD_FONT           @"Roboto-Bold"
#define CONDENSED_FONT      @"ROBOTOCONDENSED-REGULAR"
#define CONDENSED_BOLD_FONT @"ROBOTOCONDENSED-BOLD"
#define LIGHT_FONT          @"Roboto-Light"

#define QUESTIONS_PAGE_SIZE 10

#define KEY_ANSWERS_SEARCH_OPTION @"searchOption"

#define BACK_BUTTON_BLACK_IMAGE @"back-button-black"
#define BACK_BUTTON_WHITE_IMAGE @"back-button-white"
#define NEXT_BUTTON_WHITE_IMAGE @"next-button-white"

#define LEFT_BUTTON_TITLE @"Cancel"

#define HAMBURGER_BUTTON_IMAGE_WHITE @"btn-hamburger-white"
#define HAMBURGER_BUTTON_IMAGE_BLACK @"btn-hamburger-black"

#define APP_GREEN_COLOR [UIColor colorWithRed:(153/255.0) green:(207/255.0) blue:(103/255.0) alpha:1.0]
#define APP_GREY_COLOR [UIColor colorWithRed:(102/255.0) green:(102/255.0) blue:(102/255.0) alpha:1.0]

#define APP_DELEGATE (AppDelegate*)[[UIApplication sharedApplication] delegate]

#define FONT_MATRIX @[@[REGULAR_FONT,CONDENSED_FONT],@[BOLD_FONT,CONDENSED_BOLD_FONT]]

#define DATE_FORMAT_Z                               @ "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
#define DATE_FORMAT                                 @ "MM/dd/yyyy"

#ifdef DEBUG
#define Log(...) NSLog(__VA_ARGS__)
#else
#define Log(...) {;}
#endif

#define EMPTY_STRING @""

#define HOME_CONTROLLER @"HomeController"

#define ASK_SCREEN_TITLE @"Ask"

#define TITLE_ALERT_ERROR @"Error!"
#define TITLE_ALERT_EMPTY_FIELD @"Empty Field!"
#define TITLE_ALERT_INVALID_FIELD @"Invalid Field!"
#define MESSAGE_ALERT_EMPTY_FIELD @"Please provide all required fields."

#define TITLE_CHAT_OPTIONS @"Chat Options"

#define TITLE_ALERT_INVALID_EMAIL @"Email address is required."
#define TITLE_ALERT_INVALID_EMAIL_ADDRESS @"Valid Email Address is required."
#define TITLE_ALERT_INVALID_URL @"URL should start with http:// or https://."
#define TITLE_ALERT_INVALID_USER_NAME @"Please Enter Your Username"
#define TITLE_ALERT_INVALID_PASSWORD @"Password is required."
#define TITLE_ALERT_EMPTY_ABOUT_ME @"Hi Matt, can you share more about yourself?"

#define TITLE_ALERT_ACCOUNT_ACTIVATION @"Account Verification"
#define MESSAGE_ACCOUNT_ACTIVATION @"To receive advice notifications, please initiate email verification by going to Menu then Settings to have a link sent to your email on file."

#define ERROR_MESSAGE_PASSWORD_DONOT_MATCH @"Password do not match."
#define ERROR_MESSAGE_IN_VALID_PASSWORD @"Password must be between 6 to 32 characters."
#define TITLE_ALERT_INVALID_OLD_PASSWORD @"Old Password is required."
#define TITLE_ALERT_INVALID_NEW_PASSWORD @"New Password is required."
#define TITLE_ALERT_INVALID_CONFIRM_PASSWORD @"Confirm Password is required."

#define RIGHT_BAR_BUTTON_TITLE_CHAT @"Chat"
#define RIGHT_BAR_BUTTON_TITLE_CLOSE_QUESTION @"Close"

#define KEY_QUESTIONS @"Questions"
#define KEY_ANSWERS @"Answers"
#define KEY_CHAT @"Chat"
#define KEY_THUMBS_UP @"thumbsups"
#define KEY_THUMBS_DOWN @"thumbsdowns"
#define KEY_THANKS @"thanks"

#define TITLE_TIMER_CLOSED @"Closed"

#define TIMER_GOING_TO_EXPIRE_TIME 15*60

#define GURU_TAG_PLACEHOLDER @"Guru Tag"


#define CELL_REUSE_IDENTIFIER @"CellIdentifier"

#define PLIST_EXTENSION @"plist"

#define USER_TURTLE_POINTS 80.0f

#define LEVEL_PREFIX @"Level %i"

#define URL_TERMS_AND_CONDITIONS @"http://turtlewise.net/terms-conditions/"
#define URL_PRIVACY_POLICY @"http://turtlewise.net/privacy-policy/"
#define URL_REDEEM_PAGE @"%@mobile/redirect?destination=redeem&twToken=%@&id=%@"

#define SUBMIT_BUTTON_TITLE @"Submit"

#define ERROR_ALERT_TITLE @"Error"
#define ADVICE_PERIOD @"Advice Period"
#define CHAT_PERIOD @"Chat Period"
#define VALUE_DEVICE_TYPE @"iOS"

#define MY_ANSWER_SCREEN_TITLE @"My Advice"
#define THEIR_ANSWER_SCREEN_TITLE @"Their Advice"

#define GIFT_CARD_PREFIX @"gift-card"
#define TITLE_AMAZON_GIFT_CARD @"Amazon Gift Card"

//For Google Analytics

#define LABEL_EMAIL @"Email"
#define LABEL_FACEBOOK @"Facebook"
#define LABEL_LINKEDIN @"Linkedin"

#define EVENT_SIGNUP @"SignUp"
#define EVENT_SIGNIN @"SignIn"
#define EVENT_PROFILE_UPDATED @"Updated"
#define EVENT_CHAT_INITIATED @"Initiated"
#define EVENT_SUBMIT @"Submitted"

#define CATEGORY_AUTHORIZATION @"Authorization"
#define CATEGORY_PROFILE @"Profile"
#define CATEGORY_QUESTION @"Question"
#define CATEGORY_ANSWER @"Answer"
#define CATEGORY_CHAT @"Chat"

#define PHOTO_PLACEHOLDER_IMAGE @"user-placeholder"
#define PHOTO_PLACEHOLDER_COVER @"default_coverpic"
#define PHOTO_BRAND_PAGE_PLACEHOLDER_IMAGE @"tw-add-photo"

#define TRACK_EVENT_ANSWER_PAGE_OPENED @"type=Question&questionId=%@&userId=%@&action=answerPageOpened"

#define MAXIMUM_CHAT_USERS 4
#define MESSAGE_MAXIMUM_CHAT_USERS @"You can only add upto 4 Gurus per chat"
#define ALERT_INCOMPLETE_PROFILE @"Complete your TurtleWise Profile and receive 40 TurtleBucks/TurtlePoints"

typedef void (^ NavBarButtonAction)(void);
typedef void (^successCallback)(id response);
typedef void (^failureCallback)(NSError *error);
