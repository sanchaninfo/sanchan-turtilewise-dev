//
//  UITextField+Padding.m
//  TurtleWise
//
//  Created by Irfan Gul on 04/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UITextField+Padding.h"

@implementation UITextField (Padding)

-(void)addLeftPadding:(CGFloat)padding
{
    CGRect paddingFrame = self.bounds;
    paddingFrame.size.width = padding;
    UIView *paddingView = [[UIView alloc] initWithFrame:paddingFrame];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

@end
