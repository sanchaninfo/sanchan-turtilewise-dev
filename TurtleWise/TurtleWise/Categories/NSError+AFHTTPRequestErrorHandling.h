//
//  NSError+AFHTTPRequestErrorHandling.h
//  TurtleWise
//
//  Created by Waaleed Khan on 11/30/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (AFHTTPRequestErrorHandling)

- (id)initWithError:(NSError *)error;

@end
