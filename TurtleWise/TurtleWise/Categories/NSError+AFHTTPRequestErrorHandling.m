//
//  NSError+AFHTTPRequestErrorHandling.m
//  TurtleWise
//
//  Created by Waaleed Khan on 11/30/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "NSError+AFHTTPRequestErrorHandling.h"
#import "AFNetworking.h"
#import "StringUtils.h"
#import "ParserUtils.h"
#import "Keys.h"

#define KEY_ERROR @"error"

@implementation NSError (AFHTTPRequestErrorHandling)

- (id)initWithError:(NSError *)error
{
    self = [super init];
    
    if (self)
    {
        self = [self parseError:error];
    }
    
    return self;
}

#pragma mark - Private Method

- (id)parseError:(NSError *)error
{
    NSInteger code = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    
    if (errorData == NULL)
    {
        return error;
    }
    
    NSError *errorInSerialization;
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:&errorInSerialization];
    
    if (errorInSerialization)
    {
        return error;
    }
    
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey : [ParserUtils stringValue:[ParserUtils object:serializedData key:KEY_ERROR] key:KEY_ERROR_MESSAGE]
                               };
    
    return [NSError errorWithDomain:@"TWApiError" code:code userInfo:userInfo];
}

@end
