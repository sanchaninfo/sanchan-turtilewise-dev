//
//  UINavigationController+PushNotificationRoutes.m
//  TurtleWise
//
//  Created by Waleed Khan on 3/1/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UINavigationController+PushNotificationRoutes.h"
#import "UnsubscribeEmailSettingsController.h"
#import "QuestionListingController.h"
#import "NewGiveAnswerController.h"
#import "ResetPasswordController.h"
#import "ChattingController.h"
#import "SettingsController.h"
#import "AnswerController.h"
#import "HomeController.h"
#import "UserDefaults.h"
#import "ChatMessage.h"
#import "AppDelegate.h"
#import "MFSideMenu.h"
#import "TSMiniWebBrowser.h"
#import "UserDefaults.h"

@implementation UINavigationController (PushNotificationRoutes)

#pragma mark - Navigation Routes

- (void)proceedToAnswerQuestion:(NSString *)questionId
{
    [self popAllViewControllers];
    
    NSMutableArray *previousViewController = [NSMutableArray arrayWithObject:[[self viewControllers] objectAtIndex:0]];
    
    QuestionListingController *listingController = [QuestionListingController new];
    [previousViewController addObject:listingController];
    
    NewGiveAnswerController *giveAnswerController = [[NewGiveAnswerController alloc] initWithQuestionId:questionId];
    [giveAnswerController setCustomDelegate:(id)listingController];
    
    [previousViewController addObject:giveAnswerController];

    [self setViewControllers:previousViewController];
    [[self navigationBar] setHidden:NO];
}

- (void)proceedToSeeAnswers:(NSString *)questionId
{
    [self popAllViewControllers];
    
    NSMutableArray *previousViewController = [NSMutableArray arrayWithObject:[[self viewControllers] objectAtIndex:0]];
    
    QuestionListingController *listingController = [QuestionListingController new];
    [previousViewController addObject:listingController];
    
    AnswerController *answersController = [[AnswerController alloc] initWithQuestionId:questionId];
    [answersController setCustomDelegate:(id)listingController];
    
    [previousViewController addObject:answersController];
    [self setViewControllers:previousViewController];
    
    [[self navigationBar] setHidden:NO];
}

- (void)proceedToChat:(ChatMessage *)newChatMessage isChatActive:(BOOL)isChatActive
{
    if (isChatActive)
    {
        [[self getChatController] recievedNewMessage:newChatMessage];
        
        return;
    }
    
    [self setRouteForChatNotifications:[newChatMessage conversationId]];
    [[self navigationBar] setHidden:NO];
}

- (void)proceedToNewlyInvitedChat:(NSString *)chatId
{
    [self setRouteForChatNotifications:chatId];
}

- (void)popAllViewControllers
{
    MFSideMenuContainerViewController *rootViewController = [[self viewControllers] objectAtIndex:0];
    
    if ([rootViewController isKindOfClass:[MFSideMenuContainerViewController class]])
    {
        [[rootViewController centerViewController] popToRootViewControllerAnimated:NO];
        
        return;
    }
    
    [self popToRootViewControllerAnimated:NO];
}

- (void)proceedToHomeForSubscriptionEnd
{
    [self popAllViewControllers];
    
    MFSideMenuContainerViewController *rootViewController = [[self viewControllers] objectAtIndex:0];
    HomeController *homeController;
    
    if (rootViewController && [rootViewController isKindOfClass:[HomeController class]])
    {
        homeController = (HomeController *)rootViewController;
    }
    else
    {
        homeController = [[(UINavigationController *)[rootViewController centerViewController] viewControllers] objectAtIndex:0];
    }
    
    [homeController subsciptionEnded];
}

-(void) proceedToQuestionListWithRoleChange:(BOOL) changeRoleToAdvisor {
   
    [self popAllViewControllers];
    
    NSMutableArray *previousViewController = [NSMutableArray arrayWithObject:[[self viewControllers] objectAtIndex:0]];
    
    if(changeRoleToAdvisor) {
        [UserDefaults setUserRole:UserRoleAdvisor];
    }
    
    QuestionListingController *questionController = [[QuestionListingController alloc] initWithListType:TabbarViewTypeQuestion andUserRole:[UserDefaults getUserRole]];
    [previousViewController addObject:questionController];
    [self setViewControllers:previousViewController];
    
    [[self navigationBar] setHidden:NO];
}

- (void)proceedToResetPassword:(NSString *)token
{
    NSMutableArray * controllers = [self.viewControllers mutableCopy];
    if (![[controllers lastObject] isKindOfClass:[ResetPasswordController class]]) {
        ResetPasswordController * controller = [ResetPasswordController new];
        [controller setResetPasswordToken:token];
        [controllers addObject:controller];
        
        [self setViewControllers:controllers animated:YES];
    }
}

- (void)proceedToEmailNotificationSettings:(NSString *)token
{
    NSMutableArray * controllers = [self.viewControllers mutableCopy];
    if ([self.presentedViewController isKindOfClass:[TSMiniWebBrowser class]]) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if (![[controllers lastObject] isKindOfClass:[UnsubscribeEmailSettingsController class]]) {
        UnsubscribeEmailSettingsController * controller = [UnsubscribeEmailSettingsController new];
        [controller setUnsubscribeToken:token];
        [controllers addObject:controller];
        
        [self setViewControllers:controllers animated:YES];
    }
}

#pragma mark - Helper Methods

- (ChattingController *)getChatController
{
    ChattingController *chattingController;
    
    if ([[self topViewController] isKindOfClass:[ChattingController class]])
    {
        chattingController = (ChattingController *)[self topViewController];
    }
    else
    {
        MFSideMenuContainerViewController *rootController = (MFSideMenuContainerViewController *)[self topViewController];
        chattingController = (ChattingController *)[(UINavigationController *)[rootController centerViewController] topViewController];
    }
    
    return chattingController;
}

- (void)setRouteForChatNotifications:(NSString *)chatId
{
    [self popAllViewControllers];
    
    NSMutableArray *previousViewController = [NSMutableArray arrayWithObject:[[self viewControllers] objectAtIndex:0]];
    
    QuestionListingController *listingController = [[QuestionListingController alloc] initWithListType:TabbarViewTypeChat andUserRole:[UserDefaults getUserRole]];
    [previousViewController addObject:listingController];
    
    ChattingController *chattingController = [[ChattingController alloc] initWithConversationId:chatId];
    [previousViewController addObject:chattingController];
    
    [self setViewControllers:previousViewController];
}

@end
