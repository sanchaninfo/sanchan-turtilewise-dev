//
//  NSArray+Utils.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/16/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "NSArray+Utils.h"

@implementation NSArray (Utils)

#pragma mark - Utility Methods

- (BOOL)isEmpty
{
    return !(self && [self count] > 0);
}

- (NSArray *)sortArray:(NSArray *)unsortedArray orderAscending:(BOOL)ascending
{
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: ascending];
    return [unsortedArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
}

@end
