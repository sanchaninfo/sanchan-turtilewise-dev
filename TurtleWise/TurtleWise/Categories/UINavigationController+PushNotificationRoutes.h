//
//  UINavigationController+PushNotificationRoutes.h
//  TurtleWise
//
//  Created by Waleed Khan on 3/1/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatMessage;

@interface UINavigationController (PushNotificationRoutes)

- (void)proceedToAnswerQuestion:(NSString *)questionId;
- (void)proceedToSeeAnswers:(NSString *)questionId;
- (void)proceedToChat:(ChatMessage *)newChatMessage isChatActive:(BOOL)isChatActive;
- (void)proceedToNewlyInvitedChat:(NSString *)chatId;
- (void)proceedToHomeForSubscriptionEnd;
- (void)proceedToResetPassword:(NSString *)token;
- (void)proceedToEmailNotificationSettings:(NSString *)token;
- (void)proceedToQuestionListWithRoleChange:(BOOL) changeRoleToAdvisor;

@end
